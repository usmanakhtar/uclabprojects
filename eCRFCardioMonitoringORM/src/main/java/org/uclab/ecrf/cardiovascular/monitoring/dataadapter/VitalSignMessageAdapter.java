package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.VitalSignMessage;

/**
 * This is VitalSignMessageAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class VitalSignMessageAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(VitalSignMessageAdapter.class);
    public VitalSignMessageAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving VitalSignMessage.
     * @param objVitalSignMessage
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objVitalSignMessage) {
		VitalSignMessage objInnerVitalSignMessage = new VitalSignMessage();
		objInnerVitalSignMessage =  (VitalSignMessage) objVitalSignMessage;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_VitalSignMessage(?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("VitalSignID", objInnerVitalSignMessage.getVitalSignID());
	         objCallableStatement.setInt("MessageAlertID", objInnerVitalSignMessage.getMessageAlertID());
	         objCallableStatement.setString("MessageAlertAnswer", objInnerVitalSignMessage.getMessageAlertAnswer());
	         
	         objCallableStatement.registerOutParameter("VitalSignMessageID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intVitalSignMessageID = objCallableStatement.getInt("VitalSignMessageID");
	         objDbResponse.add(String.valueOf(intVitalSignMessageID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("User saved successfully, Vital Sign Message Details="+objVitalSignMessage);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, VitalSignMessage Details=");
	     	objDbResponse.add("Error in saving VitalSignMessage :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	@Override
	public <T> List<String> Update(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	  * This is implementation function for retrieving VitalSignMessage. 
	  * @param objVitalSignMessage
	  * @return List of VitalSignMessage
	  */
	@Override
	public List<VitalSignMessage> RetriveData(Object objVitalSignMessage) {
		VitalSignMessage objOuterVitalSignMessage = new VitalSignMessage();
		List<VitalSignMessage> objListInnerVitalSignMessage = new ArrayList<VitalSignMessage>();
		objOuterVitalSignMessage =  (VitalSignMessage) objVitalSignMessage;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterVitalSignMessage.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_VitalSignMessageByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterVitalSignMessage.getPatientEncounterID());
		     }
		     else if(objOuterVitalSignMessage.getRequestType() == "ByVitalSignID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_VitalSignMessageByVitalSignID(?)}");
		         objCallableStatement.setLong("VitalSignID", objOuterVitalSignMessage.getVitalSignID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	VitalSignMessage objInnerVitalSignMessage = new VitalSignMessage();
		     	objInnerVitalSignMessage.setVitalSignMessageID(objResultSet.getLong("VitalSignMessageID"));
		     	objInnerVitalSignMessage.setVitalSignID(objResultSet.getLong("VitalSignID"));
		     	objInnerVitalSignMessage.setMessageAlertID(objResultSet.getInt("MessageAlertID"));
		     	objInnerVitalSignMessage.setMessageAlertAnswer(objResultSet.getString("MessageAlertAnswer"));
		     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerVitalSignMessage.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	
		     	objInnerVitalSignMessage.setMessageAlertShortDesc(objResultSet.getString("MessageAlertShortDesc"));
		     	objInnerVitalSignMessage.setMessageAlertLongDesc(objResultSet.getString("MessageAlertLongDesc"));
		     	objInnerVitalSignMessage.setMessageAlertLongDescEng(objResultSet.getString("MessageAlertLongDescEng"));
		     	
		        objListInnerVitalSignMessage.add(objInnerVitalSignMessage);
		     }
		     objConn.close();
		     logger.info("VitalSignMessage loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting VitalSignMessage :" + e.getMessage());
		 }   
		 return objListInnerVitalSignMessage;
	}

	@Override
	public <T> List<String> Delete(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
