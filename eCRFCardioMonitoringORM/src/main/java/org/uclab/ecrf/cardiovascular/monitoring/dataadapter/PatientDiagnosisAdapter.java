package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientDiagnosis;

/**
 * This is PatientDiagnosisAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class PatientDiagnosisAdapter implements DataAccessInterface {

	
	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(PatientDiagnosisAdapter.class);
    
    public PatientDiagnosisAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving PatientDiagnosis.
     * @param objPatientDiagnosis
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatientDiagnosis) {
		PatientDiagnosis objInnerPatientDiagnosis = new PatientDiagnosis();
		objInnerPatientDiagnosis =  (PatientDiagnosis) objPatientDiagnosis;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientDiagnosis(?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientDiagnosis.getPatientEncounterID());
	         objCallableStatement.setInt("PhysicianDecision", objInnerPatientDiagnosis.getPhysicianDecision());
	         objCallableStatement.setInt("CDSSDecision", objInnerPatientDiagnosis.getcDSSDecision());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerPatientDiagnosis.getDiagnosisDate() != null)
	         {
	        	 Date dtDiagnosisDate = sdf.parse(objInnerPatientDiagnosis.getDiagnosisDate());
	             Timestamp tsDiagnosisDate = new Timestamp(dtDiagnosisDate.getYear(),dtDiagnosisDate.getMonth(), dtDiagnosisDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("DiagnosisDate", tsDiagnosisDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("DiagnosisDate", null);
	         }
	         
	         objCallableStatement.setLong("CreatedBy", objInnerPatientDiagnosis.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("PatientDiagnosisID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intPatientDiagnosisID = objCallableStatement.getInt("PatientDiagnosisID");
	         objDbResponse.add(String.valueOf(intPatientDiagnosisID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientDiagnosis saved successfully, PatientDiagnosis Details="+objPatientDiagnosis);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, PatientDiagnosis Details=");
	     	objDbResponse.add("Error in saving PatientDiagnosis :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	 /**
     * This is implementation function for updating PatientDiagnosis.
     * @param objPatientDiagnosis
     * @return List of String 
     */
	@Override
	public List<String> Update(Object objPatientDiagnosis) {
		PatientDiagnosis objInnerPatientDiagnosis = new PatientDiagnosis();
		objInnerPatientDiagnosis =  (PatientDiagnosis) objPatientDiagnosis;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_PatientDiagnosis(?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientDiagnosisID", objInnerPatientDiagnosis.getPatientDiagnosisID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientDiagnosis.getPatientEncounterID());
	         objCallableStatement.setInt("PhysicianDecision", objInnerPatientDiagnosis.getPhysicianDecision());
	         objCallableStatement.setInt("CDSSDecision", objInnerPatientDiagnosis.getcDSSDecision());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerPatientDiagnosis.getDiagnosisDate() != null)
	         {
	        	 Date dtDiagnosisDate = sdf.parse(objInnerPatientDiagnosis.getDiagnosisDate());
	             Timestamp tsDiagnosisDate = new Timestamp(dtDiagnosisDate.getYear(),dtDiagnosisDate.getMonth(), dtDiagnosisDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("DiagnosisDate", tsDiagnosisDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("DiagnosisDate", null);
	         }
	         
	         objCallableStatement.setLong("UpdatedBy", objInnerPatientDiagnosis.getUpdatedBy());
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerPatientDiagnosis.getPatientDiagnosisID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientDiagnosis Updated successfully, PatientDiagnosis Details="+objPatientDiagnosis);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in updated user, PatientDiagnosis Details=");
	     	objDbResponse.add("Error in Updating PatientDiagnosis :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving PatientDiagnosis. 
	  * @param objPatientDiagnosis
	  * @return List of PatientDiagnosis
	  */
	@Override
	public List<PatientDiagnosis> RetriveData(Object objPatientDiagnosis) {
		PatientDiagnosis objOuterPatientDiagnosis = new PatientDiagnosis();
		List<PatientDiagnosis> objListInnerPatientDiagnosis = new ArrayList<PatientDiagnosis>();
		objOuterPatientDiagnosis =  (PatientDiagnosis) objPatientDiagnosis;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientDiagnosis.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientDiagnosisByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientDiagnosis.getPatientEncounterID());
		     }
		     else if(objOuterPatientDiagnosis.getRequestType() == "ByPatientDiagnosisID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientDiagnosisByPatientDiagnosisID(?)}");
		         objCallableStatement.setLong("PatientDiagnosisID", objOuterPatientDiagnosis.getPatientDiagnosisID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 PatientDiagnosis objInnerPatientDiagnosis = new PatientDiagnosis();
		    	 objInnerPatientDiagnosis.setPatientDiagnosisID(objResultSet.getLong("PatientDiagnosisID"));
		    	 objInnerPatientDiagnosis.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerPatientDiagnosis.setPhysicianDecision(objResultSet.getInt("PhysicianDecision"));
		    	 objInnerPatientDiagnosis.setcDSSDecision(objResultSet.getInt("CDSSDecision"));
		    	
		    	 if(objResultSet.getTimestamp("DiagnosisDate") != null)
			     	{
			         	Timestamp tsDiagnosisDate = objResultSet.getTimestamp("DiagnosisDate");
			         	objInnerPatientDiagnosis.setDiagnosisDate(tsDiagnosisDate.toString());
			     	}
		    	 
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientDiagnosis.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientDiagnosis.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientDiagnosis.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientDiagnosis.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientDiagnosis.add(objInnerPatientDiagnosis);
		     }
		     objConn.close();
		     logger.info("PatientDiagnosis loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientDiagnosis :" + e.getMessage());
		 }   
		 return objListInnerPatientDiagnosis;
	}

	@Override
	public List<String> Delete(Object objPatientDiagnosis) {
		PatientDiagnosis objOuterPatientDiagnosis = new PatientDiagnosis();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterPatientDiagnosis =  (PatientDiagnosis) objPatientDiagnosis;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientDiagnosisByPatientDiagnosisID(?)}");
		     objCallableStatement.setLong("PatientDiagnosisID", objOuterPatientDiagnosis.getPatientDiagnosisID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterPatientDiagnosis.getPatientDiagnosisID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("PatientDiagnosis Deleted successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting PatientDiagnosis, PatientDiagnosis Details=");
		 	objDbResponse.add("Error in deleting PatientDiagnosis :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
