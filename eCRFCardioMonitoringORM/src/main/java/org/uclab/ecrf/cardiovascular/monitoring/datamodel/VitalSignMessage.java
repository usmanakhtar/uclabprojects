package org.uclab.ecrf.cardiovascular.monitoring.datamodel;
import java.io.Serializable;

/**
 * This a data model class for Patient Vital Signs.
 * @author Taqdir
 *
 */
public class VitalSignMessage implements Serializable {
	
	private Long vitalSignMessageID;
	private Long vitalSignID;
    private int messageAlertID;
    private String messageAlertAnswer;
    private String createdDate;
    
    private Long patientEncounterID;
    private String messageAlertShortDesc;
    private String messageAlertLongDesc;
    private String messageAlertLongDescEng;

    private String requestType;

	public Long getVitalSignMessageID() {
		return vitalSignMessageID;
	}

	public void setVitalSignMessageID(Long vitalSignMessageID) {
		this.vitalSignMessageID = vitalSignMessageID;
	}

	public Long getVitalSignID() {
		return vitalSignID;
	}

	public void setVitalSignID(Long vitalSignID) {
		this.vitalSignID = vitalSignID;
	}

	public int getMessageAlertID() {
		return messageAlertID;
	}

	public void setMessageAlertID(int messageAlertID) {
		this.messageAlertID = messageAlertID;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public String getMessageAlertShortDesc() {
		return messageAlertShortDesc;
	}

	public void setMessageAlertShortDesc(String messageAlertShortDesc) {
		this.messageAlertShortDesc = messageAlertShortDesc;
	}

	public String getMessageAlertLongDesc() {
		return messageAlertLongDesc;
	}

	public void setMessageAlertLongDesc(String messageAlertLongDesc) {
		this.messageAlertLongDesc = messageAlertLongDesc;
	}

	public String getMessageAlertLongDescEng() {
		return messageAlertLongDescEng;
	}

	public void setMessageAlertLongDescEng(String messageAlertLongDescEng) {
		this.messageAlertLongDescEng = messageAlertLongDescEng;
	}

	public String getMessageAlertAnswer() {
		return messageAlertAnswer;
	}

	public void setMessageAlertAnswer(String messageAlertAnswer) {
		this.messageAlertAnswer = messageAlertAnswer;
	}

}
