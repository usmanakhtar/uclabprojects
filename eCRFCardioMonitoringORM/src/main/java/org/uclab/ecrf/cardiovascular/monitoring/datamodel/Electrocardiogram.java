package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Clinical History.
 * @author Taqdir
 *
 */

public class Electrocardiogram implements Serializable {

	private Long electrocardiogramID;
	private Long patientEncounterID;
    private int whetherToMeasure;
    private String measurementDate;
    private int examinationFindings;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getElectrocardiogramID() {
		return electrocardiogramID;
	}

	public void setElectrocardiogramID(Long electrocardiogramID) {
		this.electrocardiogramID = electrocardiogramID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getWhetherToMeasure() {
		return whetherToMeasure;
	}

	public void setWhetherToMeasure(int whetherToMeasure) {
		this.whetherToMeasure = whetherToMeasure;
	}

	public String getMeasurementDate() {
		return measurementDate;
	}

	public void setMeasurementDate(String measurementDate) {
		this.measurementDate = measurementDate;
	}

	public int getExaminationFindings() {
		return examinationFindings;
	}

	public void setExaminationFindings(int examinationFindings) {
		this.examinationFindings = examinationFindings;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
    
}
