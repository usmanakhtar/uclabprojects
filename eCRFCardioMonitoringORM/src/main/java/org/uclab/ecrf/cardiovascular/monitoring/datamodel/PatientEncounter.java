package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for patient encounter entity.
 * @author Taqdir
 *
 */
public class PatientEncounter implements Serializable {

	private Long patientEncounterID;
	private Long patientID;
    private String patientEncounterDate;
    private Float height;
    private Float weight;
    private String admissionDate;
    private String hFDiagnosedDate;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;
    
	public Long getPatientEncounterID() {
		return patientEncounterID;
	}
	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}
	public Long getPatientID() {
		return patientID;
	}
	public void setPatientID(Long patientID) {
		this.patientID = patientID;
	}
	public String getPatientEncounterDate() {
		return patientEncounterDate;
	}
	public void setPatientEncounterDate(String patientEncounterDate) {
		this.patientEncounterDate = patientEncounterDate;
	}
	public Float getHeight() {
		return height;
	}
	public void setHeight(Float height) {
		this.height = height;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public String getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}
	public String gethFDiagnosedDate() {
		return hFDiagnosedDate;
	}
	public void sethFDiagnosedDate(String hFDiagnosedDate) {
		this.hFDiagnosedDate = hFDiagnosedDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
    
}
