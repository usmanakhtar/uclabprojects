package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Clinical History.
 * @author Taqdir
 *
 */

public class PatientClinicalHistory implements Serializable {

	private Long patientClinicalHistoryID;
	private Long patientEncounterID;
    private int cAD;
    private int arterialHypertension;
    private int expositionToCardiotoxic;
    private int useOfDiuretics;
    private int orthopnoeaParoxysmal;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getPatientClinicalHistoryID() {
		return patientClinicalHistoryID;
	}

	public void setPatientClinicalHistoryID(Long patientClinicalHistoryID) {
		this.patientClinicalHistoryID = patientClinicalHistoryID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getcAD() {
		return cAD;
	}

	public void setcAD(int cAD) {
		this.cAD = cAD;
	}

	public int getArterialHypertension() {
		return arterialHypertension;
	}

	public void setArterialHypertension(int arterialHypertension) {
		this.arterialHypertension = arterialHypertension;
	}

	public int getExpositionToCardiotoxic() {
		return expositionToCardiotoxic;
	}

	public void setExpositionToCardiotoxic(int expositionToCardiotoxic) {
		this.expositionToCardiotoxic = expositionToCardiotoxic;
	}

	public int getUseOfDiuretics() {
		return useOfDiuretics;
	}

	public void setUseOfDiuretics(int useOfDiuretics) {
		this.useOfDiuretics = useOfDiuretics;
	}

	public int getOrthopnoeaParoxysmal() {
		return orthopnoeaParoxysmal;
	}

	public void setOrthopnoeaParoxysmal(int orthopnoeaParoxysmal) {
		this.orthopnoeaParoxysmal = orthopnoeaParoxysmal;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	
    
}
