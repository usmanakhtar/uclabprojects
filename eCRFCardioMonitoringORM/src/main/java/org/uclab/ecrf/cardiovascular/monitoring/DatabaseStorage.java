/**
 * 
 */
package org.uclab.ecrf.cardiovascular.monitoring;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Echocardiography;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Electrocardiogram;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.NTproBNP;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Patient;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientClinicalHistory;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientDiagnosis;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientEncounter;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientPhysicalExam;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientSymptoms;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSignStatistics;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSigns;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Users;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.VitalSignMessage;


/**
 * This is the DatabaseStorage class, it extends the AbstractDataBridge class
 * @author Taqdir
 */
public class DatabaseStorage extends AbstractDataBridge {

	private Connection objConnection;
	private static final Logger logger = LoggerFactory.getLogger(DatabaseStorage.class);
	 
    public DatabaseStorage(DataAccessInterface objDAInterface)
    {
       setDataAdapter(objDAInterface);
    }

    /**
     * This is implementation function setter of DataAccessInterface. 
     * @param DAInterface
     */
    @Override
    public void setDataAdapter(DataAccessInterface DAInterface) {
        super.objDAInterface = DAInterface;
        
        try
        {
            if(objConnection == null || objConnection.isClosed())
            {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                objConnection = DriverManager.getConnection("jdbc:sqlserver://BILAL-OFFICE-PC\\SQLEXPRESS;database=eCRFCardioMonitoring;user=sa;password=adminsa;");
                logger.info("Database connected successfully");
            }
        }
        catch (Exception e)
        {
        	logger.info("Error in connecting database");
        }  
    }

    /**
     * This is implementation function for saving Patient.
     * @param objPatient
     * @return List of String 
     */
    @Override
    public List<String> SavePatient(Patient objPatient) {
        
        List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatient);
            logger.info("Patient saved successfully, Patient Details="+objPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Patient, Patient Details=");
        	objResponse.add("Error in saving Patient :" + ex.getMessage());
        }
        return objResponse;
    }

    /**
     * This is implementation function for updating Patient.
     * @param objPatient
     * @return List of string
     *  
     */
    @Override
    public List<String> UpdatePatient(Patient objPatient) {
        List<String> objResponse = new ArrayList<>();
        try{
        objDAInterface.ConfigureAdapter(objConnection);
        objResponse = objDAInterface.Update(objPatient);     
        logger.info("Patient udpated successfully, Patient Details="+objPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Patient, Patient Details=");
        	objResponse.add("Error in saving Patient :" + ex.getMessage());
        }
        return objResponse;
    }

    /**
     * This is implementation function for retrieving Patient. 
     * @param objPatient
     * @return List of Patient
     */
    @Override
    public List<Patient> RetrivePatient(Patient objPatient) {
    	List<Patient> objListPatient = new  ArrayList<Patient>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatient =  objDAInterface.RetriveData(objPatient);
            logger.info("Patient loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting Patient :" + ex.getMessage());
    	}
        return objListPatient;
    }

    /**
     * This is implementation function for deleting Patient.
     * @param objPatient
     * @return List of String 
     */
    @Override
    public List<String> DeletePatient(Patient objPatient) {
    	 List<String> objResponse = new ArrayList<>();
         try{
         objDAInterface.ConfigureAdapter(objConnection);
         objResponse = objDAInterface.Delete(objPatient);     
         logger.info("Patient deleted successfully, Patient Details="+objPatient);
         }
         catch(Exception ex)
         {
         	logger.info("Error in deleting Patient, Patient Details=");
         	objResponse.add("Error in deleting Patient :" + ex.getMessage());
         }
         return objResponse;
    }

    /**
     * This is implementation function for saving PatientEncounter.
     * @param objPatientEncounter
     * @return List of String 
     */
	@Override
	public List<String> SavePatientEncounter(PatientEncounter objPatientEncounter) {
		List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientEncounter);
            logger.info("Patient saved successfully, PatientEncounter Details="+objPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientEncounter, PatientEncounter Details=");
        	objResponse.add("Error in saving PatientEncounter :" + ex.getMessage());
        }
        return objResponse;
	}

	 /**
     * This is implementation function for updating PatientEncounter.
     * @param objPatientEncounter
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientEncounter(PatientEncounter objPatientEncounter) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientEncounter);     
	        logger.info("PatientEncounter udpated successfully, PatientEncounter Details="+objPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientEncounter, PatientEncounter Details=");
        	objResponse.add("Error in saving PatientEncounter :" + ex.getMessage());
        }
        return objResponse;
	}

	 /**
     * This is implementation function for retrieving PatientEncounter. 
     * @param objPatientEncounter
     * @return List of PatientEncounter
     */
	@Override
	public List<PatientEncounter> RetrivePatientEncounter(PatientEncounter objPatientEncounter) {
		List<PatientEncounter> objListPatientEncounter = new  ArrayList<PatientEncounter>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientEncounter =  objDAInterface.RetriveData(objPatientEncounter);
            logger.info("Patient loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientEncounter :" + ex.getMessage());
    	}
        return objListPatientEncounter;
	}

	/**
     * This is implementation function for deleting PatientEncounter.
     * @param objPatientEncounter
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientEncounter(PatientEncounter objPatientEncounter) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientEncounter);     
	        logger.info("PatientEncounter deleted successfully, PatientEncounter Details="+objPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientEncounter, PatientEncounter Details=");
        	objResponse.add("Error in deleting PatientEncounter :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientSymptoms.
     * @param objPatientSymptoms
     * @return List of String 
     */
	@Override
	public List<String> SavePatientSymptoms(PatientSymptoms objPatientSymptoms) {
		List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientSymptoms);
            logger.info("PatientSymptoms saved successfully, PatientSymptoms Details="+objPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientSymptoms, PatientSymptoms Details=");
        	objResponse.add("Error in saving PatientSymptoms :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientSymptoms.
     * @param objPatientSymptoms
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientSymptoms(PatientSymptoms objPatientSymptoms) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientSymptoms);     
	        logger.info("PatientSymptoms udpated successfully, PatientSymptoms Details="+objPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientSymptoms, PatientSymptoms Details=");
        	objResponse.add("Error in saving PatientSymptoms :" + ex.getMessage());
        }
        return objResponse;
	}

	 /**
     * This is implementation function for retrieving PatientSymptoms. 
     * @param objPatientSymptoms
     * @return List of PatientSymptoms
     */
	@Override
	public List<PatientSymptoms> RetrivePatientSymptoms(PatientSymptoms objPatientSymptoms) {
		List<PatientSymptoms> objListPatientSymptoms = new  ArrayList<PatientSymptoms>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientSymptoms =  objDAInterface.RetriveData(objPatientSymptoms);
            logger.info("PatientSymptoms loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientSymptoms :" + ex.getMessage());
    	}
        return objListPatientSymptoms;
	}

	/**
     * This is implementation function for deleting PatientSymptoms.
     * @param objPatientSymptoms
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientSymptoms(PatientSymptoms objPatientSymptoms) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientSymptoms);     
	        logger.info("PatientSymptoms deleted successfully, PatientSymptoms Details="+objPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientSymptoms, PatientSymptoms Details=");
        	objResponse.add("Error in deleting PatientSymptoms :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientClinicalHistory.
     * @param objPatientClinicalHistory
     * @return List of String 
     */
	@Override
	public List<String> SavePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory) {
		List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientClinicalHistory);
            logger.info("PatientClinicalHistory saved successfully, PatientClinicalHistory Details="+objPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientClinicalHistory, PatientClinicalHistory Details=");
        	objResponse.add("Error in saving PatientClinicalHistory :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientClinicalHistory.
     * @param objPatientClinicalHistory
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientClinicalHistory);     
	        logger.info("PatientClinicalHistory udpated successfully, PatientClinicalHistory Details="+objPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientClinicalHistory, PatientClinicalHistory Details=");
        	objResponse.add("Error in saving PatientClinicalHistory :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving PatientClinicalHistory. 
     * @param objPatientClinicalHistory
     * @return List of PatientClinicalHistory
     */
	@Override
	public List<PatientClinicalHistory> RetrivePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory) {
		List<PatientClinicalHistory> objListPatientClinicalHistory = new  ArrayList<PatientClinicalHistory>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientClinicalHistory =  objDAInterface.RetriveData(objPatientClinicalHistory);
            logger.info("PatientClinicalHistory loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientClinicalHistory :" + ex.getMessage());
    	}
        return objListPatientClinicalHistory;
	}

	/**
     * This is implementation function for deleting PatientClinicalHistory.
     * @param objPatientClinicalHistory
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientClinicalHistory);     
	        logger.info("PatientClinicalHistory deleted successfully, PatientClinicalHistory Details="+objPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientClinicalHistory, PatientClinicalHistory Details=");
        	objResponse.add("Error in deleting PatientClinicalHistory :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientVitalSigns.
     * @param objPatientVitalSigns
     * @return List of String 
     */
	@Override
	public List<String> SavePatientVitalSigns(PatientVitalSigns objPatientVitalSigns) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientVitalSigns);
            logger.info("PatientVitalSigns saved successfully, PatientVitalSigns Details="+objPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientVitalSigns, PatientVitalSigns Details=");
        	objResponse.add("Error in saving PatientVitalSigns :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientVitalSigns.
     * @param objPatientVitalSigns
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientVitalSigns(PatientVitalSigns objPatientVitalSigns) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientVitalSigns);     
	        logger.info("PatientVitalSigns udpated successfully, PatientVitalSigns Details="+objPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientVitalSigns, PatientVitalSigns Details=");
        	objResponse.add("Error in updating PatientVitalSigns :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving PatientVitalSigns. 
     * @param objPatientVitalSigns
     * @return List of PatientVitalSigns
     */
	@Override
	public List<PatientVitalSigns> RetrivePatientVitalSigns(PatientVitalSigns objPatientVitalSigns) {
		List<PatientVitalSigns> objListPatientVitalSigns = new  ArrayList<PatientVitalSigns>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientVitalSigns =  objDAInterface.RetriveData(objPatientVitalSigns);
            logger.info("PatientVitalSigns loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientVitalSigns :" + ex.getMessage());
    	}
        return objListPatientVitalSigns;
	}

	/**
     * This is implementation function for deleting PatientVitalSigns.
     * @param objPatientVitalSigns
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientVitalSigns(PatientVitalSigns objPatientVitalSigns) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientVitalSigns);     
	        logger.info("PatientVitalSigns deleted successfully, PatientVitalSigns Details="+objPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientVitalSigns, PatientVitalSigns Details=");
        	objResponse.add("Error in deleting PatientVitalSigns :" + ex.getMessage());
        }
        return objResponse;
	}
	
	/**
     * This is implementation function for saving VitalSignMessage.
     * @param objVitalSignMessage
     * @return List of String 
     */
	@Override
	public List<String> SaveVitalSignMessage(VitalSignMessage objVitalSignMessage) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objVitalSignMessage);
            logger.info("VitalSignMessage saved successfully, VitalSignMessage Details="+objVitalSignMessage);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving VitalSignMessage, VitalSignMessage Details=");
        	objResponse.add("Error in saving VitalSignMessage :" + ex.getMessage());
        }
        return objResponse;
	}
	
	/**
     * This is implementation function for retrieving VitalSignMessage. 
     * @param objVitalSignMessage
     * @return List of VitalSignMessage
     */
	@Override
	public List<VitalSignMessage> RetriveVitalSignMessage(VitalSignMessage objVitalSignMessage) {
		List<VitalSignMessage> objListVitalSignMessage = new  ArrayList<VitalSignMessage>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListVitalSignMessage =  objDAInterface.RetriveData(objVitalSignMessage);
            logger.info("VitalSignMessage loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting VitalSignMessage :" + ex.getMessage());
    	}
        return objListVitalSignMessage;
	}
	

	/**
     * This is implementation function for saving Electrocardiogram.
     * @param objElectrocardiogram
     * @return List of String 
     */
	@Override
	public List<String> SaveElectrocardiogram(Electrocardiogram objElectrocardiogram) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objElectrocardiogram);
            logger.info("Electrocardiogram saved successfully, Electrocardiogram Details="+objElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Electrocardiogram, Electrocardiogram Details=");
        	objResponse.add("Error in saving Electrocardiogram :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating Electrocardiogram.
     * @param objElectrocardiogram
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdateElectrocardiogram(Electrocardiogram objElectrocardiogram) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objElectrocardiogram);     
	        logger.info("Electrocardiogram udpated successfully, Electrocardiogram Details="+objElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Electrocardiogram, Electrocardiogram Details=");
        	objResponse.add("Error in updating Electrocardiogram :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving Electrocardiogram. 
     * @param objElectrocardiogram
     * @return List of Electrocardiogram
     */
	@Override
	public List<Electrocardiogram> RetriveElectrocardiogram(Electrocardiogram objElectrocardiogram) {
		List<Electrocardiogram> objListElectrocardiogram = new  ArrayList<Electrocardiogram>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListElectrocardiogram =  objDAInterface.RetriveData(objElectrocardiogram);
            logger.info("Electrocardiogram loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting Electrocardiogram :" + ex.getMessage());
    	}
        return objListElectrocardiogram;
	}

	/**
     * This is implementation function for deleting Electrocardiogram.
     * @param objElectrocardiogram
     * @return List of String 
     */
	@Override
	public List<String> DeleteElectrocardiogram(Electrocardiogram objElectrocardiogram) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objElectrocardiogram);     
	        logger.info("Electrocardiogram deleted successfully, Electrocardiogram Details="+objElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Electrocardiogram, Electrocardiogram Details=");
        	objResponse.add("Error in deleting Electrocardiogram :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving NTproBNP.
     * @param objNTproBNP
     * @return List of String 
     */
	@Override
	public List<String> SaveNTproBNP(NTproBNP objNTproBNP) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objNTproBNP);
            logger.info("NTproBNP saved successfully, NTproBNP Details="+objNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving NTproBNP, NTproBNP Details=");
        	objResponse.add("Error in saving NTproBNP :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating NTproBNP.
     * @param objNTproBNP
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdateNTproBNP(NTproBNP objNTproBNP) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objNTproBNP);     
	        logger.info("NTproBNP udpated successfully, NTproBNP Details="+objNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating NTproBNP, NTproBNP Details=");
        	objResponse.add("Error in updating NTproBNP :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving NTproBNP. 
     * @param objNTproBNP
     * @return List of NTproBNP
     */
	@Override
	public List<NTproBNP> RetriveNTproBNP(NTproBNP objNTproBNP) {
		List<NTproBNP> objListNTproBNP = new  ArrayList<NTproBNP>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListNTproBNP =  objDAInterface.RetriveData(objNTproBNP);
            logger.info("NTproBNP loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting NTproBNP :" + ex.getMessage());
    	}
        return objListNTproBNP;
	}
	
	/**
     * This is implementation function for deleting NTproBNP.
     * @param objNTproBNP
     * @return List of String 
     */
	@Override
	public List<String> DeleteNTproBNP(NTproBNP objNTproBNP) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objNTproBNP);     
	        logger.info("NTproBNP deleted successfully, NTproBNP Details="+objNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting NTproBNP, NTproBNP Details=");
        	objResponse.add("Error in deleting NTproBNP :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving Echocardiography.
     * @param objEchocardiography
     * @return List of String 
     */
	@Override
	public List<String> SaveEchocardiography(Echocardiography objEchocardiography) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objEchocardiography);
            logger.info("Echocardiography saved successfully, Echocardiography Details="+objEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Echocardiography, Echocardiography Details=");
        	objResponse.add("Error in saving Echocardiography :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating Echocardiography.
     * @param objEchocardiography
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdateEchocardiography(Echocardiography objEchocardiography) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objEchocardiography);     
	        logger.info("Echocardiography udpated successfully, Echocardiography Details="+objEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Echocardiography, Echocardiography Details=");
        	objResponse.add("Error in updating Echocardiography :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving Echocardiography. 
     * @param objEchocardiography
     * @return List of Echocardiography
     */
	@Override
	public List<Echocardiography> RetriveEchocardiography(Echocardiography objEchocardiography) {
		List<Echocardiography> objListEchocardiography = new  ArrayList<Echocardiography>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListEchocardiography =  objDAInterface.RetriveData(objEchocardiography);
            logger.info("Echocardiography loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting Echocardiography :" + ex.getMessage());
    	}
        return objListEchocardiography;
	}

	/**
     * This is implementation function for deleting Echocardiography.
     * @param objEchocardiography
     * @return List of String 
     */
	@Override
	public List<String> DeleteEchocardiography(Echocardiography objEchocardiography) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objEchocardiography);     
	        logger.info("Echocardiography deleted successfully, Echocardiography Details="+objEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Echocardiography, Echocardiography Details=");
        	objResponse.add("Error in deleting Echocardiography :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientDiagnosis.
     * @param objPatientDiagnosis
     * @return List of String 
     */
	@Override
	public List<String> SavePatientDiagnosis(PatientDiagnosis objPatientDiagnosis) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientDiagnosis);
            logger.info("PatientDiagnosis saved successfully, PatientDiagnosis Details="+objPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientDiagnosis, PatientDiagnosis Details=");
        	objResponse.add("Error in saving PatientDiagnosis :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientDiagnosis.
     * @param objPatientDiagnosis
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientDiagnosis(PatientDiagnosis objPatientDiagnosis) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientDiagnosis);     
	        logger.info("PatientDiagnosis udpated successfully, PatientDiagnosis Details="+objPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientDiagnosis, PatientDiagnosis Details=");
        	objResponse.add("Error in updating PatientDiagnosis :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving PatientDiagnosis. 
     * @param objPatientDiagnosis
     * @return List of PatientDiagnosis
     */
	@Override
	public List<PatientDiagnosis> RetrivePatientDiagnosis(PatientDiagnosis objPatientDiagnosis) {
		List<PatientDiagnosis> objListPatientDiagnosis = new  ArrayList<PatientDiagnosis>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientDiagnosis =  objDAInterface.RetriveData(objPatientDiagnosis);
            logger.info("PatientDiagnosis loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientDiagnosis :" + ex.getMessage());
    	}
        return objListPatientDiagnosis;
	}

	/**
     * This is implementation function for deleting PatientDiagnosis.
     * @param objPatientDiagnosis
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientDiagnosis(PatientDiagnosis objPatientDiagnosis) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientDiagnosis);     
	        logger.info("PatientDiagnosis deleted successfully, PatientDiagnosis Details="+objPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientDiagnosis, PatientDiagnosis Details=");
        	objResponse.add("Error in deleting PatientDiagnosis :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving Users.
     * @param objUsers
     * @return List of String 
     */
	@Override
	public List<String> SaveUsers(Users objUsers) {
		List<String> objResponse = new ArrayList<>();
		try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objUsers);
            logger.info("Users saved successfully, Users Details="+objUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Users, Users Details=");
        	objResponse.add("Error in saving Users :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating Users.
     * @param objUsers
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdateUsers(Users objUsers) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objUsers);     
	        logger.info("Users udpated successfully, Users Details="+objUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Users, Users Details=");
        	objResponse.add("Error in updating Users :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving Users. 
     * @param objUsers
     * @return List of Users
     */
	@Override
	public List<Users> RetriveUsers(Users objUsers) {
		List<Users> objListUsers = new  ArrayList<Users>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListUsers =  objDAInterface.RetriveData(objUsers);
            logger.info("Users loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting Users :" + ex.getMessage());
    	}
        return objListUsers;
	}

	/**
     * This is implementation function for deleting Users.
     * @param objUsers
     * @return List of String 
     */
	@Override
	public List<String> DeleteUsers(Users objUsers) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objUsers);     
	        logger.info("Users deleted successfully, Users Details="+objUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Users, Users Details=");
        	objResponse.add("Error in deleting Users :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientPhysicalExam.
     * @param objPatientPhysicalExam
     * @return List of String 
     */
	@Override
	public List<String> SavePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam) {
		List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientPhysicalExam);
            logger.info("PatientPhysicalExam saved successfully, PatientPhysicalExam Details="+objPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientPhysicalExam, PatientPhysicalExam Details=");
        	objResponse.add("Error in saving PatientPhysicalExam :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientPhysicalExam.
     * @param objPatientPhysicalExam
     * @return List of string
     *  
     */
	@Override
	public List<String> UpdatePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientPhysicalExam);     
	        logger.info("PatientPhysicalExam udpated successfully, PatientPhysicalExam Details="+objPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientPhysicalExam, PatientPhysicalExam Details=");
        	objResponse.add("Error in saving PatientPhysicalExam :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for retrieving PatientPhysicalExam. 
     * @param objPatientPhysicalExam
     * @return List of PatientPhysicalExam
     */
	@Override
	public List<PatientPhysicalExam> RetrivePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam) {
		List<PatientPhysicalExam> objListPatientPhysicalExam = new  ArrayList<PatientPhysicalExam>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListPatientPhysicalExam =  objDAInterface.RetriveData(objPatientPhysicalExam);
            logger.info("PatientPhysicalExam loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting PatientPhysicalExam :" + ex.getMessage());
    	}
        return objListPatientPhysicalExam;
	}

	/**
     * This is implementation function for deleting PatientPhysicalExam.
     * @param objPatientPhysicalExam
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientPhysicalExam);     
	        logger.info("PatientPhysicalExam deleted successfully, PatientPhysicalExam Details="+objPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientPhysicalExam, PatientPhysicalExam Details=");
        	objResponse.add("Error in deleting PatientPhysicalExam :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for saving PatientVitalSignStatistics.
     * @param obPatientVitalSignStatistics
     * @return List of String 
     */
	@Override
	public List<String> SavePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics) {
		List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.Save(objPatientVitalSignStatistics);
            logger.info("PatientVitalSignStatistics saved successfully, PatientVitalSignStatistics Details="+objPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientVitalSignStatistics, PatientVitalSignStatistics Details=");
        	objResponse.add("Error in saving PatientVitalSignStatistics :" + ex.getMessage());
        }
        return objResponse;
	}

	/**
     * This is implementation function for updating PatientVitalSignStatistics.
     * @param objPatientVitalSignStatistics
     * @return List of string
     *  
     */
	
	@Override
	public List<String> UpdatePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Update(objPatientVitalSignStatistics);     
	        logger.info("PatientVitalSignStatistics udpated successfully, PatientVitalSignStatistics Details="+objPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientVitalSignStatistics, PatientVitalSignStatistics Details=");
        	objResponse.add("Error in saving PatientVitalSignStatistics :" + ex.getMessage());
        }
        return objResponse;
	}

	
	/**
     * This is implementation function for retrieving PatientVitalSignStatistics. 
     * @param objPatientVitalSignStatistics
     * @return List of PatientVitalSignStatistics
     */
	@Override
	public List<PatientVitalSignStatistics> RetrivePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics) {
			List<PatientVitalSignStatistics> objListPatientVitalSignStatistics = new  ArrayList<PatientVitalSignStatistics>();
	    	try
	    	{
	    		objDAInterface.ConfigureAdapter(objConnection);
	            objListPatientVitalSignStatistics =  objDAInterface.RetriveData(objPatientVitalSignStatistics);
	            logger.info("PatientVitalSignStatistics loaded successfully.");
	    	}
	    	catch(Exception ex)
	    	{
	    		logger.info("Error in getting PatienPatientVitalSignStatisticstPhysicalExam :" + ex.getMessage());
	    	}
	        return objListPatientVitalSignStatistics;
	}

	
	/**
     * This is implementation function for deleting PatientVitalSignStatistics.
     * @param objPatientVitalSignStatistics
     * @return List of String 
     */
	@Override
	public List<String> DeletePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics) {
		List<String> objResponse = new ArrayList<>();
        try{
	        objDAInterface.ConfigureAdapter(objConnection);
	        objResponse = objDAInterface.Delete(objPatientVitalSignStatistics);     
	        logger.info("PatientVitalSignStatistics deleted successfully, PatientVitalSignStatistics Details="+objPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientVitalSignStatistics, PatientVitalSignStatistics Details=");
        	objResponse.add("Error in deleting PatientVitalSignStatistics :" + ex.getMessage());
        }
        return objResponse;
	}
    
}
