package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientClinicalHistory;

/**
 * This is PatientClinicalHistory class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientClinicalHistoryAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(PatientClinicalHistoryAdapter.class);
    
    public PatientClinicalHistoryAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving PatientClinicalHistory.
     * @param objPatientClinicalHistory
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatientClinicalHistory) {
		PatientClinicalHistory objInnerPatientClinicalHistory = new PatientClinicalHistory();
		objInnerPatientClinicalHistory =  (PatientClinicalHistory) objPatientClinicalHistory;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientClinicalHistory(?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientClinicalHistory.getPatientEncounterID());
	         objCallableStatement.setInt("CAD", objInnerPatientClinicalHistory.getcAD());
	         objCallableStatement.setInt("ArterialHypertension", objInnerPatientClinicalHistory.getArterialHypertension());
	         objCallableStatement.setInt("ExpositionToCardiotoxic", objInnerPatientClinicalHistory.getExpositionToCardiotoxic());
	         objCallableStatement.setInt("UseOfDiuretics", objInnerPatientClinicalHistory.getUseOfDiuretics());
	         objCallableStatement.setInt("OrthopnoeaParoxysmal", objInnerPatientClinicalHistory.getOrthopnoeaParoxysmal());
	         objCallableStatement.setLong("CreatedBy", objInnerPatientClinicalHistory.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("PatientClinicalHistoryID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intPatientClinicalHistoryID = objCallableStatement.getInt("PatientClinicalHistoryID");
	         objDbResponse.add(String.valueOf(intPatientClinicalHistoryID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientClinicalHistory saved successfully, PatientClinicalHistory Details="+objPatientClinicalHistory);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, PatientClinicalHistory Details=");
	     	objDbResponse.add("Error in saving PatientClinicalHistory :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for updating PatientClinicalHistory.
	  * @param objPatientClinicalHistory
	  * @return List of String 
	  */
	@Override
	public List<String> Update(Object objPatientClinicalHistory) {
		PatientClinicalHistory objInnerPatientClinicalHistory = new PatientClinicalHistory();
		objInnerPatientClinicalHistory =  (PatientClinicalHistory) objPatientClinicalHistory;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_PatientClinicalHistory(?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientClinicalHistoryID", objInnerPatientClinicalHistory.getPatientClinicalHistoryID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientClinicalHistory.getPatientEncounterID());
	         objCallableStatement.setInt("CAD", objInnerPatientClinicalHistory.getcAD());
	         objCallableStatement.setInt("ArterialHypertension", objInnerPatientClinicalHistory.getArterialHypertension());
	         objCallableStatement.setInt("ExpositionToCardiotoxic", objInnerPatientClinicalHistory.getExpositionToCardiotoxic());
	         objCallableStatement.setInt("UseOfDiuretics", objInnerPatientClinicalHistory.getUseOfDiuretics());
	         objCallableStatement.setInt("OrthopnoeaParoxysmal", objInnerPatientClinicalHistory.getOrthopnoeaParoxysmal());
	         objCallableStatement.setLong("UpdatedBy", objInnerPatientClinicalHistory.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerPatientClinicalHistory.getPatientClinicalHistoryID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientClinicalHistory udpated successfully, PatientClinicalHistory Details="+objPatientClinicalHistory);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in udpating PatientClinicalHistory, PatientClinicalHistory Details=");
	     	objDbResponse.add("Error in Updating PatientClinicalHistory :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving PatientClinicalHistory. 
	  * @param objPatientClinicalHistory
	  * @return List of PatientClinicalHistory
	  */

	@Override
	public List<PatientClinicalHistory> RetriveData(Object objPatientClinicalHistory) {
		PatientClinicalHistory objOuterPatientClinicalHistory = new PatientClinicalHistory();
		List<PatientClinicalHistory> objListInnerPatientClinicalHistory = new ArrayList<PatientClinicalHistory>();
		objOuterPatientClinicalHistory =  (PatientClinicalHistory) objPatientClinicalHistory;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientClinicalHistory.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientClinicalHistoryByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientClinicalHistory.getPatientEncounterID());
		     }
		     else if(objOuterPatientClinicalHistory.getRequestType() == "ByPatientClinicalHistoryID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientClinicalHistoryByPatientClinicalHistoryID(?)}");
		         objCallableStatement.setLong("PatientClinicalHistoryID", objOuterPatientClinicalHistory.getPatientClinicalHistoryID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 PatientClinicalHistory objInnerPatientClinicalHistory = new PatientClinicalHistory();
		    	 objInnerPatientClinicalHistory.setPatientClinicalHistoryID(objResultSet.getLong("PatientClinicalHistoryID"));
		    	 objInnerPatientClinicalHistory.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerPatientClinicalHistory.setcAD(objResultSet.getInt("CAD"));
		    	 objInnerPatientClinicalHistory.setArterialHypertension(objResultSet.getInt("ArterialHypertension"));
		    	 objInnerPatientClinicalHistory.setExpositionToCardiotoxic(objResultSet.getInt("ExpositionToCardiotoxic"));
		    	 objInnerPatientClinicalHistory.setUseOfDiuretics(objResultSet.getInt("UseOfDiuretics"));
		    	 objInnerPatientClinicalHistory.setOrthopnoeaParoxysmal(objResultSet.getInt("OrthopnoeaParoxysmal"));
		     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientClinicalHistory.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientClinicalHistory.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientClinicalHistory.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientClinicalHistory.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientClinicalHistory.add(objInnerPatientClinicalHistory);
		     }
		     objConn.close();
		     logger.info("PatientClinicalHistory loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientClinicalHistory :" + e.getMessage());
		 }   
		 return objListInnerPatientClinicalHistory;
	}

	@Override
	public List<String> Delete(Object objPatientClinicalHistory) {
		PatientClinicalHistory objOuterPatientClinicalHistory = new PatientClinicalHistory();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterPatientClinicalHistory =  (PatientClinicalHistory) objPatientClinicalHistory;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientClinicalHistoryByPatientClinicalHistoryID(?)}");
		     objCallableStatement.setLong("PatientClinicalHistoryID", objOuterPatientClinicalHistory.getPatientClinicalHistoryID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterPatientClinicalHistory.getPatientClinicalHistoryID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("PatientClinicalHistory Deleted successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting PatientClinicalHistory, PatientClinicalHistory Details=");
		 	objDbResponse.add("Error in deleting PatientClinicalHistory :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
