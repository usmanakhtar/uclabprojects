package org.uclab.mm.kcl.edkat.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.uclab.mm.kcl.edkat.datamodel.Rule;
import org.uclab.mm.kcl.edkat.datamodel.SituationRuleWrapper;
import org.uclab.mm.kcl.edkat.datamodel.User;
import org.uclab.mm.kcl.edkat.service.UserService;

@Controller
@SessionAttributes("user")
public class UserController {

	private UserService objUserService;

	@Autowired(required=true)
    @Qualifier(value="objUserService")
	public void setObjUserService(UserService objUserService) {
		this.objUserService = objUserService;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String listRules(Model model) {
		User user = new User();
		model.addAttribute("user", user);
        return "Login";
    }
	
	@RequestMapping(value= "/user/validate", method = RequestMethod.POST)
    public String validateUser(@ModelAttribute("user") User user, Model model) {

		user = objUserService.validateUser(user);
		if(user.getUserID() != 0)
		{
			model.addAttribute("user", user);
			return "redirect:/rules";
		}
		else
		{
			model.addAttribute("error", "Invalid ID or Password.");
			return "Login";
		}
	}
	
	
}
