package org.uclab.mm.kcl.edkat.datamodel;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Entity
@Table(name="tblWellnessConceptsModel")
@Proxy(lazy=false)
public class WellnessConceptsModel implements Serializable{
	@Id
    @Column(name="WellnessConceptID")
    private int wellnessConceptID;
	
	@Column(name="WellnessConceptDescription")
	private String wellnessConceptDescription;
	
	@Column(name="ActiveYNID")
	private int activeYNID;
	
	public int getWellnessConceptID() {
		return wellnessConceptID;
	}

	public void setWellnessConceptID(int wellnessConceptID) {
		this.wellnessConceptID = wellnessConceptID;
	}

	public String getWellnessConceptDescription() {
		return wellnessConceptDescription;
	}

	public void setWellnessConceptDescription(String wellnessConceptDescription) {
		this.wellnessConceptDescription = wellnessConceptDescription;
	}

	public int getActiveYNID() {
		return activeYNID;
	}

	public void setActiveYNID(int activeYNID) {
		this.activeYNID = activeYNID;
	}
	
}
