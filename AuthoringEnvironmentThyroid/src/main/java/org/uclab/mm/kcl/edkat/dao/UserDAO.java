package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.User;

/**
 * User DAO
 * User DAO
 * @author Taqdir Ali
 *
 */

public interface UserDAO {

	public User addUser(User objUser);
	public User updateUser(User objUser);
	public List<User> listUser();
	public User getUserById(int id);
    public void removeUser(int id);
    public User validateUser(User objUser);
    
}
