package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.uclab.mm.kcl.edkat.dao.RuleDAO;
import org.uclab.mm.kcl.edkat.datamodel.Rule;

public class RuleServiceImpl implements RuleService {

	private RuleDAO ruleDAO;
	public RuleDAO getRuleDAO() {
		return ruleDAO;
	}
	public void setRuleDAO(RuleDAO ruleDAO) {
		this.ruleDAO = ruleDAO;
	}



	@Transactional
	public Rule addRule(Rule objRule) {
		return this.ruleDAO.addRule(objRule);
	}

	@Transactional
	public Rule updateRule(Rule objRule) {
		return this.ruleDAO.updateRule(objRule);
	}

	public List<Rule> listRules() {
		return this.ruleDAO.listRule();
	}

	public Rule getRuleById(int id) {
		Rule objRule = this.ruleDAO.getRuleById(id);
		System.out.println("Rule ID" + objRule.getRuleID());
		try
		{
			System.out.println("Conclusion ID " + objRule.getConclusionList().get(2).getConclusionID());
			System.out.println("Condition ID " + objRule.getConditionList().get(0).getConditionID());
		}
		catch(Exception ex)
		{
			
		}
		
		return this.ruleDAO.getRuleById(id);
	}

	public void removeRule(int id) {
		this.ruleDAO.removeRule(id);
	}

}
