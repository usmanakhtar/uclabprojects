package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.Condition;

public interface ConditionService {

	public void addCondition(Condition objCondition);
	public void updateCondition(Condition objCondition);
	public List<Condition> listCondition();
	public Condition getConditionById(int id);
    public void removeCondition(int id);
    public Condition getConditionByFields(Condition objCondition);
    
}
