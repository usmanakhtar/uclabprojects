package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsModel;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsRelationships;

public interface WellnessConceptsModelService {

    public void addWellnessConcept(WellnessConceptsModel objWellnessConceptsModel);
    public void updateWellnessConcept(WellnessConceptsModel objWellnessConceptsModel);
    public List<WellnessConceptsModel> listWellnessConceptsModel(String strQuery);
    public WellnessConceptsModel getWellnessConceptById(int id);
    public void removeWellnessConcept(int id);
    public List<WellnessConceptsRelationships> listWellnessConceptsByKey(String strSelectedKey, String strQuery);
}
