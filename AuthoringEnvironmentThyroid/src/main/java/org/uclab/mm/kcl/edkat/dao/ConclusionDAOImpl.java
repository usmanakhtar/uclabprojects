package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.Conclusion;

@Repository
public class ConclusionDAOImpl implements ConclusionDAO {

   private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
   private SessionFactory sessionFactory;
    
   public void setSessionFactory(SessionFactory sf){
       this.sessionFactory = sf;
   }
	    
   
	public void addConclusion(Conclusion objConclusion) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(objConclusion);
        tx.commit();
        session.close();
        logger.info("Conclusion saved successfully, Conclusion Details="+objConclusion);
		
	}

	public void updateConclusion(Conclusion objConclusion) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objConclusion);
        tx.commit();
        session.close();
        logger.info("Conclusion updated successfully, Conclusion Details="+objConclusion);
	}

	@SuppressWarnings("unchecked")
	public List<Conclusion> listConclusion() {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Conclusion> conclusionList = session.createQuery("from Conclusion").list();
        for(Conclusion objConclusion : conclusionList){
            logger.info("Conclusion List::"+objConclusion);
        }
        session.close();
        return conclusionList;
	}

	public Conclusion getConclusionById(int id) {
		Session session = this.sessionFactory.openSession();      
		Conclusion objConclusion = (Conclusion) session.load(Conclusion.class, new Integer(id));
        logger.info("Conclusion loaded successfully, Conclusion details="+objConclusion);
        session.close();
        return objConclusion;
	}

	public void removeConclusion(int id) {
		Session session = this.sessionFactory.openSession();
		Conclusion objConclusion = (Conclusion) session.load(Conclusion.class, new Integer(id));
	        if(null != objConclusion){
	            session.delete(objConclusion);
	        }
	        logger.info("Conclusion deleted successfully, Conclusion details="+objConclusion);
	        session.close();
	}

	
}
