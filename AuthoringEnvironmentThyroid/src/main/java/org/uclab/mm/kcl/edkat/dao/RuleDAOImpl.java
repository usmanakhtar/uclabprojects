package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.Conclusion;
import org.uclab.mm.kcl.edkat.datamodel.Rule;

@Repository
public class RuleDAOImpl implements RuleDAO {

	private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
	private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
	    
	    
	public Rule addRule(Rule objRule) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        objRule = (Rule) session.merge(objRule);
        tx.commit();
        session.close();
        logger.info("Rule saved successfully, Rule Details="+objRule);
        return objRule;
	}

	public Rule updateRule(Rule objRule) {
		
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objRule); // Replaced with updated rule
        tx.commit();
        session.close();
        logger.info("Rule updated successfully, Rule Details="+objRule);
        return objRule;
	}
	
	public void removeConclusion(Rule objRule, Conclusion objConclusion)
	{
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        objRule.getConclusionList().remove(objConclusion);
        session.delete(objConclusion);
        tx.commit();
        session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Rule> listRule() {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Rule> rulesList = session.createQuery("from Rule order by RuleID DESC").list().subList(0, 100);
        for(Rule objRule : rulesList){
            logger.info("Rule List::"+objRule);
        }
        session.close();
        return rulesList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<Rule> listRule() {
		int page = 1;
		Session session = this.sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Rule.class);
        criteria.setFirstResult(0);
        criteria.setMaxResults(5);
        List<Rule> rulesList = criteria.list();
        return rulesList;
	}*/

	public Rule getRuleById(int id) {
		try
		{
			Session session = this.sessionFactory.openSession();      
	        			Rule objRule = (Rule) session.createQuery("from Rule r left outer join fetch r.conclusionList where r.RuleID=" + id).uniqueResult();
	        logger.info("Rule loaded successfully, Rule details="+objRule);
	        session.close();
	        return objRule;
		}
		catch(Exception  ex )
		{
			logger.info("Error occurred in rule loading "+ ex.getMessage());
			return null;
		}
	}

	public void removeRule(int id) {
		 Session session = this.sessionFactory.openSession();
		 Rule objRule = (Rule) session.load(Rule.class, new Integer(id));
	        if(null != objRule){
	            session.delete(objRule);
	        }
	        session.close();
	        logger.info("Rule deleted successfully, Rule details="+objRule);
	}

}
