package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.Situations;

/**
 * Situation DAO
 * Situation DAO
 * @author Taqdir Ali
 *
 */

public interface SituationDAO {

	public Situations addSituation(Situations objSituation);
	public Situations updateSituation(Situations objSituation);
	public List<Situations> listSituations();
	public Situations getSituationById(int id);
    public void removeSituation(int id);
    public Situations getExistingSituation(Situations objSituation);
}
