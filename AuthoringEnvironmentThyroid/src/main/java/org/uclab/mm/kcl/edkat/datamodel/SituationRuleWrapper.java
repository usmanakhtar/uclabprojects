package org.uclab.mm.kcl.edkat.datamodel;

import java.io.Serializable;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

public class SituationRuleWrapper implements Serializable {

	public SituationRuleWrapper() {}
	
	private Rule rule = new Rule();
	
	private Situations situations = new Situations();
	
	private User user = new User();
	
	public Rule getRule() {
		return rule;
	}
	public void setRule(Rule rule) {
		this.rule = rule;
	}
	public Situations getSituations() {
		return situations;
	}
	public void setSituations(Situations situations) {
		this.situations = situations;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
