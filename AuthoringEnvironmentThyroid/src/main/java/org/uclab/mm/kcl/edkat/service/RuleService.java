package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.Rule;

public interface RuleService {

    public Rule addRule(Rule objRule);
    public Rule updateRule(Rule objRule);
    public List<Rule> listRules();
    public Rule getRuleById(int id);
    public void removeRule(int id);
    
}
