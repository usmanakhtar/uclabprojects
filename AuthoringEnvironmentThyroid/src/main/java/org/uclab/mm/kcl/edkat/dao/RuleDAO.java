package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.Rule;

/**
 * Rule DAO
 * Rule DAO
 * @author Taqdir Ali
 *
 */

public interface RuleDAO {

	public Rule addRule(Rule objRule);
	public Rule updateRule(Rule objRule);
	public List<Rule> listRule();
	public Rule getRuleById(int id);
    public void removeRule(int id);
}
