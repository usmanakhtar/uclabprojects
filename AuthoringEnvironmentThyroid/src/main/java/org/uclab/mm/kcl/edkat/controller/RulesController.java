package org.uclab.mm.kcl.edkat.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.uclab.mm.kcl.edkat.datamodel.Condition;
import org.uclab.mm.kcl.edkat.datamodel.Rule;
import org.uclab.mm.kcl.edkat.datamodel.SituationRuleWrapper;
import org.uclab.mm.kcl.edkat.datamodel.Situations;
import org.uclab.mm.kcl.edkat.datamodel.User;
import org.uclab.mm.kcl.edkat.datamodel.dclcommunication.SituationConditions;
import org.uclab.mm.kcl.edkat.datamodel.dclcommunication.SituationEvent;
import org.uclab.mm.kcl.edkat.datamodel.dclcommunication.SituationEvents;
import org.uclab.mm.kcl.edkat.service.ConditionService;
import org.uclab.mm.kcl.edkat.service.RuleService;
import org.uclab.mm.kcl.edkat.service.SituationService;



@Controller
@SessionAttributes("user")
public class RulesController {

	private RuleService objRuleService;
	private SituationService objSituationService;
	private ConditionService objConditionService;

	
	public ConditionService getObjConditionService() {
		return objConditionService;
	}

	@Autowired(required=true)
	@Qualifier(value="objConditionService")
	public void setObjConditionService(ConditionService objConditionService) {
		this.objConditionService = objConditionService;
	}

	public RuleService getObjRuleService() {
		return objRuleService;
	}

	public SituationService getObjSituationService() {
		return objSituationService;
	}

	@Autowired(required=true)
	@Qualifier(value="objSituationService")
	public void setObjSituationService(SituationService objSituationService) {
		this.objSituationService = objSituationService;
	}

	@Autowired(required=true)
    @Qualifier(value="objRuleService")
	public void setObjRuleService(RuleService objRuleService) {
		this.objRuleService = objRuleService;
	}
	
   @RequestMapping(value = "/rules", method = RequestMethod.GET)
    public String listRules(Model model) {
		Rule rule = new Rule();
		model.addAttribute("situationRuleWrapper", rule);
        model.addAttribute("listRules", this.objRuleService.listRules());
        return "RulesDashboard";
    }
   
   @RequestMapping(value = "/rulesList", method = RequestMethod.GET)
   public @ResponseBody RuleJsonFormat rulesList() {
	   try
	   {
		   RuleJsonFormat rjf = new RuleJsonFormat();
		   rjf.setData(this.objRuleService.listRules());
		   return rjf;
	   }
	   catch(Exception ex)
	   {
		   return null;
	   }
   }
	
 //For add and update rule both
    @RequestMapping(value= "/rules/add", method = RequestMethod.POST)
    public String addRule(@ModelAttribute("situationRuleWrapper") SituationRuleWrapper situationRuleWrapper,  Model model){
        Rule objRule = new Rule();
        Situations objSituation = new Situations();
        User objUser = new User();
        
        objSituation = situationRuleWrapper.getSituations();
        objRule = situationRuleWrapper.getRule();
        objUser = situationRuleWrapper.getUser();
        
        // Start Preparation of Situation and Existing conditions
        
        String strSituationDesc = "";
        boolean blnIsSituationAttached = false;
    	for(int i = 0; i < objRule.getConditionList().size(); i++){
    		Condition objCondition = objRule.getConditionList().get(i);
    		boolean blnIsItSituation = objCondition.getIsItSituation();
        	objCondition = objConditionService.getConditionByFields(objCondition);
        	//if(objCondition.getConditionID() != 0)
        	//{ 
        		 //objRule.getConditionList().set(i, objCondition);
        		objRule.getConditionList().get(i).setConditionID(objCondition.getConditionID());
        		objRule.getConditionList().get(i).setConditionKey(objCondition.getConditionKey());
        		objRule.getConditionList().get(i).setConditionValue(objCondition.getConditionValue());
        		objRule.getConditionList().get(i).setConditionValueOperator(objCondition.getConditionValueOperator());
        	//}
        	
        	if(blnIsItSituation)
        	{
        		blnIsSituationAttached = true;
        		objSituation.getSituationConditionList().add(objCondition);
        		if(strSituationDesc.equals(""))
        		{
        			strSituationDesc = objCondition.getConditionKey() + " " + objCondition.getConditionValueOperator() + " " + objCondition.getConditionValue();
        		}
        		else
        		{
        			strSituationDesc = strSituationDesc + " and " +  objCondition.getConditionKey() + " " + objCondition.getConditionValueOperator() + " " + objCondition.getConditionValue();
        		}
        	}
        	
        }
      
        // End Preparation of Situation and Existing conditions
           	
    	if(blnIsSituationAttached)
    	{ 
    		objSituation = objSituationService.getExistingSituation(objSituation);
    		Situations objNewSituation = new Situations();
    		objSituation.setSituationDescription(strSituationDesc);
    		if(objSituation.getSituationID() == 0){
                //new rule, add it
        		objNewSituation = this.objSituationService.addSituation(objSituation);
        		
        		/* Client for the DCL to add situations-- Start */
        		SituationEvent objSituationEvent = new SituationEvent();
        		SituationEvents objSituationEvents = new SituationEvents();
        		List<SituationConditions> objListSituationConditions = new ArrayList<SituationConditions>();
        		List<SituationEvent> objListSituationEvent = new ArrayList<SituationEvent>();
        		
        		
        		try
        		{
        			for(int k = 0; k < objNewSituation.getSituationConditionList().size(); k++)
        			{
        				SituationConditions  objSituationConditions  = new SituationConditions();
        				objSituationConditions.setConditionKey(objNewSituation.getSituationConditionList().get(k).getConditionKey());
        				objSituationConditions.setConditionValue(objNewSituation.getSituationConditionList().get(k).getConditionValue());
        				objSituationConditions.setConditionValueOperator(objNewSituation.getSituationConditionList().get(k).getConditionValueOperator());
        				objSituationConditions.setConditionType("String");
        				objListSituationConditions.add(objSituationConditions);
        			}
        			
        			objSituationEvent.setListSConditions(objListSituationConditions);
        			objSituationEvent.setSituationID( Integer.toString(objNewSituation.getSituationID()));
        			objListSituationEvent.add(objSituationEvent);
        			objSituationEvents.setListSEvents(objListSituationEvent);
        			
        			/* Start Commented for Autonomous car demo
        			 * ClientConfig config = new DefaultClientConfig();
        	        config.getClasses().add(JacksonJsonProvider.class);
        	                
        	         //                Gson objJson = new Gson();
        	         //                String strResponse = objJson.toJson(objSituation);
        	               
        	        
        	         // JerseyClient client = 
        	         Client client = Client.create(config);
        	         //final String baseURI = "http://163.180.116.194:8080/testWeb/webresources/testweb/SituationAdd";
        	         final String baseURI = "http://163.180.173.135:8080/testWeb/webresources/testweb/SituationAdd";
        	         WebResource srv = client.resource(UriBuilder.fromUri(baseURI).build());
        	         //WebResource service = client.resource(UriBuilder.fromUri(baseURI).build());
        	                
        	         ClientResponse ServerResponse = srv.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, objSituationEvents);
        	         System.out.println("****** Situation Shared with DCL for Monitoring **********");
        	         System.out.println("Situation ID: " +  objNewSituation.getSituationID());
        	         System.out.println("Situation Description : " +  objNewSituation.getSituationDescription());
        	         System.out.println("****** ******** **********"); End */
        	         
        		}
        		catch(Exception ex)
        		{
        			System.out.println("****** Exception **********" + ex.getMessage());
        		}
        		
        		
        		/* Client for the DCL to add situations-- End */
        		
        		
            }else{
                //existing rule, call update
            	objNewSituation = objSituation;
            }
    		objRule.setSituationID(objNewSituation.getSituationID()); 
    		for(int i = 0; i < objRule.getConditionList().size(); i++){
        		Condition objCondition = objRule.getConditionList().get(i);
            	objCondition = objConditionService.getConditionByFields(objCondition);
            	if(objCondition.getConditionID() != 0)
            	{ 
            		 objRule.getConditionList().set(i, objCondition);
            	}
    		}
    	}
    	else
    	{ objRule.setSituationID(0); }
    	
        if(objRule.getRuleID() == 0){
            //new rule, add it
        	objRule.setRuleCreatedBy(objUser.getUserID());
        	objRule = this.objRuleService.addRule(objRule);
        }else{
            //existing rule, call update
        	objRule.setRuleUpdatedBy(objUser.getUserID());
        	//DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        	Date todayDate = new Date();
        	objRule.setRuleLastUpdatedDate(todayDate);
        	objRule = this.objRuleService.updateRule(objRule);
        }
        model.addAttribute("isSaved", "yes");
        return "redirect:/edit/" + objRule.getRuleID();
    }
    
    /**
	 * This method is physically add the rule with corresponding conditions, conclusions and situations into the knowledge base
	 * but this function internally also check the duplication for conditions, conclusions and situations.
	 * @param situationRuleWrapper 
	 * @return successfully added rule id.
	*/
    @RequestMapping(value= "/rules/addRule", method = RequestMethod.POST)
    public @ResponseBody  String addRuleForService(@RequestBody SituationRuleWrapper situationRuleWrapper){
    	Rule objRule = new Rule();
        Situations objSituation = new Situations();
        User objUser = new User();
        
    	try
		{
            objSituation = situationRuleWrapper.getSituations();
            objRule = situationRuleWrapper.getRule();
            objUser = situationRuleWrapper.getUser();
            
            String strSituationDesc = "";
            boolean blnIsSituationAttached = false;
        	for(int i = 0; i < objRule.getConditionList().size(); i++){
        		Condition objCondition = objRule.getConditionList().get(i);
        		boolean blnIsItSituation = objCondition.getIsItSituation();
            	objCondition = objConditionService.getConditionByFields(objCondition);
            	objRule.getConditionList().get(i).setConditionID(objCondition.getConditionID());
            	objRule.getConditionList().get(i).setConditionKey(objCondition.getConditionKey());
            	objRule.getConditionList().get(i).setConditionValue(objCondition.getConditionValue());
            	objRule.getConditionList().get(i).setConditionValueOperator(objCondition.getConditionValueOperator());
            	
            	if(blnIsItSituation)
            	{
            		blnIsSituationAttached = true;
            		objSituation.getSituationConditionList().add(objCondition);
            		if(strSituationDesc.equals(""))
            		{
            			strSituationDesc = objCondition.getConditionKey() + " " + objCondition.getConditionValueOperator() + " " + objCondition.getConditionValue();
            		}
            		else
            		{
            			strSituationDesc = strSituationDesc + " and " +  objCondition.getConditionKey() + " " + objCondition.getConditionValueOperator() + " " + objCondition.getConditionValue();
            		}
            	}
            	
            }
          
            // End Preparation of Situation and Existing conditions
               	
        	if(blnIsSituationAttached)
        	{ 
        		objSituation = objSituationService.getExistingSituation(objSituation);
        		Situations objNewSituation = new Situations();
        		objSituation.setSituationDescription(strSituationDesc);
        		if(objSituation.getSituationID() == 0){
                    //new rule, add it
            		objNewSituation = this.objSituationService.addSituation(objSituation);
            		
            		/* Client for the DCL to add situations-- Start */
            		SituationEvent objSituationEvent = new SituationEvent();
            		SituationEvents objSituationEvents = new SituationEvents();
            		List<SituationConditions> objListSituationConditions = new ArrayList<SituationConditions>();
            		List<SituationEvent> objListSituationEvent = new ArrayList<SituationEvent>();
            		
            		
            		try
            		{
            			for(int k = 0; k < objNewSituation.getSituationConditionList().size(); k++)
            			{
            				SituationConditions  objSituationConditions  = new SituationConditions();
            				objSituationConditions.setConditionKey(objNewSituation.getSituationConditionList().get(k).getConditionKey());
            				objSituationConditions.setConditionValue(objNewSituation.getSituationConditionList().get(k).getConditionValue());
            				objSituationConditions.setConditionValueOperator(objNewSituation.getSituationConditionList().get(k).getConditionValueOperator());
            				objSituationConditions.setConditionType("String");
            				objListSituationConditions.add(objSituationConditions);
            			}
            			
            			objSituationEvent.setListSConditions(objListSituationConditions);
            			objSituationEvent.setSituationID( Integer.toString(objNewSituation.getSituationID()));
            			objListSituationEvent.add(objSituationEvent);
            			objSituationEvents.setListSEvents(objListSituationEvent);
            			
            	         
            		}
            		catch(Exception ex)
            		{
            			System.out.println("****** Exception **********" + ex.getMessage());
            		}
            		
                }else{
                    //existing rule, call update
                	objNewSituation = objSituation;
                }
        		objRule.setSituationID(objNewSituation.getSituationID()); 
        		for(int i = 0; i < objRule.getConditionList().size(); i++){
            		Condition objCondition = objRule.getConditionList().get(i);
                	objCondition = objConditionService.getConditionByFields(objCondition);
                	if(objCondition.getConditionID() != 0)
                	{ 
                		 objRule.getConditionList().set(i, objCondition);
                	}
        		}
        	}
        	else
        	{ objRule.setSituationID(0); }
        	
            if(objRule.getRuleID() == 0){
                //new rule, add it
            	objRule.setRuleCreatedBy(objUser.getUserID());
            	objRule = this.objRuleService.addRule(objRule);
            }else{
                //existing rule, call update
            	objRule.setRuleUpdatedBy(objUser.getUserID());
            	Date todayDate = new Date();
            	objRule.setRuleLastUpdatedDate(todayDate);
            	objRule = this.objRuleService.updateRule(objRule);
            }
            
	        //logger.info("Rule saved successfully");
	        return "Rule saved succussfully with ID: " + objRule.getRuleID();
		}
		catch(Exception ex)
		{
			 //logger.info("Error in saving rule");
			 return "Error in saving rule";
		}
    }
    
    @RequestMapping("/remove/{id}")
    public String removeRule(@PathVariable("id") int id){
        this.objRuleService.removeRule(id);
        return "redirect:/rules";
    }
    
    @RequestMapping("/edit/{id}")
    public String editRule(@PathVariable("id") int id, @ModelAttribute("user") User user, Model model){
    	SituationRuleWrapper situationRuleWrapper = new SituationRuleWrapper();
    	Rule objRule = new Rule();
    	Situations objSituations = new Situations();
    	objRule = this.objRuleService.getRuleById(id);
    	situationRuleWrapper.setRule(objRule);
    	situationRuleWrapper.setUser(user);
    	if(situationRuleWrapper.getRule().getSituationID() != 0)
    	{
    		objSituations = this.objSituationService.getSituationById(objRule.getSituationID());
    		situationRuleWrapper.setSituations(objSituations);
    	}
    	for(int i = 0; i < objRule.getConditionList().size(); i++)
    	{
    		for(int j = 0; j < objSituations.getSituationConditionList().size(); j++)
    		{
    			if(objRule.getConditionList().get(i).getConditionID() == objSituations.getSituationConditionList().get(j).getConditionID())
    			{
    				objRule.getConditionList().get(i).setIsItSituation(true);
    			}
    		}
    	}
        model.addAttribute("situationRuleWrapper", situationRuleWrapper);
        return "RuleEditor";
    }
    
    @RequestMapping("/addNewRule")
    public String addNewRule(@ModelAttribute("user") User user, Model model){
    	SituationRuleWrapper situationRuleWrapper = new SituationRuleWrapper();
    	Rule objRule = new Rule();
    	Date todayDate = new Date();
    	objRule.setRuleCreatedDate(todayDate);
    	situationRuleWrapper.setRule(objRule);
    	situationRuleWrapper.setUser(user);
		model.addAttribute("situationRuleWrapper", situationRuleWrapper);
        return "RuleEditor";
    }
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, true));
        //binder.registerCustomEditor(Integer.class,"ruleTypeID", new CustomNumberEditor(Integer.class, true));
        //binder.registerCustomEditor(Integer.class, "ruleUpdatedBy", new CustomNumberEditor(Integer.class, true));
    }
    
    
    class RuleJsonFormat implements Serializable{
    	List<Rule> Data;

		
    	public List<Rule> getData() {
			return Data;
		}

		public void setData(List<Rule> data) {	
			Data = data;
		}
    	
    	
    }
	
}
