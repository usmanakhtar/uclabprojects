package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.uclab.mm.kcl.edkat.dao.UserDAO;
import org.uclab.mm.kcl.edkat.datamodel.User;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO;
	
	
	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Transactional
	public User addUser(User objUser) {
		return this.userDAO.addUser(objUser);
	}

	@Transactional
	public User updateUser(User objUser) {
		return this.userDAO.updateUser(objUser);
	}

	public List<User> listUser() {
		return this.userDAO.listUser();
	}

	public User getUserById(int id) {
		return this.userDAO.getUserById(id);
	}

	public void removeUser(int id) {
		this.userDAO.removeUser(id);
	}

	public User validateUser(User objUser) {
		return this.userDAO.validateUser(objUser);
	}

}
