package org.uclab.mm.kcl.edkat.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.JoinColumn;


/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Entity
@Table(name="tblUsers")
public class User {

	@Id
    @Column(name="UserID")
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int UserID;
	
	@Column(name="UserName")
	private String userName;
	
	@Column(name="LoginID")
	private String loginID;
	
	@Column(name="Password")
	private String password;
	
	@Column(name="EmailAddress")
	private String emailAddress;
	
	@Column(name="DateOfBirth")
	private Date dateOfBirth;
	
	@Column(name="DesignationID")
	private Integer designationID;
	
	@Column(name="ActiveYNID")
	private Integer activeYNID;

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getDesignationID() {
		return designationID;
	}

	public void setDesignationID(Integer designationID) {
		this.designationID = designationID;
	}

	public Integer getActiveYNID() {
		return activeYNID;
	}

	public void setActiveYNID(Integer activeYNID) {
		this.activeYNID = activeYNID;
	}
	
	
	
	
	
}
