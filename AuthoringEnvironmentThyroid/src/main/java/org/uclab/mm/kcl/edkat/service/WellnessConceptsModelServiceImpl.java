package org.uclab.mm.kcl.edkat.service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.uclab.mm.kcl.edkat.dao.WellnessConceptsModelDAO;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsModel;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsRelationships;

public class WellnessConceptsModelServiceImpl implements WellnessConceptsModelService{

	private WellnessConceptsModelDAO wellnessConceptsModelDAO;
	
	
	public WellnessConceptsModelDAO getWellnessConceptsModelDAO() {
		return wellnessConceptsModelDAO;
	}

	public void setWellnessConceptsModelDAO(WellnessConceptsModelDAO wellnessConceptsModelDAO) {
		this.wellnessConceptsModelDAO = wellnessConceptsModelDAO;
	}

	@Transactional
	public void addWellnessConcept(WellnessConceptsModel objWellnessConceptsModel) {
		this.wellnessConceptsModelDAO.addWellnessConcept(objWellnessConceptsModel);
	}

	@Transactional
	public void updateWellnessConcept(WellnessConceptsModel objWellnessConceptsModel) {
		this.wellnessConceptsModelDAO.updateWellnessConcept(objWellnessConceptsModel);
	}

	public List<WellnessConceptsModel> listWellnessConceptsModel(String strQuery) {
		return this.wellnessConceptsModelDAO.listWellnessConceptsModel(strQuery);
	}

	public WellnessConceptsModel getWellnessConceptById(int id) {
		return this.wellnessConceptsModelDAO.getWellnessConceptById(id);
	}

	public void removeWellnessConcept(int id) {
		this.wellnessConceptsModelDAO.removeWellnessConcept(id);
	}

	public List<WellnessConceptsRelationships> listWellnessConceptsByKey(String strSelectedKey, String strQuery) {
		return this.wellnessConceptsModelDAO.listWellnessConceptsByKey(strSelectedKey, strQuery);
	}

}
