package org.uclab.mm.kcl.edkat.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsModel;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsRelationships;
import org.uclab.mm.kcl.edkat.service.WellnessConceptsModelService;

@Controller
public class WellnessModelController {

	private WellnessConceptsModelService objWellnessConceptsModelService;

	@Autowired(required=true)
    @Qualifier(value="objWellnessConceptsModelService")
	public void setObjWellnessConceptsModelService(WellnessConceptsModelService objWellnessConceptsModelService) {
		this.objWellnessConceptsModelService = objWellnessConceptsModelService;
	}

	@RequestMapping(value = "/getAllWellnessConcepts", method = RequestMethod.GET)
    public @ResponseBody List<WellnessConceptsModel> listWellnessConceptsModel(@RequestParam("term") String query) {
		WellnessConceptsModel wellnessConceptsModel = new WellnessConceptsModel();
		List<WellnessConceptsModel> result = new ArrayList<WellnessConceptsModel>();
		result = this.objWellnessConceptsModelService.listWellnessConceptsModel(query);
		
        return result;
    }
	
	@RequestMapping(value = "/getWellnessConceptsByKey/{selectedKey}/", method = RequestMethod.GET)
    public @ResponseBody List<WellnessConceptsRelationships> getWellnessConceptsByKey(@RequestParam("term") String query, @PathVariable("selectedKey") String strSelectedKey) {
		WellnessConceptsRelationships wellnessConceptsRelationships = new WellnessConceptsRelationships();
		List<WellnessConceptsRelationships> result = new ArrayList<WellnessConceptsRelationships>();
		result = this.objWellnessConceptsModelService.listWellnessConceptsByKey(strSelectedKey, query);
		
        return result;
    }
}
