package org.uclab.mm.kcl.edkat.datamodel.dclcommunication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SituationEvent implements Serializable {

	private String SituationID;
	private List<SituationConditions> ListSConditions = new ArrayList<SituationConditions>();
	public String getSituationID() {
		return SituationID;
	}
	public void setSituationID(String situationID) {
		SituationID = situationID;
	}
	public List<SituationConditions> getListSConditions() {
		return ListSConditions;
	}
	public void setListSConditions(List<SituationConditions> listSConditions) {
		ListSConditions = listSConditions;
	}
	
}
