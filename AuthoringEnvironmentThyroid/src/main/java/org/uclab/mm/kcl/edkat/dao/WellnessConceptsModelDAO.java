package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsModel;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsRelationships;


/**
 * Rule DAO
 * Rule DAO
 * @author Taqdir Ali
 *
 */
public interface WellnessConceptsModelDAO {

	public void addWellnessConcept(WellnessConceptsModel objWellnessConceptsModel);
	public void updateWellnessConcept(WellnessConceptsModel objWellnessConceptsModel);
	public List<WellnessConceptsModel> listWellnessConceptsModel(String strQuery);
	public WellnessConceptsModel getWellnessConceptById(int id);
    public void removeWellnessConcept(int id);
    public List<WellnessConceptsRelationships> listWellnessConceptsByKey(String strSelectedKey, String strQuery);
    
}
