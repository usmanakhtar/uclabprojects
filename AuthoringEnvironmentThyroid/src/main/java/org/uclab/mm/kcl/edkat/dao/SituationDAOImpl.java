package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.Condition;
import org.uclab.mm.kcl.edkat.datamodel.Rule;
import org.uclab.mm.kcl.edkat.datamodel.Situations;


/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Repository
public class SituationDAOImpl implements SituationDAO {

	private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
	 private SessionFactory sessionFactory;
    
   public void setSessionFactory(SessionFactory sf){
       this.sessionFactory = sf;
   }
   
	public Situations addSituation(Situations objSituation) {
		Situations objNewSituation = new Situations();
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        objNewSituation = (Situations) session.merge(objSituation);
        tx.commit();
        session.close();
        logger.info("Situation saved successfully, Situation Details="+objSituation);
        return objNewSituation;
	}

	public Situations updateSituation(Situations objSituation) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objSituation); // Replaced with updated rule
        tx.commit();
        session.close();
        logger.info("Situation updated successfully, Situation Details="+objSituation);
        return objSituation;
	}

	@SuppressWarnings("unchecked")
	public List<Situations> listSituations() {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Situations> situationsList = session.createQuery("from Situations").list();
        for(Situations objSituations : situationsList){
            logger.info("Situations List::"+objSituations);
        }
        session.close();
        return situationsList;
	}

	public Situations getSituationById(int id) {
		Session session = this.sessionFactory.openSession();      
		Situations objSituations = (Situations) session.load(Situations.class, new Integer(id));
        logger.info("Situations loaded successfully, Situations details="+objSituations);
        session.close();
        return objSituations;
	}

	public void removeSituation(int id) {
		Session session = this.sessionFactory.openSession();
		Situations objSituations = (Situations) session.load(Situations.class, new Integer(id));
	        if(null != objSituations){
	            session.delete(objSituations);
	        }
	        logger.info("Situations deleted successfully, Situations details="+objSituations);
	        session.close();
		
	}

	public Situations getExistingSituation(Situations objSituation) {
			
		//objSendingSituation = objSituation;
		

		Session session = this.sessionFactory.openSession();  
		Situations objSendingSituation = objSituation;
		List<Situations> objExistingSituations = session.createQuery("from Situations s left join fetch s.situationConditionList sc where sc.conditionKey='" + objSituation.getSituationConditionList().get(0).getConditionKey() + "' and sc.conditionValue='" + objSituation.getSituationConditionList().get(0).getConditionValue() + "' and sc.conditionValueOperator='" + objSituation.getSituationConditionList().get(0).getConditionValueOperator() + "'").list();
		if(objExistingSituations.size() > 0)
		{
			
			for(Situations objDBSituation : objExistingSituations)
			{
				Session session2 = this.sessionFactory.openSession();
				Situations objFullDBSituation = (Situations) session2.createQuery("from Situations where SituationID = " + objDBSituation.getSituationID()).uniqueResult();
				//Situations objFullDBSituation = getSituationById(objDBSituation.getSituationID());
				objDBSituation = objFullDBSituation;
				if(objSituation.getSituationConditionList().size() == objDBSituation.getSituationConditionList().size())
				{
					int countEqual = 0;
					for(Condition objCommingCondition : objSituation.getSituationConditionList())
					{
						for(Condition objDBCondition : objDBSituation.getSituationConditionList())
						{
							if(objCommingCondition.getConditionKey().equals(objDBCondition.getConditionKey()) && objCommingCondition.getConditionValue().equals(objDBCondition.getConditionValue()) && objCommingCondition.getConditionValueOperator().equals(objDBCondition.getConditionValueOperator()))
							{
								countEqual = countEqual + 1;
							}
						}
					}
					
			        if(objSituation.getSituationConditionList().size() == countEqual)
			        {
			        	objSendingSituation = objDBSituation;
			        }
				}
				session2.close();
			}
		}
		session.close();

        return objSendingSituation;
	}

}
