package org.uclab.mm.kcl.edkat.datamodel.dclcommunication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SituationEvents implements Serializable {

	private List<SituationEvent> listSEvents = new ArrayList<SituationEvent>();

	public List<SituationEvent> getListSEvents() {
		return listSEvents;
	}

	public void setListSEvents(List<SituationEvent> listSEvents) {
		this.listSEvents = listSEvents;
	}
	
}
