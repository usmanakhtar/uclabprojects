package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import javax.transaction.Transactional;

import org.uclab.mm.kcl.edkat.dao.SituationDAO;
import org.uclab.mm.kcl.edkat.datamodel.Situations;

public class SituationServiceImpl implements SituationService {

	private SituationDAO situationDAO;
	
	
	public SituationDAO getSituationDAO() {
		return situationDAO;
	}

	public void setSituationDAO(SituationDAO situationDAO) {
		this.situationDAO = situationDAO;
	}

	@Transactional
	public Situations addSituation(Situations objSituation) {
		return this.situationDAO.addSituation(objSituation);
	}

	@Transactional
	public Situations updateSituation(Situations objSituation) {
		return this.situationDAO.updateSituation(objSituation);
	}

	public List<Situations> listSituations() {
		return this.situationDAO.listSituations();
	}

	public Situations getSituationById(int id) {
		return this.situationDAO.getSituationById(id);
	}

	public void removeSituation(int id) {
		this.situationDAO.removeSituation(id);
	}

	public Situations getExistingSituation(Situations objSituation) {
		return this.situationDAO.getExistingSituation(objSituation);
	}

}
