package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsModel;
import org.uclab.mm.kcl.edkat.datamodel.WellnessConceptsRelationships;

@Repository
public class WellnessConceptsModelDAOImpl implements WellnessConceptsModelDAO {

	private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
	 private SessionFactory sessionFactory;
    
   public void setSessionFactory(SessionFactory sf){
       this.sessionFactory = sf;
   }
   
	public void addWellnessConcept(WellnessConceptsModel objWellnessConceptsModel) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(objWellnessConceptsModel);
        tx.commit();
        session.close();
        logger.info("Wellness Concepts Model saved successfully, Wellness Concepts Model Details="+objWellnessConceptsModel);
		
	}

	public void updateWellnessConcept(WellnessConceptsModel objWellnessConceptsModel) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objWellnessConceptsModel);
        tx.commit();
        session.close();
        logger.info("Wellness Concepts Model updated successfully, Wellness Concepts Model Details="+objWellnessConceptsModel);
	}

	@SuppressWarnings("unchecked")
	public List<WellnessConceptsModel> listWellnessConceptsModel(String strQuery) {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<WellnessConceptsModel> wellnessConceptsModelList = session.createQuery("from WellnessConceptsModel w Where w.wellnessConceptDescription like '%" + strQuery + "%'").list();
        for(WellnessConceptsModel objWellnessConceptsModel : wellnessConceptsModelList){
            logger.info("WellnessConceptsModel List::"+objWellnessConceptsModel);
        }
        session.close();
        return wellnessConceptsModelList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WellnessConceptsRelationships> listWellnessConceptsByKey(String strKey, String strQuery) {
		Session session = this.sessionFactory.openSession();

		Query query = session.createSQLQuery("{CALL Usp_Get_WellnessConceptsByKey(:key, :query)}").addEntity(WellnessConceptsRelationships.class).setParameter("key", strKey).setParameter("query", strQuery);
		//query.setParameter("key", strKey);
		//query.setParameter("query", strQuery);
		
		List<WellnessConceptsRelationships> wellnessConceptsRelationshipslList = query.list();
        
        for(WellnessConceptsRelationships objWellnessConceptsRelationships : wellnessConceptsRelationshipslList){
            logger.info("WellnessConceptsRelationships List::"+objWellnessConceptsRelationships);
        }
        session.close();
        return wellnessConceptsRelationshipslList;
	}
	

	public WellnessConceptsModel getWellnessConceptById(int id) {
		Session session = this.sessionFactory.openSession();      
		WellnessConceptsModel objWellnessConceptsModel = (WellnessConceptsModel) session.load(WellnessConceptsModel.class, new Integer(id));
        logger.info("Wellness Concepts Model loaded successfully, WellnessConceptsModel details="+objWellnessConceptsModel);
        session.close();
        return objWellnessConceptsModel;
	}

	public void removeWellnessConcept(int id) {
		 Session session = this.sessionFactory.openSession();
		 WellnessConceptsModel objWellnessConceptsModel = (WellnessConceptsModel) session.load(WellnessConceptsModel.class, new Integer(id));
	        if(null != objWellnessConceptsModel){
	            session.delete(objWellnessConceptsModel);
	        }
	        logger.info("WellnessConceptsModel deleted successfully, Rule details="+objWellnessConceptsModel);
	        session.close();
		
	}

}
