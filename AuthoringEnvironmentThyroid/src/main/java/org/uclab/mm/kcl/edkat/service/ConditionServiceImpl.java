package org.uclab.mm.kcl.edkat.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.uclab.mm.kcl.edkat.dao.ConditionDAO;
import org.uclab.mm.kcl.edkat.datamodel.Condition;

public class ConditionServiceImpl implements ConditionService {

	private ConditionDAO conditionDAO;
	
	public ConditionDAO getConditionDAO() {
		return conditionDAO;
	}

	public void setConditionDAO(ConditionDAO conditionDAO) {
		this.conditionDAO = conditionDAO;
	}

	@Transactional
	public void addCondition(Condition objCondition) {
		this.conditionDAO.addCondition(objCondition);
	}

	@Transactional
	public void updateCondition(Condition objCondition) {
		this.conditionDAO.updateCondition(objCondition);
	}

	public List<Condition> listCondition() {
		return this.conditionDAO.listCondition();
	}

	public Condition getConditionById(int id) {
		return this.conditionDAO.getConditionById(id);
	}

	public void removeCondition(int id) {
		this.conditionDAO.removeCondition(id);
	}

	public Condition getConditionByFields(Condition objCondition) {
		return this.conditionDAO.getConditionByFields(objCondition);
	}

}
