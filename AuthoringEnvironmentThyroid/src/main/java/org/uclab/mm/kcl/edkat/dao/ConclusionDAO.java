package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.uclab.mm.kcl.edkat.datamodel.Conclusion;


/**
 * ConclusionDAO
 * ConclusionDAO
 * @author Taqdir Ali
 *
 */

public interface ConclusionDAO {

	public void addConclusion(Conclusion objConclusion);
	public void updateConclusion(Conclusion objConclusion);
	public List<Conclusion> listConclusion();
	public Conclusion getConclusionById(int id);
    public void removeConclusion(int id);
    
}
