package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.Condition;
import org.uclab.mm.kcl.edkat.datamodel.Situations;

@Repository
public class ConditionDAOImpl implements ConditionDAO {

	private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
	private SessionFactory sessionFactory;
	    
	public void setSessionFactory(SessionFactory sf){
	    this.sessionFactory = sf;
	}
	   
	public void addCondition(Condition objCondition) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(objCondition);
        tx.commit();
        session.close();
        logger.info("Condition saved successfully, Condition Details="+objCondition);
	}

	public void updateCondition(Condition objCondition) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objCondition);
        tx.commit();
        session.close();
        logger.info("Condition updated successfully, Condition Details="+objCondition);
	}

	@SuppressWarnings("unchecked")
	public List<Condition> listCondition() {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Condition> conditionList = session.createQuery("from Condition").list();
        for(Condition objCondition : conditionList){
            logger.info("Condition List::"+objCondition);
        }
        session.close();
        return conditionList;
	}
	
	@SuppressWarnings("unchecked")
	public Condition getConditionByFields(Condition objCondition) {
		Condition exCondition = new Condition();
		Session session = this.sessionFactory.openSession();
		/*Criteria crit = session.createCriteria(Condition.class);
		if(objCondition.getConditionKey() != null)
		{ crit.add(Restrictions.eq("conditionKey",objCondition.getConditionKey())); }
		
		if(objCondition.getConditionValueOperator() != null)
		{ crit.add(Restrictions.eq("conditionValueOperator",objCondition.getConditionValueOperator())); }
		
		if(objCondition.getConditionValue() != null)
		{ crit.add(Restrictions.eq("conditionValue",objCondition.getConditionValue())); }
		
		crit.setMaxResults(1);
		exCondition = (Condition) crit.uniqueResult();*/
		
		exCondition = (Condition) session.createQuery("from Condition c where c.conditionKey='" + objCondition.getConditionKey() + "' and c.conditionValue='" + objCondition.getConditionValue() + "' and c.conditionValueOperator='" + objCondition.getConditionValueOperator() + "'").setMaxResults(1).uniqueResult();
		if(exCondition == null)
		{ 
			exCondition = objCondition;
			exCondition.setConditionID(0);
		}
		session.close();
		return exCondition;
	}

	public Condition getConditionById(int id) {
		Session session = this.sessionFactory.openSession();      
		Condition objCondition = (Condition) session.load(Condition.class, new Integer(id));
        logger.info("Condition loaded successfully, Condition details="+objCondition);
        session.close();
        return objCondition;
	}

	public void removeCondition(int id) {
		Session session = this.sessionFactory.openSession();
		Condition objCondition = (Condition) session.load(Condition.class, new Integer(id));
	        if(null != objCondition){
	            session.delete(objCondition);
	        }
	        logger.info("Condition deleted successfully, Condition details="+objCondition);
	        session.close();
	}

}
