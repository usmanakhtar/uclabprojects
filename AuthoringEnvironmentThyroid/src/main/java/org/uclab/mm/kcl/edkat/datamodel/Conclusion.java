package org.uclab.mm.kcl.edkat.datamodel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Entity
@Table(name="tblConclusions")
public class Conclusion {

	@Id
    @Column(name="ConclusionID")
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int ConclusionID;
	
	/*@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="RuleID")
	private Rule ruleID;*/
	
	@Column(name="RuleID")
	private int RuleID;
	
	public int getRuleID() {
		return RuleID;
	}

	public void setRuleID(int ruleID) {
		RuleID = ruleID;
	}

	@Column(name="ConclusionKey")
	private String conclusionKey;
	
	@Column(name="ConclusionValue")
	private String conclusionValue;
	
	@Column(name="ConclusionOperator")
	private String conclusionOperator;
	
	public Conclusion()  
	 {  
	 }  
	   
/*	 public Conclusion(int ConclusionID, Rule rule, String conclusionKey, String  conclusionValue, String conclusionOperator)  
	 {  
	  super();  
	  this.ConclusionID = ConclusionID;  
	  this.ruleID = rule;  
	  this.conclusionKey = conclusionKey;  
	  this.conclusionValue = conclusionValue;
	  this.conclusionOperator = conclusionOperator;
	 } */
	 
	 
	public int getConclusionID() {
		return ConclusionID;
	}

	public void setConclusionID(int conclusionID) {
		ConclusionID = conclusionID;
	}

	/*public Rule getRuleID() {
		return ruleID;
	}

	public void setRuleID(Rule ruleID) {
		this.ruleID = ruleID;
	}*/

	public String getConclusionKey() {
		return conclusionKey;
	}

	public void setConclusionKey(String conclusionKey) {
		this.conclusionKey = conclusionKey;
	}

	public String getConclusionValue() {
		return conclusionValue;
	}

	public void setConclusionValue(String conclusionValue) {
		this.conclusionValue = conclusionValue;
	}

	public String getConclusionOperator() {
		return conclusionOperator;
	}

	public void setConclusionOperator(String conclusionOperator) {
		this.conclusionOperator = conclusionOperator;
	}
	
	
	
}
