package org.uclab.mm.kcl.edkat.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.uclab.mm.kcl.edkat.datamodel.User;

@Repository
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(RuleDAOImpl.class);
	
	 private SessionFactory sessionFactory;
    
   public void setSessionFactory(SessionFactory sf){
       this.sessionFactory = sf;
   }
   
	public User addUser(User objUser) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.merge(objUser);
        tx.commit();
        session.close();
        logger.info("User saved successfully, User Details="+objUser);
        return objUser;
	}

	public User updateUser(User objUser) {
		Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(objUser); // Replaced with updated rule
        tx.commit();
        session.close();
        logger.info("User updated successfully, User Details="+objUser);
        return objUser;
	}

	@SuppressWarnings("unchecked")
	public List<User> listUser() {
		Session session = this.sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<User> userList = session.createQuery("from User").list();
        for(User objUser : userList){
            logger.info("User List:"+objUser);
        }
        session.close();
        return userList;
	}

	public User getUserById(int id) {
		Session session = this.sessionFactory.openSession();      
		User objUser = (User) session.createQuery("from User u where u.UserID=" + id).uniqueResult();
        logger.info("User loaded successfully, User details="+objUser);
        session.close();
        return objUser;
	}

	public void removeUser(int id) {
		Session session = this.sessionFactory.openSession();
		User objUser = (User) session.load(User.class, new Integer(id));
        if(null != objUser){
            session.delete(objUser);
        }
        session.close();
        logger.info("User deleted successfully, User details="+objUser);
	}

	public User validateUser(User objUser) {
		Session session = this.sessionFactory.openSession();      
		User objDBUser = (User) session.createQuery("from User u where u.loginID ='" + objUser.getLoginID() + "' and u.password = '" + objUser.getPassword() + "'").uniqueResult();
        logger.info("User loaded successfully, User details="+objUser);
        session.close();
        if(objDBUser == null)
        { objDBUser = objUser; }
        return objDBUser;
	}

}
