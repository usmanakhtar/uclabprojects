package org.uclab.mm.kcl.edkat.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Entity
@Table(name="tblSituations")
public class Situations {

	@Id
    @Column(name="SituationID")
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int SituationID;

	@Column(name="SituationDescription")
	private String situationDescription;
	
	public int getSituationID() {
		return SituationID;
	}

	public void setSituationID(int situationID) {
		SituationID = situationID;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="SituationID")
	private List<Rule> rulesList = new ArrayList<Rule>();

	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tblSituationConditions", 
             joinColumns = { @JoinColumn(name = "SituationID") }, 
             inverseJoinColumns = { @JoinColumn(name = "ConditionID") })
    private List<Condition> situationConditionList = new ArrayList<Condition>();
	
	public Situations(){}
	
	public List<Rule> getRulesList() {
		return rulesList;
	}

	public void setRulesList(List<Rule> rulesList) {
		this.rulesList = rulesList;
	}

	

	public String getSituationDescription() {
		return situationDescription;
	}

	public void setSituationDescription(String situationDescription) {
		this.situationDescription = situationDescription;
	}

	public List<Condition> getSituationConditionList() {
		return situationConditionList;
	}

	public void setSituationConditionList(List<Condition> situationConditionList) {
		this.situationConditionList = situationConditionList;
	}

	
	
}
