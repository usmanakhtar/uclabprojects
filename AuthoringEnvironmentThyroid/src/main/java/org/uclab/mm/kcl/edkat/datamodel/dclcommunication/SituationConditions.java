package org.uclab.mm.kcl.edkat.datamodel.dclcommunication;

import java.io.Serializable;

public class SituationConditions implements Serializable {

	private String conditionKey;
    private String ConditionValueOperator;
    private String ConditionValue;
    private String ConditionType;
    
	public String getConditionKey() {
		return conditionKey;
	}
	public void setConditionKey(String conditionKey) {
		this.conditionKey = conditionKey;
	}
	public String getConditionValueOperator() {
		return ConditionValueOperator;
	}
	public void setConditionValueOperator(String conditionValueOperator) {
		ConditionValueOperator = conditionValueOperator;
	}
	public String getConditionValue() {
		return ConditionValue;
	}
	public void setConditionValue(String conditionValue) {
		ConditionValue = conditionValue;
	}
	public String getConditionType() {
		return ConditionType;
	}
	public void setConditionType(String conditionType) {
		ConditionType = conditionType;
	}
    
}
