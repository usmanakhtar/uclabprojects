package org.uclab.imp.rs.ecrf.cardiovascular.monitoring;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.AbstractDataBridge;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.DatabaseStorage;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.EchocardiographyAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.ElectrocardiogramAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.NTproBNPAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientClinicalHistoryAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientDiagnosisAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientEncounterAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientPhysicalExamAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientSymptomsAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientVitalSignStatisticsAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.PatientVitalSignsAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.UsersAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.dataadapter.VitalSignMessageAdapter;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Echocardiography;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Electrocardiogram;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.NTproBNP;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Patient;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientClinicalHistory;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientDetailPacket;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientDiagnosis;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientEncounter;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientPhysicalExam;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientSymptoms;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSigns;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Users;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.VitalSignMessage;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSignStatistics;

/**
 * Facade for the Restful Web service to handle the data curation functions 
 *
 * @author Taqdir Ali
 */
@Path("eCRFCardiovasular")
public class ORMController {

	@Context
    private UriInfo context;

    /**
     * Creates a new instance of DataCurationResource
     */
    public ORMController() {
    }
    
    @GET
    @Path("/")
    public String index(){
    	return "Test Message";
    }
    private static final Logger logger = LoggerFactory.getLogger(ORMController.class);
    
    /**
     * This function is using to Patient Registration
     * @param objOuterUser
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatient")
    public List<String> SavePatient(Patient objOuterPatient) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatient(objOuterPatient);
            logger.info("Patient saved successfully, Patient Details="+objOuterPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Patient, Patient Details=");
        	response.add("Error in saving Patient :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Patient Registration
     * @param objOuterUser
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatient")
    public List<String> UpdatePatient(Patient objOuterPatient) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatient(objOuterPatient);
            logger.info("Patient updated successfully, Patient Details="+objOuterPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Patient, Patient Details=");
        	response.add("Error in updating Patient :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get Patient by ID 
     * @param PatientID
     * @return a list of object Patient with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("Patient/{PatientID}")
    public List<Patient> GetPatientByID(@PathParam ("PatientID") String PatientID) {
    	Patient objOuterPatient = new Patient();
        List<Patient> objListPatient = new  ArrayList<Patient>();
        try
        {
            objOuterPatient.setPatientID(Long.parseLong(PatientID));
            objOuterPatient.setRequestType("PatientData");
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatient = objADBridge.RetrivePatient(objOuterPatient);
            logger.info("Get Patient successfully, Patient Details="+objOuterPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Patient");
        }
        return objListPatient;
    }
    
    /**
     * This function is using to get Patient List
     * @param 
     * @return a list of object Patient with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientsList")
    public List<Patient> GetPatientsList() {
    	Patient objOuterPatient = new Patient();
        List<Patient> objListPatient = new  ArrayList<Patient>();
        try
        {
            objOuterPatient.setRequestType("AllPatients");
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatient = objADBridge.RetrivePatient(objOuterPatient);
            logger.info("Get Patient successfully, Patient Details="+objOuterPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Patient");
        }
        return objListPatient;
    }
    
    /**
     * This function is using to Patient Registration
     * @param objOuterUser
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatient")
    public List<String> DeletePatient(Patient objOuterPatient) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatient(objOuterPatient);
            logger.info("Patient deleted successfully, Patient Details="+objOuterPatient);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Patient, Patient Details=");
        	response.add("Error in deleting Patient :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to save PatientEncounter
     * @param objOuterPatientEncounter
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientEncounter")
    public List<String> SavePatientEncounter(PatientEncounter objOuterPatientEncounter) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientEncounter(objOuterPatientEncounter);
            logger.info("PatientEncounter saved successfully, PatientEncounter Details="+objOuterPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientEncounter, PatientEncounter Details=");
        	response.add("Error in saving PatientEncounter :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to update PatientEncounter
     * @param objOuterPatientEncounter
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientEncounter")
    public List<String> UpdatePatientEncounter(PatientEncounter objOuterPatientEncounter) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientEncounter(objOuterPatientEncounter);
            logger.info("PatientEncounter updated successfully, PatientEncounter Details="+objOuterPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientEncounter, PatientEncounter Details=");
        	response.add("Error in updating PatientEncounter :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientEncounter by Patient ID 
     * @param PatientEncounterID
     * @return a list of object Patient with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientEncounterByPatientID/{PatientID}")
    public List<PatientEncounter> GetPatientEncounterByPatientID(@PathParam ("PatientID") String PatientID) {
    	PatientEncounter objOuterPatientEncounter = new PatientEncounter();
        List<PatientEncounter> objListPatientEncounter = new  ArrayList<PatientEncounter>();
        try
        {
            objOuterPatientEncounter.setPatientID(Long.parseLong(PatientID));
            objOuterPatientEncounter.setRequestType("ByPatientID");
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientEncounter = objADBridge.RetrivePatientEncounter(objOuterPatientEncounter);
            logger.info("Get PatientEncounter successfully, PatientEncounter Details="+objOuterPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientEncounter");
        }
        return objListPatientEncounter;
    }
    
    /**
     * This function is using to get PatientEncounter by Patient ID 
     * @param PatientEncounterID
     * @return a list of object Patient with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientEncounterByPatientEncounterID/{PatientEncounterID}")
    public List<PatientEncounter> GetPatientEncounterByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientEncounter objOuterPatientEncounter = new PatientEncounter();
        List<PatientEncounter> objListPatientEncounter = new  ArrayList<PatientEncounter>();
        try
        {
            objOuterPatientEncounter.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientEncounter.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientEncounter = objADBridge.RetrivePatientEncounter(objOuterPatientEncounter);
            logger.info("Get PatientEncounter successfully, PatientEncounter Details="+objOuterPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientEncounter");
        }
        return objListPatientEncounter;
    }
    
    /**
     * This function is using to delete PatientEncounter
     * @param objOuterPatientEncounter
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientEncounter")
    public List<String> DeletePatientEncounter(PatientEncounter objOuterPatientEncounter) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientEncounter(objOuterPatientEncounter);
            logger.info("PatientEncounter deleted successfully, PatientEncounter Details="+objOuterPatientEncounter);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientEncounter, PatientEncounter Details=");
        	response.add("Error in deleting PatientEncounter :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientSymptoms
     * @param objOuterPatientSymptoms
     * @return a list of object string with "Error", "No Error" and new added ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientSymptoms")
    public List<String> SavePatientSymptoms(PatientSymptoms objOuterPatientSymptoms) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientSymptoms(objOuterPatientSymptoms);
            logger.info("PatientSymptoms saved successfully, Patient Details="+objOuterPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientSymptoms, PatientSymptoms Details=");
        	response.add("Error in saving PatientSymptoms :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientSymptoms
     * @param objOuterPatientSymptoms
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientSymptoms")
    public List<String> PatientSymptoms(PatientSymptoms objOuterPatientSymptoms) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientSymptoms(objOuterPatientSymptoms);
            logger.info("PatientSymptoms updated successfully, PatientSymptoms Details="+objOuterPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientSymptoms, PatientSymptoms Details=");
        	response.add("Error in updating PatientSymptoms :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientSymptoms by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object PatientSymptoms with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientSymptomsByPatientEncounterID/{PatientEncounterID}")
    public List<PatientSymptoms> GetPatientSymptomsByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientSymptoms objOuterPatientSymptoms = new PatientSymptoms();
        List<PatientSymptoms> objListPatientSymptoms = new  ArrayList<PatientSymptoms>();
        try
        {
            objOuterPatientSymptoms.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientSymptoms.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientSymptoms = objADBridge.RetrivePatientSymptoms(objOuterPatientSymptoms);
            logger.info("Get PatientSymptoms successfully, PatientSymptoms Details="+objOuterPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientSymptoms");
        }
        return objListPatientSymptoms;
    }
    
    /**
     * This function is using to get PatientSymptoms by Patient Symptom ID 
     * @param PatientSymptomID
     * @return a list of object PatientSymptoms with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientSymptomsByPatientSymptomID/{PatientSymptomID}")
    public List<PatientSymptoms> GetPatientSymptomsByPatientSymptomID(@PathParam ("PatientSymptomID") String PatientSymptomID) {
    	PatientSymptoms objOuterPatientSymptoms = new PatientSymptoms();
        List<PatientSymptoms> objListPatientSymptoms = new  ArrayList<PatientSymptoms>();
        try
        {
            objOuterPatientSymptoms.setPatientSymptomID(Long.parseLong(PatientSymptomID));
            objOuterPatientSymptoms.setRequestType("ByPatientSymptomsID");
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientSymptoms = objADBridge.RetrivePatientSymptoms(objOuterPatientSymptoms);
            logger.info("Get PatientSymptoms successfully, PatientSymptoms Details="+objOuterPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientSymptoms");
        }
        return objListPatientSymptoms;
    }
    
    /**
     * This function is using to delete PatientSymptoms
     * @param objOuterPatientSymptoms
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientSymptoms")
    public List<String> DeletePatientSymptoms(PatientSymptoms objOuterPatientSymptoms) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientSymptoms(objOuterPatientSymptoms);
            logger.info("PatientSymptoms deleted successfully, PatientSymptoms Details="+objOuterPatientSymptoms);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientSymptoms, PatientSymptoms Details=");
        	response.add("Error in deleting PatientSymptoms :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientClinicalHistory
     * @param objOuterPatientClinicalHistory
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientClinicalHistory")
    public List<String> SavePatientClinicalHistory(PatientClinicalHistory objOuterPatientClinicalHistory) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientClinicalHistory(objOuterPatientClinicalHistory);
            logger.info("PatientClinicalHistory saved successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientClinicalHistory, PatientClinicalHistory Details=");
        	response.add("Error in saving PatientClinicalHistory :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientClinicalHistory
     * @param objOuterPatientClinicalHistory
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientClinicalHistory")
    public List<String> UpdatePatientClinicalHistory(PatientClinicalHistory objOuterPatientClinicalHistory) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientClinicalHistory(objOuterPatientClinicalHistory);
            logger.info("PatientClinicalHistory updated successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientClinicalHistory, PatientClinicalHistory Details=");
        	response.add("Error in updating PatientClinicalHistory :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientClinicalHistory by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object PatientClinicalHistory with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientClinicalHistoryByPatientEncounterID/{PatientEncounterID}")
    public List<PatientClinicalHistory> GetPatientClinicalHistoryByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientClinicalHistory objOuterPatientClinicalHistory = new PatientClinicalHistory();
        List<PatientClinicalHistory> objListPatientClinicalHistory = new  ArrayList<PatientClinicalHistory>();
        try
        {
            objOuterPatientClinicalHistory.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientClinicalHistory.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientClinicalHistory = objADBridge.RetrivePatientClinicalHistory(objOuterPatientClinicalHistory);
            logger.info("Get PatientClinicalHistory successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientClinicalHistory");
        }
        return objListPatientClinicalHistory;
    }
    
    /**
     * This function is using to get PatientClinicalHistory by Patient Clinical History ID 
     * @param PatientClinicalHistoryID
     * @return a list of object PatientClinicalHistory with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientClinicalHistoryByPatientClinicalHistoryID/{PatientClinicalHistoryID}")
    public List<PatientClinicalHistory> GetPatientClinicalHistoryByPatientClinicalHistoryID(@PathParam ("PatientClinicalHistoryID") String PatientClinicalHistoryID) {
    	PatientClinicalHistory objOuterPatientClinicalHistory = new PatientClinicalHistory();
        List<PatientClinicalHistory> objListPatientClinicalHistory = new  ArrayList<PatientClinicalHistory>();
        try
        {
            objOuterPatientClinicalHistory.setPatientClinicalHistoryID(Long.parseLong(PatientClinicalHistoryID));
            objOuterPatientClinicalHistory.setRequestType("ByPatientClinicalHistoryID");
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientClinicalHistory = objADBridge.RetrivePatientClinicalHistory(objOuterPatientClinicalHistory);
            logger.info("Get PatientClinicalHistory successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientClinicalHistory");
        }
        return objListPatientClinicalHistory;
    }
    
    /**
     * This function is using to delete PatientClinicalHistory
     * @param objOuterPatientClinicalHistory
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientClinicalHistory")
    public List<String> DeletePatientClinicalHistory(PatientClinicalHistory objOuterPatientClinicalHistory) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientClinicalHistory(objOuterPatientClinicalHistory);
            logger.info("PatientClinicalHistory deleted successfully, PatientClinicalHistory Details="+objOuterPatientClinicalHistory);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientClinicalHistory, PatientClinicalHistory Details=");
        	response.add("Error in deleting PatientClinicalHistory :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientVitalSigns
     * @param objOuterPatientVitalSigns
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientVitalSigns")
    public List<String> SavePatientVitalSigns(PatientVitalSigns objOuterPatientVitalSigns) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("PatientVitalSigns saved successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientVitalSigns, PatientVitalSigns Details=");
        	response.add("Error in saving PatientVitalSigns :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientVitalSigns
     * @param objOuterPatientVitalSigns
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientVitalSigns")
    public List<String> UpdatePatientVitalSigns(PatientVitalSigns objOuterPatientVitalSigns) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("PatientVitalSigns updated successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientVitalSigns, PatientVitalSigns Details=");
        	response.add("Error in updating PatientVitalSigns :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientVitalSigns by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object PatientVitalSigns with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignsByPatientEncounterID/{PatientEncounterID}")
    public List<PatientVitalSigns> GetPatientVitalSignsByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
        List<PatientVitalSigns> objListPatientVitalSigns = new  ArrayList<PatientVitalSigns>();
        try
        {
            objOuterPatientVitalSigns.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientVitalSigns.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSigns = objADBridge.RetrivePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSigns;
    }
    
    /**
     * This function is using to get PatientVitalSigns by Vital Sign ID 
     * @param VitalSignID
     * @return a list of object PatientVitalSigns with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignsByVitalSignID/{VitalSignID}")
    public List<PatientVitalSigns> GetPatientVitalSignsByVitalSignID(@PathParam ("VitalSignID") String VitalSignID) {
    	PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
        List<PatientVitalSigns> objListPatientVitalSigns = new  ArrayList<PatientVitalSigns>();
        try
        {
            objOuterPatientVitalSigns.setVitalSignID(Long.parseLong(VitalSignID));
            objOuterPatientVitalSigns.setRequestType("ByVitalSignID");
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSigns = objADBridge.RetrivePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSigns;
    }
    
    /**
     * This function is using to get PatientVitalSigns by Patient Encounter ID for Recommendation
     * @param PatientEncounterID
     * @return a list of object PatientVitalSigns with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignsByPatientEncounterIDRec/{PatientEncounterID}")
    public List<PatientVitalSigns> PatientVitalSignsByPatientEncounterIDRec(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
        List<PatientVitalSigns> objListPatientVitalSigns = new  ArrayList<PatientVitalSigns>();
        try
        {
            objOuterPatientVitalSigns.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientVitalSigns.setRequestType("ByPatientEncounterIDRecommedation");
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSigns = objADBridge.RetrivePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSigns;
    }
    
    /**
     * This function is using to get PatientVitalSigns by Patient Encounter ID for Recommendation
     * @param PatientEncounterID
     * @return a list of object PatientVitalSigns with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignsByPatientEncounterIDNoRec/{PatientEncounterID}")
    public List<PatientVitalSigns> PatientVitalSignsByPatientEncounterIDNoRec(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
        List<PatientVitalSigns> objListPatientVitalSigns = new  ArrayList<PatientVitalSigns>();
        try
        {
            objOuterPatientVitalSigns.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientVitalSigns.setRequestType("ByPatientEncounterIDNoRecommedation");
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSigns = objADBridge.RetrivePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSigns;
    }
    
    /**
     * This function is using to get PatientVitalSignStatistics by Patient ID for Recommendation
     * @param PatientID
     * @return a list of object PatientVitalSignStatistics with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignStatisticsByPatientIDVisitwise/{PatientID}")
    public List<PatientVitalSignStatistics> PatientVitalSignStatisticsByPatientIDVisitwise(@PathParam ("PatientID") String PatientID) {
    	PatientVitalSignStatistics objOuterPatientVitalSignStatistics = new PatientVitalSignStatistics();
        List<PatientVitalSignStatistics> objListPatientVitalSignStatistics = new  ArrayList<PatientVitalSignStatistics>();
        try
        {
            objOuterPatientVitalSignStatistics.setPatientID(Long.parseLong(PatientID));
            objOuterPatientVitalSignStatistics.setRequestType("ByPatientIDGroup");
            DataAccessInterface objDAInterface = new PatientVitalSignStatisticsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSignStatistics = objADBridge.RetrivePatientVitalSignStatistics(objOuterPatientVitalSignStatistics);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSignStatistics;
    }
    
    /**
     * This function is using to get PatientVitalSignStatistics by Patient ID Accumulate
     * @param PatientID
     * @return a list of object PatientVitalSignStatistics with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignStatisticsByPatientIDAccumulate/{PatientID}")
    public List<PatientVitalSignStatistics> PatientVitalSignStatisticsByPatientIDAccumulate(@PathParam ("PatientID") String PatientID) {
    	PatientVitalSignStatistics objOuterPatientVitalSignStatistics = new PatientVitalSignStatistics();
        List<PatientVitalSignStatistics> objListPatientVitalSignStatistics = new  ArrayList<PatientVitalSignStatistics>();
        try
        {
            objOuterPatientVitalSignStatistics.setPatientID(Long.parseLong(PatientID));
            objOuterPatientVitalSignStatistics.setRequestType("ByPatientIDAccumulate");
            DataAccessInterface objDAInterface = new PatientVitalSignStatisticsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSignStatistics = objADBridge.RetrivePatientVitalSignStatistics(objOuterPatientVitalSignStatistics);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSignStatistics;
    }
    
    /**
     * This function is using to get PatientVitalSignStatistics by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object PatientVitalSignStatistics with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientVitalSignStatisticsByPatientEncounterID/{PatientEncounterID}")
    public List<PatientVitalSignStatistics> PatientVitalSignStatisticsByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientVitalSignStatistics objOuterPatientVitalSignStatistics = new PatientVitalSignStatistics();
        List<PatientVitalSignStatistics> objListPatientVitalSignStatistics = new  ArrayList<PatientVitalSignStatistics>();
        try
        {
            objOuterPatientVitalSignStatistics.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientVitalSignStatistics.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientVitalSignStatisticsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSignStatistics = objADBridge.RetrivePatientVitalSignStatistics(objOuterPatientVitalSignStatistics);
            logger.info("Get PatientVitalSigns successfully, PatientVitalSigns Details="+objOuterPatientVitalSignStatistics);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientVitalSigns");
        }
        return objListPatientVitalSignStatistics;
    }
    
    
    /**
     * This function is using to delete PatientVitalSigns
     * @param objOuterPatientVitalSigns
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientVitalSigns")
    public List<String> DeletePatientVitalSigns(PatientVitalSigns objOuterPatientVitalSigns) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientVitalSigns(objOuterPatientVitalSigns);
            logger.info("PatientVitalSigns deleted successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientVitalSigns, PatientVitalSigns Details=");
        	response.add("Error in deleting PatientVitalSigns :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to VitalSignMessage
     * @param objOuterVitalSignMessage
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SaveVitalSignMessage")
    public List<String> SaveVitalSignMessage(VitalSignMessage objOuterVitalSignMessage) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new VitalSignMessageAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveVitalSignMessage(objOuterVitalSignMessage);
            logger.info("VitalSignMessage saved successfully, VitalSignMessage Details="+objOuterVitalSignMessage);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving VitalSignMessage, VitalSignMessage Details=");
        	response.add("Error in saving VitalSignMessage :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get VitalSignMessage by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object VitalSignMessage with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("VitalSignMessageByPatientEncounterID/{PatientEncounterID}")
    public List<VitalSignMessage> VitalSignMessageByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	VitalSignMessage objOuterVitalSignMessage = new VitalSignMessage();
        List<VitalSignMessage> objListVitalSignMessage = new  ArrayList<VitalSignMessage>();
        try
        {
            objOuterVitalSignMessage.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterVitalSignMessage.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new VitalSignMessageAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListVitalSignMessage = objADBridge.RetriveVitalSignMessage(objOuterVitalSignMessage);
            logger.info("Get VitalSignMessage successfully, VitalSignMessage Details="+objOuterVitalSignMessage);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting VitalSignMessage");
        }
        return objListVitalSignMessage;
    }
    
    
    /**
     * This function is using to get VitalSignMessage by Patient Encounter ID 
     * @param VitalSignID
     * @return a list of object VitalSignMessage with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("VitalSignMessageByVitalSignID/{VitalSignID}")
    public List<VitalSignMessage> VitalSignMessageByVitalSignID(@PathParam ("VitalSignID") String VitalSignID) {
    	VitalSignMessage objOuterVitalSignMessage = new VitalSignMessage();
        List<VitalSignMessage> objListVitalSignMessage = new  ArrayList<VitalSignMessage>();
        try
        {
            objOuterVitalSignMessage.setVitalSignID(Long.parseLong(VitalSignID));
            objOuterVitalSignMessage.setRequestType("ByVitalSignID");
            DataAccessInterface objDAInterface = new VitalSignMessageAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListVitalSignMessage = objADBridge.RetriveVitalSignMessage(objOuterVitalSignMessage);
            logger.info("Get VitalSignMessage successfully, VitalSignMessage Details="+objOuterVitalSignMessage);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting VitalSignMessage");
        }
        return objListVitalSignMessage;
    }
    
    
    /**
     * This function is using to Electrocardiogram
     * @param objOuterElectrocardiogram
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SaveElectrocardiogram")
    public List<String> SaveElectrocardiogram(Electrocardiogram objOuterElectrocardiogram) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveElectrocardiogram(objOuterElectrocardiogram);
            logger.info("Electrocardiogram saved successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Electrocardiogram, Electrocardiogram Details=");
        	response.add("Error in saving Electrocardiogram :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Electrocardiogram
     * @param objOuterElectrocardiogram
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdateElectrocardiogram")
    public List<String> UpdateElectrocardiogram(Electrocardiogram objOuterElectrocardiogram) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdateElectrocardiogram(objOuterElectrocardiogram);
            logger.info("Electrocardiogram updated successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Electrocardiogram, Electrocardiogram Details=");
        	response.add("Error in updating Electrocardiogram :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get Electrocardiogram by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object Electrocardiogram with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("ElectrocardiogramByPatientEncounterID/{PatientEncounterID}")
    public List<Electrocardiogram> GetElectrocardiogramByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	Electrocardiogram objOuterElectrocardiogram = new Electrocardiogram();
        List<Electrocardiogram> objListElectrocardiogram = new  ArrayList<Electrocardiogram>();
        try
        {
            objOuterElectrocardiogram.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterElectrocardiogram.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListElectrocardiogram = objADBridge.RetriveElectrocardiogram(objOuterElectrocardiogram);
            logger.info("Get Electrocardiogram successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Electrocardiogram");
        }
        return objListElectrocardiogram;
    }
    
    /**
     * This function is using to get Electrocardiogram by Electrocardiogram ID 
     * @param ElectrocardiogramID
     * @return a list of object Electrocardiogram with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("ElectrocardiogramByElectrocardiogramID/{ElectrocardiogramID}")
    public List<Electrocardiogram> GetElectrocardiogramByElectrocardiogramID(@PathParam ("ElectrocardiogramID") String ElectrocardiogramID) {
    	Electrocardiogram objOuterElectrocardiogram = new Electrocardiogram();
        List<Electrocardiogram> objListElectrocardiogram = new  ArrayList<Electrocardiogram>();
        try
        {
            objOuterElectrocardiogram.setElectrocardiogramID(Long.parseLong(ElectrocardiogramID));
            objOuterElectrocardiogram.setRequestType("ByElectrocardiogramID");
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListElectrocardiogram = objADBridge.RetriveElectrocardiogram(objOuterElectrocardiogram);
            logger.info("Get Electrocardiogram successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Electrocardiogram");
        }
        return objListElectrocardiogram;
    }
    
    /**
     * This function is using to delete Electrocardiogram
     * @param objOuterElectrocardiogram
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeleteElectrocardiogram")
    public List<String> DeleteElectrocardiogram(Electrocardiogram objOuterElectrocardiogram) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeleteElectrocardiogram(objOuterElectrocardiogram);
            logger.info("Electrocardiogram deleted successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Electrocardiogram, Electrocardiogram Details=");
        	response.add("Error in deleting Electrocardiogram :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to NTproBNP
     * @param objOuterNTproBNP
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SaveNTproBNP")
    public List<String> SaveNTproBNP(NTproBNP objOuterNTproBNP) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveNTproBNP(objOuterNTproBNP);
            logger.info("NTproBNP saved successfully, NTproBNP Details="+objOuterNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving NTproBNP, NTproBNP Details=");
        	response.add("Error in saving NTproBNP :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to NTproBNP
     * @param objOuterNTproBNP
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdateNTproBNP")
    public List<String> UpdateNTproBNP(NTproBNP objOuterNTproBNP) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdateNTproBNP(objOuterNTproBNP);
            logger.info("NTproBNP updated successfully, NTproBNP Details="+objOuterNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating NTproBNP, NTproBNP Details=");
        	response.add("Error in updating NTproBNP :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get NTproBNP by Patient Encounter ID 
     * @param NTproBNPID
     * @return a list of object NTproBNP with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("NTproBNPByPatientEncounterID/{PatientEncounterID}")
    public List<NTproBNP> GetNTproBNPByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	NTproBNP objOuterNTproBNP = new NTproBNP();
        List<NTproBNP> objListNTproBNP = new  ArrayList<NTproBNP>();
        try
        {
            objOuterNTproBNP.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterNTproBNP.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListNTproBNP = objADBridge.RetriveNTproBNP(objOuterNTproBNP);
            logger.info("Get NTproBNP successfully, NTproBNP Details="+objOuterNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Electrocardiogram");
        }
        return objListNTproBNP;
    }
    
    /**
     * This function is using to get NTproBNP by NTproBNP ID 
     * @param NTproBNPID
     * @return a list of object NTproBNP with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("NTproBNPByNTproBNPID/{NTproBNPID}")
    public List<NTproBNP> GetNTproBNPByNTproBNPID(@PathParam ("NTproBNPID") String NTproBNPID) {
    	NTproBNP objOuterNTproBNP = new NTproBNP();
        List<NTproBNP> objListNTproBNP = new  ArrayList<NTproBNP>();
        try
        {
            objOuterNTproBNP.setnTproBNPID(Long.parseLong(NTproBNPID));
            objOuterNTproBNP.setRequestType("ByNTproBNPID");
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListNTproBNP = objADBridge.RetriveNTproBNP(objOuterNTproBNP);
            logger.info("Get NTproBNP successfully, NTproBNP Details="+objOuterNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Electrocardiogram");
        }
        return objListNTproBNP;
    }
    
    /**
     * This function is using to delete NTproBNP
     * @param objOuterNTproBNP
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeleteNTproBNP")
    public List<String> DeleteNTproBNP(NTproBNP objOuterNTproBNP) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeleteNTproBNP(objOuterNTproBNP);
            logger.info("NTproBNP deleted successfully, NTproBNP Details="+objOuterNTproBNP);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting NTproBNP, NTproBNP Details=");
        	response.add("Error in deleting NTproBNP :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Echocardiography
     * @param objOuterEchocardiography
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SaveEchocardiography")
    public List<String> SaveEchocardiography(Echocardiography objOuterEchocardiography) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveEchocardiography(objOuterEchocardiography);
            logger.info("Echocardiography saved successfully, Echocardiography Details="+objOuterEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Echocardiography, Echocardiography Details=");
        	response.add("Error in saving Echocardiography :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Echocardiography
     * @param objOuterEchocardiography
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdateEchocardiography")
    public List<String> UpdateEchocardiography(Echocardiography objOuterEchocardiography) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdateEchocardiography(objOuterEchocardiography);
            logger.info("Echocardiography updated successfully, Echocardiography Details="+objOuterEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Echocardiography, Echocardiography Details=");
        	response.add("Error in updating Echocardiography :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get Echocardiography by Patient Encounter ID 
     * @param EchocardiographyID
     * @return a list of object Echocardiography with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("EchocardiographyByPatientEncounterID/{PatientEncounterID}")
    public List<Echocardiography> GetEchocardiographyByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	Echocardiography objOuterEchocardiography = new Echocardiography();
        List<Echocardiography> objListEchocardiography = new  ArrayList<Echocardiography>();
        try
        {
            objOuterEchocardiography.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterEchocardiography.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListEchocardiography = objADBridge.RetriveEchocardiography(objOuterEchocardiography);
            logger.info("Get Echocardiography successfully, Echocardiography Details="+objOuterEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Echocardiography");
        }
        return objListEchocardiography;
    }
    
    /**
     * This function is using to get Echocardiography by Echocardiography ID 
     * @param EchocardiographyID
     * @return a list of object Echocardiography with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("EchocardiographyByEchocardiographyID/{EchocardiographyID}")
    public List<Echocardiography> GetEchocardiographyByEchocardiographyID(@PathParam ("EchocardiographyID") String EchocardiographyID) {
    	Echocardiography objOuterEchocardiography = new Echocardiography();
        List<Echocardiography> objListEchocardiography = new  ArrayList<Echocardiography>();
        try
        {
            objOuterEchocardiography.setEchocardiographyID(Long.parseLong(EchocardiographyID));
            objOuterEchocardiography.setRequestType("ByEchocardiographyID");
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListEchocardiography = objADBridge.RetriveEchocardiography(objOuterEchocardiography);
            logger.info("Get Echocardiography successfully, Echocardiography Details="+objOuterEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Echocardiography");
        }
        return objListEchocardiography;
    }
    
    /**
     * This function is using to delete Echocardiography
     * @param objOuterEchocardiography
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeleteEchocardiography")
    public List<String> DeleteEchocardiography(Echocardiography objOuterEchocardiography) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeleteEchocardiography(objOuterEchocardiography);
            logger.info("Echocardiography deleted successfully, Echocardiography Details="+objOuterEchocardiography);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Echocardiography, Echocardiography Details=");
        	response.add("Error in deleting Echocardiography :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientDiagnosis
     * @param objOuterPatientDiagnosis
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientDiagnosis")
    public List<String> SavePatientDiagnosis(PatientDiagnosis objOuterPatientDiagnosis) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientDiagnosis(objOuterPatientDiagnosis);
            logger.info("PatientDiagnosis saved successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientDiagnosis, PatientDiagnosis Details=");
        	response.add("Error in saving PatientDiagnosis :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientDiagnosis
     * @param objOuterPatientDiagnosis
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientDiagnosis")
    public List<String> UpdatePatientDiagnosis(PatientDiagnosis objOuterPatientDiagnosis) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientDiagnosis(objOuterPatientDiagnosis);
            logger.info("PatientDiagnosis updated successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientDiagnosis, PatientDiagnosis Details=");
        	response.add("Error in updating PatientDiagnosis :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientDiagnosis by Patient Encounter ID 
     * @param PatientDiagnosisID
     * @return a list of object PatientDiagnosis with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientDiagnosisByPatientEncounterID/{PatientEncounterID}")
    public List<PatientDiagnosis> GetPatientDiagnosisByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientDiagnosis objOuterPatientDiagnosis = new PatientDiagnosis();
        List<PatientDiagnosis> objListPatientDiagnosis = new  ArrayList<PatientDiagnosis>();
        try
        {
            objOuterPatientDiagnosis.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientDiagnosis.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientDiagnosis = objADBridge.RetrivePatientDiagnosis(objOuterPatientDiagnosis);
            logger.info("Get PatientDiagnosis successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientDiagnosis");
        }
        return objListPatientDiagnosis;
    }
    
    /**
     * This function is using to get PatientDiagnosis by PatientDiagnosis ID 
     * @param PatientDiagnosisID
     * @return a list of object PatientDiagnosis with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientDiagnosisByPatientDiagnosisID/{PatientDiagnosisID}")
    public List<PatientDiagnosis> GetPatientDiagnosisByPatientDiagnosisID(@PathParam ("PatientDiagnosisID") String PatientDiagnosisID) {
    	PatientDiagnosis objOuterPatientDiagnosis = new PatientDiagnosis();
        List<PatientDiagnosis> objListPatientDiagnosis = new  ArrayList<PatientDiagnosis>();
        try
        {
            objOuterPatientDiagnosis.setPatientDiagnosisID(Long.parseLong(PatientDiagnosisID));
            objOuterPatientDiagnosis.setRequestType("ByPatientDiagnosisID");
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientDiagnosis = objADBridge.RetrivePatientDiagnosis(objOuterPatientDiagnosis);
            logger.info("Get PatientDiagnosis successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientDiagnosis");
        }
        return objListPatientDiagnosis;
    }
    
    /**
     * This function is using to delete PatientDiagnosis
     * @param objOuterPatientDiagnosis
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientDiagnosis")
    public List<String> DeletePatientDiagnosis(PatientDiagnosis objOuterPatientDiagnosis) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientDiagnosis(objOuterPatientDiagnosis);
            logger.info("PatientDiagnosis deleted successfully, PatientDiagnosis Details="+objOuterPatientDiagnosis);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientDiagnosis, PatientDiagnosis Details=");
        	response.add("Error in deleting PatientDiagnosis :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Retrieves Patients all details in a single packet by Patient ID and Encounter ID 
     * @param objOuterPatientDetailPacket
     * @return a list of object string with "Error", "No Error"
     */
    
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("GetPatientAllDetails")
    public PatientDetailPacket GetPatientAllDetails(PatientDetailPacket objOuterPatientDetailPacket) {
        
    	//PatientDetailPacket objInnerPatientDetailPacket = new PatientDetailPacket();
    	 
        try
        {
        	List<Patient> objListPatient = new ArrayList<Patient>();
        	Patient objOuterPatient = new Patient();
            objOuterPatient.setRequestType("PatientData");
            objOuterPatient.setPatientID(objOuterPatientDetailPacket.getPatientID());
        
            DataAccessInterface objDAInterface = new PatientAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatient = objADBridge.RetrivePatient(objOuterPatient);
            objOuterPatientDetailPacket.setObjListPatient(objListPatient);
            logger.info("Patient loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading Patient, Patient Details=");
        }
        
        try
        {
        	List<PatientEncounter> objListPatientEncounter = new ArrayList<PatientEncounter>();
        	PatientEncounter objOuterPatientEncounter = new PatientEncounter();
            objOuterPatientEncounter.setRequestType("ByPatientEncounterID");
            objOuterPatientEncounter.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new PatientEncounterAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientEncounter = objADBridge.RetrivePatientEncounter(objOuterPatientEncounter);
            objOuterPatientDetailPacket.setObjListPatientEncounter(objListPatientEncounter);
            logger.info("Patient Encounter loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading Patient Encounter, Patient Encounter Details=");
        }
        
        try
        {
        	List<PatientSymptoms> objListPatientSymptoms = new ArrayList<PatientSymptoms>();
        	PatientSymptoms objOuterPatientSymptoms = new PatientSymptoms();
            objOuterPatientSymptoms.setRequestType("ByPatientEncounterID");
            objOuterPatientSymptoms.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new PatientSymptomsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientSymptoms = objADBridge.RetrivePatientSymptoms(objOuterPatientSymptoms);
            objOuterPatientDetailPacket.setObjListPatientSymptoms(objListPatientSymptoms);
            logger.info("Patient Symptoms loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading Patient Symptoms, Patient Symptoms Details=");
        }
        
        try
        {
        	List<PatientClinicalHistory> objListPatientClinicalHistory = new ArrayList<PatientClinicalHistory>();
        	PatientClinicalHistory objOuterPatientClinicalHistory = new PatientClinicalHistory();
            objOuterPatientClinicalHistory.setRequestType("ByPatientEncounterID");
            objOuterPatientClinicalHistory.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new PatientClinicalHistoryAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientClinicalHistory = objADBridge.RetrivePatientClinicalHistory(objOuterPatientClinicalHistory);
            objOuterPatientDetailPacket.setObjListPatientClinicalHistory(objListPatientClinicalHistory);
            logger.info("PatientClinicalHistory loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading PatientClinicalHistory, PatientClinicalHistory Details=");
        }
        
        try
        {
        	List<PatientVitalSigns> objListPatientVitalSigns = new ArrayList<PatientVitalSigns>();
        	PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
            objOuterPatientVitalSigns.setRequestType("ByPatientEncounterID");
            objOuterPatientVitalSigns.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new PatientVitalSignsAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientVitalSigns = objADBridge.RetrivePatientVitalSigns(objOuterPatientVitalSigns);
            objOuterPatientDetailPacket.setObjListPatientVitalSigns(objListPatientVitalSigns);
            logger.info("PatientVitalSigns loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading PatientVitalSigns, PatientVitalSigns Details=");
        }
        
        try
        {
        	List<Electrocardiogram> objListElectrocardiogram = new ArrayList<Electrocardiogram>();
        	Electrocardiogram objOuterElectrocardiogram = new Electrocardiogram();
            objOuterElectrocardiogram.setRequestType("ByPatientEncounterID");
            objOuterElectrocardiogram.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new ElectrocardiogramAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListElectrocardiogram = objADBridge.RetriveElectrocardiogram(objOuterElectrocardiogram);
            objOuterPatientDetailPacket.setObjListElectrocardiogram(objListElectrocardiogram);
            logger.info("Electrocardiogram loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading Electrocardiogram, Electrocardiogram Details=");
        }
        
        try
        {
        	List<NTproBNP> objListNTproBNP = new ArrayList<NTproBNP>();
        	NTproBNP objOuterNTproBNP = new NTproBNP();
            objOuterNTproBNP.setRequestType("ByPatientEncounterID");
            objOuterNTproBNP.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new NTproBNPAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListNTproBNP = objADBridge.RetriveNTproBNP(objOuterNTproBNP);
            objOuterPatientDetailPacket.setObjListNTproBNP(objListNTproBNP);
            logger.info("NTproBNP loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading NTproBNP, NTproBNP Details=");
        }
        
        try
        {
        	List<Echocardiography> objListEchocardiography = new ArrayList<Echocardiography>();
        	Echocardiography objOuterEchocardiography = new Echocardiography();
            objOuterEchocardiography.setRequestType("ByPatientEncounterID");
            objOuterEchocardiography.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new EchocardiographyAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListEchocardiography = objADBridge.RetriveEchocardiography(objOuterEchocardiography);
            objOuterPatientDetailPacket.setObjListEchocardiography(objListEchocardiography);
            logger.info("Echocardiography loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading Echocardiography, Echocardiography Details=");
        }
        
        try
        {
        	List<PatientDiagnosis> objListPatientDiagnosis = new ArrayList<PatientDiagnosis>();
        	PatientDiagnosis objOuterPatientDiagnosis = new PatientDiagnosis();
            objOuterPatientDiagnosis.setRequestType("ByPatientEncounterID");
            objOuterPatientDiagnosis.setPatientEncounterID(objOuterPatientDetailPacket.getPatientEncounterID());
        
            DataAccessInterface objDAInterface = new PatientDiagnosisAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientDiagnosis = objADBridge.RetrivePatientDiagnosis(objOuterPatientDiagnosis);
            objOuterPatientDetailPacket.setObjListPatientDiagnosis(objListPatientDiagnosis);
            logger.info("PatientDiagnosis loaded successfully");
        }
        catch(Exception ex)
        {
        	logger.info("Error in loading PatientDiagnosis, PatientDiagnosis Details=");
        }
        
        return objOuterPatientDetailPacket;

    }
    
    /**
     * This function is using to Users
     * @param objOuterUsers
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SaveUsers")
    public List<String> SaveUsers(Users objOuterUsers) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveUsers(objOuterUsers);
            logger.info("Users saved successfully, Users Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving Users, Users Details=");
        	response.add("Error in saving Users :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to Users
     * @param objOuterUsers
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdateUsers")
    public List<String> UpdateUsers(Users objOuterUsers) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdateUsers(objOuterUsers);
            logger.info("Users updated successfully, Users Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating Users, Users Details=");
        	response.add("Error in updating Users :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get Users List
     * @param 
     * @return a list of object Users with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UsersList")
    public List<Users> GetUsersList() {
    	Users objOuterUsers = new Users();
        List<Users> objListUsers = new  ArrayList<Users>();
        try
        {
            objOuterUsers.setRequestType("AllUsersList");
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListUsers = objADBridge.RetriveUsers(objOuterUsers);
            logger.info("Get Users successfully, Users Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Users");
        }
        return objListUsers;
    }
    
    
    /**
     * This function is using to get Users by user id
     * @param UserID
     * @return a list of object Users with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UsersByUserID/{UserID}")
    public List<Users> UsersByUserID(@PathParam ("UserID") String UserID) {
    	Users objOuterUsers = new Users();
        List<Users> objListUsers = new  ArrayList<Users>();
        try
        {
            objOuterUsers.setUserID(Long.parseLong(UserID));
            objOuterUsers.setRequestType("ByUserID");
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListUsers = objADBridge.RetriveUsers(objOuterUsers);
            logger.info("Get Users successfully, Users Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting Users");
        }
        return objListUsers;
    }
    
    /**
     * Authenticate user by user login and password
     * @param objOuterUsers
     * @return an instance of list of Users
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("ValidateUser")
    public List<Users> ValidateUser(Users objOuterUsers) {
        
        List<Users> objListUsers = new  ArrayList<Users>();
        try
        {
            objOuterUsers.setRequestType("AuthenticateUser");
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListUsers = objADBridge.RetriveUsers(objOuterUsers);
            logger.info("User authenticated successfully, User Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objListUsers;
    }
    
    /**
     * This function is using to delete Users
     * @param objOuterUsers
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeleteUsers")
    public List<String> DeleteUsers(Users objOuterUsers) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new UsersAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeleteUsers(objOuterUsers);
            logger.info("Users deleted successfully, Users Details="+objOuterUsers);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting Users, Users Details=");
        	response.add("Error in deleting Users :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientPhysicalExam
     * @param objOuterPatientPhysicalExam
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SavePatientPhysicalExam")
    public List<String> SavePatientPhysicalExam(PatientPhysicalExam objOuterPatientPhysicalExam) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientPhysicalExamAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SavePatientPhysicalExam(objOuterPatientPhysicalExam);
            logger.info("PatientPhysicalExam saved successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving PatientPhysicalExam, PatientPhysicalExam Details=");
        	response.add("Error in saving PatientPhysicalExam :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to PatientPhysicalExam
     * @param objOuterPatientPhysicalExam
     * @return a list of object string with "Error", "No Error" and Updated by ID
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("UpdatePatientPhysicalExam")
    public List<String> UpdatePatientPhysicalExam(PatientPhysicalExam objOuterPatientPhysicalExam) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientPhysicalExamAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.UpdatePatientPhysicalExam(objOuterPatientPhysicalExam);
            logger.info("PatientPhysicalExam updated successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in updating PatientPhysicalExam, PatientPhysicalExam Details=");
        	response.add("Error in updating PatientPhysicalExam :" + ex.getMessage());
        }
        return response;
    }
    
    /**
     * This function is using to get PatientPhysicalExam by Patient Encounter ID 
     * @param PatientEncounterID
     * @return a list of object PatientPhysicalExam with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientPhysicalExamByPatientEncounterID/{PatientEncounterID}")
    public List<PatientPhysicalExam> GetPatientPhysicalExamByPatientEncounterID(@PathParam ("PatientEncounterID") String PatientEncounterID) {
    	PatientPhysicalExam objOuterPatientPhysicalExam = new PatientPhysicalExam();
        List<PatientPhysicalExam> objListPatientPhysicalExam = new  ArrayList<PatientPhysicalExam>();
        try
        {
            objOuterPatientPhysicalExam.setPatientEncounterID(Long.parseLong(PatientEncounterID));
            objOuterPatientPhysicalExam.setRequestType("ByPatientEncounterID");
            DataAccessInterface objDAInterface = new PatientPhysicalExamAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientPhysicalExam = objADBridge.RetrivePatientPhysicalExam(objOuterPatientPhysicalExam);
            logger.info("Get PatientPhysicalExam successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientPhysicalExam");
        }
        return objListPatientPhysicalExam;
    }
    
    /**
     * This function is using to get PatientPhysicalExam by Patient Physical Exam ID 
     * @param PatientPhysicalExamID
     * @return a list of object PatientPhysicalExam with "Error", "No Error"
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("PatientPhysicalExamByPatientPhysicalExamID/{PatientPhysicalExamID}")
    public List<PatientPhysicalExam> GetPatientPhysicalExamByPatientPhysicalExamID(@PathParam ("PatientPhysicalExamID") String PatientPhysicalExamID) {
    	PatientPhysicalExam objOuterPatientPhysicalExam = new PatientPhysicalExam();
        List<PatientPhysicalExam> objListPatientPhysicalExam = new  ArrayList<PatientPhysicalExam>();
        try
        {
            objOuterPatientPhysicalExam.setPatientPhysicalExamID(Long.parseLong(PatientPhysicalExamID));
            objOuterPatientPhysicalExam.setRequestType("ByPatientPhysicalExamID");
            DataAccessInterface objDAInterface = new PatientPhysicalExamAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListPatientPhysicalExam = objADBridge.RetrivePatientPhysicalExam(objOuterPatientPhysicalExam);
            logger.info("Get PatientPhysicalExam successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting PatientPhysicalExam");
        }
        return objListPatientPhysicalExam;
    }
    
    /**
     * This function is using to delete PatientPhysicalExam
     * @param objOuterPatientPhysicalExam
     * @return a list of object string with "Error", "No Error"
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("DeletePatientPhysicalExam")
    public List<String> DeletePatientPhysicalExam(PatientPhysicalExam objOuterPatientPhysicalExam) {
        
        List<String> response = new ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new PatientPhysicalExamAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.DeletePatientPhysicalExam(objOuterPatientPhysicalExam);
            logger.info("PatientPhysicalExam deleted successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
        }
        catch(Exception ex)
        {
        	logger.info("Error in deleting PatientPhysicalExam, PatientPhysicalExam Details=");
        	response.add("Error in deleting PatientPhysicalExam :" + ex.getMessage());
        }
        return response;
    }
    
}
