package org.uclab.IMP.rs.rdr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.IMP.datamodel.AbstractDataBridge;
import org.uclab.IMP.datamodel.DataAccessInterface;
import org.uclab.IMP.datamodel.DatabaseStorage;
import org.uclab.IMP.datamodel.RDR.LogMatcher;
import org.uclab.IMP.datamodel.RDR.Node;
import org.uclab.IMP.datamodel.RDR.RuleType;
import org.uclab.IMP.datamodel.RDR.Rules;
import org.uclab.IMP.datamodel.RDR.dataadapter.RuleDataAdapter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
//import org.apache.tomcat.util.digester.Rule;
//import org.uclab.IMP.datamodel.RDR.RDRTree;
//


/**
 * Facade for the Restful Web service to handle the data curation functions 
 *
 * @author Atif
 */
@Path("rdrInferencing")
public class RDRresource{

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DataCurationResource
     */
    public RDRresource() {
    }
    
    private static final Logger logger = LoggerFactory.getLogger(RDRresource.class);
    
    /**
     * This function is using to save rule
     * @param objOuterRule
     * @return a list of object string with "Error", "No Error" and new added ID
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     */


    @POST
    @Produces("application/json") 
    @Consumes("application/json")
    @Path("SaveRule")
    public List<String> SaveRule(Object objOuterRule) throws JsonParseException, JsonMappingException, IOException {

        HashMap<String, String> map = new HashMap<>();
        map = (HashMap) objOuterRule;
        
        Node nodeRule = new Node();
        nodeRule.setTitle(map.get("Title"));
        nodeRule.setInstitution(map.get("Institution"));
        nodeRule.setDescription(map.get("Description"));
        nodeRule.setCreator(Integer.parseInt(map.get("Creator")));
        nodeRule.setSpecialistName(map.get("Specialist"));
        nodeRule.setParentID(Integer.parseInt(map.get("ParentID")));
        nodeRule.setCornerstoneCase(map.get("CornerstoneCase"));
        nodeRule.setRule(map.get("RuleCondition"));
        nodeRule.setTypeID(Integer.parseInt(map.get("RuleTypeID")));
        nodeRule.setConclusion(map.get("Conclusion"));

        System.out.println("ID:"+ nodeRule.getTypeID());
        List<String> response = new ArrayList<String>();
        try
        { 
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            response = objADBridge.SaveRule(nodeRule);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving rule, rule Details=");
        	response.add("Error in saving rule");
        }
        return response;
    }    
    
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("ClassifyCase")
    public Rules Classify(Object objOuterRule) throws JsonParseException, JsonMappingException, IOException {
    	System.out.println(objOuterRule);
        HashMap<String, String> map = new HashMap<>();
        map = (HashMap) objOuterRule;
        Rules foundRule = new Rules();
        try
        {
	        DataAccessInterface objDAInterface = new RuleDataAdapter();
	        AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
	        foundRule = objADBridge.Classify(map);
	        logger.info("Classification done!");
        }
        catch(Exception ex)
        {
        	logger.info("Error in classification");
        	System.out.println("Couldnt Classify Properly!");
        }
        return foundRule;
} 
    
    
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("SearchLog")
    public List<LogMatcher> SearchLog(Object objOuterCase) throws JsonParseException, JsonMappingException, IOException {
    	System.out.println(objOuterCase);
        HashMap<String, String> map = new HashMap<>();
        List<LogMatcher> topMatchings= new ArrayList<>();
        map = (HashMap) objOuterCase;
        Rules foundRule = new Rules();
        try
        {
	        DataAccessInterface objDAInterface = new RuleDataAdapter();
	        AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
	        topMatchings = objADBridge.SearchLog(map);
        }
        catch(Exception ex)
        {
        	System.out.println("Couldnt search Properly!");
        }
        return topMatchings;
} 
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("RetriveRulesbyParentID/{parentID}")
    public List<Rules> RetriveRulesbyParentID(@PathParam ("parentID") String parentID) {
        List<Rules> objListRules = new  ArrayList<Rules>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListRules = objADBridge.RetriveRulesbyParentID(Integer.parseInt(parentID));
            logger.info("Get rule successfully, rules Details="+objListRules);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objListRules;
    }
    
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("getConceptsbyType/{TypeID}")
    public List<String> getConceptsbyType(@PathParam ("TypeID") String TypeID) {
        List<String> objListRules = new  ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListRules = objADBridge.getConceptsbyType(Integer.parseInt(TypeID));
            logger.info("Get rule successfully, rules Details="+objListRules);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objListRules;
    }
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("getConceptsList")
    public List<String> getConceptsList() {
        List<String> objListRules = new  ArrayList<String>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListRules = objADBridge.getConceptsList();
            logger.info("Get rule successfully, rules Details="+objListRules);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objListRules;
    }
    

    
    
    
    /**
     * This function is using to get rule by ID 
     * @param UserID
     * @return a Rule object with "Error", "No Error" 
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("RetriveRulebyID/{RuleID}")
    public List<Rules> RetriveRulebyID(@PathParam ("RuleID") String RuleID) {

       	List<Rules> objRule = new ArrayList<Rules>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objRule = objADBridge.RetriveRulebyID(Integer.parseInt(RuleID));
            logger.info("Get rule successfully, rules Details="+objRule);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objRule;
    }
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("RetriveRuleTypes")
    public List<RuleType> RetriveRuleTypes() {

       	List<RuleType> objRule = new ArrayList<RuleType>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objRule = objADBridge.RetriveRuleTypes();
            logger.info("Get rule successfully, rules Details="+objRule);
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting user");
        }
        return objRule;
    }
    
    /**
     * This function is using to get user by ID 
     * @param UserID
     * @return a list of object Users with "Error", "No Error" and new added ID
     */
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("RetriveRules")
    public List<Rules> RetriveRules() {
        Rules objOuterRules = new Rules();
        List<Rules> objListRules = new  ArrayList<Rules>();
        try
        {
            DataAccessInterface objDAInterface = new RuleDataAdapter();
            AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
            objListRules = objADBridge.RetriveRules();
            logger.info("Get all rules successfully, rules Details="+objOuterRules);
            System.out.println("Here la la l ala ");
        }
        catch(Exception ex)
        {
        	logger.info("Error in getting rules");
        }
        return objListRules;
    	
    }    
    
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("test")
    public String Test() {
    	return "Hello World";
    }
    
    
}

