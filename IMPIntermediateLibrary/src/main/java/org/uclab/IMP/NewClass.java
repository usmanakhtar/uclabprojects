/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uclab.IMP;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author Taqdir Ali
 */
public class NewClass {
    public static void main(String[] args){
    	System.setProperty("java.net.preferIPv6Addresses", "true");  
	    try {
	    	DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
	    	String dbURL = "jdbc:sqlserver://163.180.116.118:1433;instance=SQLEXPRESS;databaseName=KnowledgeEvolutionDB;user=sa;password=adminsa";
	    	Connection conn = DriverManager.getConnection(dbURL);
	    	if (conn != null) {
	    	    System.out.println("Connected");
	    	    
	    	    DatabaseMetaData md = conn.getMetaData();
	    	    ResultSet rs = md.getTables(null, null, "%", null);
	    	    while (rs.next()) {
	    	      System.out.println(rs.getString(3));
	    	    }
	    	}
	    	conn.close();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
    }
}
