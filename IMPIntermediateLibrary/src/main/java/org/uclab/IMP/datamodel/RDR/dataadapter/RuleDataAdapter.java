/*
 Copyright [2016] [Taqdir Ali]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

package org.uclab.IMP.datamodel.RDR.dataadapter;

//import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptException;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.IMP.datamodel.DataAccessInterface;
import org.uclab.IMP.datamodel.RDR.Conditions;
import org.uclab.IMP.datamodel.RDR.Node;
import org.uclab.IMP.datamodel.RDR.RuleType;
import org.uclab.IMP.datamodel.RDR.LogMatcher;
//import org.uclab.IMP.datamodel.RDR.Case;
//import org.uclab.IMP.datamodel.RDR.Classification;
//import org.uclab.IMP.datamodel.RDR.Node;
//import org.uclab.IMP.datamodel.RDR.RDRTree;
import org.uclab.IMP.datamodel.RDR.Rules;
import org.uclab.IMP.datamodel.RDR.Tree;

/**
 * This is UserDataAdapter class which implements the Data Access Interface for CRUD operations
 * @author Atif
 */
public class RuleDataAdapter implements DataAccessInterface{

    private Connection objConn;
    
    private static final Logger logger = LoggerFactory.getLogger(RuleDataAdapter.class);

    
    public RuleDataAdapter()
    {
//        try
//        {
//           objConn = (Connection)objConf;
//           System.out.println("DB Connected | ConfigureAdapter");
//           logger.info("Database connected successfully");
//        }
//        catch(Exception ex)
//        {
//       	 logger.info("Error in connection to Database");
//        }
    }

    public boolean addToMatcher(List<LogMatcher> history, LogMatcher current)
    {
    	for(int i =0; i<history.size(); i++)
    	{
    		Set<String> keysOnly= new HashSet<String>();
    		keysOnly= history.get(i).getCase().keySet();
    		
    		Set<String> keysOnlyCurrent= new HashSet<String>();
    		keysOnlyCurrent = current.getCase().keySet();
    		
    		if(keysOnly.equals(keysOnlyCurrent))
    		{
    			history.get(i).IncrementFreq();
    			return true;
    		}
    	}
    	history.add(current);
    	return true;
    }
    
    public boolean calculateSimilarity(List<LogMatcher> history, HashMap<String, String> current)
    {
    	Set<String> keysOnlyCurrent= new HashSet<String>();
		keysOnlyCurrent = current.keySet();
//		System.out.println("Current: "+ keysOnlyCurrent.toString());
		
    	for(int i =0; i<history.size(); i++)
    	{
    		Set<String> keysOnly= new HashSet<String>();
    		keysOnly= history.get(i).getCase().keySet();	
//    		System.out.println("Loop: "+ keysOnly.toString());
    		float Score=0;
    		if(keysOnly.size()>=keysOnlyCurrent.size())
    		{
    			Set<String> intersection = new HashSet<String>(keysOnly); 
    	        intersection.retainAll(keysOnlyCurrent);
//    	        System.out.println("Intersection: "+ intersection.toString());
    	        
    	        if(intersection.size()==keysOnly.size())
    	        {
    	        	history.get(i).setScore(0);
    	        }
    	        else
    	        {
    	        Score= (float)intersection.size()/keysOnly.size();
    	        history.get(i).setScore(Score);
    	        }

    		}
    		else
    		{
    			history.get(i).setScore(0);
    		}
    	}
    	return true;
    }
    
    @Override
    public List<LogMatcher> SearchLog(HashMap<String, String> Case)
    {
    	int Top= 2;
    	List<LogMatcher> newList = new ArrayList<>();
    	List<LogMatcher> previousCases = new ArrayList<>();
//    	List<HashMap<String, String>> previousCases = new ArrayList<>();
    	try 
    	{
//    		System.out.println("step 1: ");
    		CallableStatement objCallableStatement = null;
    		objCallableStatement = objConn.prepareCall("{call usp_Get_Log()}");
       		ResultSet objResultSet = objCallableStatement.executeQuery();
       		while(objResultSet.next())
			{
       			LogMatcher temp = new LogMatcher();
				int LogID = objResultSet.getInt("LogID");
                String MatchedRules= objResultSet.getString("MatchedRules");
                String InputCase = objResultSet.getString("InputCase");
                
                HashMap<String, String> Case1= new HashMap<>();
                
                InputCase= InputCase.replace("{", "");
                InputCase= InputCase.replace("}", "");
                InputCase= InputCase.replace(" ", "");
                String [] Cases= InputCase.split(",");
                for(int i=0;i<Cases.length;i++)
                {
                	String [] OneCase = Cases[i].split("=");
                	Case1.put(OneCase[0], OneCase[1]);
                	
                }

                temp.setCase(Case1);
                temp.setID(LogID);
                temp.setMatchedRules(MatchedRules);
                
                this.addToMatcher(previousCases, temp);
			}
       		
       	 calculateSimilarity(previousCases, Case);
       	 
//       	System.out.println("STARTTT"+ previousCases.size());
//    	 for(int i=0;i<previousCases.size();i++)
//         {
////    		 System.out.println("Scire cgecking n  ");
//    		 previousCases.get(i).displayOne();
//         	
//         }
       	float toDelete=1;
       	
       	
       	for(int x=0;x<previousCases.size();x++)
        {	
        	if( previousCases.get(x).getScore()==0.0)
        	{
        		previousCases.remove(x);
        		x--;
        	}
         }
//     System.out.println("+=++=++=++++=++=++=++++=++=++=+++");
//   	 for(int i=0;i<previousCases.size();i++)
//     {
//   		previousCases.get(i).displayOne();
//     	
//     }
//   	System.out.println("+=++=++=++++=++=++=++++=++=++=+++");
       	 
       	 
       	 if(Top<= previousCases.size())
       	 {
	       	 for (int index =0; index < Top; index++) 
	       	 {
	       		float maxScore= -234567;
	          	LogMatcher tempRow= new LogMatcher();
	          	int MAX=0;
	       		for(int i=0;i<previousCases.size();i++)
	            {
	            	if(previousCases.get(i).getScore()>maxScore)
	            	{
	            		tempRow= previousCases.get(i);
	            		maxScore= previousCases.get(i).getScore();
	            		MAX=i;
	            	}
	            } 
	       		previousCases.remove(MAX);
	       		newList.add(tempRow);	
	       	 }
       	 }
       	 else
       	 {
       		newList.clear();
       		for(int i=0;i<previousCases.size();i++)
            {
            	newList.add(previousCases.get(i));
            } 	
       	 }
       	 for(int i=0;i<newList.size();i++)
         {
       		newList.get(i).displayOne();
         	
         }
    	}
    	catch (Exception e)
    	{
    		System.out.println("Error "+e);
    	}
		return newList;
    	
    }
    
    /**
     * This is implementation function for saving Rule.
     * @param objRule
     * @return List of String 
     */
    @Override
    public List<String> SaveRule(Node objRule) {
        
    	Node objInnerNode= new Node();
    	objInnerNode= objRule;
    	List<String> ruleID= new ArrayList<>();
    	
    	
    	try {
    		System.out.println("step 1: ");
    		CallableStatement objCallableStatement = null;
    		objCallableStatement = objConn.prepareCall("{call usp_Add_CreatedRule(?,?,?,?,?,?,?,?,?,?)}");
    		objCallableStatement.setString("RuleTitle", objInnerNode.getTitle());
    		System.out.println("step 2: ");
    		objCallableStatement.setString("Institution", objInnerNode.getInstitution());
    		objCallableStatement.setString("RuleDescription", objInnerNode.getDescription());
    		objCallableStatement.setString("RuleCondition", "NULL");
    		objCallableStatement.setString("RuleConclusion", objInnerNode.getConclusion());
    		System.out.println("step 3: ");
    		objCallableStatement.setInt("RuleCreatedBy", objInnerNode.getCreator());
    		objCallableStatement.setString("SpecialistName", objInnerNode.getSpecialistName());
    		objCallableStatement.setInt("ParentID", objInnerNode.getParentID());
    		System.out.println("step 4: ");
    		objCallableStatement.setString("CornerstoneCase", objInnerNode.getCornerstoneCase());
    		objCallableStatement.setInt("TypeID", objInnerNode.getTypeID());
    		System.out.println("step 5: "+ objInnerNode.getTypeID());
    		
       		objCallableStatement.executeQuery();
    		System.out.println("step 6: ");

       		int num=0;
			CallableStatement objCallableStatement1 = null;
			objCallableStatement1 = objConn.prepareCall("{call dbo.usp_Get_LastID}");
			ResultSet objResultSet = objCallableStatement1.executeQuery();
    		System.out.println("step 7: ");

			while (objResultSet.next()) {
                num = objResultSet.getInt("RuleID");
			}
			
			System.out.println("step 8: ");
			ruleID.add(String.valueOf(num)); 
            
			List<String> ConditionIDs= new ArrayList<>();
            String[] conditions = objInnerNode.getRule().split("&&");
            System.out.println("Len of Cond: "+conditions.length);
            String[] broken= null;
            String key=null, value= null, operator= null;
            System.out.println("step 9: ");
            for(int j=0;j<conditions.length;j++)
            {	broken= null;
            	conditions[j]= conditions[j].replace(" ", "");
            	
            	if(conditions[j].contains(">") && !conditions[j].contains(">="))
            	{
            		broken = conditions[j].split(">");
            		System.out.println("Len of broken: "+broken.length);
            		key=broken[0];
            		operator= ">";
            		value= broken[1];
            	}
            	else if(conditions[j].contains("<") && !conditions[j].contains("<="))
            	{
            		broken = conditions[j].split("<");
            		key=broken[0];
            		operator= "<";
            		value= broken[1];
            	}
            	else if(conditions[j].contains(">="))
            	{
            		broken = conditions[j].split(">=");
            		key=broken[0];
            		operator= ">=";
            		value= broken[1];
            	}
            	else if(conditions[j].contains("<="))
            	{
            		broken = conditions[j].split("<=");
            		key=broken[0];
            		operator= "<=";
            		value= broken[1];
            	}
            	else if(conditions[j].contains("=="))
            	{
            		broken = conditions[j].split("==");
            		key=broken[0];
            		operator= "==";
            		value= broken[1];
            	}
            	
            	CallableStatement objCallableStatement3 = null;
        		objCallableStatement3 = objConn.prepareCall("{call usp_Add_Condition(?,?,?,?)}");
        		objCallableStatement3.setString("ConditionValueOperator", operator);
        		System.out.println("step 21: ");
        		objCallableStatement3.setString("ConditionKey", key);
        		objCallableStatement3.setString("ConditionType", "1");
        		System.out.println("step 22: ");
        		objCallableStatement3.setString("ConditionValue", value);
        		
        		System.out.println("step 23: ");
                objCallableStatement3.execute();
                
                int num1=0;
    			CallableStatement objCallableStatement4 = null;
    			objCallableStatement4 = objConn.prepareCall("{call dbo.usp_Get_LastCID}");
    			ResultSet objResultSet1 = objCallableStatement4.executeQuery();
    			
    			while (objResultSet1.next()) {
                    num1 = objResultSet1.getInt("cID");
                    ConditionIDs.add(String.valueOf(num1));
    			}
            }
            
            
            CallableStatement objCallableStatement5 = null;
    		objCallableStatement5 = objConn.prepareCall("{call usp_Add_Conclusion(?,?,?,?)}");
    		objCallableStatement5.setString("RuleID", String.valueOf(num));
    		System.out.println("step 31: ");
    		objCallableStatement5.setString("ConclusionKey", "Recommendation");
    		objCallableStatement5.setString("ConclusionValue", objInnerNode.getConclusion());
    		System.out.println("step 32: ");
    		objCallableStatement5.setString("ConclusionOperator", "==");
    		
    		System.out.println("step 33: ");
            objCallableStatement5.execute();
            
           
       		for(int l=0; l<ConditionIDs.size();l++)
       		{
       			
       			CallableStatement objCallableStatement6 = null;
        		objCallableStatement6 = objConn.prepareCall("{call usp_Add_ConditionRuleMap(?,?)}");
        		objCallableStatement6.setString("RuleID", String.valueOf(num));
        		System.out.println("step 41: ");
        		objCallableStatement6.setString("ConditionID", ConditionIDs.get(l));
        		System.out.println("step 42: ");
                objCallableStatement6.execute();
       			
       			System.out.println("CIDs"+ ConditionIDs.get(l));
       		}
            System.out.println("RuleID"+ num);
            
            
            
            
    		
    	}
    	catch (Exception e)
        {
        	logger.info("Error in saving rule, Rule Details=");
        	System.out.println("Error in adding to TBL RULES1"+ e);
        }   
        
        return ruleID;
    }

    /**
     * This is implementation function for updating Rule.
     * @param objRule
     * @return List of string
     *  
     */
    @Override
    public List<String> Update(Object objRule) {
        
        Rules objInnerRule = new Rules();
        objInnerRule =  (Rules) objRule;
        List<String> objDbResponse = new ArrayList<>();
        
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call usp_Add_CreatedRule(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            
            objCallableStatement.setString("RuleTitle", objInnerRule.getTitle());
            objCallableStatement.setString("Institution", objInnerRule.getInstitution());
            objCallableStatement.setString("RuleDescription", objInnerRule.getDescription());
            objCallableStatement.setString("RuleCondition", objInnerRule.getRuleCondition());
            objCallableStatement.setString("RuleConclusion", objInnerRule.getConclusion());
            objCallableStatement.setString("RuleCreatedBy", objInnerRule.getSpcialistName());
            objCallableStatement.setString("SpecialistName", objInnerRule.getSpcialistName());
            objCallableStatement.setInt("ruleTypeID", objInnerRule.getTypeID());
            
      
            objCallableStatement.execute();
            
            objDbResponse.add(String.valueOf(objInnerRule.getRuleID()));
            objDbResponse.add("No Error");
            
//            objConn.close();
            logger.info("Rule udpated successfully, Rule Details="+objRule);
        }
        catch (Exception e)
        {
        	logger.info("Error in updating rule, rule Details=");
        	objDbResponse.add("Error in saving rule");
        }   
        
        return objDbResponse;
    }

    public List<RuleType> RetriveRuleTypes()
    {
    	List<RuleType> objInnerList= new ArrayList<RuleType>();
    	try {
			CallableStatement objCallableStatement = null;
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_RuleType}");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			objInnerList.clear();
			while(objResultSet.next())
			{
				int TypeID = objResultSet.getInt("RuleTypeID");
                String Desc = objResultSet.getString("RuleTypeDescription");
                RuleType obj= new RuleType();
                obj.setTypeID(TypeID);
                obj.setDescription(Desc);
              
                System.out.println("---------------------");
                System.out.println(obj.getDescription());
                System.out.println(obj.getTypeID());
                objInnerList.add(obj);
			}
    }
    	catch(Exception e)
    	{
    		System.out.println("Exception"+e);
    	}
    	return objInnerList;
    }
    @Override
    public List<Rules> RetriveRulesbyParentID(int parentID){
    	System.out.println("INSIDE RETRIVE RULES DATA ADAPTER");
    	
    	List<Rules> objListInnerRule = new ArrayList<Rules>();
    	List<Rules> finalruleslist = new ArrayList<Rules>();
    	int found=0;
    	
    	try {
    		int num=0;
			CallableStatement objCallableStatement = null;
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_RulesCount}");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			
			while (objResultSet.next()) {
                num = objResultSet.getInt("count1");
			}
			
			int array[] = new int[num];
	        int index = 0;
	        
			CallableStatement objCallableStatement1 = null;
			objCallableStatement1 = objConn.prepareCall("{call dbo.usp_Get_onlyIDs}");
			ResultSet objResultSet1 = objCallableStatement1.executeQuery();
			
            while (objResultSet1.next()) {
                array[index] = objResultSet1.getInt("RuleID");
                index++;
            }
            
            int ruleID1 = 0;
            int pRuleID = 0;

            String rule = "";
            String classi = "";
            
			CallableStatement objCallableStatement2 = null;
			objCallableStatement2 = objConn.prepareCall("{call dbo.usp_Get_rulesForTree}");
			ResultSet objResultSet2 = objCallableStatement2.executeQuery();
			
	           int i = 0;
	            int y=0;

	            while (objResultSet2.next()) {

	                int ruleid = objResultSet2.getInt("RuleID");
	                String cv = objResultSet2.getString("ConditionValue");
	                String ck = objResultSet2.getString("ConditionKey");
	                String op = objResultSet2.getString("ConditionValueOperator");
	                String co = objResultSet2.getString("ConclusionValue");
	                
	                
	                if(op.equals("="))
	                {
	                    op="==";
	                }
	                
	                if (pRuleID == ruleid) {
	                    rule = rule + " && " + ck + " " + op + " " + cv;
	                    classi = co;
	                    ruleID1 = ruleid;
	                } else {
	                    ruleID1 = ruleid;
	                    rule = ck + " " + op + " " + cv;
	                    classi = co;
	                }
	                ruleID1 = ruleid;
	                pRuleID = ruleid;

	                if (i < num - 1) {
	                    if (ruleid == array[i + 1]) {

	                    } else {
	                
	                        Rules r= new Rules();

	                        y++;                        
	                        r.setConclusion(classi);
	                        r.setRuleID(ruleid);
	                        r.setRuleString(rule);
	                        System.out.println("FINAL RULE IS ::::: " + rule.toString());
	                        objListInnerRule.add(r);
	                    }
	                }
	                i++;
	            }
//	            Node n = new Node();
	            Rules r= new Rules();
                r.setConclusion(classi);
                r.setRuleID(ruleID1);
                r.setRuleString(rule); 
//                Node dummy = new Node();
                objListInnerRule.add(r);
                System.out.println("LAST FINAL RULE IS ::::: " + r.getRuleID()+r.getRuleCondition()+r.getConclusion()+r.getCornerstoneCase());

    	}
    	catch(Exception e)
    	{
    		System.out.println("Exception"+e);
    	}
  
    	try{
    		
    		CallableStatement objCallableStatement = null;
    		objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_AllRulesList}");

      
    		ResultSet objResultSet = objCallableStatement.executeQuery();
//    		System.out.println("Connection: "+objConn.isClosed());
    		while(objResultSet.next())
    		{
				Date a= (objResultSet.getDate("RuleCreatedDate"));
				String desc = (objResultSet.getString("RuleDescription"));
				int id=(objResultSet.getInt("RuleID"));
				String insti= (objResultSet.getString("Institution"));
				String sn= (objResultSet.getString("SpecialistName"));
				String rt= (objResultSet.getString("RuleTitle"));
				Date luDate= (objResultSet.getDate("RuleLastUpdatedDate"));
				int typeID= (objResultSet.getInt("RuleTypeID"));
				String updatedby= (objResultSet.getString("RuleUpdatedBy"));
				String csc= (objResultSet.getString("CornerstoneCase"));
				int pid=(objResultSet.getInt("ParentID"));
//				objResultSet.get
				
				
				for(int i=0;i<objListInnerRule.size();i++)
				{
					if(objListInnerRule.get(i).getRuleID()==id)
					{
						
						objListInnerRule.get(i).setCreatedDate(a);
						objListInnerRule.get(i).setDescription(desc);
						objListInnerRule.get(i).setInstitution(insti);
						objListInnerRule.get(i).setSpcialistName(sn);
						objListInnerRule.get(i).setTitle(rt);
						objListInnerRule.get(i).setUpdatedDate(luDate);
						objListInnerRule.get(i).setTypeID(typeID);
						objListInnerRule.get(i).setRuleUpdatedBy(updatedby);
						objListInnerRule.get(i).setCornerstoneCase(csc);
						objListInnerRule.get(i).setParentID(pid);
						
					}
				}
				
    		}
//    		objConn.close();
    		logger.info("Rules loaded successfully.");
    		System.out.println("Rules loaded successfully.");
		}
		catch(Exception e)
		{
			logger.info("Rules not loaded successfully");
			System.out.println("Rules not loaded successfully");
		}
    	
    	
		for(int i=0;i<objListInnerRule.size();i++)
		{
			if(objListInnerRule.get(i).getParentID()==parentID)
			{
				finalruleslist.add(objListInnerRule.get(i));
				found=1;
				System.out.println("In loop to search");
				
			}
		}
//    	ArrayList<Rules> temp= new ArrayList<Rules>();
    	List<Rules> temp = new ArrayList<Rules>();
    	if(found==1)
    	{
    		System.out.println("FOUNDDD RULE");
    		return finalruleslist;	
    	}
    	else
    	{
    		System.out.println("FOUNDDD NOT RULELEE");
    		return temp;
    	}
		
    	
    }

    /**
     * This is implementation function for retrieving 1 rule by ID. 
     * @param ruleID
     * @return Rule
     */
    @Override
    public List<Rules> RetriveRulebyID(int ruleID){
    	System.out.println("INSIDE RETRIVE RULES DATA ADAPTER");
    	Rules objOuterRule = new Rules();
    	List<Rules> objListRules = new ArrayList<Rules>();
    	List<Rules> objListInnerRule = new ArrayList<Rules>();
    	int found=0;
    	
    	try {
    		int num=0;
			CallableStatement objCallableStatement = null;
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_RulesCount}");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			
			while (objResultSet.next()) {
                num = objResultSet.getInt("count1");
			}
			
			int array[] = new int[num];
	        int index = 0;
	        
			CallableStatement objCallableStatement1 = null;
			objCallableStatement1 = objConn.prepareCall("{call dbo.usp_Get_onlyIDs}");
			ResultSet objResultSet1 = objCallableStatement1.executeQuery();
			
            while (objResultSet1.next()) {
                array[index] = objResultSet1.getInt("RuleID");
                index++;
            }
            
            int ruleID1 = 0;
            int pRuleID = 0;

            String rule = "";
            String classi = "";
            
			CallableStatement objCallableStatement2 = null;
			objCallableStatement2 = objConn.prepareCall("{call dbo.usp_Get_rulesForTree}");
			ResultSet objResultSet2 = objCallableStatement2.executeQuery();
			
	           int i = 0;
	            int y=0;

	            while (objResultSet2.next()) {

	                int ruleid = objResultSet2.getInt("RuleID");
	                String cv = objResultSet2.getString("ConditionValue");
	                String ck = objResultSet2.getString("ConditionKey");
	                String op = objResultSet2.getString("ConditionValueOperator");
	                String co = objResultSet2.getString("ConclusionValue");
	                
	                
	                if(op.equals("="))
	                {
	                    op="==";
	                }
	                
	                if (pRuleID == ruleid) {
	                    rule = rule + " && " + ck + " " + op + " " + cv;
	                    classi = co;
	                    ruleID1 = ruleid;
	                } else {
	                    ruleID1 = ruleid;
	                    rule = ck + " " + op + " " + cv;
	                    classi = co;
	                }
	                ruleID1 = ruleid;
	                pRuleID = ruleid;

	                if (i < num - 1) {
	                    if (ruleid == array[i + 1]) {

	                    } else {
	                
	                        Rules r= new Rules();

	                        y++;                        
	                        r.setConclusion(classi);
	                        r.setRuleID(ruleid);
	                        r.setRuleString(rule);
	                        System.out.println("FINAL RULE IS ::::: " + rule.toString());
	                        objListInnerRule.add(r);
	                    }
	                }
	                i++;
	            }
//	            Node n = new Node();
	            Rules r= new Rules();
                r.setConclusion(classi);
                r.setRuleID(ruleID1);
                r.setRuleString(rule); 
//                Node dummy = new Node();
                objListInnerRule.add(r);
                System.out.println("LAST FINAL RULE IS ::::: " + r.getRuleID()+r.getRuleCondition()+r.getConclusion()+r.getCornerstoneCase());

    	}
    	catch(Exception e)
    	{
    		System.out.println("Exception"+e);
    	}
  
    	try{
    		
    		CallableStatement objCallableStatement = null;
    		objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_AllRulesList}");

      
    		ResultSet objResultSet = objCallableStatement.executeQuery();
    		System.out.println("Connection: "+objConn.isClosed());
    		while(objResultSet.next())
    		{
				Date a= (objResultSet.getDate("RuleCreatedDate"));
				String desc = (objResultSet.getString("RuleDescription"));
				int id=(objResultSet.getInt("RuleID"));
				String insti= (objResultSet.getString("Institution"));
				String sn= (objResultSet.getString("SpecialistName"));
				String rt= (objResultSet.getString("RuleTitle"));
				Date luDate= (objResultSet.getDate("RuleLastUpdatedDate"));
				int typeID= (objResultSet.getInt("RuleTypeID"));
				String updatedby= (objResultSet.getString("RuleUpdatedBy"));
				String csc= (objResultSet.getString("CornerstoneCase"));
				int pid=(objResultSet.getInt("ParentID"));
//				objResultSet.get
				
				
				for(int i=0;i<objListInnerRule.size();i++)
				{
					if(objListInnerRule.get(i).getRuleID()==id)
					{
						
						objListInnerRule.get(i).setCreatedDate(a);
						objListInnerRule.get(i).setDescription(desc);
						objListInnerRule.get(i).setInstitution(insti);
						objListInnerRule.get(i).setSpcialistName(sn);
						objListInnerRule.get(i).setTitle(rt);
						objListInnerRule.get(i).setUpdatedDate(luDate);
						objListInnerRule.get(i).setTypeID(typeID);
						objListInnerRule.get(i).setRuleUpdatedBy(updatedby);
						objListInnerRule.get(i).setCornerstoneCase(csc);
						objListInnerRule.get(i).setParentID(pid);
						
					}
				}
				
    		}
//    		objConn.close();
    		logger.info("Rules loaded successfully.");
    		System.out.println("Rules loaded successfully.");
		}
		catch(Exception e)
		{
			logger.info("Rules not loaded successfully");
			System.out.println("Rules not loaded successfully");
		}
    	
    	
		for(int i=0;i<objListInnerRule.size();i++)
		{
			if(objListInnerRule.get(i).getRuleID()==ruleID)
			{
				objListRules.add(objListInnerRule.get(i));
				
				found=1;
				System.out.println("In loop to search");
				
			}
		}
    	
    	List<Rules> temp = new ArrayList<Rules>();
    	if(found==1)
    	{
    		System.out.println("FOUNDDD RULE");
    		return objListRules;	
    	}
    	else
    	{
    		System.out.println("FOUNDDD NOT RULELEE");
    		return objListRules;
    	}
		
    	
    }
    
    /**
     * This is implementation function for retrieving all rules 
     * @param none
     * @return List of Rule
     */
    
    @Override
    public List<Rules> RetriveRules() {
//    	List<Rules> objListInnerRule = new ArrayList<Rules>();
//    	Rules array= new Rules();
//    	array.setConclusion("ABCD");
////    	array.set);
//    	objListInnerRule.add(array);
//    	return objListInnerRule;
    	System.out.println("INSIDE RETRIVE RULES DATA ADAPTER");
    	Rules objOuterRule = new Rules();
    	List<Rules> objListInnerRule = new ArrayList<Rules>();
    	
    	try {
    		int num=0;
			CallableStatement objCallableStatement = null;
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_RulesCount}");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			
			while (objResultSet.next()) {
                num = objResultSet.getInt("count1");
			}
			
			int array[] = new int[num];
	        int index = 0;
	        
			CallableStatement objCallableStatement1 = null;
			objCallableStatement1 = objConn.prepareCall("{call dbo.usp_Get_onlyIDs}");
			ResultSet objResultSet1 = objCallableStatement1.executeQuery();
			
            while (objResultSet1.next()) {
                array[index] = objResultSet1.getInt("RuleID");
                index++;
            }
            
            int ruleID = 0;
            int pRuleID = 0;

            String rule = "";
            String classi = "";
            
			CallableStatement objCallableStatement2 = null;
			objCallableStatement2 = objConn.prepareCall("{call dbo.usp_Get_rulesForTree}");
			ResultSet objResultSet2 = objCallableStatement2.executeQuery();
			
	           int i = 0;
	            int y=0;

	            while (objResultSet2.next()) {

	                int ruleid = objResultSet2.getInt("RuleID");
	                String cv = objResultSet2.getString("ConditionValue");
	                String ck = objResultSet2.getString("ConditionKey");
	                String op = objResultSet2.getString("ConditionValueOperator");
	                String co = objResultSet2.getString("ConclusionValue");
	                
	                
	                if(op.equals("="))
	                {
	                    op="==";
	                }
	                
	                if (pRuleID == ruleid) {
	                    rule = rule + " && " + ck + " " + op + " " + cv;
	                    classi = co;
	                    ruleID = ruleid;
	                } else {
	                    ruleID = ruleid;
	                    rule = ck + " " + op + " " + cv;
	                    classi = co;
	                }
	                ruleID = ruleid;
	                pRuleID = ruleid;

	                if (i < num - 1) {
	                    if (ruleid == array[i + 1]) {

	                    } else {
	                
	                        Rules r= new Rules();

	                        y++;                        
	                        r.setConclusion(classi);
	                        r.setRuleID(ruleid);
	                        r.setRuleString(rule);
	                        System.out.println("FINAL RULE IS ::::: " + rule.toString());
	                        objListInnerRule.add(r);
	                    }
	                }
	                i++;
	            }
	            Rules r= new Rules();
                r.setConclusion(classi);
                r.setRuleID(ruleID);
                r.setRuleString(rule); 
//                Node dummy = new Node();
                objListInnerRule.add(r);
                System.out.println("LAST FINAL RULE IS ::::: " + r.getRuleID()+r.getRuleCondition()+r.getConclusion()+r.getCornerstoneCase());

    	}
    	catch(Exception e)
    	{
    		System.out.println("Exception"+e);
    	}
  
    	try{
    		
    		CallableStatement objCallableStatement = null;
    		objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_AllRulesList}");

      
    		ResultSet objResultSet = objCallableStatement.executeQuery();
    		System.out.println("Connection: "+objConn.isClosed());
    		while(objResultSet.next())
    		{
				Date a= (objResultSet.getDate("RuleCreatedDate"));
				String desc = (objResultSet.getString("RuleDescription"));
				int id=(objResultSet.getInt("RuleID"));
				String insti= (objResultSet.getString("Institution"));
				String sn= (objResultSet.getString("SpecialistName"));
				String rt= (objResultSet.getString("RuleTitle"));
				Date luDate= (objResultSet.getDate("RuleLastUpdatedDate"));
				int typeID= (objResultSet.getInt("RuleTypeID"));
				String updatedby= (objResultSet.getString("RuleUpdatedBy"));
				String csc= (objResultSet.getString("CornerstoneCase"));
				int pid=(objResultSet.getInt("ParentID"));
//				objResultSet.get
				
				for(int i=0;i<objListInnerRule.size();i++)
				{
					if(objListInnerRule.get(i).getRuleID()==id)
					{
						objListInnerRule.get(i).setCreatedDate(a);
						objListInnerRule.get(i).setDescription(desc);
						objListInnerRule.get(i).setInstitution(insti);
						objListInnerRule.get(i).setSpcialistName(sn);
						objListInnerRule.get(i).setTitle(rt);
						objListInnerRule.get(i).setUpdatedDate(luDate);
						objListInnerRule.get(i).setTypeID(typeID);
						objListInnerRule.get(i).setRuleUpdatedBy(updatedby);
						objListInnerRule.get(i).setCornerstoneCase(csc);
						objListInnerRule.get(i).setParentID(pid);
						
						
						
					}
				}
				
    		}
//    		objConn.close();
    		logger.info("Rules loaded successfully.");
    		System.out.println("Rules loaded successfully.");
		}
		catch(Exception e)
		{
			logger.info("Rules not loaded successfully");
			System.out.println("Rules not loaded successfully");
		}
  
    	return objListInnerRule;
    }
    
    /**
     * This is implementation function for creating RDR tree out of all rules
     * read from the Database
     * returns a RDRTree which can be used for classification/inferencing
     */

    
    
    
    
    /**
     * This is implementation function for connection database.
     * @param objConf
     */
    @Override
     public void ConfigureAdapter(Object objConf) {
         try
         {
            objConn = (Connection)objConf;
            System.out.println("DB Connected | ConfigureAdapter");
            logger.info("Database connected successfully");
         }
         catch(Exception ex)
         {
        	 logger.info("Error in connection to Database");
         }
    }

	@Override
	public <T> List<T> Delete(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<String> getConceptsList()
	{
		List<String> objResponse = new ArrayList<>();

		try
		{
			CallableStatement objCallableStatement = null;
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_rulesKeys}");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			System.out.println("Connection: "+objConn.isClosed());
			
			while(objResultSet.next())
			{
				String Key=(objResultSet.getString("ConditionKey"));
				objResponse.add(Key);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in getting db");
		}
		
		return objResponse;
	}
	
	
	public List<String> getConceptsbyType(int typeID)
	{
		List<String> objResponse = new ArrayList<>();

		try
		{
			CallableStatement objCallableStatement = null;
			System.out.print("step 1");
			objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_rulesKeysbyType(?)}");
			objCallableStatement.setInt("typeID", typeID);
			
			System.out.print("step 2");
			ResultSet objResultSet = objCallableStatement.executeQuery();
			System.out.print("step 3");
//			System.out.println("Connection: "+objConn.isClosed());
			
			while(objResultSet.next())
			{
				String Key=(objResultSet.getString("ConditionKey"));
				objResponse.add(Key);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in getting db");
		}
		
		return objResponse;
	}
	
	public Rules Classify(HashMap<String, String> C)
	{
		List<Rules> objListRules= this.RetriveRules();

		Tree t1= new Tree();
		
		for(int i=0; i<objListRules.size();i++)
		{	
			ArrayList<Conditions> ruleTemp = new ArrayList<Conditions>();
	        String[] data = objListRules.get(i).getRuleCondition().split("&&");
	        System.out.println("Len: "+data.length);
	        for(int j =0 ; j<data.length; j++)
	        {
	        	data[j]=data[j].replace(" ", "");
	        	String[] conditions=null;
	        	Conditions temp= new Conditions();
	        	if(data[j].contains(">") && !data[j].contains(">="))
	        	{
	        		conditions=data[j].split(">");
		        	temp.setKey(conditions[0]);
		        	temp.setOp(">");
		        	temp.setValue(conditions[1]);
	        	}
	        	else if(data[j].contains("<") && !data[j].contains("<="))
	        	{
	        		conditions=data[j].split("<");
	        		temp.setKey(conditions[0]);
		        	temp.setOp("<");
		        	temp.setValue(conditions[1]);
	        		
	        	}
	        	else if(data[j].contains("=="))
	        	{
	        		conditions=data[j].split("==");
	        		temp.setKey(conditions[0]);
		        	temp.setOp("==");
		        	temp.setValue(conditions[1]);
	        	}
	        	else if(data[j].contains(">="))
	        	{
	        		conditions=data[j].split(">=");
	        		temp.setKey(conditions[0]);
		        	temp.setOp(">=");
		        	temp.setValue(conditions[1]);
	        	}
	        	else if(data[j].contains("<="))
	        	{
	        		conditions=data[j].split("<=");
	        		temp.setKey(conditions[0]);
		        	temp.setOp("<=");
		        	temp.setValue(conditions[1]);
	        	}	        	
	        	ruleTemp.add(temp);
	        }
	        Node node= new Node();
        	node.setConclusion(objListRules.get(i).getConclusion());
        	node.setConditions(ruleTemp);
        	node.setRuleID(objListRules.get(i).getRuleID());
        	node.setParentID(objListRules.get(i).getParentID());
        	if(objListRules.get(i).getRuleCondition().equals("1 == 1"))
        	{
        		node.rootNode=true;
        	}
        	t1.InsertNode(node);
		}
		
//        t1.printTree();
        
//        HashMap c1 = new HashMap<String, String>();
//        c1.put("1", "1");
//        c1.put("ABC", "Black");
        t1.tempCase= C;
        t1.printTree();
        System.out.println("\n\nCase to be tested: "+ C.toString());

        Node retur = t1.searchTree(t1.Nodes.get(0), C);
        System.out.println("Matced Rules are: "  +t1.matchedRules.toString());
        try {
        	addLog(t1.tempCase, t1);
        }
        catch(Exception e)
        {
        	System.out.println("Exception here!  "+e);
        }
        if(retur.isTrue==true)
        {
            System.out.println("Case Complete Match: "+ retur.getConclusion());
        }
        else
        {
            System.out.println("No Complete Match, Last True: "+ retur.getConclusion());

        }
        System.out.println(retur.getConclusion());
		
        for(int index=0; index<objListRules.size();index++)
        {
        	if(objListRules.get(index).getRuleID()==retur.getRuleID())
        	{
        		return objListRules.get(index);
        	}
        }
        
        Rules temp = new Rules();
        return temp;
	}
	
    public void addLog(HashMap<String, String> Case, Tree t1) throws SQLException
    {
		CallableStatement objCallableStatement = null;
		objCallableStatement = objConn.prepareCall("{call usp_Add_Log(?,?)}");
		objCallableStatement.setString("InputCase", Case.toString());
		objCallableStatement.setString("MatchedRules", t1.matchedRules.toString());
   		objCallableStatement.execute();
    }
	
}
