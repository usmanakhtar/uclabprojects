package org.uclab.IMP.datamodel;

import java.util.HashMap;
import java.util.List;

import org.uclab.IMP.datamodel.RDR.Node;
import org.uclab.IMP.datamodel.RDR.RuleType;
//import org.uclab.IMP.datamodel.RDR.Classification;
//import org.uclab.IMP.datamodel.RDR.RDRTree;
import org.uclab.IMP.datamodel.RDR.Rules;
import org.uclab.IMP.datamodel.RDR.LogMatcher;



public abstract class AbstractDataBridge {

	public DataAccessInterface objDAInterface;
    public AbstractDataBridge(DataAccessInterface objDAInterface)
    {
        this.objDAInterface = objDAInterface;
    }
    
    /**
     * This constructor of the abstract class
     */
    public AbstractDataBridge()
    {
        
    }

    /**
     * This is abstract function for saving Rule.
     * @param objRule
     * @return List of String 
     */
    public abstract List<String> SaveRule(Node objRule);
    
    
    /**
     * This is abstract function for retrieving Rule.
     * @param ruleID
     * @return List of String 
     */
    public abstract List<Rules> RetriveRulebyID(int ruleID);
    
    
    public abstract List<Rules> RetriveRulesbyParentID(int parentID);
    
    public abstract void setDataAdapter(DataAccessInterface DAInterface);
    
//    public abstract RDRTree createTree();

	public abstract List<Rules> RetriveRules();

	public abstract Rules Classify(HashMap<String, String> C);
	public abstract List<LogMatcher> SearchLog(HashMap<String, String> C);
	public abstract List<RuleType> RetriveRuleTypes();
	
	public abstract List<String> getConceptsList();
	public abstract List<String> getConceptsbyType(int typeID);

//	public abstract Classification classify();




}
