package org.uclab.IMP.datamodel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.IMP.datamodel.RDR.LogMatcher;
import org.uclab.IMP.datamodel.RDR.Node;
import org.uclab.IMP.datamodel.RDR.RuleType;
import org.uclab.IMP.datamodel.RDR.Rules;
//import org.uclab.IMP.datamodel.RDR.Classification;
//import org.uclab.IMP.datamodel.RDR.RDRTree;

/**
 * This is the DatabaseStorage class, it extends the AbstractDataBridge class
 * @author Atif
 */
public class DatabaseStorage extends AbstractDataBridge{

	private Connection objConnection;

	public DatabaseStorage(DataAccessInterface objDAInterface)
	{
	   setDataAdapter(objDAInterface);
	}

	private static final Logger logger = LoggerFactory.getLogger(DatabaseStorage.class);
    
    /**
     * This is implementation function setter of DataAccessInterface. 
     * @param DAInterface
     */
    @Override
    public void setDataAdapter(DataAccessInterface DAInterface) {
        super.objDAInterface = DAInterface;
        System.out.println("objConnection: "+ objConnection);
//        String dbDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//        String dbURL = "jdbc:sqlserver://localhost:49152;databaseName=IMPIKATDB_V1_Diabetes";
//        // Database name to access 
//        String dbName = "IMPIKATDB_V1_Diabetes;";
//        String dbUsername = "sa";
//        String dbPassword = "adminsa";
        try
        {
            if(objConnection == null || objConnection.isClosed())
            {
            	
            	DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
    	    	String dbURL = "jdbc:sqlserver://163.180.116.118:1433;instance=SQLEXPRESS;databaseName=KnowledgeEvolutionDB;user=sa;password=adminsa";
    	    	objConnection = DriverManager.getConnection(dbURL);
    	    	
                //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//                objConnection = DriverManager.getConnection(dbURL, dbUsername, dbPassword);
                //System.out.println("jdbc:sqlserver://DESKTOP-F7E8J7S:49152;databaseName=IMPIKATDB_V1_Diabetes;user=sa;password=adminsa;");
                //objConnection = DriverManager.getConnection("jdbc:sqlserver://DESKTOP-F7E8J7S:49152;databaseName=IMPIKATDB_V1_Diabetes;user=sa;password=adminsa;");
                //objConnection = DriverManager.getConnection("jdbc:sqlserver://MUSARRAT-DSKTP:1433;databaseName=KnowledgeEvolutionDB;user=sa;password=adminsa;");
                //objConnection = DriverManager.getConnection("jdbc:sqlserver://MUSARRAT-DSKTP\\SQLEXPRESS:1433;databaseName=KnowledgeEvolutionDB;user=sa;password=adminsa;");
                logger.info("Database connected successfully");
                System.out.println(" - ACTUAL -Database Connection Successful");
            }
        }
        catch (Exception e)
        {
        	logger.info("Error in connecting database");
        	System.out.println("ERRROR 23 "+e);
        }  
    }
	
    /**
     * This is implementation function for retrieving Rule. 
     * @param int RuleID
     * @return List of Users
     */
    @Override
    public List<Rules> RetriveRulebyID(int ruleID) {
    	List<Rules> objListRule = new ArrayList<Rules>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListRule =  objDAInterface.RetriveRulebyID(ruleID);
            logger.info("Rule loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting rule");
    	}
        return objListRule;
    }
    
    
    @Override
    public List<Rules> RetriveRulesbyParentID(int parentID) {
    	List<Rules> objListRule = new ArrayList<Rules>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListRule =  objDAInterface.RetriveRulesbyParentID(parentID);
            logger.info("Rule loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting rule");
    	}
        return objListRule;
    }
    
    
    
    /**
     * This is implementation function for retrieving Rule. 
     * @param int RuleID
     * @return List of Users
     */
    @Override
    public List<Rules> RetriveRules() {
    	List<Rules> objListRule = new ArrayList<Rules>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objListRule =  objDAInterface.RetriveRules();
            logger.info("Rule loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting rule");
    	}
        return objListRule;
    }
    
    @Override
    public List<RuleType> RetriveRuleTypes(){
    	List<RuleType> objInnerList= new ArrayList<RuleType>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
    		objInnerList =  objDAInterface.RetriveRuleTypes();
            logger.info("Rule Types loaded successfully.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in getting rule");
    	}
    	return objInnerList;
    }
    
    @Override
    public Rules Classify(HashMap<String, String> C)
    {
    	Rules found= new Rules();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            found =  objDAInterface.Classify(C);
            logger.info("Rule inferening successful.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in classification!");
    		System.out.println("Error in classification of rule!");
    	}
    	return found;
    }
    
    @Override
    public List<LogMatcher> SearchLog(HashMap<String, String> Case)
    {
    	List<LogMatcher> foundLog= new ArrayList<>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
    		foundLog =  objDAInterface.SearchLog(Case);
            logger.info("Rule inferening successful.");
    	}
    	catch(Exception ex)
    	{
    		logger.info("Error in classification!");
    		System.out.println("Error in classification of rule!");
    	}
    	return foundLog;
    }

	@Override
	public List<String> SaveRule(Node objRule) {
        List<String> objResponse = new ArrayList<>();
        try
        {
            objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.SaveRule(objRule);
            logger.info("Rule saved successfully, Rule Details="+objRule);
        }
        catch(Exception ex)
        {
        	logger.info("Error in saving rule, Rule Details=");
//        	objResponse.add("Error in saving rule");
        }
        return objResponse;
	}

	
    @Override
    public List<String> getConceptsList()
    {
    	List<String> objResponse = new ArrayList<>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.getConceptsList();
            logger.info("Rule types retrived successfully, Rule Details="+objResponse);
    	}
    	catch(Exception e)
    	{
    		logger.info("Error in saving rule, Rule Details=");
    	}
    	return objResponse;
    }
	
    @Override
    public List<String> getConceptsbyType(int typeID)
    {
    	List<String> objResponse = new ArrayList<>();
    	try
    	{
    		objDAInterface.ConfigureAdapter(objConnection);
            objResponse = objDAInterface.getConceptsbyType(typeID);
            logger.info("Rule types retrived successfully, Rule Details="+objResponse);
    	}
    	catch(Exception e)
    	{
    		logger.info("Error in saving rule, Rule Details=");
    	}
    	return objResponse;
    }

}
