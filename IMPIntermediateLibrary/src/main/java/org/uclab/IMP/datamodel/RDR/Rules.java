/*
 Copyright [2019] [Atif]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */
package org.uclab.IMP.datamodel.RDR;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import org.uclab.IMP.datamodel.RDR.Case;

/**
 *
 * @author DEVSPOTAI1
 */
@Entity
public class Rules implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id = null;
    private int ruleID;
    private String ruleCondition;
    private String conclusion;
    private String cornerstoneCase;
    private String institution;
    private int parentID;
    private int ruleTypeID;
    private String title;
    private String description;
    private Date createdDate;
    private String spcialistName;
    private String ruleUpdatedBy;
    private Date updatedDate;
    
    
    @Override
    public String toString() {
        return "org.uclab.IMP.datamodel.RDR.Rules[ id=" + id + " ]";
    }


    public int getRuleID() {
		return ruleID;
	}


	public void setRuleID(int ruleID) {
		this.ruleID = ruleID;
	}
	
	public void setInstitution(String institute) {
		this.institution=institute;
	}

	public String getInstitution() {
		return institution;
	}

	public String getRuleCondition() {
		return ruleCondition;
	}


	public void setRuleString(String ruleString) {
		this.ruleCondition = ruleString;
	}


	public String getConclusion() {
		return conclusion;
	}


	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}


	public String getCornerstoneCase() {
		return cornerstoneCase;
	}


	public void setCornerstoneCase(String cornerstoneCase) {
		this.cornerstoneCase = cornerstoneCase;
	}


	public int getParentID() {
		return parentID;
	}


	public void setParentID(int parentID) {
		this.parentID = parentID;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}

	
	public void setTypeID(int type) {
		this.ruleTypeID= type;
	}


	public int getTypeID() {
		return ruleTypeID;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public String getSpcialistName() {
		return spcialistName;
	}


	public void setSpcialistName(String spcialistName) {
		this.spcialistName = spcialistName;
	}


	public String getRuleUpdatedBy() {
		return ruleUpdatedBy;
	}


	public void setRuleUpdatedBy(String ruleUpdatedBy) {
		this.ruleUpdatedBy = ruleUpdatedBy;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public Long getId() {
        return id;
    }
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rules)) {
            return false;
        }
        Rules other = (Rules) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    
}
