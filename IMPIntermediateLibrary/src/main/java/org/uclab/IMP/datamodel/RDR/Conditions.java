package org.uclab.IMP.datamodel.RDR;

public class Conditions {
	
	 public String ConditionOp;
	    public String ConditionValue;
	    public String ConditionKey;
	    public boolean checked;
	    
	    public Conditions()
	    {
	        this.ConditionOp="";
	        this.ConditionKey="";
	        this.ConditionValue="";
	        checked=false;
	    }
	    public void setOp(String op)
	    {
	        this.ConditionOp= op;
	    }
	    public void setValue(String value)
	    {
	        this.ConditionValue= value;
	    }
	    public void setKey(String Key)
	    {
	        this.ConditionKey= Key;
	    }
	    public String getOp()
	    {
	        return this.ConditionOp;
	    }
	    public String getValue()
	    {
	        return this.ConditionValue;
	    }
	    public String getKey()
	    {
	        return this.ConditionKey;
	    }
	    public boolean Print()
	    {
	        System.out.print("{"+ConditionKey+" "+ConditionOp+" "+ConditionValue+"}");
	        return false;
	    }

}
