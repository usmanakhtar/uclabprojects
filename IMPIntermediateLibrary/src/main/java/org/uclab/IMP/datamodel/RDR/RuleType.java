package org.uclab.IMP.datamodel.RDR;

public class RuleType {
	int TypeID;
	String TypeDescription;
	
	public RuleType()
	{
		this.TypeID=0;
		this.TypeDescription="";
	}
	
	public int getTypeID()
	{
		return this.TypeID;
	}
	public String getDescription()
	{
		return this.TypeDescription;
	}
	
	public void setTypeID(int ID)
	{
		this.TypeID= ID;
	}
	public void setDescription(String desc)
	{
		this.TypeDescription= desc;
	}
}
