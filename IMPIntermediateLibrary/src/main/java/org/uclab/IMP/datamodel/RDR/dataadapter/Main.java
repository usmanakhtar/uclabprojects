package org.uclab.IMP.datamodel.RDR.dataadapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.uclab.IMP.datamodel.AbstractDataBridge;
import org.uclab.IMP.datamodel.DataAccessInterface;
import org.uclab.IMP.datamodel.DatabaseStorage;
import org.uclab.IMP.datamodel.RDR.LogMatcher;
import org.uclab.IMP.datamodel.RDR.Rules;
//import org.uclab.IMP.datamodel.RDR.Case;
//import org.uclab.IMP.datamodel.RDR.Classification;
//import org.uclab.IMP.datamodel.RDR.Node;
//import org.uclab.IMP.datamodel.RDR.RDRTree;

public class Main {
	
	public static void main(String[] args) throws SQLException
	{
//		HashMap<String, String> a = new HashMap<>();
//		a.put("a", "2");
//		a.put("b", "2");
//		a.put("c", "2");
//		
		HashMap<String, String> b = new HashMap<>();
		b.put("FPG", "3");
		b.put("Ace", "1");
//		b.put("1", "2");
//		b.put("1", "2");
//		
//		Set<String> a1= a.keySet();
//		Set<String> a2= b.keySet();
//		
//		System.out.println("Equal= "+a1.equals(a2));
		
		
        HashMap<String, String> map = new HashMap<>();
        List<LogMatcher> topMatchings= new ArrayList<>();
        map = (HashMap) b;
        Rules foundRule = new Rules();
        try
        {
	        DataAccessInterface objDAInterface = new RuleDataAdapter();
	        AbstractDataBridge objADBridge = new DatabaseStorage(objDAInterface);
	        topMatchings = objADBridge.SearchLog(b);
        }
        catch(Exception ex)
        {
        	System.out.println("Couldnt search Properly!");
        }
}
}

