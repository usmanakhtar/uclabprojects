package org.uclab.IMP.datamodel.RDR;

import java.awt.List;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import org.uclab.IMP.datamodel.RDR.Rules;
import org.uclab.IMP.datamodel.RDR.Conditions;

public class Node {
	   private int ruleID;
	    private int parentID;

	    public ArrayList<Conditions> Conditions;
	    private String conclusion;
	    private String CornerstoneCase;
	    public ArrayList<Integer> Children;
	    public String ruleCondition;

	    private String institution;
	    private String title;
	    private String description;
	    private Date createdDate;
	    private String specialistName;
	    private String ruleUpdatedBy;
	    private int createdBy;
	    private Date updatedDate;
	    public boolean printed;
	    public int TypeID;
	    
	    public boolean isTrue;
	    public boolean isLastTrue;
        public boolean rootNode;
        public boolean traversed;

	    
	    
	    public Node() {
	        ruleID = 0;
	        parentID = 0;

	        Conditions = new ArrayList<Conditions>();
	        conclusion = null;
	        CornerstoneCase = null;
	        Children = new ArrayList<Integer>();

	        institution = null;
	        title = null;
	        description = null;
	        createdDate = null;
	        specialistName = null;
	        ruleUpdatedBy = null;
	        createdBy = 0;
	        updatedDate = null;
	        printed=false;
	        isTrue= false;
	        isLastTrue= false;
	        rootNode=false;
	        traversed=false;
	    }

	    public void addCondition(Conditions r)
	    {
	        Conditions.add(r);
	    }
	    public ArrayList<Conditions> getConditions() {
	        return this.Conditions;
	    }

	    public void setConditions(ArrayList<Conditions> Conditions) {
	        this.Conditions = Conditions;
	    }

	    public int getRuleID() {
	        return ruleID;
	    }

	    public int getTypeID()
	    {
	    	return this.TypeID;
	    }
	    
	    public void setTypeID(int Id)
	    {
	    	this.TypeID= Id;
	    }
	    public void setRuleID(int ruleID) {
	        this.ruleID = ruleID;
	    }

	    public void setInstitution(String institute) {
	        this.institution = institute;
	    }

	    public String getInstitution() {
	        return institution;
	    }

	    public String getRule()
	    {
	    	return this.ruleCondition;
	    }
	    public void setRule(String rule)
	    {
	    	this.ruleCondition= rule;
	    }
	    public String getConclusion() {
	        return conclusion;
	    }

	    public void setConclusion(String conclusion) {
	        this.conclusion = conclusion;
	    }

	    public String getCornerstoneCase() {
	        return CornerstoneCase;
	    }

	    public void setCornerstoneCase(String cornerstoneCase) {
	        this.CornerstoneCase = cornerstoneCase;
	    }

	    public int getParentID() {
	        return parentID;
	    }

	    public void setParentID(int parentID) {
	        this.parentID = parentID;
	    }

	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	        this.title = title;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {
	        this.description = description;
	    }

	    public Date getCreatedDate() {
	        return createdDate;
	    }

	    public void setCreatedDate(Date createdDate) {
	        this.createdDate = createdDate;
	    }

	    public String getSpecialistName() {
	        return this.specialistName;
	    }

	    public void setSpecialistName(String specialistName) {
	        this.specialistName = specialistName;
	    }

	    public int getCreator() {
	        return this.createdBy;
	    }

	    public void setCreator(int CreatorName) {
	        this.createdBy = CreatorName;
	    }

	    public String getRuleUpdatedBy() {
	        return ruleUpdatedBy;
	    }

	    public void setRuleUpdatedBy(String ruleUpdatedBy) {
	        this.ruleUpdatedBy = ruleUpdatedBy;
	    }

	    public Date getUpdatedDate() {
	        return updatedDate;
	    }

	    public void setUpdatedDate(Date updatedDate) {
	        this.updatedDate = updatedDate;
	    }
	    
	    public void printNode()
	    {
	    
	        System.out.println("+-++-++-++++-++-++-++++-++-++-++++-++-++-+++");
	        System.out.println("ID: "+ruleID+ "| Parent's Rule ID: "+ this.parentID);
	        for(int i=0;i<this.Conditions.size();i++)
	        {
	            this.Conditions.get(i).Print();
	        }
//	                "Condition: "+ this.Conditions.get(0).ConditionKey);
	        System.out.println("Children: "+this.Children.size());
	        System.out.println("Conclusion: "+this.getConclusion());
	        System.out.println("isRootNode: "+ this.rootNode);

	    }

}
