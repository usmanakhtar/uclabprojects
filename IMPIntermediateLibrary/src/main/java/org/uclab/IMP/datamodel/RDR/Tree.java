package org.uclab.IMP.datamodel.RDR;

import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Tree {

    public List<Node> Nodes;
    private Node trueNode;
    public HashMap<String, String> tempCase;
    public Set<String> doneSet;
    public ArrayList<String> matchedRules;
    

    /**
     * @param args the command line arguments
     */

    public Tree() {
        Nodes = new ArrayList<Node>();          
          doneSet = new HashSet<String>();

    }

    public void InsertNode(Node n) {
        if (n.getParentID() == 0) {
            n.Children.clear();
        } else {
            for (int i = 0; i < Nodes.size(); i++) {
                if (Nodes.get(i).getRuleID() == n.getParentID()) {
                    Nodes.get(i).Children.add(n.getRuleID());

                }
            }
        }
        this.Nodes.add(n);
    }

    public void deleteNode(int ruleID) {
        for (int i = 0; i < Nodes.size(); i++) {
            if (Nodes.get(i).getRuleID() == ruleID) {
                if (Nodes.get(i).Children.size() == 0) {
                    Nodes.remove(i);
                } else {
                    for (int j = 0; j < Nodes.get(i).Children.size(); j++) {
                        for (int k = 0; k < Nodes.size(); k++) {
                        }
                    }
                }
            }
        }
    }

    public boolean allPrinted() {
        for (int i = 0; i < Nodes.size(); i++) {
            if (Nodes.get(i).printed == true) {

            } else {
                return false;
            }
        }
        return true;
    }

    public void printTree() {

        for (int i = 0; i < Nodes.size(); i++) {
            Nodes.get(i).printNode();
        }
    }
    
    public boolean checkTrue(Node Current)
    {
        int repeat_score=0;
        for(int i=0;i<Current.Children.size();i++)
        {
            Node temp= new Node();
            int index= Current.Children.get(i);
            for(int j=0;j<Nodes.size();j++)
            {
                if(Nodes.get(j).getRuleID()==index)
                {
                    temp=Nodes.get(j);
                }
            }
            for(int k=0;k<temp.getConditions().size();k++)
            {
                if(doneSet.contains(temp.getConditions().get(k).ConditionKey))
                {
                    repeat_score++;
                }
            }
        }
        if(repeat_score==0)
        {
            return true;
        }
        return false;
    }
    
    
    
    
    int Child = 0;
    int counter = 0;
    Node newNode = new Node();
    public Node searchTree(Node N, HashMap<String, String> Case) {
    	System.out.println("Node testing: "+N.getConditions()+ " Case: "+Case);
        if(counter==0)
        {
        	matchedRules= new ArrayList<String>();
            tempCase = new HashMap<String, String>(Case);
            counter++;	
        }
        boolean result=compare(N.getConditions(), Case);
        if (result) {
        	matchedRules.add(String.valueOf(N.getRuleID()));
            if (Case.size() == 0 && checkTrue(N)) {
                N.isTrue=true;
                newNode = N;
                return newNode;

            }
            Child = 0;

            if (N.Children.size() == 0) {
                N.isLastTrue = true;
                newNode = N;
                return newNode;
            } else {

                //0th Child 
                int next = N.Children.get(0);
                //            next--;
                Node temp = new Node();
                for (int i = 0; i < this.Nodes.size(); i++) {
                    if (Nodes.get(i).getRuleID() == next) {
                        temp = Nodes.get(i);
                    }
                }
                

                for (int m = 0; m < temp.getConditions().size(); m++) 
                {
                    if(doneSet.contains(temp.getConditions().get(m).ConditionKey))
                    {
                        Case.put(temp.getConditions().get(m).ConditionKey, tempCase.get(temp.getConditions().get(m).ConditionKey));
                    }
                }

                searchTree(temp, Case);
            }
        } else if (!result) {
            if (N.rootNode == true) {
                newNode = N;
                return newNode;
            } else {
                int pID = N.getParentID();
                    Node Parent = new Node();
                    for (int i = 0; i < this.Nodes.size(); i++) {
                        if (Nodes.get(i).getRuleID() == pID) {
                            Parent = Nodes.get(i);
                        }
                    }
                Child++;
                if (Child < Parent.Children.size()) {
                    int next = Parent.Children.get(Child);
                    Node temp = new Node();
                    for (int i = 0; i < this.Nodes.size(); i++) {
                        if (Nodes.get(i).getRuleID() == next) {
                            temp = Nodes.get(i);
                        }
                    }
                    
                    for (int n = 0; n < temp.getConditions().size(); n++) 
                    {
                        if(doneSet.contains(temp.getConditions().get(n).ConditionKey))
                        {
                            Case.put(temp.getConditions().get(n).ConditionKey, tempCase.get(temp.getConditions().get(n).ConditionKey));
                        }
                    }

                    searchTree(temp, Case);
                } else {
                    
                    Parent.isLastTrue = true;
                    newNode = Parent;
                    return newNode;

                }
            }

        }

        return newNode;
    }

    public boolean compare(List<Conditions> rule, HashMap<String, String> c) {
        boolean result;
        int count = 0;

        for (int i = 0; i < rule.size(); i++) {

            int value = 0;
            String value_ = "";
                   if(c.containsKey(rule.get(i).ConditionKey))
                   {
                        if (rule.get(i).ConditionValue.matches("[0-9]+")) {
                value = Integer.valueOf(rule.get(i).ConditionValue);

                if (rule.get(i).ConditionOp.equals(">")) {
                    if (Integer.valueOf(c.get(rule.get(i).ConditionKey)) > value) {
                        count++;
                        rule.get(i).checked = true;
                        doneSet.add(rule.get(i).ConditionKey);
                        c.remove(rule.get(i).ConditionKey);
                    }
                    else
                    {
                        result=false;
                    }
                }
                else if (rule.get(i).ConditionOp.equals("<")) {
                    if (Integer.valueOf(c.get(rule.get(i).ConditionKey)) < value) {
                        count++;
                        rule.get(i).checked = true;
                        doneSet.add(rule.get(i).ConditionKey);
                        c.remove(rule.get(i).ConditionKey);

                    } else {
                        result = false;
                    }
                }
                else if (rule.get(i).ConditionOp.equals(">=")) {
                    if (Integer.valueOf(c.get(rule.get(i).ConditionKey)) >= value) {
                        count++;
                        rule.get(i).checked = true;
                        doneSet.add(rule.get(i).ConditionKey);
                        c.remove(rule.get(i).ConditionKey);
                    } else {
                        result = false;
                    }
                }
                else if (rule.get(i).ConditionOp.equals("<=")) {
                    if (Integer.valueOf(c.get(rule.get(i).ConditionKey)) <= value) {
                        
                        count++;
                        rule.get(i).checked = true;
                        doneSet.add(rule.get(i).ConditionKey);
                        c.remove(rule.get(i).ConditionKey);

                    } else {
                        result = false;
                    }
                }
                else if (rule.get(i).ConditionOp.equals("==")) {
                    if (Integer.valueOf(c.get(rule.get(i).ConditionKey)) == value) {
                        count++;
                        rule.get(i).checked = true;
                        doneSet.add(rule.get(i).ConditionKey);
                        c.remove(rule.get(i).ConditionKey);
                    } else {
                        result = false;

                    }
                }
            } else {
            	System.out.println("234353345 435Hellooduhwedheocheuocni");
                value_ = rule.get(i).ConditionValue;
                if (c.get(rule.get(i).ConditionKey).equals(value_)) {
                	System.out.println("Hellooduhwedheocheuocni");
                    count++;
                    rule.get(i).checked = true;
                    doneSet.add(rule.get(i).ConditionKey);
                    c.remove(rule.get(i).ConditionKey);

                } else {
                    result = false;
                }
            }

        }
        }
        
        if (count == rule.size()) {
        	System.out.println("Match");
            result = true;

        } else {
        	System.out.println("No Match");
            result = false;
        }
        return result;
    }

//    public static void main(String[] args) {
//        // TODO code application logic here
//    	Tree t1 = new Tree();
//
//        
//        Conditions temp12 = new Conditions();
//        temp12.setKey("1");
//        temp12.setOp("==");
//        temp12.setValue("1");
//        ArrayList<Conditions> rules8 = new ArrayList<Conditions>();
//        rules8.add(temp12);
//        Node n8 = new Node();
//        n8.setConclusion("TrueNode");
//        n8.setConditions(rules8);
//        n8.setRuleID(1);
//        n8.setParentID(0);
//        n8.rootNode = true;
//        
//        
//        
//        Conditions temp = new Conditions();
//        temp.setKey("FPG");
//        temp.setOp("<=");
//        temp.setValue("100");
//        Conditions temp1 = new Conditions();
//        temp1.setKey("Symptoms");
//        temp1.setOp("==");
//        temp1.setValue("Yes");
//        ArrayList<Conditions> rules = new ArrayList<Conditions>();
//        rules.add(temp);
//        rules.add(temp1);
//        Node n1 = new Node();
//        n1.setConclusion("Diabetes");
//        n1.setConditions(rules);
//        n1.setRuleID(2);
//        n1.setParentID(1);
//        
//
//        Conditions temp2 = new Conditions();
//        temp2.setKey("ABC");
//        temp2.setOp("==");
//        temp2.setValue("Black");
//        ArrayList<Conditions> rules2 = new ArrayList<Conditions>();
//        rules2.add(temp2);
//        Node n2 = new Node();
//        n2.setConclusion("YEs Diabetes");
//        n2.setConditions(rules2);
//        n2.setRuleID(3);
//        n2.setParentID(1);
//
//        Conditions temp3 = new Conditions();
//        temp3.setKey("Raza");
//        temp3.setOp("<=");
//        temp3.setValue("50"); 
//        Conditions temp4 = new Conditions();
//        temp4.setKey("Atif");
//        temp4.setOp("<");
//        temp4.setValue("100");
//        ArrayList<Conditions> rules3 = new ArrayList<Conditions>();
//        rules3.add(temp3);
//        rules3.add(temp4);
//        Node n3 = new Node();
//        n3.setConclusion("CoolBoys");
//        n3.setConditions(rules3);
//        n3.setRuleID(4);
//        n3.setParentID(2);
//
//
//        t1.InsertNode(n8);
//        t1.InsertNode(n1);
//        t1.InsertNode(n2);
//        t1.InsertNode(n3);
//        
//        HashMap c1 = new HashMap<String, String>();
//        c1.put("1", "1");
//
//        c1.put("ABC", "Black");
//        t1.tempCase= c1;
//        t1.printTree();
//        System.out.println("\n\nCase to be tested: "+ c1.toString());
//
//        Node retur = t1.searchTree(t1.Nodes.get(0), c1);
//
//        if(retur.isTrue==true)
//        {
//            System.out.println("Case Complete Match: "+ retur.getConclusion());
//        }
//        else
//        {
//            System.out.println("No Complete Match, Last True: "+ retur.getConclusion());
//
//        }
//        System.out.println(retur.getConclusion());
//
//
//        System.out.println(t1.tempCase.toString());
//
//    }


}
