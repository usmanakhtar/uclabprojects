package org.uclab.IMP.datamodel.RDR;

import java.util.HashMap;

public class LogMatcher {

	private float Score;
	private int Freq;
	private int ID;
	private HashMap<String, String> Case;
	private String MatchedRules;
	
		
	public LogMatcher()
	{
		Score=0;
		Freq=1;
		ID=0;
		Case= new HashMap<>();
		MatchedRules=null;
	}
	public float getScore() {
		return Score;
	}

	public void setScore(float score) {
		Score = score;
	}

	public int getFreq() {
		return Freq;
	}

	public void setFreq(int freq) {
		Freq = freq;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public HashMap<String, String> getCase() {
		return Case;
	}

	public void setCase(HashMap<String, String> case1) {
		Case = case1;
	}

	public String getMatchedRules() {
		return MatchedRules;
	}
	
	public void IncrementFreq() {
		this.Freq++;
	}

	public void setMatchedRules(String matchedRules) {
		MatchedRules = matchedRules;
	}	
	
	public void displayOne()
	{
		System.out.println("--------------------------");
		System.out.println("SCORE: "+ this.Score);
		System.out.println("FREQ: "+ this.Freq);
		System.out.println("ID: "+ this.ID);
		System.out.println("CASE: "+ this.Case.toString());
		System.out.println("MATCHED: "+ this.MatchedRules);
		System.out.println("--------------------------");
	}
}
