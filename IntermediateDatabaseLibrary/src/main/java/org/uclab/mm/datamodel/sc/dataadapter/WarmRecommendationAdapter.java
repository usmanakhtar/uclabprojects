/*
 Copyright [2016] [Taqdir Ali]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

package org.uclab.mm.datamodel.sc.dataadapter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.sc.WarmRecommendation;

/**
 * This is WarmWarmRecommendationAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir Ali
 */
public class WarmRecommendationAdapter implements DataAccessInterface {

	 private Connection objConn;
	    private static final Logger logger = LoggerFactory.getLogger(WarmRecommendationAdapter.class);
	    public WarmRecommendationAdapter()
	    {
	        
	    }
	    
	    /**
	     * This is implementation function for saving WarmRecommendation. 
	     * @param objWarmRecommendation
	     * @return List of String
	     */
	    @Override
	    public List<String> Save(Object objWarmRecommendation) {
	        WarmRecommendation objInnerWarmRecommendation = new WarmRecommendation();
	        objInnerWarmRecommendation =  (WarmRecommendation) objWarmRecommendation;
	        List<String> objDbResponse = new ArrayList<>();
	        
	        try
	        {

	            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_WarmRecommendation(?, ?, ?, ?, ?, ?)}");
	            
	            objCallableStatement.setLong("UserID", objInnerWarmRecommendation.getUserID());
	            objCallableStatement.setString("RecommendationDescription", objInnerWarmRecommendation.getRecommendationDescription());
	            objCallableStatement.setInt("RecommendationTypeID", objInnerWarmRecommendation.getRecommendationTypeID());
	            objCallableStatement.setInt("RecommendationStatusID", objInnerWarmRecommendation.getRecommendationStatusID());
	            
	             SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm	
	            Date dtDate = sdf.parse(objInnerWarmRecommendation.getRecommendationDate());
	            
	            Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), dtDate.getHours(), dtDate.getMinutes(), dtDate.getSeconds(), 00);
	            objCallableStatement.setTimestamp("RecommendationDate", tsDate);
	            
	            objCallableStatement.registerOutParameter("RecommendationID", Types.BIGINT);
	            objCallableStatement.execute();
	            
	            Long intWarmRecommendationID = objCallableStatement.getLong("RecommendationID");
	            objDbResponse.add(String.valueOf(intWarmRecommendationID));
	            objDbResponse.add("No Error");

	            objConn.close();
	            logger.info("Warm Recommendation saved successfully, Warm Recommendation Details="+objWarmRecommendation);
	        }
	        catch (Exception e)
	        {
	        	logger.info("Error in adding WarmRecommendation");
	        	objDbResponse.add("Error in adding WarmRecommendation");
	        } 
	        return objDbResponse;
	    }

	    /**
	     * This is implementation function for updating Warm Recommendation. 
	     * @param objWarmRecommendation
	     * @return List of String
	     */
	    @Override
	    public List<String> Update(Object objWarmRecommendation) {
	        WarmRecommendation objInnerWarmRecommendation = new WarmRecommendation();
	        objInnerWarmRecommendation =  (WarmRecommendation) objWarmRecommendation;
	        List<String> objDbResponse = new ArrayList<>();
	        
	        try
	        {

	            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_WarmRecommendation(?, ?, ?, ?, ?, ?)}");
	            
	            objCallableStatement.setLong("RecommendationID", objInnerWarmRecommendation.getRecommendationID());
	            objCallableStatement.setLong("UserID", objInnerWarmRecommendation.getUserID());
	            objCallableStatement.setString("RecommendationDescription", objInnerWarmRecommendation.getRecommendationDescription());
	            objCallableStatement.setInt("RecommendationTypeID", objInnerWarmRecommendation.getRecommendationTypeID());
	            objCallableStatement.setInt("RecommendationStatusID", objInnerWarmRecommendation.getRecommendationStatusID());
	            
	             SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm	
	            Date dtDate = sdf.parse(objInnerWarmRecommendation.getRecommendationDate());
	            
	            Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), dtDate.getHours(), dtDate.getMinutes(), dtDate.getSeconds(), 00);
	            objCallableStatement.setTimestamp("RecommendationDate", tsDate);
	            
	            objCallableStatement.execute();
	            
	            Long intWarmRecommendationID = objInnerWarmRecommendation.getRecommendationID();
	            objDbResponse.add(String.valueOf(intWarmRecommendationID));
	            objDbResponse.add("No Error");

	            objConn.close();
	            logger.info("Warm Recommendation saved successfully, Warm Recommendation Details="+objWarmRecommendation);
	        }
	        catch (Exception e)
	        {
	        	logger.info("Error in updating WarmRecommendation");
	        	objDbResponse.add("Error in updating WarmRecommendation");
	        } 
	        return objDbResponse;
	    }

	    /**
	     * This is implementation function for retrieving WarmRecommendation. 
	     * @param objWarmRecommendation
	     * @return List of WarmRecommendation
	     */
	    @Override
	    public List<WarmRecommendation> RetriveData(Object objWarmRecommendation) {
	        WarmRecommendation objOuterWarmRecommendation = new WarmRecommendation();
	        List<WarmRecommendation> objListInnerWarmRecommendation = new ArrayList<WarmRecommendation>();
	        objOuterWarmRecommendation =  (WarmRecommendation) objWarmRecommendation;
	        
	        try
	        {
	            CallableStatement objCallableStatement = null;
	            if(objOuterWarmRecommendation.getRequestType() == "ByUserOnly")
	            {
	                objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_WarmRecommendationByUser(?)}");
	                objCallableStatement.setLong("UserID", objOuterWarmRecommendation.getUserID());
	            }
	            else if(objOuterWarmRecommendation.getRequestType() == "ByUserandDate")
	            {
	                objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_WarmRecommendationByUserIDDate(?, ?, ?)}");
	                
	                SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm	
	               Date dtStartDate = sdf.parse(objOuterWarmRecommendation.getStartDate());
	               Timestamp tsStartDate = new Timestamp(dtStartDate.getYear(),dtStartDate.getMonth(), dtStartDate.getDate(), dtStartDate.getHours(), dtStartDate.getMinutes(),dtStartDate.getSeconds(),0);

	               Date dtEndDate =sdf.parse( objOuterWarmRecommendation.getEndDate()); //.getTime();
	               Timestamp tsEndDate = new Timestamp(dtEndDate.getYear(),dtEndDate.getMonth(), dtEndDate.getDate(), dtEndDate.getHours(), dtEndDate.getMinutes(),dtEndDate.getSeconds(),0);
	            
	                objCallableStatement.setLong("UserID", objOuterWarmRecommendation.getUserID());
	                objCallableStatement.setTimestamp("StartTime", tsStartDate);
	                objCallableStatement.setTimestamp("EndTime", tsEndDate);
	            }
	           
	            ResultSet objResultSet = objCallableStatement.executeQuery();

	            while(objResultSet.next())
	            {
	                WarmRecommendation objInnerWarmRecommendation = new WarmRecommendation();
	                objInnerWarmRecommendation.setRecommendationID(objResultSet.getLong("RecommendationID"));
	                objInnerWarmRecommendation.setUserID(objResultSet.getLong("UserID"));
	                objInnerWarmRecommendation.setRecommendationDescription(objResultSet.getString("RecommendationDescription"));
	                objInnerWarmRecommendation.setRecommendationTypeID(objResultSet.getInt("RecommendationTypeID"));
	                objInnerWarmRecommendation.setRecommendationStatusID(objResultSet.getInt("RecommendationStatusID"));
	                
	                if(objResultSet.getTimestamp("RecommendationDate") != null)
	                {
	                    Timestamp tsWarmRecommendationDate = objResultSet.getTimestamp("RecommendationDate");
	                    objInnerWarmRecommendation.setRecommendationDate(tsWarmRecommendationDate.toString());
	                }
	                   
	               objInnerWarmRecommendation.setRecommendationTypeDescription(objResultSet.getString("RecommendationTypeDescription"));
	               objInnerWarmRecommendation.setRecommendationStatusDescription(objResultSet.getString("RecommendationStatusDescription"));
	                              
	               objListInnerWarmRecommendation.add(objInnerWarmRecommendation);
	            }
	            objConn.close();
	            logger.info("Warm Recommendation loaded successfully");
	        }
	        catch (Exception e)
	        {
	        	logger.info("Error in loading Warm Recommendation");
	        }   
	        return objListInnerWarmRecommendation;
	    }

	    @Override
	    public <T> List<T> Delete(T objEntity) {
	        throw new UnsupportedOperationException("Not supported yet."); 
	    }

	    /**
	     * This is implementation function for connection database.
	     * @param objConf
	     */
	    @Override
	     public void ConfigureAdapter(Object objConf) {
	         try
	         {
	            objConn = (Connection)objConf;
	            logger.info("Database connected successfully");
	         }
	         catch(Exception ex)
	         {
	        	 logger.info("Error in connection to Database");
	         }
	    }

}
