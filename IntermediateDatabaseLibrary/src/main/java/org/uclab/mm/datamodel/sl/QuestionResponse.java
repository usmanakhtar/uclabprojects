package org.uclab.mm.datamodel.sl;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.GregorianCalendar;

/**
*
* @author Taqdir
*/
@Entity
public class QuestionResponse {

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Long questionResponseID;
    private Long questionID;
    private String questionResponseDescription;
    private int responseWeight;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getQuestionResponseID() {
		return questionResponseID;
	}
	public void setQuestionResponseID(Long questionResponseID) {
		this.questionResponseID = questionResponseID;
	}
	public Long getQuestionID() {
		return questionID;
	}
	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}
	public String getQuestionResponseDescription() {
		return questionResponseDescription;
	}
	public void setQuestionResponseDescription(String questionResponseDescription) {
		this.questionResponseDescription = questionResponseDescription;
	}
	public int getResponseWeight() {
		return responseWeight;
	}
	public void setResponseWeight(int responseWeight) {
		this.responseWeight = responseWeight;
	}
       
}
