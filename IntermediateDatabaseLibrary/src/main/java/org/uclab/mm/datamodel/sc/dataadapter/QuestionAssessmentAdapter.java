package org.uclab.mm.datamodel.sc.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.sc.QuestionAssessment;

public class QuestionAssessmentAdapter implements DataAccessInterface{
  
  private Connection objConn;
  private static final Logger logger = LoggerFactory.getLogger(QuestionAssessmentAdapter.class);
  
  public QuestionAssessmentAdapter(){}
  
  @Override
  public List<String> Save(Object objQuestionAssessment) {
    QuestionAssessment objInnerQuestionAssessment = new QuestionAssessment();
    objInnerQuestionAssessment =  (QuestionAssessment) objQuestionAssessment;
    List<String> objDbResponse = new ArrayList<>();
    
    try
    {

        CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_QuestionAssessment(?, ?, ?, ?, ?, ?)}");
        
        objCallableStatement.setLong("UserID", objInnerQuestionAssessment.getUserID());
        objCallableStatement.setString("Questions", objInnerQuestionAssessment.getQuestions());
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
        Date dtTimestamp = sdf.parse(objInnerQuestionAssessment.getCreatedDate());
        Timestamp tsTimestamp = new Timestamp(dtTimestamp.getYear(),dtTimestamp.getMonth(), dtTimestamp.getDate(), dtTimestamp.getHours(), dtTimestamp.getMinutes(), dtTimestamp.getSeconds(), 00);
        objCallableStatement.setTimestamp("CreatedDate", tsTimestamp);
        
        objCallableStatement.setInt("Status", objInnerQuestionAssessment.getStatus());
        objCallableStatement.setString("Week", objInnerQuestionAssessment.getWeek());
                               
        objCallableStatement.registerOutParameter("QuestionAssessmentID", Types.BIGINT);
        objCallableStatement.execute();
        
        Long intQuestionAssessmentID = objCallableStatement.getLong("QuestionAssessmentID");
        objDbResponse.add(String.valueOf(intQuestionAssessmentID));
        objDbResponse.add("No Error");

        objConn.close();
        logger.info("Question Assessment saved successfully, Question Assessment Details="+objQuestionAssessment);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      logger.info("Error in adding Question Assessment");
      objDbResponse.add("Error in adding Question Assessment");
    } 
    return objDbResponse;
  }

  @Override
  public List<String> Update(Object objQuestionAssessment) {
    QuestionAssessment objInnerQuestionAssessment = new QuestionAssessment();
    objInnerQuestionAssessment =  (QuestionAssessment) objQuestionAssessment;
    List<String> objDbResponse = new ArrayList<>();
    
    try
    {

        CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_QuestionAssessment(?, ?, ?, ?, ?, ?)}");
        
        objCallableStatement.setLong("QuestionAssessmentID", objInnerQuestionAssessment.getQuestionAssessmentID());
        objCallableStatement.setLong("UserID", objInnerQuestionAssessment.getUserID());
        objCallableStatement.setString("Questions", objInnerQuestionAssessment.getQuestions());

        objCallableStatement.setInt("Status", objInnerQuestionAssessment.getStatus());
        objCallableStatement.setString("Week", objInnerQuestionAssessment.getWeek());
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm  
        Date dtDate = sdf.parse(objInnerQuestionAssessment.getCreatedDate());
        
        Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), dtDate.getHours(), dtDate.getMinutes(), dtDate.getSeconds(), 00);
        objCallableStatement.setTimestamp("createdDate", tsDate);
        
        objCallableStatement.execute();
        
        Long intQuestionAssessmentID = objInnerQuestionAssessment.getQuestionAssessmentID();
        objDbResponse.add(String.valueOf(intQuestionAssessmentID));
        objDbResponse.add("No Error");

        objConn.close();
        logger.info("QuestionAssessment updated successfully, QuestionAssessment Details="+objQuestionAssessment);
    }
    catch (Exception e)
    {
      logger.info("Error in updating QuestionAssessment");
      objDbResponse.add("Error in updating QuestionAssessment");
    } 
    return objDbResponse;
  }

  @Override
  public List<QuestionAssessment> RetriveData(Object objQuestionAssessment){
      QuestionAssessment objOuterQuestionAssessment = new QuestionAssessment();
      List<QuestionAssessment> objListInnerQuestionAssessment = new ArrayList<QuestionAssessment>();
      objOuterQuestionAssessment =  (QuestionAssessment) objQuestionAssessment;
      
      try
      {
          CallableStatement objCallableStatement = null;
          if(objOuterQuestionAssessment.getRequestType() == "ByQuestionAssessmentID")
          {
              objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionAssessmentByQuestionAssessmentID(?)}");
              objCallableStatement.setLong("QuestionAssessmentID", objOuterQuestionAssessment.getQuestionAssessmentID());
              
          }else if(objOuterQuestionAssessment.getRequestType() == "ByUserID"){
              
              objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionAssessmentByUserID(?)}");
              objCallableStatement.setLong("UserID", objOuterQuestionAssessment.getUserID());
          }
          else if(objOuterQuestionAssessment.getRequestType() == "ByCreatedDate")
          {
              objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionAssessmentByCreatedDate(?, ?, ?)}");
              objCallableStatement.setLong("UserID", objOuterQuestionAssessment.getUserID());
              
              SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm  
          
              Date dtStartTimeStamp = sdf.parse(objOuterQuestionAssessment.getStartTimestamp());
              Timestamp tsStartTimeStamp = new Timestamp(dtStartTimeStamp.getYear(),dtStartTimeStamp.getMonth(), dtStartTimeStamp.getDate(), dtStartTimeStamp.getHours(), dtStartTimeStamp.getMinutes(), dtStartTimeStamp.getSeconds(), 00);
              objCallableStatement.setTimestamp("StartTimeStamp", tsStartTimeStamp);

              Date dtEndTimeStamp = sdf.parse(objOuterQuestionAssessment.getEndTimestamp());
              Timestamp tsEndTimeStamp = new Timestamp(dtEndTimeStamp.getYear(),dtEndTimeStamp.getMonth(), dtEndTimeStamp.getDate(), dtEndTimeStamp.getHours(), dtEndTimeStamp.getMinutes(), dtEndTimeStamp.getSeconds(), 00);
              objCallableStatement.setTimestamp("EndTimeStamp", tsEndTimeStamp);
          }
          else if(objOuterQuestionAssessment.getRequestType() == "ByUserAndWeek")
          {
            objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionAssessmentByUserAndWeek(?, ?)}");
            objCallableStatement.setLong("UserID", objOuterQuestionAssessment.getUserID());
            objCallableStatement.setString("Week", objOuterQuestionAssessment.getWeek());
        }
          
          ResultSet objResultSet = objCallableStatement.executeQuery();

          while(objResultSet.next())
          {
              QuestionAssessment objInnerQuestionAssessment = new QuestionAssessment();
              objInnerQuestionAssessment.setQuestionAssessmentID(objResultSet.getLong("QuestionAssessmentID"));
              objInnerQuestionAssessment.setUserID(objResultSet.getLong("UserID"));
              objInnerQuestionAssessment.setQuestions(objResultSet.getString("Questions"));
              
              Timestamp tsTimestamp = objResultSet.getTimestamp("CreatedDate"); //updated
              objInnerQuestionAssessment.setCreatedDate(tsTimestamp.toString());
              objInnerQuestionAssessment.setStatus(objResultSet.getInt("Status"));
              objInnerQuestionAssessment.setWeek(objResultSet.getString("Week"));
              
              objListInnerQuestionAssessment.add(objInnerQuestionAssessment);
          }
          objConn.close();
          logger.info("Question Assessment loaded successfully");
      }
      catch (Exception e)
      {
        logger.info("Error in loading Question Assessment");
      }   
      return objListInnerQuestionAssessment;
  }

  @Override
  public <T> List<T> Delete(T objEntity) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void ConfigureAdapter(Object objConf) {
    try
    {
       objConn = (Connection)objConf;
       logger.info("Database connected successfully");
    }
    catch(Exception ex)
    {
      logger.info("Error in connection to Database");
    }
    
  }

}
