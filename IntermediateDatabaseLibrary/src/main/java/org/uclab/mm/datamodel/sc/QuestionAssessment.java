package org.uclab.mm.datamodel.sc;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class QuestionAssessment implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long questionAssessmentID;
  private Long userID;
  private String questions;
  private String createdDate;
  private Integer status;
  private String week;
  
  private String requestType;
  private String startTimestamp;
  public String getWeek() {
	return week;
}
public void setWeek(String week) {
	this.week = week;
}

private String endTimestamp;
  
  public Long getQuestionAssessmentID() {
    return questionAssessmentID;
  }
  public void setQuestionAssessmentID(Long questionAssessmentID) {
    this.questionAssessmentID = questionAssessmentID;
  }
  public Long getUserID() {
    return userID;
  }
  public void setUserID(Long userID) {
    this.userID = userID;
  }
  public String getQuestions() {
    return questions;
  }
  public void setQuestions(String questions) {
    this.questions = questions;
  }
  public String getCreatedDate() {
    return createdDate;
  }
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }
  public Integer getStatus() {
    return status;
  }
  public void setStatus(Integer status) {
    this.status = status;
  }
  public String getRequestType() {
    return requestType;
  }
  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }
  public String getStartTimestamp() {
    return startTimestamp;
  }
  public void setStartTimestamp(String startTimestamp) {
    this.startTimestamp = startTimestamp;
  }
  public String getEndTimestamp() {
    return endTimestamp;
  }
  public void setEndTimestamp(String endTimestamp) {
    this.endTimestamp = endTimestamp;
  }
  
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime
        * result
        + ((questionAssessmentID == null) ? 0 : questionAssessmentID.hashCode());
    return result;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    QuestionAssessment other = (QuestionAssessment) obj;
    if (questionAssessmentID == null) {
      if (other.questionAssessmentID != null)
        return false;
    } else if (!questionAssessmentID.equals(other.questionAssessmentID))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return "QuestionAssessment [questionAssessmentID=" + questionAssessmentID
        + ", userID=" + userID + ", questions=" + questions + ", createdDate="
        + createdDate + ", status=" + status + ", requestType="+requestType+"]";
  }
  
}
