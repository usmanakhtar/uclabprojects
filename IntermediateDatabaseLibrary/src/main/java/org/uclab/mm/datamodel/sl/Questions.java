package org.uclab.mm.datamodel.sl;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
*
* @author Taqdir
*/
@Entity
public class Questions {

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Long questionID;
    private int questionnaireTypeID;
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getQuestionID() {
		return questionID;
	}
	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}
	public int getQuestionnaireTypeID() {
		return questionnaireTypeID;
	}
	public void setQuestionnaireTypeID(int questionnaireTypeID) {
		this.questionnaireTypeID = questionnaireTypeID;
	}
	public String getQuestionDescription() {
		return questionDescription;
	}
	public void setQuestionDescription(String questionDescription) {
		this.questionDescription = questionDescription;
	}
	public String getQuestionnaireTypeDescription() {
		return questionnaireTypeDescription;
	}
	public void setQuestionnaireTypeDescription(String questionnaireTypeDescription) {
		this.questionnaireTypeDescription = questionnaireTypeDescription;
	}
	public List<QuestionResponse> getObjListQuestionResponse() {
		return objListQuestionResponse;
	}
	public void setObjListQuestionResponse(List<QuestionResponse> objListQuestionResponse) {
		this.objListQuestionResponse = objListQuestionResponse;
	}
	private String questionDescription;
    private String questionnaireTypeDescription;
    private  List<QuestionResponse> objListQuestionResponse = new ArrayList<QuestionResponse>();
    
}
