/*
 Copyright [2016] [Taqdir Ali]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

package org.uclab.mm.datamodel.dc.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.dc.UsersVitalSign;

/**
 * This is UsersVitalSignAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class UsersVitalSignAdapter implements DataAccessInterface {

    private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(UsersVitalSignAdapter.class);
    public UsersVitalSignAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving UsersVitalSign. 
     * @param objUsersVitalSign
     * @return List of String
     */
    
   	@Override
	public  List<String> Save(Object objUsersVitalSign) {
   		UsersVitalSign objInnerUsersVitalSign = new UsersVitalSign();
        objInnerUsersVitalSign =  (UsersVitalSign) objUsersVitalSign;
        List<String> objDbResponse = new ArrayList<>();
        
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_UsersVitalSign(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            
            objCallableStatement.setLong("UserID", objInnerUsersVitalSign.getUserID());
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
            Date dtDate = sdf.parse(objInnerUsersVitalSign.getVitalSignRecordDate());
            Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), 00, 00, 00, 00);
            objCallableStatement.setTimestamp("VitalSignRecordDate", tsDate);
            objCallableStatement.setFloat("SystolicBloodPressure", objInnerUsersVitalSign.getSystolicBloodPressure());
            objCallableStatement.setFloat("DiastolicBloodPressure", objInnerUsersVitalSign.getDiastolicBloodPressure());
            
            objCallableStatement.setFloat("BloodSugarRandom", objInnerUsersVitalSign.getBloodSugarRandom());
            objCallableStatement.setFloat("BloodSugarFasting", objInnerUsersVitalSign.getBloodSugarFasting());
            objCallableStatement.setFloat("CholesterolLevel", objInnerUsersVitalSign.getCholesterolLevel());
            objCallableStatement.setFloat("TriglyceridesLevel", objInnerUsersVitalSign.getTriglyceridesLevel());
            
            objCallableStatement.registerOutParameter("UserVitalSignID", Types.BIGINT);
            objCallableStatement.execute();
            
            int intUserVitalSignID = objCallableStatement.getInt("UserVitalSignID");
            objDbResponse.add(String.valueOf(intUserVitalSignID));
            objDbResponse.add("No Error");

            objConn.close();
            logger.info("Users Vital Sign saved successfully, User Details="+objUsersVitalSign);
        }
        catch (Exception e)
        {
             e.printStackTrace();
             objDbResponse.add("Error in adding Users Vital Sign");
             logger.info("Error in adding Users Vital Sign");
        } 
        return objDbResponse;
	}

    /**
     * This is implementation function for updating UsersVitalSign. 
     * @param objUsersVitalSign
     * @return List of String
     */
     @Override
	public List<String> Update(Object objUsersVitalSign) {
    	 UsersVitalSign objInnerUsersVitalSign = new UsersVitalSign();
         objInnerUsersVitalSign =  (UsersVitalSign) objUsersVitalSign;
         List<String> objDbResponse = new ArrayList<>();
         
         try
         {

             CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_UsersVitalSign(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
             
             objCallableStatement.setLong("UserVitalSignID", objInnerUsersVitalSign.getUserVitalSignID());
             objCallableStatement.setLong("UserID", objInnerUsersVitalSign.getUserID());
             
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
             Date dtDate = sdf.parse(objInnerUsersVitalSign.getVitalSignRecordDate());
             Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("VitalSignRecordDate", tsDate);
             objCallableStatement.setFloat("SystolicBloodPressure", objInnerUsersVitalSign.getSystolicBloodPressure());
             objCallableStatement.setFloat("DiastolicBloodPressure", objInnerUsersVitalSign.getDiastolicBloodPressure());
             
             objCallableStatement.setFloat("BloodSugarRandom", objInnerUsersVitalSign.getBloodSugarRandom());
             objCallableStatement.setFloat("BloodSugarFasting", objInnerUsersVitalSign.getBloodSugarFasting());
             objCallableStatement.setFloat("CholesterolLevel", objInnerUsersVitalSign.getCholesterolLevel());
             objCallableStatement.setFloat("TriglyceridesLevel", objInnerUsersVitalSign.getTriglyceridesLevel());
             
             objCallableStatement.execute();
             
             objDbResponse.add(String.valueOf(objInnerUsersVitalSign.getUserVitalSignID()));
             objDbResponse.add("No Error");

             objConn.close();
             logger.info("Physiological factor updated successfully, User Details="+objUsersVitalSign);
         }
         catch (Exception e)
         {
         	logger.info("Error in updated Users Vital Sign");
         	objDbResponse.add("Error in updated Users Vital Sign");
         } 
         return objDbResponse;
	}

     /**
      * This is implementation function for retrieving UsersVitalSign.
      * @param objUsersVitalSign
      * @return List of UsersVitalSign 
      */
	@Override
	public List<UsersVitalSign> RetriveData(Object objUsersVitalSign) {
        UsersVitalSign objOuterUsersVitalSign = new UsersVitalSign();
        List<UsersVitalSign> objListInnerUsersVitalSign = new ArrayList<UsersVitalSign>();
        objOuterUsersVitalSign =  (UsersVitalSign) objUsersVitalSign;
        
        try
        {
        	CallableStatement objCallableStatement = null;
        	if(objOuterUsersVitalSign.getRequestType() == "ByUserOnly")
            {
        		objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_UsersVitalSignByUserID(?)}");
                objCallableStatement.setLong("UserID", objOuterUsersVitalSign.getUserID());
            }
            else if(objOuterUsersVitalSign.getRequestType() == "ByUserandDate")
            {
                objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_UsersVitalSignByUserIDDate(?, ?, ?)}");
                
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm	
               Date dtStartDate = sdf.parse(objOuterUsersVitalSign.getStartDate());
               Timestamp tsStartDate = new Timestamp(dtStartDate.getYear(),dtStartDate.getMonth(), dtStartDate.getDate(), dtStartDate.getHours(), dtStartDate.getMinutes(),dtStartDate.getSeconds(),0);

               Date dtEndDate =sdf.parse( objOuterUsersVitalSign.getEndDate()); //.getTime();
               Timestamp tsEndDate = new Timestamp(dtEndDate.getYear(),dtEndDate.getMonth(), dtEndDate.getDate(), dtEndDate.getHours(), dtEndDate.getMinutes(),dtEndDate.getSeconds(),0);
            
                objCallableStatement.setLong("UserID", objOuterUsersVitalSign.getUserID());
                objCallableStatement.setTimestamp("StartDate", tsStartDate);
                objCallableStatement.setTimestamp("EndDate", tsEndDate);
            }
            
            ResultSet objResultSet = objCallableStatement.executeQuery();

            while(objResultSet.next())
            {
                UsersVitalSign objInnerUsersVitalSign = new UsersVitalSign();
                objInnerUsersVitalSign.setUserVitalSignID(objResultSet.getLong("UserVitalSignID"));
                objInnerUsersVitalSign.setUserID(objResultSet.getLong("UserID"));
                                
                Timestamp tsDate = objResultSet.getTimestamp("VitalSignRecordDate");
                objInnerUsersVitalSign.setVitalSignRecordDate(tsDate.toString());
                
                objInnerUsersVitalSign.setSystolicBloodPressure(objResultSet.getFloat("SystolicBloodPressure"));
                objInnerUsersVitalSign.setDiastolicBloodPressure(objResultSet.getFloat("DiastolicBloodPressure"));
                
                objInnerUsersVitalSign.setBloodSugarRandom(objResultSet.getFloat("BloodSugarRandom"));
                objInnerUsersVitalSign.setBloodSugarFasting(objResultSet.getFloat("BloodSugarFasting"));
                objInnerUsersVitalSign.setCholesterolLevel(objResultSet.getFloat("CholesterolLevel"));
                objInnerUsersVitalSign.setTriglyceridesLevel(objResultSet.getFloat("TriglyceridesLevel"));
                                               
                objListInnerUsersVitalSign.add(objInnerUsersVitalSign);
            }
            objConn.close();
            logger.info("Users Vital Sign is loaded successfully");
            
        }
        catch (Exception e)
        {
             logger.info("Error in Loading Users Vital Sign ");
        }   
        return objListInnerUsersVitalSign;
    }

    /**
     * This is implementation function for retrieving Users Vital Sign.
     * Not implemented
     */
	@Override
	public <T> List<T> Delete(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}

    /**
     * This is implementation function for connection database.
     * @param objConf
     */
	@Override
	 public void ConfigureAdapter(Object objConf) {
        try
        {
           objConn = (Connection)objConf;
           logger.info("Database connected successfully");
        }
        catch(Exception ex)
        {
       	 logger.info("Error in connection to Database");
        }
   }


}
