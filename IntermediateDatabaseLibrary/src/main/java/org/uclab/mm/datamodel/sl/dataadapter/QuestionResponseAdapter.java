package org.uclab.mm.datamodel.sl.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.sl.QuestionResponse;

/**
 * This is QuestionResponseAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class QuestionResponseAdapter implements DataAccessInterface {
	 private Connection objConn;
	    private static final Logger logger = LoggerFactory.getLogger(QuestionResponseAdapter.class);
	    public QuestionResponseAdapter()
	    {
	        
	    }
	    
	    /**
	     * This is implementation function for saving QuestionResponseAdapter. 
	     * @param objQuestionResponse
	     * @return List of String
	     */
	    
	   	@Override
		public  List<String> Save(Object objQuestionResponse) {
	   		QuestionResponse objInnerQuestionResponse = new QuestionResponse();
	        objInnerQuestionResponse =  (QuestionResponse) objQuestionResponse;
	        List<String> objDbResponse = new ArrayList<>();
	        
	        try
	        {

	            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_QuestionResponse(?, ?, ?, ?)}");
	            
	            	            
	            objCallableStatement.setLong("QuestionID", objInnerQuestionResponse.getQuestionID());
	            objCallableStatement.setString("QuestionResponseDescription", objInnerQuestionResponse.getQuestionResponseDescription());
	            objCallableStatement.setInt("ResponseWeight", objInnerQuestionResponse.getResponseWeight());
	           	            
	            objCallableStatement.registerOutParameter("QuestionResponseID", Types.BIGINT);
	            objCallableStatement.execute();
	            
	            int intQuestionResponseID = objCallableStatement.getInt("QuestionResponseID");
	            objDbResponse.add(String.valueOf(intQuestionResponseID));
	            objDbResponse.add("No Error");

	            objConn.close();
	            logger.info("Question Response saved successfully, Question Response ="+objQuestionResponse);
	        }
	        catch (Exception e)
	        {
	             e.printStackTrace();
	             objDbResponse.add("Error in adding Question Response");
	             logger.info("Error in adding Question Response");
	        } 
	        return objDbResponse;
		}

	   	/**
	     * This is implementation function for updating QuestionResponseAdapter. 
	     * @param objQuestionResponse
	     * @return List of String
	     */
	     @Override
		public List<String> Update(Object objQuestionResponse) {
	    	 QuestionResponse objInnerQuestionResponse = new QuestionResponse();
	         objInnerQuestionResponse =  (QuestionResponse) objQuestionResponse;
	         List<String> objDbResponse = new ArrayList<>();
	         
	         try
	         {

	             CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_QuestionResponse(?, ?, ?, ?)}");
	             
	             objCallableStatement.setLong("QuestionResponseID", objInnerQuestionResponse.getQuestionResponseID());
	             objCallableStatement.setLong("QuestionID", objInnerQuestionResponse.getQuestionID());
		            objCallableStatement.setString("QuestionResponseDescription", objInnerQuestionResponse.getQuestionResponseDescription());
		            objCallableStatement.setInt("ResponseWeight", objInnerQuestionResponse.getResponseWeight());
	             
	             objCallableStatement.execute();
	             
	             objDbResponse.add(String.valueOf(objInnerQuestionResponse.getQuestionResponseID()));
	             objDbResponse.add("No Error");

	             objConn.close();
	             logger.info("Question Response updated successfully, Question Response="+objQuestionResponse);
	         }
	         catch (Exception e)
	         {
	         	logger.info("Error in updated Question Response");
	         	objDbResponse.add("Error in updated Question Response");
	         } 
	         return objDbResponse;
		}

	     /**
	      * This is implementation function for retrieving QuestionResponse.
	      * @param objUsersVitalSign
	      * @return List of UsersVitalSign 
	      */
		@Override
		public List<QuestionResponse> RetriveData(Object objQuestionResponse) {
			QuestionResponse objOuterQuestionResponse = new QuestionResponse();
	        List<QuestionResponse> objListInnerQuestionResponse = new ArrayList<QuestionResponse>();
	        objOuterQuestionResponse =  (QuestionResponse) objQuestionResponse;
	        
	        try
	        {
	        	CallableStatement objCallableStatement = null;
				objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionResponseByQuestionID(?)}");
			
				objCallableStatement.setLong("QuestionID", objOuterQuestionResponse.getQuestionID());
				            
				ResultSet objResultSet = objCallableStatement.executeQuery();

	            while(objResultSet.next())
	            {
	            	QuestionResponse objInnerQuestionResponse = new QuestionResponse();
	            	objInnerQuestionResponse.setQuestionResponseID(objResultSet.getLong("QuestionResponseID"));
	            	objInnerQuestionResponse.setQuestionID(objResultSet.getLong("QuestionID"));
	            	objInnerQuestionResponse.setQuestionResponseDescription(objResultSet.getString("QuestionResponseDescription"));
	            	objInnerQuestionResponse.setResponseWeight(objResultSet.getInt("ResponseWeight"));
                                               
	            	objListInnerQuestionResponse.add(objInnerQuestionResponse);
	            }
	            objConn.close();
	            logger.info("Users Vital Sign is loaded successfully");
	            
	        }
	        catch (Exception e)
	        {
	             logger.info("Error in Loading Users Vital Sign ");
	        }   
	        return objListInnerQuestionResponse;
	    }

	    /**
	     * This is implementation function for retrieving Users Vital Sign.
	     * Not implemented
	     */
		@Override
		public <T> List<T> Delete(T objEntity) {
			// TODO Auto-generated method stub
			return null;
		}

	    /**
	     * This is implementation function for connection database.
	     * @param objConf
	     */
		@Override
		 public void ConfigureAdapter(Object objConf) {
	        try
	        {
	           objConn = (Connection)objConf;
	           logger.info("Database connected successfully");
	        }
	        catch(Exception ex)
	        {
	       	 logger.info("Error in connection to Database");
	        }
	   }
}
