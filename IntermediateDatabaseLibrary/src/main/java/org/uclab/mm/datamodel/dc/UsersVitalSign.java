/*
 Copyright [2016] [Taqdir Ali]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

package org.uclab.mm.datamodel.dc;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.GregorianCalendar;
/**
 *
 * @author Taqdir
 */
@Entity
public class UsersVitalSign implements Serializable {

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Long userVitalSignID;
    private Long userID;
    private String vitalSignRecordDate;
    private float systolicBloodPressure;
    private float diastolicBloodPressure;
    private float bloodSugarRandom;
    private float bloodSugarFasting;
    private float cholesterolLevel;
    private float triglyceridesLevel;
    
    private String requestType;
    private String startDate;
    private String endDate;
    
       
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserVitalSignID() {
		return userVitalSignID;
	}
	public void setUserVitalSignID(Long userVitalSignID) {
		this.userVitalSignID = userVitalSignID;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public String getVitalSignRecordDate() {
		return vitalSignRecordDate;
	}
	public void setVitalSignRecordDate(String vitalSignRecordDate) {
		this.vitalSignRecordDate = vitalSignRecordDate;
	}
	public float getSystolicBloodPressure() {
		return systolicBloodPressure;
	}
	public void setSystolicBloodPressure(float systolicBloodPressure) {
		this.systolicBloodPressure = systolicBloodPressure;
	}
	public float getDiastolicBloodPressure() {
		return diastolicBloodPressure;
	}
	public void setDiastolicBloodPressure(float diastolicBloodPressure) {
		this.diastolicBloodPressure = diastolicBloodPressure;
	}
	public float getBloodSugarRandom() {
		return bloodSugarRandom;
	}
	public void setBloodSugarRandom(float bloodSugarRandom) {
		this.bloodSugarRandom = bloodSugarRandom;
	}
	public float getBloodSugarFasting() {
		return bloodSugarFasting;
	}
	public void setBloodSugarFasting(float bloodSugarFasting) {
		this.bloodSugarFasting = bloodSugarFasting;
	}
	public float getCholesterolLevel() {
		return cholesterolLevel;
	}
	public void setCholesterolLevel(float cholesterolLevel) {
		this.cholesterolLevel = cholesterolLevel;
	}
	public float getTriglyceridesLevel() {
		return triglyceridesLevel;
	}
	public void setTriglyceridesLevel(float triglyceridesLevel) {
		this.triglyceridesLevel = triglyceridesLevel;
	}

}
