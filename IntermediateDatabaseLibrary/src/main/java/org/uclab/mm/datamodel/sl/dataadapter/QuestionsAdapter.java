package org.uclab.mm.datamodel.sl.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.sl.QuestionResponse;
import org.uclab.mm.datamodel.sl.Questions;

/**
 * This is QuestionResponseAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class QuestionsAdapter implements DataAccessInterface {
	 private Connection objConn;
	    private static final Logger logger = LoggerFactory.getLogger(QuestionsAdapter.class);
	    public QuestionsAdapter()
	    {
	        
	    }
	    
	    /**
	     * This is implementation function for saving QuestionsAdapter. 
	     * @param objQuestionResponse
	     * @return List of String
	     */
	    
	   	@Override
		public  List<String> Save(Object objQuestionResponse) {
	        List<String> objDbResponse = new ArrayList<>();
	        
	       /* Will implement later*/
	        return objDbResponse;
		}

	   	/**
	     * This is implementation function for updating QuestionsAdapter. 
	     * @param objQuestionResponse
	     * @return List of String
	     */
	     @Override
		public List<String> Update(Object objQuestionResponse) {
	    	 List<String> objDbResponse = new ArrayList<>();
		        
		       /* Will implement later*/
		        return objDbResponse;
		}

	     /**
	      * This is implementation function for retrieving Questions.
	      * @param objUsersVitalSign
	      * @return List of UsersVitalSign 
	      */
		@Override
		public List<Questions> RetriveData(Object objQuestions) {
			Questions objOuterQuestions = new Questions();
	        List<Questions> objListInnerQuestions = new ArrayList<Questions>();
	        objOuterQuestions =  (Questions) objQuestions;
	        
	        try
	        {
	        	CallableStatement objCallableStatement = null;
				objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_QuestionsByQuestionID(?)}");
			
				objCallableStatement.setLong("QuestionID", objOuterQuestions.getQuestionID());
				            
				ResultSet objResultSet = objCallableStatement.executeQuery();

	            while(objResultSet.next())
	            {
	            	objOuterQuestions.setQuestionID(objResultSet.getLong("QuestionID"));
	            	objOuterQuestions.setQuestionnaireTypeID(objResultSet.getInt("QuestionnaireTypeID"));
	            	objOuterQuestions.setQuestionDescription(objResultSet.getString("QuestionDescription"));
	            	objOuterQuestions.setQuestionnaireTypeDescription(objResultSet.getString("QuestionnaireTypeDescription"));
	            	                                           
	            	objListInnerQuestions.add(objOuterQuestions);
	            }
	            objConn.close();
	            logger.info("Questions is loaded successfully");
	            
	        }
	        catch (Exception e)
	        {
	             logger.info("Error in Loading Questions ");
	        }   
	        return objListInnerQuestions;
	    }

	    /**
	     * This is implementation function for retrieving Users Vital Sign.
	     * Not implemented
	     */
		@Override
		public <T> List<T> Delete(T objEntity) {
			// TODO Auto-generated method stub
			return null;
		}

	    /**
	     * This is implementation function for connection database.
	     * @param objConf
	     */
		@Override
		 public void ConfigureAdapter(Object objConf) {
	        try
	        {
	           objConn = (Connection)objConf;
	           logger.info("Database connected successfully");
	        }
	        catch(Exception ex)
	        {
	       	 logger.info("Error in connection to Database");
	        }
	   }
}
