/*
 Copyright [2017] [Taqdir Ali]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */


package org.uclab.mm.datamodel.sl.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.mm.datamodel.DataAccessInterface;
import org.uclab.mm.datamodel.sl.UserQuestionnaireResponse;

/**
 * This is UserQuestionnaireResponseAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class UserQuestionnaireResponseAdapter implements DataAccessInterface {

    private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(UserQuestionnaireResponseAdapter.class);
    public UserQuestionnaireResponseAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving User Questionnaire Response. 
     * @param objUserQuestionnaireResponse
     * @return List of String
     */
    @Override
	public  List<String> Save(Object objUserQuestionnaireResponse) {
   		UserQuestionnaireResponse objInnerUserQuestionnaireResponse = new UserQuestionnaireResponse();
        objInnerUserQuestionnaireResponse =  (UserQuestionnaireResponse) objUserQuestionnaireResponse;
        List<String> objDbResponse = new ArrayList<>();
        
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_UserQuestionnaireResponse(?, ?, ?, ?, ?)}");
            
            objCallableStatement.setLong("UserID", objInnerUserQuestionnaireResponse.getUserID());
            objCallableStatement.setLong("QuestionID", objInnerUserQuestionnaireResponse.getQuestionID());
            
        
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
            Date dtDate = sdf.parse(objInnerUserQuestionnaireResponse.getResponseDate());
            Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), 00, 00, 00, 00);
            objCallableStatement.setTimestamp("ResponseDate", tsDate);
            objCallableStatement.setInt("Response", objInnerUserQuestionnaireResponse.getResponse());
                       
            objCallableStatement.registerOutParameter("UserQuestionnaireResponseID", Types.BIGINT);
            objCallableStatement.execute();
            
            int intUserQuestionnaireResponseID = objCallableStatement.getInt("UserQuestionnaireResponseID");
            objDbResponse.add(String.valueOf(intUserQuestionnaireResponseID));
            objDbResponse.add("No Error");

            objConn.close();
            logger.info("User Questionnaire Response saved successfully, User Details="+objUserQuestionnaireResponse);
        }
        catch (Exception e)
        {
             e.printStackTrace();
             objDbResponse.add("Error in adding User Questionnaire Response");
             logger.info("Error in adding User Questionnaire Response");
        } 
        return objDbResponse;
	}


    /**
     * This is implementation function for updating UserQuestionnaireResponse. 
     * @param objUserQuestionnaireResponse
     * @return List of String
     */
     @Override
	public List<String> Update(Object objUserQuestionnaireResponse) {
    	 UserQuestionnaireResponse objInnerUserQuestionnaireResponse = new UserQuestionnaireResponse();
         objInnerUserQuestionnaireResponse =  (UserQuestionnaireResponse) objUserQuestionnaireResponse;
         List<String> objDbResponse = new ArrayList<>();
         
         try
         {

             CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_UserQuestionnaireResponse(?, ?, ?, ?, ?)}");
             
             objCallableStatement.setLong("UserQuestionnaireResponseID", objInnerUserQuestionnaireResponse.getUserQuestionnaireResponseID());
             objCallableStatement.setLong("UserID", objInnerUserQuestionnaireResponse.getUserID());
             objCallableStatement.setLong("QuestionID", objInnerUserQuestionnaireResponse.getQuestionID());
             
             
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
             Date dtDate = sdf.parse(objInnerUserQuestionnaireResponse.getResponseDate());
             Timestamp tsDate = new Timestamp(dtDate.getYear(),dtDate.getMonth(), dtDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("ResponseDate", tsDate);
             objCallableStatement.setInt("Response", objInnerUserQuestionnaireResponse.getResponse());
             
             objCallableStatement.execute();
             
             objDbResponse.add(String.valueOf(objInnerUserQuestionnaireResponse.getUserQuestionnaireResponseID()));
             objDbResponse.add("No Error");

             objConn.close();
             logger.info("User Questionnaire Response updated successfully, User Details="+objUserQuestionnaireResponse);
         }
         catch (Exception e)
         {
         	logger.info("Error in updated User Questionnaire Response");
         	objDbResponse.add("Error in updated User Questionnaire Response");
         } 
         return objDbResponse;
	}

     /**
      * This is implementation function for retrieving User Questionnaire Response.
      * @param objUserQuestionnaireResponse
      * @return List of UserQuestionnaireResponse 
      */
	@Override
	public List<UserQuestionnaireResponse> RetriveData(Object objUserQuestionnaireResponse) {
        UserQuestionnaireResponse objOuterUserQuestionnaireResponse = new UserQuestionnaireResponse();
        List<UserQuestionnaireResponse> objListInnerUserQuestionnaireResponse = new ArrayList<UserQuestionnaireResponse>();
        objOuterUserQuestionnaireResponse =  (UserQuestionnaireResponse) objUserQuestionnaireResponse;
        
        try
        {
            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_UserQuestionnaireResponseByUserID(?)}");
            objCallableStatement.setLong("UserID", objOuterUserQuestionnaireResponse.getUserID());
            ResultSet objResultSet = objCallableStatement.executeQuery();

            while(objResultSet.next())
            {
                UserQuestionnaireResponse objInnerUserQuestionnaireResponse = new UserQuestionnaireResponse();
                
                objInnerUserQuestionnaireResponse.setUserQuestionnaireResponseID(objResultSet.getLong("UserQuestionnaireResponseID"));
                objInnerUserQuestionnaireResponse.setUserID(objResultSet.getLong("UserID"));
                objInnerUserQuestionnaireResponse.setQuestionID(objResultSet.getLong("QuestionID"));
                                
                Timestamp tsDate = objResultSet.getTimestamp("ResponseDate");
                objInnerUserQuestionnaireResponse.setResponseDate(tsDate.toString());
                
                objInnerUserQuestionnaireResponse.setResponse(objResultSet.getInt("Response"));
                                               
                objListInnerUserQuestionnaireResponse.add(objInnerUserQuestionnaireResponse);
            }
            objConn.close();
            logger.info("User Questionnaire Response is loaded successfully");
            
        }
        catch (Exception e)
        {
             logger.info("Error in Loading User Questionnaire Response");
        }   
        return objListInnerUserQuestionnaireResponse;
    }

	@Override
	public <T> List<T> Delete(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}

    /**
     * This is implementation function for connection database.
     * @param objConf
     */
	@Override
	 public void ConfigureAdapter(Object objConf) {
        try
        {
           objConn = (Connection)objConf;
           logger.info("Database connected successfully");
        }
        catch(Exception ex)
        {
       	 logger.info("Error in connection to Database");
        }
   }

}
