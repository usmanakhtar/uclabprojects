package org.uclab.ecrf.cardiovascular.monitoring;


/**
 *
 * @author Taqdir
 */
public enum StorageMedium {
	DATABASE, RDF, XML, OWL
}
