package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientPhysicalExam;

/**
 * This is PatientPhysicalExam Adapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientPhysicalExamAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(PatientPhysicalExamAdapter.class);
    
    public PatientPhysicalExamAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving PatientPhysicalExam.
     * @param objPatientPhysicalExam
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatientPhysicalExam) {
		PatientPhysicalExam objInnerPatientPhysicalExam = new PatientPhysicalExam();
		objInnerPatientPhysicalExam =  (PatientPhysicalExam) objPatientPhysicalExam;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientPhysicalExam(?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientPhysicalExam.getPatientEncounterID());
	         objCallableStatement.setInt("Rales", objInnerPatientPhysicalExam.getRales());
	         objCallableStatement.setInt("BilateralAnkleEdema", objInnerPatientPhysicalExam.getBilateralAnkleEdema());
	         objCallableStatement.setInt("HeartMurmur", objInnerPatientPhysicalExam.getHeartMurmur());
	         objCallableStatement.setInt("JugularVenousDilatation", objInnerPatientPhysicalExam.getJugularVenousDilatation());
	         objCallableStatement.setInt("LaterallyDisplacedApicalPulse", objInnerPatientPhysicalExam.getLaterallyDisplacedApicalPulse());
	         objCallableStatement.setLong("CreatedBy", objInnerPatientPhysicalExam.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("PatientPhysicalExamID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intPatientPhysicalExamID = objCallableStatement.getInt("PatientPhysicalExamID");
	         objDbResponse.add(String.valueOf(intPatientPhysicalExamID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientPhysicalExam saved successfully, PatientPhysicalExam Details="+objPatientPhysicalExam);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, PatientPhysicalExam Details=");
	     	objDbResponse.add("Error in saving PatientPhysicalExam :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for updating PatientPhysicalExam.
	  * @param objPatientPhysicalExam
	  * @return List of String 
	  */
	@Override
	public List<String> Update(Object objPatientPhysicalExam) {
		PatientPhysicalExam objInnerPatientPhysicalExam = new PatientPhysicalExam();
		objInnerPatientPhysicalExam =  (PatientPhysicalExam) objPatientPhysicalExam;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_PatientPhysicalExam(?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientPhysicalExamID", objInnerPatientPhysicalExam.getPatientPhysicalExamID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientPhysicalExam.getPatientEncounterID());
	         objCallableStatement.setInt("Rales", objInnerPatientPhysicalExam.getRales());
	         objCallableStatement.setInt("BilateralAnkleEdema", objInnerPatientPhysicalExam.getBilateralAnkleEdema());
	         objCallableStatement.setInt("HeartMurmur", objInnerPatientPhysicalExam.getHeartMurmur());
	         objCallableStatement.setInt("JugularVenousDilatation", objInnerPatientPhysicalExam.getJugularVenousDilatation());
	         objCallableStatement.setInt("LaterallyDisplacedApicalPulse", objInnerPatientPhysicalExam.getLaterallyDisplacedApicalPulse());
	         objCallableStatement.setLong("UpdatedBy", objInnerPatientPhysicalExam.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerPatientPhysicalExam.getPatientPhysicalExamID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientPhysicalExam udpated successfully, PatientPhysicalExam Details="+objPatientPhysicalExam);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in udpating PatientPhysicalExam, PatientPhysicalExam Details=");
	     	objDbResponse.add("Error in Updating PatientPhysicalExam :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving PatientPhysicalExam. 
	  * @param objPatientPhysicalExam
	  * @return List of PatientPhysicalExam
	  */

	@Override
	public List<PatientPhysicalExam> RetriveData(Object objPatientPhysicalExam) {
		PatientPhysicalExam objOuterPatientPhysicalExam = new PatientPhysicalExam();
		List<PatientPhysicalExam> objListInnerPatientPhysicalExam = new ArrayList<PatientPhysicalExam>();
		objOuterPatientPhysicalExam =  (PatientPhysicalExam) objPatientPhysicalExam;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientPhysicalExam.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientPhysicalExamByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientPhysicalExam.getPatientEncounterID());
		     }
		     else if(objOuterPatientPhysicalExam.getRequestType() == "ByPatientPhysicalExamID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientPhysicalExamByPatientPhysicalExamID(?)}");
		         objCallableStatement.setLong("PatientPhysicalExamID", objOuterPatientPhysicalExam.getPatientPhysicalExamID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 PatientPhysicalExam objInnerPatientPhysicalExam = new PatientPhysicalExam();
		    	 objInnerPatientPhysicalExam.setPatientPhysicalExamID(objResultSet.getLong("PatientPhysicalExamID"));
		    	 objInnerPatientPhysicalExam.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerPatientPhysicalExam.setRales(objResultSet.getInt("Rales"));
		    	 objInnerPatientPhysicalExam.setBilateralAnkleEdema(objResultSet.getInt("BilateralAnkleEdema"));
		    	 objInnerPatientPhysicalExam.setHeartMurmur(objResultSet.getInt("HeartMurmur"));
		    	 objInnerPatientPhysicalExam.setJugularVenousDilatation(objResultSet.getInt("JugularVenousDilatation"));
		    	 objInnerPatientPhysicalExam.setLaterallyDisplacedApicalPulse(objResultSet.getInt("LaterallyDisplacedApicalPulse"));
		     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientPhysicalExam.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientPhysicalExam.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientPhysicalExam.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientPhysicalExam.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientPhysicalExam.add(objInnerPatientPhysicalExam);
		     }
		     objConn.close();
		     logger.info("PatientPhysicalExam loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientPhysicalExam :" + e.getMessage());
		 }   
		 return objListInnerPatientPhysicalExam;
	}

	@Override
	public List<String> Delete(Object objPatientPhysicalExam) {
		PatientPhysicalExam objOuterPatientPhysicalExam = new PatientPhysicalExam();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterPatientPhysicalExam =  (PatientPhysicalExam) objPatientPhysicalExam;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientPhysicalExamByPatientPhysicalExamID(?)}");
		     objCallableStatement.setLong("PatientPhysicalExamID", objOuterPatientPhysicalExam.getPatientPhysicalExamID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterPatientPhysicalExam.getPatientPhysicalExamID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("PatientPhysicalExam Deleted successfully, PatientPhysicalExam Details="+objOuterPatientPhysicalExam);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting PatientPhysicalExam, PatientPhysicalExam Details=");
		 	objDbResponse.add("Error in deleting PatientPhysicalExam :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
