package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Echocardiography.
 * @author Taqdir
 *
 */
public class Echocardiography implements Serializable {

	
	private Long echocardiographyID;
	private Long patientEncounterID;
    private int wetherToMeasure;
    private String examinationDate;
    private Float lVEF;
    private Float lAVI;
    private Float lVMI;
    private Float ee;
    private Float eSeptal;
    private Float longitudinalStrain;
    private Float tRV;
    private int eCG;
    
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getEchocardiographyID() {
		return echocardiographyID;
	}

	public void setEchocardiographyID(Long echocardiographyID) {
		this.echocardiographyID = echocardiographyID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getWetherToMeasure() {
		return wetherToMeasure;
	}

	public void setWetherToMeasure(int wetherToMeasure) {
		this.wetherToMeasure = wetherToMeasure;
	}

	public String getExaminationDate() {
		return examinationDate;
	}

	public void setExaminationDate(String examinationDate) {
		this.examinationDate = examinationDate;
	}

	public Float getlVEF() {
		return lVEF;
	}

	public void setlVEF(Float lVEF) {
		this.lVEF = lVEF;
	}

	public Float getlAVI() {
		return lAVI;
	}

	public void setlAVI(Float lAVI) {
		this.lAVI = lAVI;
	}

	public Float getlVMI() {
		return lVMI;
	}

	public void setlVMI(Float lVMI) {
		this.lVMI = lVMI;
	}

	public Float getEe() {
		return ee;
	}

	public void setEe(Float ee) {
		this.ee = ee;
	}

	public Float geteSeptal() {
		return eSeptal;
	}

	public void seteSeptal(Float eSeptal) {
		this.eSeptal = eSeptal;
	}

	public Float getLongitudinalStrain() {
		return longitudinalStrain;
	}

	public void setLongitudinalStrain(Float longitudinalStrain) {
		this.longitudinalStrain = longitudinalStrain;
	}

	public Float gettRV() {
		return tRV;
	}

	public void settRV(Float tRV) {
		this.tRV = tRV;
	}

	public int geteCG() {
		return eCG;
	}

	public void seteCG(int eCG) {
		this.eCG = eCG;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	
    
}
