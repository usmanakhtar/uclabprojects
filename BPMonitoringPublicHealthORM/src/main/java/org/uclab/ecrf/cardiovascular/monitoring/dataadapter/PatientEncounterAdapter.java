package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientEncounter;

/**
 * This is PatientEncounterAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientEncounterAdapter implements DataAccessInterface {

	 private Connection objConn;
	    
	    private static final Logger logger = LoggerFactory.getLogger(PatientEncounterAdapter.class);
	    
	    public PatientEncounterAdapter()
	    {
	        
	    }

 /**
  * This is implementation function for saving PatientEncounter.
  * @param objPatientEncounter
  * @return List of String 
  */
	@Override
public List<String> Save(Object objPatientEncounter) {
	PatientEncounter objInnerPatientEncounter = new PatientEncounter();
	objInnerPatientEncounter =  (PatientEncounter) objPatientEncounter;
	List<String> objDbResponse = new ArrayList<>();
     
     try
     {

         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientEncounter(?, ?, ?, ?, ?, ?, ?, ?)}");
         
         objCallableStatement.setLong("PatientID", objInnerPatientEncounter.getPatientID());
         
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
         if(objInnerPatientEncounter.getPatientEncounterDate() != null)
         {
        	 Date dtPatientEncounterDate = sdf.parse(objInnerPatientEncounter.getPatientEncounterDate());
             Timestamp tsPatientEncounterDate = new Timestamp(dtPatientEncounterDate.getYear(),dtPatientEncounterDate.getMonth(), dtPatientEncounterDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("PatientEncounterDate", tsPatientEncounterDate);
         }
         else
         {
        	 objCallableStatement.setTimestamp("PatientEncounterDate", null);
         }
         if (objInnerPatientEncounter.getHeight() != null)
         {
        	 objCallableStatement.setFloat("Height", objInnerPatientEncounter.getHeight());
         }
         else
         {
        	 objCallableStatement.setNull("Height", java.sql.Types.FLOAT);
         }
         if (objInnerPatientEncounter.getWeight() != null)
         {
        	 objCallableStatement.setFloat("Weight", objInnerPatientEncounter.getWeight());
         }
         else
         {
        	 objCallableStatement.setNull("Weight", java.sql.Types.FLOAT);
         }
         
         if(objInnerPatientEncounter.getAdmissionDate() != null)
         {
        	 Date dtAdmissionDate = sdf.parse(objInnerPatientEncounter.getAdmissionDate());
             Timestamp tsAdmissionDate = new Timestamp(dtAdmissionDate.getYear(),dtAdmissionDate.getMonth(), dtAdmissionDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("AdmissionDate", tsAdmissionDate);
         }
         else
         {
        	 objCallableStatement.setTimestamp("AdmissionDate", null);
         }
         
         if(objInnerPatientEncounter.gethFDiagnosedDate() != null)
         {
        	 Date dtHFDiagnosedDate = sdf.parse(objInnerPatientEncounter.gethFDiagnosedDate());
             Timestamp tsHFDiagnosedDate = new Timestamp(dtHFDiagnosedDate.getYear(),dtHFDiagnosedDate.getMonth(), dtHFDiagnosedDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("HFDiagnosedDate", tsHFDiagnosedDate);
         }
         else
         {
        	 objCallableStatement.setTimestamp("HFDiagnosedDate", null);
         }
         
         objCallableStatement.setLong("CreatedBy", objInnerPatientEncounter.getCreatedBy());
         
         objCallableStatement.registerOutParameter("PatientEncounterID", Types.BIGINT);
         objCallableStatement.execute();
         
         int intPatientEncounterID = objCallableStatement.getInt("PatientEncounterID");
         objDbResponse.add(String.valueOf(intPatientEncounterID));
         objDbResponse.add("No Error");
         
         objConn.close();
         logger.info("User saved successfully, PatientEncounter Details="+objPatientEncounter);
     }
     catch (Exception e)
     {
     	logger.info("Error in saving user, PatientEncounter Details=");
     	objDbResponse.add("Error in saving PatientEncounter :" + e.getMessage());
     }   
     
     return objDbResponse;
	}

	/**
  * This is implementation function for updating PatientEncounter.
  * @param objPatientEncounter
  * @return List of String 
  */
	
	@Override
	public List<String> Update(Object objPatientEncounter) {
		PatientEncounter objInnerPatientEncounter = new PatientEncounter();
     objInnerPatientEncounter =  (PatientEncounter) objPatientEncounter;
     List<String> objDbResponse = new ArrayList<>();
     
     try
     {

         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_PatientEncounter(?, ?, ?, ?, ?, ?, ?, ?)}");
         
         objCallableStatement.setLong("PatientEncounterID", objInnerPatientEncounter.getPatientEncounterID());
         objCallableStatement.setLong("PatientID", objInnerPatientEncounter.getPatientID());
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
         
         if(objInnerPatientEncounter.getPatientEncounterDate() != null)
         {
        	 Date dtPatientEncounterDate = sdf.parse(objInnerPatientEncounter.getPatientEncounterDate());
             Timestamp tsPatientEncounterDate = new Timestamp(dtPatientEncounterDate.getYear(),dtPatientEncounterDate.getMonth(), dtPatientEncounterDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("PatientEncounterDate", tsPatientEncounterDate);
         }
         
         if (objInnerPatientEncounter.getHeight() != null)
         {
        	 objCallableStatement.setFloat("Height", objInnerPatientEncounter.getHeight());
         }
         else
         {
        	 objCallableStatement.setNull("Height", java.sql.Types.FLOAT);
         }
         if (objInnerPatientEncounter.getWeight() != null)
         {
        	 objCallableStatement.setFloat("Weight", objInnerPatientEncounter.getWeight());
         }
         else
         {
        	 objCallableStatement.setNull("Weight", java.sql.Types.FLOAT);
         }
         
         if(objInnerPatientEncounter.getAdmissionDate() != null)
         {
        	 Date dtAdmissionDate = sdf.parse(objInnerPatientEncounter.getAdmissionDate());
             Timestamp tsAdmissionDate = new Timestamp(dtAdmissionDate.getYear(),dtAdmissionDate.getMonth(), dtAdmissionDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("AdmissionDate", tsAdmissionDate);
         }
         
         
         if(objInnerPatientEncounter.getAdmissionDate() != null)
         {
        	 Date dtHFDiagnosedDate = sdf.parse(objInnerPatientEncounter.gethFDiagnosedDate());
             Timestamp tsHFDiagnosedDate = new Timestamp(dtHFDiagnosedDate.getYear(),dtHFDiagnosedDate.getMonth(), dtHFDiagnosedDate.getDate(), 00, 00, 00, 00);
             objCallableStatement.setTimestamp("HFDiagnosedDate", tsHFDiagnosedDate);
         }
        
         objCallableStatement.setLong("UpdatedBy", objInnerPatientEncounter.getUpdatedBy());
         
         objCallableStatement.execute();
         
         objDbResponse.add(String.valueOf(objInnerPatientEncounter.getPatientEncounterID()));
         objDbResponse.add("No Error");
         
         objConn.close();
         logger.info("PatientEncounter udpated successfully, PatientEncounter Details="+objPatientEncounter);
     }
     catch (Exception e)
     {
     	logger.info("Error in updating PatientEncounter, PatientEncounter Details=");
     	objDbResponse.add("Error in saving PatientEncounter :" + e.getMessage());
     }   
     
     return objDbResponse;
	}
	
 /**
  * This is implementation function for retrieving PatientEncounter. 
  * @param objPatientEncounter
  * @return List of PatientEncounters
  */

@Override
public  List<PatientEncounter> RetriveData(Object objPatientEncounter) {
		PatientEncounter objOuterPatientEncounter = new PatientEncounter();
		List<PatientEncounter> objListInnerPatientEncounter = new ArrayList<PatientEncounter>();
		objOuterPatientEncounter =  (PatientEncounter) objPatientEncounter;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientEncounter.getRequestType() == "ByPatientID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientEncounterByPatientID(?)}");
		         objCallableStatement.setLong("PatientID", objOuterPatientEncounter.getPatientID());
		     }
		     else if(objOuterPatientEncounter.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientEncounterByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientEncounter.getPatientEncounterID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		     	PatientEncounter objInnerPatientEncounter = new PatientEncounter();
		     	objInnerPatientEncounter.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		     	objInnerPatientEncounter.setPatientID(objResultSet.getLong("PatientID"));
		     	if(objResultSet.getTimestamp("PatientEncounterDate") != null)
		     	{
		 		  Timestamp tsPatientEncounterDate = objResultSet.getTimestamp("PatientEncounterDate");
		           objInnerPatientEncounter.setPatientEncounterDate(tsPatientEncounterDate.toString());
		     	}
		     	
		     	if(objResultSet.getObject("Height") != null)
		     	{
		     		objInnerPatientEncounter.setHeight(objResultSet.getFloat("Height"));
		     	}
		     	
		     	if(objResultSet.getObject("Weight") != null)
		     	{
		     		objInnerPatientEncounter.setWeight(objResultSet.getFloat("Weight"));
		     	}
		     	
		     	if(objResultSet.getTimestamp("AdmissionDate") != null)
		     	{
		 		  Timestamp tsAdmissionDate = objResultSet.getTimestamp("AdmissionDate");
		           objInnerPatientEncounter.setAdmissionDate(tsAdmissionDate.toString());
		     	}
		     	
		     	if(objResultSet.getTimestamp("HFDiagnosedDate") != null)
		     	{
		     		Timestamp tsHFDiagnosedDate = objResultSet.getTimestamp("HFDiagnosedDate");
		     		objInnerPatientEncounter.sethFDiagnosedDate(tsHFDiagnosedDate.toString());
		     	}
		     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientEncounter.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientEncounter.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientEncounter.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientEncounter.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientEncounter.add(objInnerPatientEncounter);
		     }
		     objConn.close();
		     logger.info("PatientEncounter loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientEncounter :" + e.getMessage());
		 }   
		 return objListInnerPatientEncounter;
	}

@Override
public List<String> Delete(Object objPatientEncounter) {
	 PatientEncounter objOuterPatientEncounter = new PatientEncounter();
	 List<String> objDbResponse = new ArrayList<>();
	 objOuterPatientEncounter =  (PatientEncounter) objPatientEncounter;
	 try
	 {
	     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientEncounterByPatientEncounterID(?)}");
	     objCallableStatement.setLong("PatientEncounterID", objOuterPatientEncounter.getPatientEncounterID());
	     objCallableStatement.execute();
	 
	     objDbResponse.add(String.valueOf(objOuterPatientEncounter.getPatientEncounterID()));
	     objDbResponse.add("No Error");
	 
	     objConn.close();
	     logger.info("PatientEncounter Deleted successfully, PatientEncounter Details="+objOuterPatientEncounter);
	 }
	 catch (Exception e)
	 {
	 	logger.info("Error in deleting PatientEncounter, PatientEncounter Details=");
	 	objDbResponse.add("Error in deleting PatientEncounter :" + e.getMessage());
	 }   
	 return objDbResponse;
}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
