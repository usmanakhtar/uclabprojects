package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Vital Signs statistics.
 * @author Taqdir
 *
 */

public class PatientVitalSignStatistics  implements Serializable {

	private Long patientID;
	private Long patientEncounterID;
    
    private Float systolicBloodPressureAvg;
    private Float diastolicBloodPressureAvg;
    private Float heartRateAvg;
    private Float systolicBloodPressureSTD;
    private Float diastolicBloodPressureSTD;
    private Float heartRateSTD;
    
   private String requestType;

public Long getPatientID() {
	return patientID;
}

public void setPatientID(Long patientID) {
	this.patientID = patientID;
}

public Long getPatientEncounterID() {
	return patientEncounterID;
}

public void setPatientEncounterID(Long patientEncounterID) {
	this.patientEncounterID = patientEncounterID;
}

public Float getSystolicBloodPressureAvg() {
	return systolicBloodPressureAvg;
}

public void setSystolicBloodPressureAvg(Float systolicBloodPressureAvg) {
	this.systolicBloodPressureAvg = systolicBloodPressureAvg;
}

public Float getDiastolicBloodPressureAvg() {
	return diastolicBloodPressureAvg;
}

public void setDiastolicBloodPressureAvg(Float diastolicBloodPressureAvg) {
	this.diastolicBloodPressureAvg = diastolicBloodPressureAvg;
}

public Float getHeartRateAvg() {
	return heartRateAvg;
}

public void setHeartRateAvg(Float heartRateAvg) {
	this.heartRateAvg = heartRateAvg;
}

public Float getSystolicBloodPressureSTD() {
	return systolicBloodPressureSTD;
}

public void setSystolicBloodPressureSTD(Float systolicBloodPressureSTD) {
	this.systolicBloodPressureSTD = systolicBloodPressureSTD;
}

public Float getDiastolicBloodPressureSTD() {
	return diastolicBloodPressureSTD;
}

public void setDiastolicBloodPressureSTD(Float diastolicBloodPressureSTD) {
	this.diastolicBloodPressureSTD = diastolicBloodPressureSTD;
}

public Float getHeartRateSTD() {
	return heartRateSTD;
}

public void setHeartRateSTD(Float heartRateSTD) {
	this.heartRateSTD = heartRateSTD;
}

public String getRequestType() {
	return requestType;
}

public void setRequestType(String requestType) {
	this.requestType = requestType;
}
    
}
