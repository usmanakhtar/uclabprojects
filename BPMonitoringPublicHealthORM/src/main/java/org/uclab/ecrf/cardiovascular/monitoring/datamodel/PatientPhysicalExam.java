package org.uclab.ecrf.cardiovascular.monitoring.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Physical Exam.
 * @author Taqdir
 *
 */
public class PatientPhysicalExam implements Serializable {

	private Long patientPhysicalExamID;
	private Long patientEncounterID;
    private int rales;
    private int bilateralAnkleEdema;
    private int heartMurmur;
    private int jugularVenousDilatation;
    private int laterallyDisplacedApicalPulse;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getPatientPhysicalExamID() {
		return patientPhysicalExamID;
	}

	public void setPatientPhysicalExamID(Long patientPhysicalExamID) {
		this.patientPhysicalExamID = patientPhysicalExamID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getRales() {
		return rales;
	}

	public void setRales(int rales) {
		this.rales = rales;
	}

	public int getBilateralAnkleEdema() {
		return bilateralAnkleEdema;
	}

	public void setBilateralAnkleEdema(int bilateralAnkleEdema) {
		this.bilateralAnkleEdema = bilateralAnkleEdema;
	}

	public int getHeartMurmur() {
		return heartMurmur;
	}

	public void setHeartMurmur(int heartMurmur) {
		this.heartMurmur = heartMurmur;
	}

	public int getJugularVenousDilatation() {
		return jugularVenousDilatation;
	}

	public void setJugularVenousDilatation(int jugularVenousDilatation) {
		this.jugularVenousDilatation = jugularVenousDilatation;
	}

	public int getLaterallyDisplacedApicalPulse() {
		return laterallyDisplacedApicalPulse;
	}

	public void setLaterallyDisplacedApicalPulse(int laterallyDisplacedApicalPulse) {
		this.laterallyDisplacedApicalPulse = laterallyDisplacedApicalPulse;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
}
