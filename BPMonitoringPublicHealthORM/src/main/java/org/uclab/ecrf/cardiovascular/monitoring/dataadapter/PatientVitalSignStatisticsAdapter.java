package org.uclab.ecrf.cardiovascular.monitoring.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.monitoring.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSignStatistics;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSigns;

/**
 * This is PatientVitalSignStatisticsAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class PatientVitalSignStatisticsAdapter implements DataAccessInterface {
	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(PatientVitalSignsAdapter.class);
    public PatientVitalSignStatisticsAdapter()
    {
        
    }

	/**
	  * This is implementation function for retrieving PatientVitalSigns. 
	  * @param objPatientVitalSigns
	  * @return List of PatientVitalSigns
	  */
	@Override
	public List<PatientVitalSignStatistics> RetriveData(Object objPatientVitalSignStatistics) {
		PatientVitalSignStatistics objOuterPatientVitalSignStatistics = new PatientVitalSignStatistics();
		List<PatientVitalSignStatistics> objListInnerPatientVitalSignStatistics = new ArrayList<PatientVitalSignStatistics>();
		objOuterPatientVitalSignStatistics =  (PatientVitalSignStatistics) objPatientVitalSignStatistics;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientVitalSignStatistics.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientVitalSignsSTDAVGByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientVitalSignStatistics.getPatientEncounterID());
		     }
		     else if(objOuterPatientVitalSignStatistics.getRequestType() == "ByPatientIDAccumulate")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientVitalSignsSTDAVGByPatientIDAccumulate(?)}");
		         objCallableStatement.setLong("PatientID", objOuterPatientVitalSignStatistics.getPatientID());
		     }
		     else if(objOuterPatientVitalSignStatistics.getRequestType() == "ByPatientIDGroup")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientVitalSignsSTDAVGByPatientID(?)}");
		         objCallableStatement.setLong("PatientID", objOuterPatientVitalSignStatistics.getPatientID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	PatientVitalSignStatistics objInnerPatientVitalSignStatistics = new PatientVitalSignStatistics();
		    	try
		    	{
		    		if(objResultSet.getObject("PatientID") != null)
			     	{
			     		objInnerPatientVitalSignStatistics.setPatientID(objResultSet.getLong("PatientID"));
			     	}
		    	}
		    	catch(Exception ex)
		    	{
		    		objInnerPatientVitalSignStatistics.setPatientID(null);
		    	}
		    	try
		    	{
		    		if(objResultSet.getObject("PatientEncounterID") != null)
			     	{
			     		objInnerPatientVitalSignStatistics.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
			     	}
		    	}
		    	catch(Exception ex)
		    	{
		    		objInnerPatientVitalSignStatistics.setPatientEncounterID(null);
		    	}
		     	
		     	if(objResultSet.getObject("StdDEvSystolic") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setSystolicBloodPressureSTD(objResultSet.getFloat("StdDEvSystolic"));
		     	}
		     	if(objResultSet.getObject("StdDEvDiastolic") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setDiastolicBloodPressureSTD(objResultSet.getFloat("StdDEvDiastolic"));
		     	}
		     	if(objResultSet.getObject("StdDEvHeartRate") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setHeartRateSTD(objResultSet.getFloat("StdDEvHeartRate"));
		     	}
		     	if(objResultSet.getObject("AvgSystolic") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setSystolicBloodPressureAvg(objResultSet.getFloat("AvgSystolic"));
		     	}
		     	if(objResultSet.getObject("AvgDiastolic") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setDiastolicBloodPressureAvg(objResultSet.getFloat("AvgDiastolic"));
		     	}
		     	if(objResultSet.getObject("AvgHeartRate") != null)
		     	{
		     		objInnerPatientVitalSignStatistics.setHeartRateAvg(objResultSet.getFloat("AvgHeartRate"));
		     	}
                         
		         objListInnerPatientVitalSignStatistics.add(objInnerPatientVitalSignStatistics);
		     }
		     objConn.close();
		     logger.info("PatientVitalSigns Statistics loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientVitalSigns Statistics :" + e.getMessage());
		 }   
		 return objListInnerPatientVitalSignStatistics;
	}
	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}


	@Override
	public <T> List<String> Save(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <T> List<String> Update(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <T> List<String> Delete(T objEntity) {
		// TODO Auto-generated method stub
		return null;
	}
}
