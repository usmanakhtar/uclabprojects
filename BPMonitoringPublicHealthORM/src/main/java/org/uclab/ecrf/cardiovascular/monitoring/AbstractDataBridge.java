package org.uclab.ecrf.cardiovascular.monitoring;

import java.util.List;

import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Echocardiography;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Electrocardiogram;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.NTproBNP;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Patient;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientClinicalHistory;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientDiagnosis;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientEncounter;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientPhysicalExam;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientSymptoms;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSignStatistics;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.PatientVitalSigns;
import org.uclab.ecrf.cardiovascular.monitoring.datamodel.Users;


/**
 * This abstract class is bridge for the CRUD operations between Database and all corresponding implementation classes.
 * @author Taqdir Ali
 */

public abstract class AbstractDataBridge {

		protected DataAccessInterface objDAInterface;
	    public AbstractDataBridge(DataAccessInterface objDAInterface)
	    {
	        this.objDAInterface = objDAInterface;
	    }
	    
	    /**
	     * This constructor of the abstract class
	     */
	    public AbstractDataBridge()
	    {
	        
	    }
	    
	    /**
	     * This is abstract function setter of DataAccessInterface. 
	     * @param DAInterface
	     *
	     */
	    public abstract void setDataAdapter(DataAccessInterface DAInterface);
	    
	    /**
	     * This is abstract function for saving Patient.
	     * @param objPatient
	     * @return List of String 
	     */
	    public abstract List<String> SavePatient(Patient objPatient);
	    /**
	     * This is abstract function for updating Patient.
	     * @param objPatient
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatient(Patient objPatient);
	    /**
	     * This is abstract function for retrieving Patient. 
	     * @param objPatient
	     * @return List of Patient
	     */
	    public abstract List<Patient> RetrivePatient(Patient objPatient);
	    /**
	     * This is abstract function for deleting Patient.
	     * @param objPatient
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatient(Patient objPatient);
	    
	    /**
	     * This is abstract function for saving PatientEncounter.
	     * @param objPatientEncounter
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientEncounter(PatientEncounter objPatientEncounter);
	    /**
	     * This is abstract function for updating PatientEncounter.
	     * @param objPatientEncounter
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientEncounter(PatientEncounter objPatientEncounter);
	    /**
	     * This is abstract function for retrieving Patient. 
	     * @param objPatientEncounter
	     * @return List of PatientEncounter
	     */
	    public abstract List<PatientEncounter> RetrivePatientEncounter(PatientEncounter objPatientEncounter);
	    /**
	     * This is abstract function for deleting PatientEncounter.
	     * @param objPatientEncounter
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientEncounter(PatientEncounter objPatientEncounter);
	    
	    /**
	     * This is abstract function for saving PatientSymptoms.
	     * @param objPatientSymptoms
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientSymptoms(PatientSymptoms objPatientSymptoms);
	    /**
	     * This is abstract function for updating PatientSymptoms.
	     * @param objPatientSymptoms
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientSymptoms(PatientSymptoms objPatientSymptoms);
	    /**
	     * This is abstract function for retrieving PatientSymptoms. 
	     * @param objPatientSymptoms
	     * @return List of PatientSymptoms
	     */
	    public abstract List<PatientSymptoms> RetrivePatientSymptoms(PatientSymptoms objPatientSymptoms);
	    /**
	     * This is abstract function for deleting PatientSymptoms.
	     * @param objPatientSymptoms
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientSymptoms(PatientSymptoms objPatientSymptoms);
	    
	    /**
	     * This is abstract function for saving PatientClinicalHistory.
	     * @param objPatientClinicalHistory
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory);
	    /**
	     * This is abstract function for updating PatientClinicalHistory.
	     * @param objPatientClinicalHistory
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory);
	    /**
	     * This is abstract function for retrieving PatientClinicalHistory. 
	     * @param objPatientClinicalHistory
	     * @return List of PatientClinicalHistory
	     */
	    public abstract List<PatientClinicalHistory> RetrivePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory);
	    /**
	     * This is abstract function for deleting PatientClinicalHistory.
	     * @param objPatientClinicalHistory
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientClinicalHistory(PatientClinicalHistory objPatientClinicalHistory);
	    
	    /**
	     * This is abstract function for saving PatientVitalSigns.
	     * @param objPatientVitalSigns
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientVitalSigns(PatientVitalSigns objPatientVitalSigns);
	    /**
	     * This is abstract function for updating PatientVitalSigns.
	     * @param objPatientVitalSigns
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientVitalSigns(PatientVitalSigns objPatientVitalSigns);
	    /**
	     * This is abstract function for retrieving PatientVitalSigns. 
	     * @param objPatientVitalSigns
	     * @return List of PatientVitalSigns
	     */
	    public abstract List<PatientVitalSigns> RetrivePatientVitalSigns(PatientVitalSigns objPatientVitalSigns);
	    /**
	     * This is abstract function for deleting PatientVitalSigns.
	     * @param objPatientVitalSigns
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientVitalSigns(PatientVitalSigns objPatientVitalSigns);
	    
	    /**
	     * This is abstract function for saving Electrocardiogram.
	     * @param objElectrocardiogram
	     * @return List of String 
	     */
	    public abstract List<String> SaveElectrocardiogram(Electrocardiogram objElectrocardiogram);
	    /**
	     * This is abstract function for updating Electrocardiogram.
	     * @param objElectrocardiogram
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdateElectrocardiogram(Electrocardiogram objElectrocardiogram);
	    /**
	     * This is abstract function for retrieving Electrocardiogram. 
	     * @param objElectrocardiogram
	     * @return List of Electrocardiogram
	     */
	    public abstract List<Electrocardiogram> RetriveElectrocardiogram(Electrocardiogram objElectrocardiogram);
	    /**
	     * This is abstract function for deleting Electrocardiogram.
	     * @param objElectrocardiogram
	     * @return List of String 
	     */
	    public abstract List<String> DeleteElectrocardiogram(Electrocardiogram objElectrocardiogram);
	    
	    /**
	     * This is abstract function for saving NTproBNP.
	     * @param objNTproBNP
	     * @return List of String 
	     */
	    public abstract List<String> SaveNTproBNP(NTproBNP objNTproBNP);
	    /**
	     * This is abstract function for updating NTproBNP.
	     * @param objNTproBNP
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdateNTproBNP(NTproBNP objNTproBNP);
	    /**
	     * This is abstract function for retrieving NTproBNP. 
	     * @param objNTproBNP
	     * @return List of NTproBNP
	     */
	    public abstract List<NTproBNP> RetriveNTproBNP(NTproBNP objNTproBNP);
	    /**
	     * This is abstract function for deleting NTproBNP.
	     * @param objNTproBNP
	     * @return List of String 
	     */
	    public abstract List<String> DeleteNTproBNP(NTproBNP objNTproBNP);
	    
	    /**
	     * This is abstract function for saving Echocardiography.
	     * @param objEchocardiography
	     * @return List of String 
	     */
	    public abstract List<String> SaveEchocardiography(Echocardiography objEchocardiography);
	    /**
	     * This is abstract function for updating Echocardiography.
	     * @param objEchocardiography
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdateEchocardiography(Echocardiography objEchocardiography);
	    /**
	     * This is abstract function for retrieving Echocardiography. 
	     * @param objEchocardiography
	     * @return List of Echocardiography
	     */
	    public abstract List<Echocardiography> RetriveEchocardiography(Echocardiography objEchocardiography);
	    /**
	     * This is abstract function for deleting Echocardiography.
	     * @param objEchocardiography
	     * @return List of String 
	     */
	    public abstract List<String> DeleteEchocardiography(Echocardiography objEchocardiography);
	    
	    /**
	     * This is abstract function for saving PatientDiagnosis.
	     * @param objPatientDiagnosis
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientDiagnosis(PatientDiagnosis objPatientDiagnosis);
	    /**
	     * This is abstract function for updating PatientDiagnosis.
	     * @param objPatientDiagnosis
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientDiagnosis(PatientDiagnosis objPatientDiagnosis);
	    /**
	     * This is abstract function for retrieving PatientDiagnosis. 
	     * @param objPatientDiagnosis
	     * @return List of PatientDiagnosis
	     */
	    public abstract List<PatientDiagnosis> RetrivePatientDiagnosis(PatientDiagnosis objPatientDiagnosis);
	    /**
	     * This is abstract function for deleting PatientDiagnosis.
	     * @param objPatientDiagnosis
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientDiagnosis(PatientDiagnosis objPatientDiagnosis);
	    
	    /**
	     * This is abstract function for saving Users.
	     * @param objUsers
	     * @return List of String 
	     */
	    public abstract List<String> SaveUsers(Users objUsers);
	    /**
	     * This is abstract function for updating Users.
	     * @param objUsers
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdateUsers(Users objUsers);
	    /**
	     * This is abstract function for retrieving Users. 
	     * @param objUsers
	     * @return List of Users
	     */
	    public abstract List<Users> RetriveUsers(Users objUsers);
	    /**
	     * This is abstract function for deleting Users.
	     * @param objUsers
	     * @return List of String 
	     */
	    public abstract List<String> DeleteUsers(Users objUsers);
	    
	    /**
	     * This is abstract function for saving PatientPhysicalExam.
	     * @param objPatientPhysicalExam
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam);
	    /**
	     * This is abstract function for updating PatientPhysicalExam.
	     * @param objPatientPhysicalExam
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam);
	    /**
	     * This is abstract function for retrieving PatientPhysicalExam. 
	     * @param objPatientPhysicalExam
	     * @return List of PatientPhysicalExam
	     */
	    public abstract List<PatientPhysicalExam> RetrivePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam);
	    /**
	     * This is abstract function for deleting PatientPhysicalExam.
	     * @param objPatientPhysicalExam
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientPhysicalExam(PatientPhysicalExam objPatientPhysicalExam);
	    
	    /**
	     * This is abstract function for saving PatientVitalSignStatistics.
	     * @param objPatientVitalSignStatistics
	     * @return List of String 
	     */
	    public abstract List<String> SavePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics);
	    /**
	     * This is abstract function for updating PatientVitalSignStatistics.
	     * @param objPatientVitalSignStatistics
	     * @return List of string
	     *  
	     */
	    public abstract List<String> UpdatePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics);
	    /**
	     * This is abstract function for retrieving PatientVitalSignStatistics. 
	     * @param objPatientVitalSignStatistics
	     * @return List of PatientVitalSignStatistics
	     */
	    public abstract List<PatientVitalSignStatistics> RetrivePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics);
	    /**
	     * This is abstract function for deleting PatientVitalSignStatistics.
	     * @param objPatientVitalSignStatistics
	     * @return List of String 
	     */
	    public abstract List<String> DeletePatientVitalSignStatistics(PatientVitalSignStatistics objPatientVitalSignStatistics);
	    
	    
	 
}
