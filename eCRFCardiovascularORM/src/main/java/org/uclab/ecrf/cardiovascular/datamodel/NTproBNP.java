package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;

/**
 * This a data model class for NT-proBNP.
 * @author Taqdir
 *
 */

public class NTproBNP implements Serializable {

	private Long nTproBNPID;
	private Long patientEncounterID;
    private int wetherToExamination;
    private String examinationDate;
    private Float nTproBNPValue;
    private int nTproBNPStatus;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getnTproBNPID() {
		return nTproBNPID;
	}

	public void setnTproBNPID(Long nTproBNPID) {
		this.nTproBNPID = nTproBNPID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getWetherToExamination() {
		return wetherToExamination;
	}

	public void setWetherToExamination(int wetherToExamination) {
		this.wetherToExamination = wetherToExamination;
	}

	public String getExaminationDate() {
		return examinationDate;
	}

	public void setExaminationDate(String examinationDate) {
		this.examinationDate = examinationDate;
	}

	public Float getnTproBNPValue() {
		return nTproBNPValue;
	}

	public void setnTproBNPValue(Float nTproBNPValue) {
		this.nTproBNPValue = nTproBNPValue;
	}

	public int getnTproBNPStatus() {
		return nTproBNPStatus;
	}

	public void setnTproBNPStatus(int nTproBNPStatus) {
		this.nTproBNPStatus = nTproBNPStatus;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
    
}
