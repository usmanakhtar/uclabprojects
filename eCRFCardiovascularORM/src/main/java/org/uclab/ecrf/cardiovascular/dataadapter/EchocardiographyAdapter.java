package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.Echocardiography;
/**
 * This is EchocardiographyAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class EchocardiographyAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(EchocardiographyAdapter.class);
    
    public EchocardiographyAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving Echocardiography.
     * @param objEchocardiography
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objEchocardiography) {
		Echocardiography objInnerEchocardiography = new Echocardiography();
		objInnerEchocardiography =  (Echocardiography) objEchocardiography;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_Echocardiography(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerEchocardiography.getPatientEncounterID());
	         objCallableStatement.setInt("WetherToMeasure", objInnerEchocardiography.getWetherToMeasure());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerEchocardiography.getExaminationDate() != null)
	         {
	        	 Date dtExaminationDate = sdf.parse(objInnerEchocardiography.getExaminationDate());
	             Timestamp tsExaminationDate = new Timestamp(dtExaminationDate.getYear(),dtExaminationDate.getMonth(), dtExaminationDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("ExaminationDate", tsExaminationDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("ExaminationDate", null);
	         }
	         if (objInnerEchocardiography.getlVEF() != null)
	         {
	        	 objCallableStatement.setFloat("LVEF", objInnerEchocardiography.getlVEF());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LVEF", java.sql.Types.FLOAT);
	         }
	         
	         if (objInnerEchocardiography.getlAVI() != null)
	         {
	        	 objCallableStatement.setFloat("LAVI", objInnerEchocardiography.getlAVI());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LAVI", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getlVMI() != null)
	         {
	        	 objCallableStatement.setFloat("LVMI", objInnerEchocardiography.getlVMI());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LVMI", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getEe() != null)
	         {
	        	 objCallableStatement.setFloat("Ee", objInnerEchocardiography.getEe());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("Ee", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.geteSeptal() != null)
	         {
	        	 objCallableStatement.setFloat("eSeptal", objInnerEchocardiography.geteSeptal());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("eSeptal", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getLongitudinalStrain() != null)
	         {
	        	 objCallableStatement.setFloat("LongitudinalStrain", objInnerEchocardiography.getLongitudinalStrain());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LongitudinalStrain", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.gettRV() != null)
	         {
	        	 objCallableStatement.setFloat("TRV", objInnerEchocardiography.gettRV());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("TRV", java.sql.Types.FLOAT);
	         }
	         
	         objCallableStatement.setInt("ECG", objInnerEchocardiography.geteCG());
	         objCallableStatement.setLong("CreatedBy", objInnerEchocardiography.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("EchocardiographyID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intEchocardiographyID = objCallableStatement.getInt("EchocardiographyID");
	         objDbResponse.add(String.valueOf(intEchocardiographyID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Echocardiography saved successfully, Echocardiography Details="+objEchocardiography);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, Echocardiography Details=");
	     	objDbResponse.add("Error in saving Echocardiography :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
     * This is implementation function for Updating Echocardiography.
     * @param objEchocardiography
     * @return List of String 
     */
	@Override
	public List<String> Update(Object objEchocardiography) {
		Echocardiography objInnerEchocardiography = new Echocardiography();
		objInnerEchocardiography =  (Echocardiography) objEchocardiography;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_Echocardiography(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("EchocardiographyID", objInnerEchocardiography.getEchocardiographyID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerEchocardiography.getPatientEncounterID());
	         objCallableStatement.setInt("WetherToMeasure", objInnerEchocardiography.getWetherToMeasure());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerEchocardiography.getExaminationDate() != null)
	         {
	        	 Date dtExaminationDate = sdf.parse(objInnerEchocardiography.getExaminationDate());
	             Timestamp tsExaminationDate = new Timestamp(dtExaminationDate.getYear(),dtExaminationDate.getMonth(), dtExaminationDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("ExaminationDate", tsExaminationDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("ExaminationDate", null);
	         }
	         
	         
	         if (objInnerEchocardiography.getlVEF() != null)
	         {
	        	 objCallableStatement.setFloat("LVEF", objInnerEchocardiography.getlVEF());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LVEF", java.sql.Types.FLOAT);
	         }
	         
	         if (objInnerEchocardiography.getlAVI() != null)
	         {
	        	 objCallableStatement.setFloat("LAVI", objInnerEchocardiography.getlAVI());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LAVI", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getlVMI() != null)
	         {
	        	 objCallableStatement.setFloat("LVMI", objInnerEchocardiography.getlVMI());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LVMI", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getEe() != null)
	         {
	        	 objCallableStatement.setFloat("Ee", objInnerEchocardiography.getEe());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("Ee", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.geteSeptal() != null)
	         {
	        	 objCallableStatement.setFloat("eSeptal", objInnerEchocardiography.geteSeptal());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("eSeptal", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.getLongitudinalStrain() != null)
	         {
	        	 objCallableStatement.setFloat("LongitudinalStrain", objInnerEchocardiography.getLongitudinalStrain());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("LongitudinalStrain", java.sql.Types.FLOAT);
	         }
	         if (objInnerEchocardiography.gettRV() != null)
	         {
	        	 objCallableStatement.setFloat("TRV", objInnerEchocardiography.gettRV());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("TRV", java.sql.Types.FLOAT);
	         }
	         
	         objCallableStatement.setInt("ECG", objInnerEchocardiography.geteCG());
	         objCallableStatement.setLong("UpdatedBy", objInnerEchocardiography.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerEchocardiography.getEchocardiographyID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Echocardiography updated successfully, Echocardiography Details="+objEchocardiography);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in updating user, Echocardiography Details=");
	     	objDbResponse.add("Error in updating Echocardiography :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving Echocardiography. 
	  * @param objEchocardiography
	  * @return List of Echocardiography
	  */
	@Override
	public List<Echocardiography> RetriveData(Object objEchocardiography) {
		Echocardiography objOuterEchocardiography = new Echocardiography();
		List<Echocardiography> objListInnerEchocardiography = new ArrayList<Echocardiography>();
		objOuterEchocardiography =  (Echocardiography) objEchocardiography;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterEchocardiography.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_EchocardiographyByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterEchocardiography.getPatientEncounterID());
		     }
		     else if(objOuterEchocardiography.getRequestType() == "ByEchocardiographyID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_EchocardiographyByEchocardiographyID(?)}");
		         objCallableStatement.setLong("EchocardiographyID", objOuterEchocardiography.getEchocardiographyID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 Echocardiography objInnerEchocardiography = new Echocardiography();
		    	 objInnerEchocardiography.setEchocardiographyID(objResultSet.getLong("EchocardiographyID"));
		    	 objInnerEchocardiography.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerEchocardiography.setWetherToMeasure(objResultSet.getInt("WetherToMeasure"));
		    	
		    	 if(objResultSet.getTimestamp("ExaminationDate") != null)
			     	{
			         	Timestamp tsExaminationDate = objResultSet.getTimestamp("ExaminationDate");
			         	objInnerEchocardiography.setExaminationDate(tsExaminationDate.toString());
			     	}
		    	 
				 if(objResultSet.getObject("LVEF") != null)
				 {
					 objInnerEchocardiography.setlVEF(objResultSet.getFloat("LVEF"));
				 }		    	 
				 if(objResultSet.getObject("LAVI") != null)
				 {
					 objInnerEchocardiography.setlAVI(objResultSet.getFloat("LAVI"));
				 }	
				 if(objResultSet.getObject("LVMI") != null)
				 {
					 objInnerEchocardiography.setlVMI(objResultSet.getFloat("LVMI"));
				 }
				 if(objResultSet.getObject("Ee") != null)
				 {
					 objInnerEchocardiography.setEe(objResultSet.getFloat("Ee"));
				 }
				 if(objResultSet.getObject("eSeptal") != null)
				 {
					 objInnerEchocardiography.seteSeptal(objResultSet.getFloat("eSeptal"));
				 }
				 if(objResultSet.getObject("LongitudinalStrain") != null)
				 {
					 objInnerEchocardiography.setLongitudinalStrain(objResultSet.getFloat("LongitudinalStrain"));
				 }
				 if(objResultSet.getObject("TRV") != null)
				 {
					 objInnerEchocardiography.settRV(objResultSet.getFloat("TRV"));
				 }
				 
				 objInnerEchocardiography.seteCG(objResultSet.getInt("ECG"));
		    	 
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerEchocardiography.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerEchocardiography.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerEchocardiography.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerEchocardiography.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerEchocardiography.add(objInnerEchocardiography);
		     }
		     objConn.close();
		     logger.info("Echocardiography loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting Echocardiography :" + e.getMessage());
		 }   
		 return objListInnerEchocardiography;
	}

	@Override
	public List<String> Delete(Object objEchocardiography) {
		Echocardiography objOuterEchocardiography = new Echocardiography();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterEchocardiography =  (Echocardiography) objEchocardiography;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_EchocardiographyByEchocardiographyID(?)}");
		     objCallableStatement.setLong("EchocardiographyID", objOuterEchocardiography.getEchocardiographyID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterEchocardiography.getEchocardiographyID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("Echocardiography Deleted successfully, Echocardiography Details="+objOuterEchocardiography);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting Echocardiography, Echocardiography Details=");
		 	objDbResponse.add("Error in deleting Echocardiography :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
