package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.Users;

/**
 * This is UsersAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class UsersAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(UsersAdapter.class);
    
    public UsersAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving Users.
     * @param objUsers
     * @return List of String 
     */
    @Override
	public List<String> Save(Object objUsers) {
		Users objInnerUsers = new Users();
		objInnerUsers =  (Users) objUsers;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_Users(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setString("UserName", objInnerUsers.getUserName());
	         objCallableStatement.setString("LoginID", objInnerUsers.getLoginID());
	         objCallableStatement.setString("Password", objInnerUsers.getPassword());
	         objCallableStatement.setString("EmailAddress", objInnerUsers.getEmailAddress());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerUsers.getDateOfBirth() != null)
	         {
	        	 Date dtDateOfBirth = sdf.parse(objInnerUsers.getDateOfBirth());
	             Timestamp tsDateOfBirth = new Timestamp(dtDateOfBirth.getYear(),dtDateOfBirth.getMonth(), dtDateOfBirth.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("DateOfBirth", tsDateOfBirth);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("DateOfBirth", null);
	         }
	         
	         objCallableStatement.setInt("DesignationID", objInnerUsers.getDesignationID());
	         objCallableStatement.setInt("ActiveYNID", objInnerUsers.getActiveYNID());
	         objCallableStatement.setLong("CreatedBy", objInnerUsers.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("UserID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intUserID = objCallableStatement.getInt("UserID");
	         objDbResponse.add(String.valueOf(intUserID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Users saved successfully, Users Details="+objUsers);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, Users Details=");
	     	objDbResponse.add("Error in saving Users :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

    
    /**
     * This is implementation function for updating Users.
     * @param objUsers
     * @return List of String 
     */
	@Override
	public List<String> Update(Object objUsers) {
		Users objInnerUsers = new Users();
		objInnerUsers =  (Users) objUsers;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_Users(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("UserID", objInnerUsers.getUserID());
	         objCallableStatement.setString("UserName", objInnerUsers.getUserName());
	         objCallableStatement.setString("LoginID", objInnerUsers.getLoginID());
	         objCallableStatement.setString("Password", objInnerUsers.getPassword());
	         objCallableStatement.setString("EmailAddress", objInnerUsers.getEmailAddress());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerUsers.getDateOfBirth() != null)
	         {
	        	 Date dtDateOfBirth = sdf.parse(objInnerUsers.getDateOfBirth());
	             Timestamp tsDateOfBirth = new Timestamp(dtDateOfBirth.getYear(),dtDateOfBirth.getMonth(), dtDateOfBirth.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("DateOfBirth", tsDateOfBirth);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("DateOfBirth", null);
	         }
	         
	         objCallableStatement.setInt("DesignationID", objInnerUsers.getDesignationID());
	         objCallableStatement.setInt("ActiveYNID", objInnerUsers.getActiveYNID());
	         objCallableStatement.setLong("UpdatedBy", objInnerUsers.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerUsers.getUserID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Users Updated successfully, Users Details="+objUsers);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in Updating user, Users Details=");
	     	objDbResponse.add("Error in Updating Users :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving Users. 
	  * @param objUsers
	  * @return List of Users
	  */
	@Override
	public List<Users> RetriveData(Object objUsers) {
		Users objOuterUsers = new Users();
		List<Users> objListInnerUsers = new ArrayList<Users>();
		objOuterUsers =  (Users) objUsers;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterUsers.getRequestType() == "AllUsersList")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_AllUsersList}");
		     }
		     else if(objOuterUsers.getRequestType() == "ByUserID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_UsersByUserID(?)}");
		         objCallableStatement.setLong("UserID", objOuterUsers.getUserID());
		     }
		     else if(objOuterUsers.getRequestType() == "AuthenticateUser")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_UsersByLoginIDAndPassword(?, ?)}");
		         objCallableStatement.setString("LoginID", objOuterUsers.getLoginID());
		         objCallableStatement.setString("Password", objOuterUsers.getPassword());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 Users objInnerUsers = new Users();
		    	 objInnerUsers.setUserID(objResultSet.getLong("UserID"));
		    	 objInnerUsers.setUserName(objResultSet.getString("UserName"));
		    	 objInnerUsers.setLoginID(objResultSet.getString("LoginID"));
		    	 objInnerUsers.setPassword(objResultSet.getString("Password"));
		    	 objInnerUsers.setEmailAddress(objResultSet.getString("EmailAddress"));
		    	 if(objResultSet.getTimestamp("DateOfBirth") != null)
			     	{
			         	Timestamp tsDateOfBirth = objResultSet.getTimestamp("DateOfBirth");
			         	objInnerUsers.setDateOfBirth(tsDateOfBirth.toString());
			     	}
		    	 
		    	 objInnerUsers.setDesignationID(objResultSet.getInt("DesignationID"));
		    	 objInnerUsers.setActiveYNID(objResultSet.getInt("ActiveYNID"));
		    	 
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerUsers.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerUsers.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerUsers.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerUsers.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerUsers.add(objInnerUsers);
		     }
		     objConn.close();
		     logger.info("Users loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting Users :" + e.getMessage());
		 }   
		 return objListInnerUsers;
	}

	@Override
	public List<String> Delete(Object objUsers) {
		Users objOuterUsers = new Users();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterUsers =  (Users) objUsers;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_UsersByUserID(?)}");
		     objCallableStatement.setLong("UserID", objOuterUsers.getUserID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterUsers.getUserID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("Users Deleted successfully, Users Details="+objOuterUsers);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting Users, Users Details=");
		 	objDbResponse.add("Error in deleting Users :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
