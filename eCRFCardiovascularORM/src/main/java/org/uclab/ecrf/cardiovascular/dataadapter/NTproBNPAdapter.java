package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.NTproBNP;

/**
 * This is NTproBNPAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class NTproBNPAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(NTproBNPAdapter.class);
    
    public NTproBNPAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving NTproBNP.
     * @param objNTproBNP
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objNTproBNP) {
		NTproBNP objInnerNTproBNP = new NTproBNP();
		objInnerNTproBNP =  (NTproBNP) objNTproBNP;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_NTproBNP(?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerNTproBNP.getPatientEncounterID());
	         objCallableStatement.setInt("WetherToExamination", objInnerNTproBNP.getWetherToExamination());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerNTproBNP.getExaminationDate() != null)
	         {
	        	 Date dtExaminationDate = sdf.parse(objInnerNTproBNP.getExaminationDate());
	             Timestamp tsExaminationDate = new Timestamp(dtExaminationDate.getYear(),dtExaminationDate.getMonth(), dtExaminationDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("ExaminationDate", tsExaminationDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("ExaminationDate", null);
	         }
	         if (objInnerNTproBNP.getnTproBNPValue() != null)
	         {
	        	 objCallableStatement.setFloat("NTproBNPValue", objInnerNTproBNP.getnTproBNPValue());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("NTproBNPValue", java.sql.Types.FLOAT);
	         }
	         
	         objCallableStatement.setInt("NTproBNPStatus", objInnerNTproBNP.getnTproBNPStatus());
	         objCallableStatement.setLong("CreatedBy", objInnerNTproBNP.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("NTproBNPID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intNTproBNPID = objCallableStatement.getInt("NTproBNPID");
	         objDbResponse.add(String.valueOf(intNTproBNPID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("NTproBNP saved successfully, NTproBNP Details="+objNTproBNP);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, NTproBNP Details=");
	     	objDbResponse.add("Error in saving NTproBNP :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
     * This is implementation function for updating NTproBNP.
     * @param objNTproBNP
     * @return List of String 
     */
	@Override
	public List<String> Update(Object objNTproBNP) {
		NTproBNP objInnerNTproBNP = new NTproBNP();
		objInnerNTproBNP =  (NTproBNP) objNTproBNP;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_NTproBNP(?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("NTproBNPID", objInnerNTproBNP.getnTproBNPID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerNTproBNP.getPatientEncounterID());
	         objCallableStatement.setInt("WetherToExamination", objInnerNTproBNP.getWetherToExamination());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerNTproBNP.getExaminationDate() != null)
	         {
	        	 Date dtExaminationDate = sdf.parse(objInnerNTproBNP.getExaminationDate());
	             Timestamp tsExaminationDate = new Timestamp(dtExaminationDate.getYear(),dtExaminationDate.getMonth(), dtExaminationDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("ExaminationDate", tsExaminationDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("ExaminationDate", null);
	         }
	         
	         if (objInnerNTproBNP.getnTproBNPValue() != null)
	         {
	        	 objCallableStatement.setFloat("NTproBNPValue", objInnerNTproBNP.getnTproBNPValue());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("NTproBNPValue", java.sql.Types.FLOAT);
	         }
	         objCallableStatement.setInt("NTproBNPStatus", objInnerNTproBNP.getnTproBNPStatus());
	         objCallableStatement.setLong("UpdatedBy", objInnerNTproBNP.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerNTproBNP.getnTproBNPID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("NTproBNP Updated successfully, NTproBNP Details="+objNTproBNP);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in Updating user, NTproBNP Details=");
	     	objDbResponse.add("Error in Updating NTproBNP :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}


	/**
	  * This is implementation function for retrieving NTproBNP. 
	  * @param objNTproBNP
	  * @return List of NTproBNP
	  */
	@Override
	public List<NTproBNP> RetriveData(Object objNTproBNP) {
		NTproBNP objOuterNTproBNP = new NTproBNP();
		List<NTproBNP> objListInnerNTproBNP = new ArrayList<NTproBNP>();
		objOuterNTproBNP =  (NTproBNP) objNTproBNP;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterNTproBNP.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_NTproBNPByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterNTproBNP.getPatientEncounterID());
		     }
		     else if(objOuterNTproBNP.getRequestType() == "ByNTproBNPID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_NTproBNPByNTproBNPID(?)}");
		         objCallableStatement.setLong("NTproBNPID", objOuterNTproBNP.getnTproBNPID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 NTproBNP objInnerNTproBNP = new NTproBNP();
		    	 objInnerNTproBNP.setnTproBNPID(objResultSet.getLong("NTproBNPID"));
		    	 objInnerNTproBNP.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerNTproBNP.setWetherToExamination(objResultSet.getInt("WetherToExamination"));
		    	
		    	 if(objResultSet.getTimestamp("ExaminationDate") != null)
			     	{
			         	Timestamp tsExaminationDate = objResultSet.getTimestamp("ExaminationDate");
			         	objInnerNTproBNP.setExaminationDate(tsExaminationDate.toString());
			     	}
		    	 
				if(objResultSet.getObject("NTproBNPValue") != null)
				{
					objInnerNTproBNP.setnTproBNPValue(objResultSet.getFloat("NTproBNPValue"));
				}
		    	 
		    	objInnerNTproBNP.setnTproBNPStatus(objResultSet.getInt("NTproBNPStatus"));
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerNTproBNP.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerNTproBNP.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerNTproBNP.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerNTproBNP.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerNTproBNP.add(objInnerNTproBNP);
		     }
		     objConn.close();
		     logger.info("NTproBNP loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting NTproBNP :" + e.getMessage());
		 }   
		 return objListInnerNTproBNP;
	}

	@Override
	public List<String> Delete(Object objNTproBNP) {
		NTproBNP objOuterNTproBNP = new NTproBNP();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterNTproBNP =  (NTproBNP) objNTproBNP;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_NTproBNPByNTproBNPID(?)}");
		     objCallableStatement.setLong("NTproBNPID", objOuterNTproBNP.getnTproBNPID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterNTproBNP.getnTproBNPID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("NTproBNP Deleted successfully, NTproBNP Details="+objOuterNTproBNP);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting NTproBNP, NTproBNP Details=");
		 	objDbResponse.add("Error in deleting NTproBNP :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
