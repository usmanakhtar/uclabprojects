/**
 * 
 */
package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;

/**
 * This a data model class for patient entity.
 * @author Taqdir
 *
 */
public class Patient implements Serializable {

	private Long patientID;
    private String patientMRNNo;
    private String patientName;
    private String dateOfBirth;
    private String gender;
    private int consentForm;
    private String consentDate;
    private String screeningNumber;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;
    
    
    public Long getPatientID() {
		return patientID;
	}

	public void setPatientID(Long patientID) {
		this.patientID = patientID;
	}
	public String getPatientMRNNo() {
		return patientMRNNo;
	}
	public void setPatientMRNNo(String patientMRNNo) {
		this.patientMRNNo = patientMRNNo;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getConsentForm() {
		return consentForm;
	}
	public void setConsentForm(int consentForm) {
		this.consentForm = consentForm;
	}
	public String getConsentDate() {
		return consentDate;
	}
	public void setConsentDate(String consentDate) {
		this.consentDate = consentDate;
	}
	public String getScreeningNumber() {
		return screeningNumber;
	}
	public void setScreeningNumber(String screeningNumber) {
		this.screeningNumber = screeningNumber;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

}
