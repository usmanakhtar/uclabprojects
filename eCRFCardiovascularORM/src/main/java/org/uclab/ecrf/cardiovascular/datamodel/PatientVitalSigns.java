package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Vital Signs.
 * @author Taqdir
 *
 */


public class PatientVitalSigns implements Serializable {

	
	private Long vitalSignID;
	private Long patientEncounterID;
    private int isItMeasure;
    private Integer systolicBloodPressure;
    private Integer diastolicBloodPressure;
    private Float heartRate;
    private Float bT;
    private int nYHAFunctionalClass;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getVitalSignID() {
		return vitalSignID;
	}

	public void setVitalSignID(Long vitalSignID) {
		this.vitalSignID = vitalSignID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getIsItMeasure() {
		return isItMeasure;
	}

	public void setIsItMeasure(int isItMeasure) {
		this.isItMeasure = isItMeasure;
	}

	public Integer getSystolicBloodPressure() {
		return systolicBloodPressure;
	}

	public void setSystolicBloodPressure(Integer systolicBloodPressure) {
		this.systolicBloodPressure = systolicBloodPressure;
	}

	public Integer getDiastolicBloodPressure() {
		return diastolicBloodPressure;
	}

	public void setDiastolicBloodPressure(Integer diastolicBloodPressure) {
		this.diastolicBloodPressure = diastolicBloodPressure;
	}

	public Float getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(Float heartRate) {
		this.heartRate = heartRate;
	}

	public Float getbT() {
		return bT;
	}

	public void setbT(Float bT) {
		this.bT = bT;
	}

	public int getnYHAFunctionalClass() {
		return nYHAFunctionalClass;
	}

	public void setnYHAFunctionalClass(int nYHAFunctionalClass) {
		this.nYHAFunctionalClass = nYHAFunctionalClass;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	
    
}
