package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This a data model class for Patient Detail information Packet.
 * @author Taqdir
 *
 */
public class PatientDetailPacket implements Serializable {

	private Long patientID;
	private Long patientEncounterID;
	
	private List<Patient> objListPatient = new ArrayList<Patient>();
	 private List<PatientEncounter> objListPatientEncounter = new ArrayList<PatientEncounter>();
	 private List<PatientSymptoms> objListPatientSymptoms = new ArrayList<PatientSymptoms>();
	 private List<PatientClinicalHistory> objListPatientClinicalHistory = new ArrayList<PatientClinicalHistory>();
	 private List<PatientVitalSigns> objListPatientVitalSigns = new ArrayList<PatientVitalSigns>();
	 private List<Electrocardiogram> objListElectrocardiogram = new ArrayList<Electrocardiogram>();
	 private List<NTproBNP> objListNTproBNP = new ArrayList<NTproBNP>();
	 private List<Echocardiography> objListEchocardiography = new ArrayList<Echocardiography>();
	 private List<PatientDiagnosis> objListPatientDiagnosis = new ArrayList<PatientDiagnosis>();
	public Long getPatientID() {
		return patientID;
	}
	public void setPatientID(Long patientID) {
		this.patientID = patientID;
	}
	public Long getPatientEncounterID() {
		return patientEncounterID;
	}
	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}
	public List<Patient> getObjListPatient() {
		return objListPatient;
	}
	public void setObjListPatient(List<Patient> objListPatient) {
		this.objListPatient = objListPatient;
	}
	public List<PatientEncounter> getObjListPatientEncounter() {
		return objListPatientEncounter;
	}
	public void setObjListPatientEncounter(List<PatientEncounter> objListPatientEncounter) {
		this.objListPatientEncounter = objListPatientEncounter;
	}
	public List<PatientSymptoms> getObjListPatientSymptoms() {
		return objListPatientSymptoms;
	}
	public void setObjListPatientSymptoms(List<PatientSymptoms> objListPatientSymptoms) {
		this.objListPatientSymptoms = objListPatientSymptoms;
	}
	public List<PatientClinicalHistory> getObjListPatientClinicalHistory() {
		return objListPatientClinicalHistory;
	}
	public void setObjListPatientClinicalHistory(List<PatientClinicalHistory> objListPatientClinicalHistory) {
		this.objListPatientClinicalHistory = objListPatientClinicalHistory;
	}
	public List<PatientVitalSigns> getObjListPatientVitalSigns() {
		return objListPatientVitalSigns;
	}
	public void setObjListPatientVitalSigns(List<PatientVitalSigns> objListPatientVitalSigns) {
		this.objListPatientVitalSigns = objListPatientVitalSigns;
	}
	public List<Electrocardiogram> getObjListElectrocardiogram() {
		return objListElectrocardiogram;
	}
	public void setObjListElectrocardiogram(List<Electrocardiogram> objListElectrocardiogram) {
		this.objListElectrocardiogram = objListElectrocardiogram;
	}
	public List<NTproBNP> getObjListNTproBNP() {
		return objListNTproBNP;
	}
	public void setObjListNTproBNP(List<NTproBNP> objListNTproBNP) {
		this.objListNTproBNP = objListNTproBNP;
	}
	public List<Echocardiography> getObjListEchocardiography() {
		return objListEchocardiography;
	}
	public void setObjListEchocardiography(List<Echocardiography> objListEchocardiography) {
		this.objListEchocardiography = objListEchocardiography;
	}
	public List<PatientDiagnosis> getObjListPatientDiagnosis() {
		return objListPatientDiagnosis;
	}
	public void setObjListPatientDiagnosis(List<PatientDiagnosis> objListPatientDiagnosis) {
		this.objListPatientDiagnosis = objListPatientDiagnosis;
	}
	 
	
}
