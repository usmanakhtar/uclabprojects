package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.PatientSymptoms;
import org.uclab.ecrf.cardiovascular.datamodel.PatientVitalSigns;

/**
 * This is PatientVitalSignsAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientVitalSignsAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(PatientVitalSignsAdapter.class);
    public PatientVitalSignsAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving PatientVitalSigns.
     * @param objPatientVitalSigns
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatientVitalSigns) {
		PatientVitalSigns objInnerPatientVitalSigns = new PatientVitalSigns();
		objInnerPatientVitalSigns =  (PatientVitalSigns) objPatientVitalSigns;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientVitalSigns(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientVitalSigns.getPatientEncounterID());
	         objCallableStatement.setInt("IsItMeasure", objInnerPatientVitalSigns.getIsItMeasure());
	         if (objInnerPatientVitalSigns.getSystolicBloodPressure() != null)
	         {
	        	 objCallableStatement.setInt("SystolicBloodPressure", objInnerPatientVitalSigns.getSystolicBloodPressure());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("SystolicBloodPressure", java.sql.Types.INTEGER);
	         }
	         
	         if (objInnerPatientVitalSigns.getDiastolicBloodPressure() != null)
	         {
	        	 objCallableStatement.setInt("DiastolicBloodPressure", objInnerPatientVitalSigns.getDiastolicBloodPressure());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("DiastolicBloodPressure", java.sql.Types.INTEGER);
	         }
	         
	         if (objInnerPatientVitalSigns.getHeartRate() != null)
	         {
	        	 objCallableStatement.setFloat("HeartRate", objInnerPatientVitalSigns.getHeartRate());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("HeartRate", java.sql.Types.FLOAT);
	         }
	         
	         if (objInnerPatientVitalSigns.getbT() != null)
	         {
	        	 objCallableStatement.setFloat("BT", objInnerPatientVitalSigns.getbT());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("BT", java.sql.Types.FLOAT);
	         }
	         objCallableStatement.setInt("NYHAFunctionalClass", objInnerPatientVitalSigns.getnYHAFunctionalClass());
	         objCallableStatement.setLong("CreatedBy", objInnerPatientVitalSigns.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("VitalSignID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intVitalSignID = objCallableStatement.getInt("VitalSignID");
	         objDbResponse.add(String.valueOf(intVitalSignID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("User saved successfully, PatientVitalSigns Details="+objPatientVitalSigns);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, PatientVitalSigns Details=");
	     	objDbResponse.add("Error in saving PatientVitalSigns :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}
	
	/**
     * This is implementation function for Updating PatientVitalSigns.
     * @param objPatientVitalSigns
     * @return List of String 
     */
	@Override
	public  List<String> Update(Object objPatientVitalSigns) {
		PatientVitalSigns objInnerPatientVitalSigns = new PatientVitalSigns();
		objInnerPatientVitalSigns =  (PatientVitalSigns) objPatientVitalSigns;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_update_PatientVitalSigns(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("VitalSignID", objInnerPatientVitalSigns.getVitalSignID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientVitalSigns.getPatientEncounterID());
	         objCallableStatement.setInt("IsItMeasure", objInnerPatientVitalSigns.getIsItMeasure());
	         if (objInnerPatientVitalSigns.getSystolicBloodPressure() != null)
	         {
	        	 objCallableStatement.setInt("SystolicBloodPressure", objInnerPatientVitalSigns.getSystolicBloodPressure());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("SystolicBloodPressure", java.sql.Types.INTEGER);
	         }
	         
	         if (objInnerPatientVitalSigns.getDiastolicBloodPressure() != null)
	         {
	        	 objCallableStatement.setInt("DiastolicBloodPressure", objInnerPatientVitalSigns.getDiastolicBloodPressure());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("DiastolicBloodPressure", java.sql.Types.INTEGER);
	         }
	         
	         if (objInnerPatientVitalSigns.getHeartRate() != null)
	         {
	        	 objCallableStatement.setFloat("HeartRate", objInnerPatientVitalSigns.getHeartRate());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("HeartRate", java.sql.Types.FLOAT);
	         }
	         
	         if (objInnerPatientVitalSigns.getbT() != null)
	         {
	        	 objCallableStatement.setFloat("BT", objInnerPatientVitalSigns.getbT());
	         }
	         else
	         {
	        	 objCallableStatement.setNull("BT", java.sql.Types.FLOAT);
	         }
	         
	         objCallableStatement.setInt("NYHAFunctionalClass", objInnerPatientVitalSigns.getnYHAFunctionalClass());
	         objCallableStatement.setLong("UpdatedBy", objInnerPatientVitalSigns.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerPatientVitalSigns.getVitalSignID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("PatientVitalSigns updated successfully, PatientVitalSigns Details="+objPatientVitalSigns);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in updating user, PatientVitalSigns Details=");
	     	objDbResponse.add("Error in updating PatientVitalSigns :" + e.getMessage());
	     	System.out.println("Error Occured: " + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}
	
	/**
	  * This is implementation function for retrieving PatientVitalSigns. 
	  * @param objPatientVitalSigns
	  * @return List of PatientVitalSigns
	  */
	@Override
	public List<PatientVitalSigns> RetriveData(Object objPatientVitalSigns) {
		PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
		List<PatientVitalSigns> objListInnerPatientVitalSigns = new ArrayList<PatientVitalSigns>();
		objOuterPatientVitalSigns =  (PatientVitalSigns) objPatientVitalSigns;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientVitalSigns.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientVitalSignsByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientVitalSigns.getPatientEncounterID());
		     }
		     else if(objOuterPatientVitalSigns.getRequestType() == "ByVitalSignID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientVitalSignsByVitalSignID(?)}");
		         objCallableStatement.setLong("VitalSignID", objOuterPatientVitalSigns.getVitalSignID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		     	PatientVitalSigns objInnerPatientVitalSigns = new PatientVitalSigns();
		     	objInnerPatientVitalSigns.setVitalSignID(objResultSet.getLong("VitalSignID"));
		     	objInnerPatientVitalSigns.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		     	objInnerPatientVitalSigns.setIsItMeasure(objResultSet.getInt("IsItMeasure"));
		     	if(objResultSet.getObject("SystolicBloodPressure") != null)
		     	{
		     		objInnerPatientVitalSigns.setSystolicBloodPressure(objResultSet.getInt("SystolicBloodPressure"));
		     	}
		     	if(objResultSet.getObject("DiastolicBloodPressure") != null)
		     	{
		     		objInnerPatientVitalSigns.setDiastolicBloodPressure(objResultSet.getInt("DiastolicBloodPressure"));
		     	}		     	
		     	if(objResultSet.getObject("HeartRate") != null)
		     	{
		     		objInnerPatientVitalSigns.setHeartRate(objResultSet.getFloat("HeartRate"));
		     	}
		     	if(objResultSet.getObject("BT") != null)
		     	{
		     		objInnerPatientVitalSigns.setbT(objResultSet.getFloat("BT"));
		     	}

		     	objInnerPatientVitalSigns.setnYHAFunctionalClass(objResultSet.getInt("NYHAFunctionalClass"));
	     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientVitalSigns.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientVitalSigns.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientVitalSigns.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientVitalSigns.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientVitalSigns.add(objInnerPatientVitalSigns);
		     }
		     objConn.close();
		     logger.info("PatientVitalSigns loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientVitalSigns :" + e.getMessage());
		 }   
		 return objListInnerPatientVitalSigns;
	}
	@Override
	public List<String> Delete(Object objPatientVitalSigns) {
		PatientVitalSigns objOuterPatientVitalSigns = new PatientVitalSigns();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterPatientVitalSigns =  (PatientVitalSigns) objPatientVitalSigns;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientVitalSignsByVitalSignID(?)}");
		     objCallableStatement.setLong("VitalSignID", objOuterPatientVitalSigns.getVitalSignID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterPatientVitalSigns.getVitalSignID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("PatientVitalSigns Deleted successfully, PatientVitalSigns Details="+objOuterPatientVitalSigns);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting PatientVitalSigns, PatientVitalSigns Details=");
		 	objDbResponse.add("Error in deleting PatientVitalSigns :" + e.getMessage());
		 }   
		 return objDbResponse;
	}
	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}
    
}
