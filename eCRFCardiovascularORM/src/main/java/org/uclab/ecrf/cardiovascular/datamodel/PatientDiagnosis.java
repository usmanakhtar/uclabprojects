package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;

/**
 * This a data model class for PatientDiagnosis.
 * @author Taqdir
 *
 */
public class PatientDiagnosis implements Serializable {

	private Long patientDiagnosisID;
	private Long patientEncounterID;
    private int physicianDecision;
    private int cDSSDecision;
    private String diagnosisDate;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getPatientDiagnosisID() {
		return patientDiagnosisID;
	}

	public void setPatientDiagnosisID(Long patientDiagnosisID) {
		this.patientDiagnosisID = patientDiagnosisID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getPhysicianDecision() {
		return physicianDecision;
	}

	public void setPhysicianDecision(int physicianDecision) {
		this.physicianDecision = physicianDecision;
	}

	public int getcDSSDecision() {
		return cDSSDecision;
	}

	public void setcDSSDecision(int cDSSDecision) {
		this.cDSSDecision = cDSSDecision;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisDate(String diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

    
}
