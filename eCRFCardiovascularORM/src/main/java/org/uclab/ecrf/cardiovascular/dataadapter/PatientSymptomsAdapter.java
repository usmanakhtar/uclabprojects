package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.PatientSymptoms;

/**
 * This is PatientSymptomsAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientSymptomsAdapter implements DataAccessInterface  {

	private Connection objConn;
    
    private static final Logger logger = LoggerFactory.getLogger(PatientSymptomsAdapter.class);
    
    public PatientSymptomsAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving PatientSymptoms.
     * @param objPatientSymptoms
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatientSymptoms) {
		PatientSymptoms objInnerPatientSymptoms = new PatientSymptoms();
		objInnerPatientSymptoms =  (PatientSymptoms) objPatientSymptoms;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_PatientSymptoms(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientSymptoms.getPatientEncounterID());
	         objCallableStatement.setInt("Breathlessness", objInnerPatientSymptoms.getBreathlessness());
	         objCallableStatement.setInt("Orthopnoea", objInnerPatientSymptoms.getOrthopnoea());
	         objCallableStatement.setInt("PND", objInnerPatientSymptoms.getpND());
	         objCallableStatement.setInt("ReducedExerciseTolerance", objInnerPatientSymptoms.getReducedExerciseTolerance());
	         objCallableStatement.setInt("Fatigue", objInnerPatientSymptoms.getFatigue());
	         objCallableStatement.setInt("Tiredness", objInnerPatientSymptoms.getTiredness());
	         objCallableStatement.setInt("AnkleSwelling", objInnerPatientSymptoms.getAnkleSwelling());
	         objCallableStatement.setLong("CreatedBy", objInnerPatientSymptoms.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("PatientSymptomID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intPatientSymptomID = objCallableStatement.getInt("PatientSymptomID");
	         objDbResponse.add(String.valueOf(intPatientSymptomID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("User saved successfully, PatientSymptoms Details="+objPatientSymptoms);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, PatientSymptoms Details=");
	     	objDbResponse.add("Error in saving PatientSymptoms :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	
	/**
	  * This is implementation function for updating PatientSymptoms.
	  * @param objPatientSymptoms
	  * @return List of String 
	  */
	@Override
	public List<String> Update(Object objPatientSymptoms) {
		PatientSymptoms objInnerPatientSymptoms = new PatientSymptoms();
		objInnerPatientSymptoms =  (PatientSymptoms) objPatientSymptoms;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_PatientSymptoms(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientSymptomID", objInnerPatientSymptoms.getPatientSymptomID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerPatientSymptoms.getPatientEncounterID());
	         objCallableStatement.setInt("Breathlessness", objInnerPatientSymptoms.getBreathlessness());
	         objCallableStatement.setInt("Orthopnoea", objInnerPatientSymptoms.getOrthopnoea());
	         objCallableStatement.setInt("PND", objInnerPatientSymptoms.getpND());
	         objCallableStatement.setInt("ReducedExerciseTolerance", objInnerPatientSymptoms.getReducedExerciseTolerance());
	         objCallableStatement.setInt("Fatigue", objInnerPatientSymptoms.getFatigue());
	         objCallableStatement.setInt("Tiredness", objInnerPatientSymptoms.getTiredness());
	         objCallableStatement.setInt("AnkleSwelling", objInnerPatientSymptoms.getAnkleSwelling());
	         objCallableStatement.setLong("UpdatedBy", objInnerPatientSymptoms.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerPatientSymptoms.getPatientSymptomID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("User udpated successfully, PatientSymptoms Details="+objPatientSymptoms);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in udpating PatientSymptoms, PatientSymptoms Details=");
	     	objDbResponse.add("Error in Updating PatientSymptoms :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}
	
	/**
	  * This is implementation function for retrieving PatientSymptoms. 
	  * @param objPatientSymptoms
	  * @return List of PatientSymptoms
	  */

	@Override
	public List<PatientSymptoms> RetriveData(Object objPatientSymptoms) {
		PatientSymptoms objOuterPatientSymptoms = new PatientSymptoms();
		List<PatientSymptoms> objListInnerPatientSymptoms = new ArrayList<PatientSymptoms>();
		objOuterPatientSymptoms =  (PatientSymptoms) objPatientSymptoms;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterPatientSymptoms.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientSymptomsByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterPatientSymptoms.getPatientEncounterID());
		     }
		     else if(objOuterPatientSymptoms.getRequestType() == "ByPatientSymptomsID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientSymptomsByPatientSymptomsID(?)}");
		         objCallableStatement.setLong("PatientSymptomID", objOuterPatientSymptoms.getPatientSymptomID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		     	PatientSymptoms objInnerPatientSymptoms = new PatientSymptoms();
		     	objInnerPatientSymptoms.setPatientSymptomID(objResultSet.getLong("PatientSymptomID"));
		     	objInnerPatientSymptoms.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		     	objInnerPatientSymptoms.setBreathlessness(objResultSet.getInt("Breathlessness"));
		     	objInnerPatientSymptoms.setOrthopnoea(objResultSet.getInt("Orthopnoea"));
		     	objInnerPatientSymptoms.setpND(objResultSet.getInt("PND"));
		     	objInnerPatientSymptoms.setReducedExerciseTolerance(objResultSet.getInt("ReducedExerciseTolerance"));
		     	objInnerPatientSymptoms.setFatigue(objResultSet.getInt("Fatigue"));
		     	objInnerPatientSymptoms.setTiredness(objResultSet.getInt("Tiredness"));
		     	objInnerPatientSymptoms.setAnkleSwelling(objResultSet.getInt("AnkleSwelling"));
		     	
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerPatientSymptoms.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerPatientSymptoms.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerPatientSymptoms.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerPatientSymptoms.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerPatientSymptoms.add(objInnerPatientSymptoms);
		     }
		     objConn.close();
		     logger.info("PatientSymptoms loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting PatientSymptoms :" + e.getMessage());
		 }   
		 return objListInnerPatientSymptoms;
	}

	@Override
	public List<String> Delete(Object objPatientSymptoms) {
		PatientSymptoms objOuterPatientSymptoms = new PatientSymptoms();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterPatientSymptoms =  (PatientSymptoms) objPatientSymptoms;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientSymptomsByPatientSymptomsID(?)}");
		     objCallableStatement.setLong("PatientSymptomID", objOuterPatientSymptoms.getPatientSymptomID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterPatientSymptoms.getPatientSymptomID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("PatientSymptoms Deleted successfully, PatientSymptoms Details="+objOuterPatientSymptoms);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting PatientSymptoms, PatientSymptoms Details=");
		 	objDbResponse.add("Error in deleting PatientSymptoms :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
		
	}

}
