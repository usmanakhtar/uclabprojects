package org.uclab.ecrf.cardiovascular;


/**
 *
 * @author Taqdir
 */
public enum StorageMedium {
	DATABASE, RDF, XML, OWL
}
