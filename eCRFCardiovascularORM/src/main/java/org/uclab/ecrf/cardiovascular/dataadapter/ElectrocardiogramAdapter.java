package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.Electrocardiogram;
/**
 * This is ElectrocardiogramAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */

public class ElectrocardiogramAdapter implements DataAccessInterface {

	private Connection objConn;
    private static final Logger logger = LoggerFactory.getLogger(ElectrocardiogramAdapter.class);
    
    public ElectrocardiogramAdapter()
    {
        
    }
    
    /**
     * This is implementation function for saving Electrocardiogram.
     * @param objElectrocardiogram
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objElectrocardiogram) {
		Electrocardiogram objInnerElectrocardiogram = new Electrocardiogram();
		objInnerElectrocardiogram =  (Electrocardiogram) objElectrocardiogram;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_Electrocardiogram(?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("PatientEncounterID", objInnerElectrocardiogram.getPatientEncounterID());
	         objCallableStatement.setInt("WhetherToMeasure", objInnerElectrocardiogram.getWhetherToMeasure());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerElectrocardiogram.getMeasurementDate() != null)
	         {
	        	 Date dtMeasurementDate = sdf.parse(objInnerElectrocardiogram.getMeasurementDate());
	             Timestamp tsMeasurementDate = new Timestamp(dtMeasurementDate.getYear(),dtMeasurementDate.getMonth(), dtMeasurementDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("MeasurementDate", tsMeasurementDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("MeasurementDate", null);
	         }
	         
	         
	         objCallableStatement.setInt("ExaminationFindings", objInnerElectrocardiogram.getExaminationFindings());
	         objCallableStatement.setLong("CreatedBy", objInnerElectrocardiogram.getCreatedBy());
	         
	         objCallableStatement.registerOutParameter("ElectrocardiogramID", Types.BIGINT);
	         objCallableStatement.execute();
	         
	         int intElectrocardiogramID = objCallableStatement.getInt("ElectrocardiogramID");
	         objDbResponse.add(String.valueOf(intElectrocardiogramID));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Electrocardiogram saved successfully, Electrocardiogram Details="+objElectrocardiogram);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in saving user, Electrocardiogram Details=");
	     	objDbResponse.add("Error in saving Electrocardiogram :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
     * This is implementation function for saving Electrocardiogram.
     * @param objElectrocardiogram
     * @return List of String 
     */
	@Override
	public List<String> Update(Object objElectrocardiogram) {
		Electrocardiogram objInnerElectrocardiogram = new Electrocardiogram();
		objInnerElectrocardiogram =  (Electrocardiogram) objElectrocardiogram;
		List<String> objDbResponse = new ArrayList<>();
	     
	     try
	     {

	         CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_Electrocardiogram(?, ?, ?, ?, ?, ?)}");
	         
	         objCallableStatement.setLong("ElectrocardiogramID", objInnerElectrocardiogram.getElectrocardiogramID());
	         objCallableStatement.setLong("PatientEncounterID", objInnerElectrocardiogram.getPatientEncounterID());
	         objCallableStatement.setInt("WhetherToMeasure", objInnerElectrocardiogram.getWhetherToMeasure());
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
	         if(objInnerElectrocardiogram.getMeasurementDate() != null)
	         {
	        	 Date dtMeasurementDate = sdf.parse(objInnerElectrocardiogram.getMeasurementDate());
	             Timestamp tsMeasurementDate = new Timestamp(dtMeasurementDate.getYear(),dtMeasurementDate.getMonth(), dtMeasurementDate.getDate(), 00, 00, 00, 00);
	             objCallableStatement.setTimestamp("MeasurementDate", tsMeasurementDate);
	         }
	         else
	         {
	        	 objCallableStatement.setTimestamp("MeasurementDate", null);
	         }
	         
	         
	         objCallableStatement.setInt("ExaminationFindings", objInnerElectrocardiogram.getExaminationFindings());
	         objCallableStatement.setLong("UpdatedBy", objInnerElectrocardiogram.getUpdatedBy());
	         
	         objCallableStatement.execute();
	         
	         objDbResponse.add(String.valueOf(objInnerElectrocardiogram.getElectrocardiogramID()));
	         objDbResponse.add("No Error");
	         
	         objConn.close();
	         logger.info("Electrocardiogram updated successfully, Electrocardiogram Details="+objElectrocardiogram);
	     }
	     catch (Exception e)
	     {
	     	logger.info("Error in updating user, Electrocardiogram Details=");
	     	objDbResponse.add("Error in updating Electrocardiogram :" + e.getMessage());
	     }   
	     
	     return objDbResponse;
	}

	/**
	  * This is implementation function for retrieving Electrocardiogram. 
	  * @param objElectrocardiogram
	  * @return List of Electrocardiogram
	  */

	@Override
	public List<Electrocardiogram> RetriveData(Object objElectrocardiogram) {
		Electrocardiogram objOuterElectrocardiogram = new Electrocardiogram();
		List<Electrocardiogram> objListInnerElectrocardiogram = new ArrayList<Electrocardiogram>();
		objOuterElectrocardiogram =  (Electrocardiogram) objElectrocardiogram;
     
		 try
		 {
		     CallableStatement objCallableStatement = null;
		     if(objOuterElectrocardiogram.getRequestType() == "ByPatientEncounterID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_ElectrocardiogramByPatientEncounterID(?)}");
		         objCallableStatement.setLong("PatientEncounterID", objOuterElectrocardiogram.getPatientEncounterID());
		     }
		     else if(objOuterElectrocardiogram.getRequestType() == "ByElectrocardiogramID")
		     {
		         objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_ElectrocardiogramByElectrocardiogramID(?)}");
		         objCallableStatement.setLong("ElectrocardiogramID", objOuterElectrocardiogram.getElectrocardiogramID());
		     }
		     
		     ResultSet objResultSet = objCallableStatement.executeQuery();
		
		     while(objResultSet.next())
		     {
		    	 Electrocardiogram objInnerElectrocardiogram = new Electrocardiogram();
		    	 objInnerElectrocardiogram.setElectrocardiogramID(objResultSet.getLong("ElectrocardiogramID"));
		    	 objInnerElectrocardiogram.setPatientEncounterID(objResultSet.getLong("PatientEncounterID"));
		    	 objInnerElectrocardiogram.setWhetherToMeasure(objResultSet.getInt("WhetherToMeasure"));
		    	
		    	 if(objResultSet.getTimestamp("MeasurementDate") != null)
			     	{
			         	Timestamp tsMeasurementDate = objResultSet.getTimestamp("MeasurementDate");
			         	objInnerElectrocardiogram.setMeasurementDate(tsMeasurementDate.toString());
			     	}
		    	 
		    	 objInnerElectrocardiogram.setExaminationFindings(objResultSet.getInt("ExaminationFindings"));
		    	 
		     	if(objResultSet.getTimestamp("CreatedDate") != null)
		     	{
		         	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
		         	objInnerElectrocardiogram.setCreatedDate(tsCreatedDate.toString());
		     	}
		     	objInnerElectrocardiogram.setCreatedBy(objResultSet.getLong("CreatedBy"));
		         if(objResultSet.getTimestamp("UpdatedDate") != null)
		     	{
		         	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
		         	objInnerElectrocardiogram.setUpdatedDate(tsUpdatedDate.toString());
		     	}
		         objInnerElectrocardiogram.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
		                         
		         objListInnerElectrocardiogram.add(objInnerElectrocardiogram);
		     }
		     objConn.close();
		     logger.info("Electrocardiogram loaded successfully.");
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in getting Electrocardiogram :" + e.getMessage());
		 }   
		 return objListInnerElectrocardiogram;
	}

	@Override
	public  List<String> Delete(Object objElectrocardiogram) {
		Electrocardiogram objOuterElectrocardiogram = new Electrocardiogram();
		 List<String> objDbResponse = new ArrayList<>();
		 objOuterElectrocardiogram =  (Electrocardiogram) objElectrocardiogram;
		 try
		 {
		     CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_ElectrocardiogramByElectrocardiogramID(?)}");
		     objCallableStatement.setLong("ElectrocardiogramID", objOuterElectrocardiogram.getElectrocardiogramID());
		     objCallableStatement.execute();
		 
		     objDbResponse.add(String.valueOf(objOuterElectrocardiogram.getElectrocardiogramID()));
		     objDbResponse.add("No Error");
		 
		     objConn.close();
		     logger.info("Electrocardiogram Deleted successfully, Electrocardiogram Details="+objOuterElectrocardiogram);
		 }
		 catch (Exception e)
		 {
		 	logger.info("Error in deleting Electrocardiogram, Electrocardiogram Details=");
		 	objDbResponse.add("Error in deleting Electrocardiogram :" + e.getMessage());
		 }   
		 return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
		 {
		    objConn = (Connection)objConf;
		    logger.info("Database connected successfully");
		 }
		 catch(Exception ex)
		 {
			 logger.info("Error in connection to Database");
		 }
	}

}
