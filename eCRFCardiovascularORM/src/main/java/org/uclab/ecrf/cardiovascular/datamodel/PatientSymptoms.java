package org.uclab.ecrf.cardiovascular.datamodel;

import java.io.Serializable;

/**
 * This a data model class for Patient Symptoms.
 * @author Taqdir
 *
 */

public class PatientSymptoms implements Serializable {

	private Long patientSymptomID;
	private Long patientEncounterID;
    private int breathlessness;
    private int orthopnoea;
    private int pND;
    private int reducedExerciseTolerance;
    private int fatigue;
    private int tiredness;
    private int ankleSwelling;
    private String createdDate;
    private Long createdBy;
    private String updatedDate;
    private Long updatedBy;
    
    private String requestType;

	public Long getPatientSymptomID() {
		return patientSymptomID;
	}

	public void setPatientSymptomID(Long patientSymptomID) {
		this.patientSymptomID = patientSymptomID;
	}

	public Long getPatientEncounterID() {
		return patientEncounterID;
	}

	public void setPatientEncounterID(Long patientEncounterID) {
		this.patientEncounterID = patientEncounterID;
	}

	public int getBreathlessness() {
		return breathlessness;
	}

	public void setBreathlessness(int breathlessness) {
		this.breathlessness = breathlessness;
	}

	public int getOrthopnoea() {
		return orthopnoea;
	}

	public void setOrthopnoea(int orthopnoea) {
		this.orthopnoea = orthopnoea;
	}

	public int getpND() {
		return pND;
	}

	public void setpND(int pND) {
		this.pND = pND;
	}

	public int getReducedExerciseTolerance() {
		return reducedExerciseTolerance;
	}

	public void setReducedExerciseTolerance(int reducedExerciseTolerance) {
		this.reducedExerciseTolerance = reducedExerciseTolerance;
	}

	public int getFatigue() {
		return fatigue;
	}

	public void setFatigue(int fatigue) {
		this.fatigue = fatigue;
	}

	public int getTiredness() {
		return tiredness;
	}

	public void setTiredness(int tiredness) {
		this.tiredness = tiredness;
	}

	public int getAnkleSwelling() {
		return ankleSwelling;
	}

	public void setAnkleSwelling(int ankleSwelling) {
		this.ankleSwelling = ankleSwelling;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

   
	
}
