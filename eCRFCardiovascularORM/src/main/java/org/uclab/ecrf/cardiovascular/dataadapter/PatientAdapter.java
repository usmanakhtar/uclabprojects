package org.uclab.ecrf.cardiovascular.dataadapter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uclab.ecrf.cardiovascular.DataAccessInterface;
import org.uclab.ecrf.cardiovascular.datamodel.Patient;

/**
 * This is PatientAdapter class which implements the Data Access Interface for CRUD operations
 * @author Taqdir
 */
public class PatientAdapter implements DataAccessInterface {

	 private Connection objConn;
	    
	    private static final Logger logger = LoggerFactory.getLogger(PatientAdapter.class);
	    
	    public PatientAdapter()
	    {
	        
	    }

    /**
     * This is implementation function for saving Patient.
     * @param objPatient
     * @return List of String 
     */
	@Override
	public List<String> Save(Object objPatient) {
		Patient objInnerPatient = new Patient();
        objInnerPatient =  (Patient) objPatient;
        List<String> objDbResponse = new ArrayList<>();
        
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Add_Patient(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            
            objCallableStatement.setString("PatientMRNNo", objInnerPatient.getPatientMRNNo());
            objCallableStatement.setString("PatientName", objInnerPatient.getPatientName());
           	            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
            if(objInnerPatient.getDateOfBirth() != null)
            {
            	Date dtDateOfBirth = sdf.parse(objInnerPatient.getDateOfBirth());
                Timestamp tsDateOfBirth = new Timestamp(dtDateOfBirth.getYear(),dtDateOfBirth.getMonth(), dtDateOfBirth.getDate(), 00, 00, 00, 00);
                objCallableStatement.setTimestamp("DateOfBirth", tsDateOfBirth);
            }
            else
            {
            	objCallableStatement.setTimestamp("DateOfBirth", null);
            }
            
            objCallableStatement.setString("Gender", objInnerPatient.getGender());
            objCallableStatement.setInt("ConsentForm", objInnerPatient.getConsentForm());
            
            if(objInnerPatient.getConsentDate() != null)
            {
            	Date dtConsentDate = sdf.parse(objInnerPatient.getConsentDate());
                Timestamp tsConsentDate = new Timestamp(dtConsentDate.getYear(),dtConsentDate.getMonth(), dtConsentDate.getDate(), 00, 00, 00, 00);
                objCallableStatement.setTimestamp("ConsentDate", tsConsentDate);
                
            }
            else
            {
            	objCallableStatement.setTimestamp("ConsentDate", null);
            }
            
            objCallableStatement.setString("ScreeningNumber", objInnerPatient.getScreeningNumber());
            objCallableStatement.setLong("CreatedBy", objInnerPatient.getCreatedBy());
            
            objCallableStatement.registerOutParameter("PatientID", Types.BIGINT);
            objCallableStatement.execute();
            
            int intPatientID = objCallableStatement.getInt("PatientID");
            objDbResponse.add(String.valueOf(intPatientID));
            objDbResponse.add("No Error");
            

            objConn.close();
            logger.info("User saved successfully, Patient Details="+objPatient);
        }
        catch (Exception e)
        {
        	logger.info("Error in saving user, Patient Details=");
        	objDbResponse.add("Error in saving Patient :" + e.getMessage());
        }   
        
        return objDbResponse;
	}

	/**
     * This is implementation function for updating Patient.
     * @param objPatient
     * @return List of String 
     */
	
	@Override
	public List<String> Update(Object objPatient) {
		Patient objInnerPatient = new Patient();
        objInnerPatient =  (Patient) objPatient;
        List<String> objDbResponse = new ArrayList<>();
        
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Update_Patient(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
            
            objCallableStatement.setLong("PatientID", objInnerPatient.getPatientID());
            objCallableStatement.setString("PatientMRNNo", objInnerPatient.getPatientMRNNo());
            objCallableStatement.setString("PatientName", objInnerPatient.getPatientName());
           	            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss"); // month updated to MM from mm
            if(objInnerPatient.getDateOfBirth() != null)
            {
            	Date dtDateOfBirth = sdf.parse(objInnerPatient.getDateOfBirth());
                Timestamp tsDateOfBirth = new Timestamp(dtDateOfBirth.getYear(),dtDateOfBirth.getMonth(), dtDateOfBirth.getDate(), 00, 00, 00, 00);
                objCallableStatement.setTimestamp("DateOfBirth", tsDateOfBirth);
            }
            else
            {
            	objCallableStatement.setTimestamp("DateOfBirth", null);
            }
            
            objCallableStatement.setString("Gender", objInnerPatient.getGender());
            objCallableStatement.setInt("ConsentForm", objInnerPatient.getConsentForm());
            
            if(objInnerPatient.getConsentDate() != null)
            {
            	Date dtConsentDate = sdf.parse(objInnerPatient.getConsentDate());
                Timestamp tsConsentDate = new Timestamp(dtConsentDate.getYear(),dtConsentDate.getMonth(), dtConsentDate.getDate(), 00, 00, 00, 00);
                objCallableStatement.setTimestamp("ConsentDate", tsConsentDate);
            }
            else
            {
            	objCallableStatement.setTimestamp("ConsentDate", null);
            }
                        
            objCallableStatement.setString("ScreeningNumber", objInnerPatient.getScreeningNumber());
            objCallableStatement.setLong("UpdatedBy", objInnerPatient.getUpdatedBy());
            
            objCallableStatement.execute();
            
            objDbResponse.add(String.valueOf(objInnerPatient.getPatientID()));
            objDbResponse.add("No Error");
            
            objConn.close();
            logger.info("User udpated successfully, Patient Details="+objPatient);
        }
        catch (Exception e)
        {
        	logger.info("Error in updating Patient, Patient Details=");
        	objDbResponse.add("Error in saving Patient :" + e.getMessage());
        }   
        
        return objDbResponse;
	}
	
    /**
     * This is implementation function for retrieving Patient. 
     * @param objPatient
     * @return List of Patients
     */

	@Override
	public  List<Patient> RetriveData(Object objPatient) {
		Patient objOuterPatient = new Patient();
        List<Patient> objListInnerPatient = new ArrayList<Patient>();
        objOuterPatient =  (Patient) objPatient;
        
        try
        {
            CallableStatement objCallableStatement = null;
            if(objOuterPatient.getRequestType() == "PatientData")
            {
                objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_PatientByPatientID(?)}");
                objCallableStatement.setLong("PatientID", objOuterPatient.getPatientID());
            }
            else if(objOuterPatient.getRequestType() == "AllPatients")
            {
                objCallableStatement = objConn.prepareCall("{call dbo.usp_Get_AllPatients}");
            }
            
            ResultSet objResultSet = objCallableStatement.executeQuery();

            while(objResultSet.next())
            {
            	Patient objInnerPatient = new Patient();
            	objInnerPatient.setPatientID(objResultSet.getLong("PatientID"));
            	objInnerPatient.setPatientMRNNo(objResultSet.getString("PatientMRNNo"));
            	objInnerPatient.setPatientName(objResultSet.getString("PatientName"));
            	  
            	if(objResultSet.getTimestamp("DateOfBirth") != null)
            	{
        		  Timestamp tsDateOfBirth = objResultSet.getTimestamp("DateOfBirth");
                  objInnerPatient.setDateOfBirth(tsDateOfBirth.toString());
            	}
            	  
                objInnerPatient.setGender(objResultSet.getString("Gender"));
                objInnerPatient.setConsentForm(objResultSet.getInt("ConsentForm"));
                
                if(objResultSet.getTimestamp("ConsentDate") != null)
            	{
                	Timestamp tsConsentDate = objResultSet.getTimestamp("ConsentDate");
                    objInnerPatient.setConsentDate(tsConsentDate.toString());
            	}
                               
                objInnerPatient.setScreeningNumber(objResultSet.getString("ScreeningNumber"));
                if(objResultSet.getTimestamp("CreatedDate") != null)
            	{
                	Timestamp tsCreatedDate = objResultSet.getTimestamp("CreatedDate");
                    objInnerPatient.setCreatedDate(tsCreatedDate.toString());
            	}
                objInnerPatient.setCreatedBy(objResultSet.getLong("CreatedBy"));
                if(objResultSet.getTimestamp("UpdatedDate") != null)
            	{
                	Timestamp tsUpdatedDate = objResultSet.getTimestamp("UpdatedDate");
                    objInnerPatient.setUpdatedDate(tsUpdatedDate.toString());
            	}
                objInnerPatient.setUpdatedBy(objResultSet.getLong("UpdatedBy"));
                                
                objListInnerPatient.add(objInnerPatient);
            }
            objConn.close();
            logger.info("Patient loaded successfully.");
        }
        catch (Exception e)
        {
        	logger.info("Error in getting Patient :" + e.getMessage());
        }   
        return objListInnerPatient;
	}

	@Override
	public List<String> Delete(Object objPatient) {
		Patient objOuterPatient = new Patient();
        List<String> objDbResponse = new ArrayList<>();
        objOuterPatient =  (Patient) objPatient;
        try
        {

            CallableStatement objCallableStatement = objConn.prepareCall("{call dbo.usp_Delete_PatientByPatientID(?)}");
            
            objCallableStatement.setLong("PatientID", objOuterPatient.getPatientID());
            objCallableStatement.execute();
            
            objDbResponse.add(String.valueOf(objOuterPatient.getPatientID()));
            objDbResponse.add("No Error");
            
            objConn.close();
            logger.info("Patient Deleted successfully, Patient Details="+objPatient);
        }
        catch (Exception e)
        {
        	logger.info("Error in deleting Patient, Patient Details=");
        	objDbResponse.add("Error in deleting Patient :" + e.getMessage());
        }   
        return objDbResponse;
	}

	@Override
	public void ConfigureAdapter(Object objConf) {
		try
        {
           objConn = (Connection)objConf;
           logger.info("Database connected successfully");
        }
        catch(Exception ex)
        {
       	 logger.info("Error in connection to Database");
        }
		
	}

}
