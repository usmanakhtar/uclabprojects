/**
 * 
 */
package org.uclab.ecrf.cardiovascular;

import java.util.List;

/**
 * @author Taqdir
 *
 */
public interface DataAccessInterface {
	/**
     * This is interface for accessing database with all CRUD operations
     * @param <T> T is any entity persistent object.
     * @param objEntity 
     * @return Returns list of string indicating status of the storage.
     */
	
	
	/**
	 * This is Save interface for insertion data
	 * @param objEntity
	 * @return List of String
	 */
    public <T> List<String> Save(T objEntity);
    
    /**
	 * This is Update interface for editing data
	 * @param objEntity
	 * @return List of String
	 */
    public <T> List<String> Update(T objEntity);
    
    /**
	 * This is RetrieveData interface for fetching data
	 * @param objEntity
	 * @return List of any entity
	 */
    public <T> List<T> RetriveData(T objEntity);
    
    /**
	 * This is Delete interface for deleting data
	 * @param objEntity
	 * @return List of any entity
	 */
    public <T> List<String> Delete(T objEntity);
    
    /**
	 * This is ConfigureAdapter interface for making connection with database
	 * @param objConf
	 */
    public <T> void ConfigureAdapter(T objConf);
}
