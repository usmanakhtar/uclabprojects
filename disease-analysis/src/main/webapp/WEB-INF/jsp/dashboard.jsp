<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
<title>Disease Analysis</title>

<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<spring:url value="/resources/core/css/bootstrap.css"
	var="bootstrapmain" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapmain}" rel="stylesheet" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript">

	//Table Search Bar.
	function search(){
		    var value = jQuery(jQuery("#myInput")).val().toLowerCase();
		    jQuery("#myTable tr").filter(function() {
		    	jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
		    });
	}

</script>
</head>
<body bgcolor="white">

	<form name="dashBoardForm" id="dashBoardForm">

		<div class="card" style="width: 100%">
			<div class="card-header" style="text-align: center">Intelligent
				Knowledge</div>
		</div>
		<table style="width: 100%">
			<tr>
				<td style="width: 10%; vertical-align: baseline;"><jsp:include
						page="menubar.jsp" /></td>
				<td style="width: 40%; vertical-align: -webkit-baseline-middle;">
					<table>
						<tr>
							<td>
								<table style="width: 100%; height: 65px">
									<tr>

										<td><spring:url value="/createNewDT" var="createNewDT"></spring:url>
											<input type="button" value="Add New DT"
											class="btn btn-primary" id="newDTClick"
											onclick="location.href=('${createNewDT}');"></td>
									</tr>
								</table>
							</td>

						</tr>

						<tr>
							<td><input class="form-control" id="myInput" type="text"
								placeholder="Search.." onkeyup="search();"></td>
						</tr>
						<tr>
							<td>
								<table class="table table-bordered table-striped"
									style="width: 100%">
									<thead class="thead-light">
										<tr>
											<th scope="col">DT Name</th>
											<th scope="col">Author</th>
											<th scope="col">Institution</th>
											<th scope="col">Created Date</th>
											<th scope="col">Citation</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody id="myTable">
										<c:forEach items="${DecisionTreeList}" var="model"
											varStatus="index">

											<tr>
												<td>${model.dtName}</td>
												<td>${model.author}</td>
												<td>${model.institution}</td>
												<td>${model.createdDate}</td>
												<td>${model.citation}</td>
												<td><spring:url value="/viewDT/${model.id}"
														var="modelId" /> <spring:url
														value="/dashboard/${model.id}/delete" var="deleteUrl" />
													<input type="button" value="View" class="btn btn-info"
													onclick="location.href='${modelId}'"> <input
													class="btn btn-danger" type="button" value="Delete"
													onclick="location.href=('${deleteUrl}');"> <input
													type="hidden" id="row_${index.index}" value="${model.id}">
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
						</tr>

					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>