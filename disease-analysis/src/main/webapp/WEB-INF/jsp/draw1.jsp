<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--
Copyright (c) 2006-2013, JGraph Ltd

Toolbar example for mxGraph. This example demonstrates using
existing cells as templates for creating new cells.
-->

<html>

<head>
    <title>ToolBar</title>

    <!-- Sets the basepath for the library if not in same directory -->
    <script type="text/javascript">
        mxBasePath = '../javascript/src';
    </script>

    <!-- Loads and initializes the library -->
    <script type="text/javascript" src="../javascript/mxClient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Example code -->
    <script type="text/javascript">
        var graph;
        // Program starts here. Creates a sample graph in the
        // DOM node with the specified ID. This function is invoked
        // from the onLoad event handler of the document (see below).
        function main() {
            // Defines an icon for creating new connections in the connection handler.
            // This will automatically disable the highlighting of the source vertex.
            //mxConnectionHandler.prototype.connectImage = new mxImage('images/connector.gif', 16, 16);
            mxConnectionHandler.prototype.connectImage = new mxImage('../javascript/resources/images/green-dot.gif', 14, 14);
            // Checks if browser is supported
            if (!mxClient.isBrowserSupported()) {
                // Displays an error message if the browser is
                // not supported.
                mxUtils.error('Browser is not supported!', 200, false);
            }
            else {
                // Creates the div for the toolbar
                var tbContainer = document.createElement('div');
                tbContainer.style.position = 'absolute';
                tbContainer.style.overflow = 'hidden';
                tbContainer.style.padding = '2px';
                tbContainer.style.left = '0px';
                tbContainer.style.top = '26px';
                tbContainer.style.width = '24px';
                tbContainer.style.bottom = '0px';

                document.body.appendChild(tbContainer);

                // Creates new toolbar without event processing
                var toolbar = new mxToolbar(tbContainer);
                toolbar.enabled = false

                // Creates the div for the graph
                container = document.createElement('div');
                container.style.position = 'absolute';
                container.style.overflow = 'scroll';
                container.style.left = '24px';
                container.style.top = '26px';
                container.style.right = '0px';
                container.style.bottom = '0px';
                container.style.height = '400px';
                container.style.background = 'url("../javascript/resources/editors/images/grid.gif")';

                document.body.appendChild(container);

        /*         document.body.appendChild(mxUtils.button('View XML', function()
                {
                    var encoder = new mxCodec();
                    var node = encoder.encode(graph.getModel());
                    mxUtils.popup(mxUtils.getXml(node), true);
                })); */

                // Workaround for Internet Explorer ignoring certain styles
                if (mxClient.IS_QUIRKS) {
                    document.body.style.overflow = 'hidden';
                    new mxDivResizer(tbContainer);
                    new mxDivResizer(container);
                }

                // Creates the model and the graph inside the container
                // using the fastest rendering available on the browser

                var model = new mxGraphModel();
                graph = new mxGraph(container, model);

                var layout = new mxParallelEdgeLayout(graph);
                var layoutMgr = new mxLayoutManager(graph);
                graph.dropEnabled = true;

                layoutMgr.getLayout = function(cell)
                {
                    if (cell.getChildCount() > 0)
                    {
                        return layout;
                    }
                };

                // Matches DnD inside the graph
                mxDragSource.prototype.getDropTarget = function (graph, x, y) {
                    var cell = graph.getCellAt(x, y);

                    if (!graph.isValidDropTarget(cell)) {
                        cell = null;
                    }

                    return cell;
                };

                // Enables new connections in the graph
                graph.setConnectable(true);
                graph.setMultigraph(false);

                var style = graph.getStylesheet().getDefaultEdgeStyle();
                style[mxConstants.STYLE_ROUNDED] = true;
                style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;

                graph.alternateEdgeStyle = 'elbow=vertical';

                // Stops editing on enter or escape keypress
                var keyHandler = new mxKeyHandler(graph);
                var rubberband = new mxRubberband(graph);

                var addVertex = function (icon, w, h, style) {
                    var vertex = new mxCell(null, new mxGeometry(0, 0, w, h), style);
                    vertex.setVertex(true);

                    addToolbarItem(graph, toolbar, vertex, icon);
                };

                //addVertex('editors/images/swimlane.gif', 120, 160, 'shape=swimlane;startSize=0;');
                addVertex('../javascript/resources/editors/images/rectangle.gif', 120, 50, 'shape=rectangle,width=100%');
                //addVertex('editors/images/rounded.gif', 100, 40, 'shape=rounded');
                addVertex('../javascript/resources/editors/images/ellipse.gif', 100, 70, 'shape=ellipse');
                addVertex('../javascript/resources/editors/images/rhombus.gif', 100, 70, 'shape=rhombus');
                //addVertex('editors/images/triangle.gif', 40, 40, 'shape=triangle');
                //addVertex('editors/images/cylinder.gif', 40, 40, 'shape=cylinder');
                //addVertex('editors/images/actor.gif', 30, 40, 'shape=actor');
                toolbar.addLine();

                // For Deleting objects from Graph on delete button.
                var keyHandler = new mxKeyHandler(graph);
                keyHandler.bindKey(46, function (evt) {
                    if (graph.isEnabled()) {
                        graph.removeCells();
                    }
                });


            }
            
            // Loads the mxGraph file format (XML file)
            
            if (document.getElementById('dtXmlId').value > 0){
            	load();
            }
            //read(graph, document.getElementById('payloadData').value);
        }

        function addToolbarItem(graph, toolbar, prototype, image) {
            // Function that is executed when the image is dropped on
            // the graph. The cell argument points to the cell under
            // the mousepointer if there is one.
            var funct = function (graph, evt, cell) {
                graph.stopEditing(false);

                var pt = graph.getPointForEvent(evt);
                var vertex = graph.getModel().cloneCell(prototype);
                vertex.geometry.x = pt.x;
                vertex.geometry.y = pt.y;

                graph.setSelectionCells(graph.importCells([vertex], 0, 0, cell));
            }

            // Creates the image which is used as the drag icon (preview)
            var img = toolbar.addMode(null, image, funct);
            mxUtils.makeDraggable(img, graph, funct);
        }

        // Parses the mxGraph XML file format
        function read(graph, payload)
        {
            

            var xml = parseXml(payload);
            var root = xml.documentElement;

            var dec = new mxCodec(xml.ownerDocument);
            dec.decode(root, graph.getModel());
        };

        function parseXml(xmlStr) {
            return new window.DOMParser().parseFromString(xmlStr, "text/xml");
        }

        function save(){

            var result = confirm("Are you want to Save changes!");

            if(result){
                var encoder = new mxCodec();
                var node = encoder.encode(graph.getModel());
                //alert(mxUtils.getXml(node));

                //location.href = '/save?payload=testing';
                var payloadData = {payload : mxUtils.getXml(node),
                		dtName : document.getElementById("dtName").value,
                		author : document.getElementById("author").value,
                		dtXmlId : document.getElementById("dtXmlId").value};
                
                jQuery.ajax({
                    type: "POST",
                    url: "save",
                    data: payloadData,
                    success: function (result) {
                    	 document.getElementById("dtXmlId").value = result ;
                        // do something.
                    },
                    error: function (result) {
                        // do something.
                    }
                });
            }
        }

        function load() {
            //location.href = '/load';

            var payloadData = {dtXmlId : document.getElementById('dtXmlId').value};
     
                jQuery.ajax({
                    type: "GET",
                    url: "load",
                    data: payloadData,
                    success: function (result) {
                        
                        // do something.
                        //var model = new mxGraphModel();
                        //graph = new mxGraph(container, model);

                        read(graph, result.dtXML);
                        document.getElementById("dtName").value = result.dtName;
                        document.getElementById("author").value = result.author;
                    },
                    error: function (result) {
                        // do something.
                    }
                });
            
        }
    </script>
</head>

<!-- Calls the main function after the page has loaded. Container is dynamically created. -->

<body onload="main();">

<input type="button" value="Save" onclick="save()">
<!-- <input type="button" value="Load Information" onclick="load()"> -->
<input type = "text" id = "dtName">
<input type = "text" id = "author">
<c:set var="object" value="${dtXmlId}" />
<input type="hidden" id="dtXmlId" value="${object}"/>

</body>

</html>