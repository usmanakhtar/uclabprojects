package com.aku.diseaseanalysis.repository;


import com.aku.diseaseanalysis.model.Conclusions;
import org.springframework.data.repository.CrudRepository;

public interface ConclusionsRepository extends CrudRepository<Conclusions, Long> {
}
