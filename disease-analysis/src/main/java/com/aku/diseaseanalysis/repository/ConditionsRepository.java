package com.aku.diseaseanalysis.repository;


import com.aku.diseaseanalysis.model.Conditions;
import org.springframework.data.repository.CrudRepository;

public interface ConditionsRepository extends CrudRepository<Conditions, Long> {
}
