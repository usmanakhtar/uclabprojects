package com.aku.diseaseanalysis.repository;


import com.aku.diseaseanalysis.model.KnowledgeDecisionTree;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface knowledgeDecisionTreeRepository extends CrudRepository<KnowledgeDecisionTree, Long>{

    @Query(value = "select u from KnowledgeDecisionTree u where u.Id = :dtXmlId")
    KnowledgeDecisionTree findKnowledgeDecisionTreeById(@Param("dtXmlId") Long dtXMlId );
}
