package com.aku.diseaseanalysis.repository;

import com.aku.diseaseanalysis.model.Rules;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Administrator on 11/13/2019.
 */
public interface RuleRepository extends CrudRepository<Rules, Long> {
}
