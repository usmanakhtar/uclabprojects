package com.aku.diseaseanalysis.repository;


import com.aku.diseaseanalysis.model.RulesConditions;
import org.springframework.data.repository.CrudRepository;

public interface RulesConditionsRepository extends CrudRepository<RulesConditions, Long> {
}
