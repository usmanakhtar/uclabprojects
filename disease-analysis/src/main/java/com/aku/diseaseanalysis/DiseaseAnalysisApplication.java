package com.aku.diseaseanalysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiseaseAnalysisApplication {


    public static void main(String[] args) {
        //System.out.print("hello world");
        SpringApplication.run(DiseaseAnalysisApplication.class, args);
    }

}
