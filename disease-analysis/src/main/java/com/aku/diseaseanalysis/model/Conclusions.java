package com.aku.diseaseanalysis.model;


import javax.persistence.*;
import java.io.Serializable;

@Table(name = "Conclusions")
@Entity
public class Conclusions implements Serializable {

    @Id
    @Column(name = "ConclusionID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long conclusionID;

    @Column(name = "ConclusionKey", length = Integer.MAX_VALUE)
    private String conclusionKey;

    @Column(name = "ConclusionValue", length = Integer.MAX_VALUE)
    private String conclusionValue;

    @Column(name = "ConclusionOperator", length = Integer.MAX_VALUE)
    private String conclusionOperator;

    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name = "ruleID", referencedColumnName = "RuleID")
    private Rules rule;

    public Rules getRule() {
        return rule;
    }

    public void setRule(Rules rule) {
        this.rule = rule;
    }

    public Long getConclusionID() {
        return conclusionID;
    }

    public void setConclusionID(Long conclusionID) {
        this.conclusionID = conclusionID;
    }

    public String getConclusionKey() {
        return conclusionKey;
    }

    public void setConclusionKey(String conclusionKey) {
        this.conclusionKey = conclusionKey;
    }

    public String getConclusionValue() {
        return conclusionValue;
    }

    public void setConclusionValue(String conclusionValue) {
        this.conclusionValue = conclusionValue;
    }

    public String getConclusionOperator() {
        return conclusionOperator;
    }

    public void setConclusionOperator(String conclusionOperator) {
        this.conclusionOperator = conclusionOperator;
    }


}
