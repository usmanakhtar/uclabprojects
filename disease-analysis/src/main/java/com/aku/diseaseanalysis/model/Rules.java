package com.aku.diseaseanalysis.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Table(name = "Rules")
@Entity
public class Rules implements Serializable {

    @Id
    @Column(name = "RuleID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ruleID;

    @Column(name = "RuleTitle", length = Integer.MAX_VALUE)
    private String ruleTitle;

    @Column(name = "Institution", length = Integer.MAX_VALUE)
    private String institution;

    @Column(name = "RuleDescription", length = Integer.MAX_VALUE)
    private String ruleDescription;

    @Column(name = "RuleCondition", length = Integer.MAX_VALUE)
    private String ruleCondition;

    @Column(name = "RuleConclusion", length = Integer.MAX_VALUE)
    private String ruleConclusion;

    @Column(name = "RuleCreateDate")
    @Temporal(TemporalType.DATE)
    private Date ruleCreatedDate;

    @Column(name = "RuleCreatedBy", length = Integer.MAX_VALUE)
    private String ruleCreateBy;

    @Column(name = "SpecialistName", length = Integer.MAX_VALUE)
    private String specialistName;

    @Column(name = "RuleUpdatedBy", length = Integer.MAX_VALUE)
    private String ruleUpdatedBy;

    @Column(name = "RuleLastUpdatedDate")
    @Temporal(TemporalType.DATE)
    private Date ruleLastUpdatedDate;

    @Column(name = "RuleTypeID")
    private Long ruleTypeID;

    @Column(name = "SourceClassName", length = Integer.MAX_VALUE)
    private String sourceClassName;

    @Column(name = "SituationID")
    private Long situationID;

    @Column(name = "TreeReferencePath", length = Integer.MAX_VALUE)
    private String treeReferencePath;

    @Column(name = "RulePriority", length = Integer.MAX_VALUE)
    private String rulePriority;

    @OneToMany(mappedBy = "rule")
    Set<RulesConditions> rulesConditions;

    /*@OneToOne(mappedBy = "rule", optional = true)
    private Conclusions conclusion;

    public Conclusions getConclusion() {
        return conclusion;
    }

    public void setConclusion(Conclusions conclusion) {
        this.conclusion = conclusion;
    }*/

    public Set<RulesConditions> getRulesConditions() {
        return rulesConditions;
    }

    public void setRulesConditions(Set<RulesConditions> rulesConditions) {
        this.rulesConditions = rulesConditions;
    }

    public Long getRuleID() {
        return ruleID;
    }

    public void setRuleID(Long ruleID) {
        this.ruleID = ruleID;
    }

    public String getRuleTitle() {
        return ruleTitle;
    }

    public void setRuleTitle(String ruleTitle) {
        this.ruleTitle = ruleTitle;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        this.ruleDescription = ruleDescription;
    }

    public String getRuleCondition() {
        return ruleCondition;
    }

    public void setRuleCondition(String ruleCondition) {
        this.ruleCondition = ruleCondition;
    }

    public String getRuleConclusion() {
        return ruleConclusion;
    }

    public void setRuleConclusion(String ruleConclusion) {
        this.ruleConclusion = ruleConclusion;
    }

    public Date getRuleCreatedDate() {
        return ruleCreatedDate;
    }

    public void setRuleCreatedDate(Date ruleCreatedDate) {
        this.ruleCreatedDate = ruleCreatedDate;
    }

    public String getRuleCreateBy() {
        return ruleCreateBy;
    }

    public void setRuleCreateBy(String ruleCreateBy) {
        this.ruleCreateBy = ruleCreateBy;
    }

    public String getSpecialistName() {
        return specialistName;
    }

    public void setSpecialistName(String specialistName) {
        this.specialistName = specialistName;
    }

    public String getRuleUpdatedBy() {
        return ruleUpdatedBy;
    }

    public void setRuleUpdatedBy(String ruleUpdatedBy) {
        this.ruleUpdatedBy = ruleUpdatedBy;
    }

    public Date getRuleLastUpdatedDate() {
        return ruleLastUpdatedDate;
    }

    public void setRuleLastUpdatedDate(Date ruleLastUpdatedDate) {
        this.ruleLastUpdatedDate = ruleLastUpdatedDate;
    }

    public Long getRuleTypeID() {
        return ruleTypeID;
    }

    public void setRuleTypeID(Long ruleTypeID) {
        this.ruleTypeID = ruleTypeID;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Long getSituationID() {
        return situationID;
    }

    public void setSituationID(Long situationID) {
        this.situationID = situationID;
    }

    public String getTreeReferencePath() {
        return treeReferencePath;
    }

    public void setTreeReferencePath(String treeReferencePath) {
        this.treeReferencePath = treeReferencePath;
    }

    public String getRulePriority() {
        return rulePriority;
    }

    public void setRulePriority(String rulePriority) {
        this.rulePriority = rulePriority;
    }
}
