package com.aku.diseaseanalysis.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name = "Conditions")
@Entity
public class Conditions implements Serializable{

    @Id
    @Column(name = "ConditionID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long conditionID;

    @Column(name = "ConditionKey", length = Integer.MAX_VALUE)
    private String conditionKey;

    @Column(name = "ConditionValue", length = Integer.MAX_VALUE)
    private String conditionValue;

    @Column(name = "ConditionType", length = Integer.MAX_VALUE)
    private String conditionType;

    @Column(name = "ConditionValueOperator", length = Integer.MAX_VALUE)
    private String conditionValueOperator;

    @OneToMany(mappedBy = "condition")
    Set<RulesConditions> rulesConditions;

    public Set<RulesConditions> getRulesConditions() {
        return rulesConditions;
    }

    public void setRulesConditions(Set<RulesConditions> rulesConditions) {
        this.rulesConditions = rulesConditions;
    }

    public Long getConditionID() {
        return conditionID;
    }

    public void setConditionID(Long conditionID) {
        this.conditionID = conditionID;
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public void setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public String getConditionType() {
        return conditionType;
    }

    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getConditionValueOperator() {
        return conditionValueOperator;
    }

    public void setConditionValueOperator(String conditionValueOperator) {
        this.conditionValueOperator = conditionValueOperator;
    }
}
