package com.aku.diseaseanalysis.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "RulesConditions")
@Entity
public class RulesConditions implements Serializable{

    @Id
    @Column(name = "RuleConditionID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ruleConditionID;

    @ManyToOne
    @JoinColumn
    Rules rule;

    @ManyToOne
    @JoinColumn
    Conditions condition;

    public Rules getRule() {
        return rule;
    }

    public void setRule(Rules rule) {
        this.rule = rule;
    }

    public Conditions getCondition() {
        return condition;
    }

    public void setCondition(Conditions condition) {
        this.condition = condition;
    }

    public Long getRuleConditionID() {
        return ruleConditionID;
    }

    public void setRuleConditionID(Long ruleConditionID) {
        this.ruleConditionID = ruleConditionID;
    }
}
