package com.aku.diseaseanalysis.model;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "Knowledge_Decision_Tree")
@Entity
public class KnowledgeDecisionTree implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "Author", length = Integer.MAX_VALUE)
    private String author;

    @Column(name = "DT_Name", length = Integer.MAX_VALUE)
    private String dtName;

    @Column(name = "DT_Purpose", length = Integer.MAX_VALUE)
    private String dtPurpose;

    @Column(name = "Institution", length = Integer.MAX_VALUE)
    private String institution;

    @Column(name = "Created_Date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Column(name = "Citation", length = Integer.MAX_VALUE)
    private String citation;

    @Column(name = "Domain", length = Integer.MAX_VALUE)
    private String domain;
    
    @Column(name = "DT_XML")
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String dtXML;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDtName() {
        return dtName;
    }

    public void setDtName(String dtName) {
        this.dtName = dtName;
    }

    public String getDtPurpose() {
        return dtPurpose;
    }

    public void setDtPurpose(String dtPurpose) {
        this.dtPurpose = dtPurpose;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCitation() {
        return citation;
    }

    public void setCitation(String citation) {
        this.citation = citation;
    }

    public String getDtXML() {
        return dtXML;
    }

    public void setDtXML(String dtXML) {
        this.dtXML = dtXML;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }
    public String getDomain() {
        return domain;
    }
    
    @Override
    public String toString() {
        return "KnowledgeDecisionTree{" +
                "Id=" + Id +
                ", author='" + author + '\'' +
                ", dtName='" + dtName + '\'' +
                ", dtPurpose='" + dtPurpose + '\'' +
                ", institution='" + institution + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", citation='" + citation + '\'' +
                ", dtXML='" + dtXML + '\'' +
                '}';
    }

}
