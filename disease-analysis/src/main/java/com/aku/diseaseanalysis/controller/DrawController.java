package com.aku.diseaseanalysis.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aku.diseaseanalysis.model.Conclusions;
import com.aku.diseaseanalysis.model.Conditions;
import com.aku.diseaseanalysis.model.KnowledgeDecisionTree;
import com.aku.diseaseanalysis.model.Rules;
import com.aku.diseaseanalysis.model.RulesConditions;
import com.aku.diseaseanalysis.model.jaxb.MxGraphModel;
import com.aku.diseaseanalysis.repository.ConclusionsRepository;
import com.aku.diseaseanalysis.repository.ConditionsRepository;
import com.aku.diseaseanalysis.repository.RuleRepository;
import com.aku.diseaseanalysis.repository.RulesConditionsRepository;
import com.aku.diseaseanalysis.repository.knowledgeDecisionTreeRepository;
import com.aku.diseaseanalysis.utils.Graph;

@Controller
public class DrawController {

	@Autowired
	private knowledgeDecisionTreeRepository knowledgeDecisionTreeRepository;

	@Autowired
	ConditionsRepository conditionsRepository;

	@Autowired
	RulesConditionsRepository rulesConditionsRepository;

	@Autowired
	RuleRepository ruleRepository;

	@Autowired
	ConclusionsRepository conclusionsRepository;

	@RequestMapping(value = "/createNewDT", method = RequestMethod.GET)
	public String createNewDT() {

		// model.addAttribute("test", null);
		return "/draw";
	}

	@RequestMapping(value = "/viewDT/{id}", method = RequestMethod.GET)
	public String viewDT(@PathVariable(value = "id") Long dtId, ModelMap model) {

		model.addAttribute("dtXmlId", dtId);

		return "/draw";
	}

	@RequestMapping(value = { "/save", "/viewDT/save" }, method = RequestMethod.POST)
	public @ResponseBody Long save(@RequestParam(value = "payload") String payload,
			@RequestParam(value = "dtName") String dtName, @RequestParam(value = "author") String author,
			@RequestParam(value = "institution") String institution, @RequestParam(value = "citation") String citation,
			@RequestParam(value = "createdDate") String createdDate, @RequestParam(value = "domain") String domain,
			@RequestParam(value = "purpose") String purpose, @RequestParam(value = "dtXmlId") Long dtXmlId)
			throws ParseException {

		KnowledgeDecisionTree knowledgeDecisionTree = new KnowledgeDecisionTree();
		if (dtXmlId != null) {
			knowledgeDecisionTree.setId(dtXmlId);
		}
		Date date=Date.valueOf(createdDate);

		knowledgeDecisionTree.setAuthor(author);
		knowledgeDecisionTree.setCitation(citation);
		knowledgeDecisionTree.setCreatedDate(date);
		knowledgeDecisionTree.setDtName(dtName);
		knowledgeDecisionTree.setDtPurpose(purpose);
		knowledgeDecisionTree.setDtXML(payload);
		knowledgeDecisionTree.setInstitution(institution);
		knowledgeDecisionTree.setDomain(domain);

		knowledgeDecisionTreeRepository.save(knowledgeDecisionTree);

		MxGraphModel item = null;
		List<MxGraphModel.Root.MxCell> edges = new ArrayList<>();
		List<MxGraphModel.Root.MxCell> vertices = new ArrayList<>();

		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(MxGraphModel.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			InputStream inputStream = new ByteArrayInputStream(payload.getBytes());
			item = (MxGraphModel) unmarshaller.unmarshal(inputStream);

			List<MxGraphModel.Root.MxCell> mxCells = item.getRoot().getMxCell();

			for (MxGraphModel.Root.MxCell mxCell : mxCells){
				if(mxCell.getEdge() != null){
					edges.add(mxCell);
				}else {
					vertices.add(mxCell);
				}
			}

			Graph g = new Graph(200);

			for(MxGraphModel.Root.MxCell edge : edges){
				g.addEdge(edge.getSource(), edge.getTarget());
			}

			//source and destination id
			Integer sourceId = searchSourceVertix(vertices, edges);
			List<Integer> destinationIds = searchDestinationVertices(vertices, edges);

			for(Integer destinationId : destinationIds) {
				List<List<Integer>> paths = g.printAllPaths(sourceId, destinationId);

				for (List path : paths) {
					String destinationVerixValue = searchVertix(destinationId, vertices);

					Rules rule = new Rules();
					rule.setInstitution(institution);
					rule.setRuleCreateBy(author);
					rule.setRuleConclusion(destinationVerixValue);
					rule.setRuleCreatedDate(date);

					ruleRepository.save(rule);

					Conclusions conclusion = new Conclusions();
					conclusion.setConclusionKey("DiagnosisRecommendation");
					conclusion.setConclusionOperator("=");
					conclusion.setConclusionValue(destinationVerixValue);
					conclusion.setRule(rule);

					conclusionsRepository.save(conclusion);

					for (int i = 0; i < path.size(); i++) {
						int source = (Integer) path.get(i);
						if (source == destinationId) {
							break;
						}
						int destination = (Integer) path.get(i + 1);

						String edgeValue = searchEdges(source, destination, edges);
						if(edgeValue != null){
							String vertixValue = searchVertix(source, vertices);

							String conditionValue = getConditionValue(edgeValue);

							Conditions conditions = new Conditions();
							conditions.setConditionKey(vertixValue);
							conditions.setConditionValue(conditionValue);
							conditions.setConditionType(getConditionType(conditionValue));
							conditions.setConditionValueOperator(getConditionValueOperator(edgeValue));

							conditionsRepository.save(conditions);

							RulesConditions ruleCondition = new RulesConditions();
							ruleCondition.setRule(rule);
							ruleCondition.setCondition(conditions);

							rulesConditionsRepository.save(ruleCondition);
						}
					}
				}
			}

		}catch (Exception e){}

		return knowledgeDecisionTree.getId();
	}

	public String getConditionValue(String edgeValue){
		String conditionValue = edgeValue;

		if(edgeValue.equalsIgnoreCase("Yes")){
			conditionValue = "1";
		}else if (edgeValue.equalsIgnoreCase("No")){
			conditionValue = "0";
		}else if (edgeValue.contains(">=")){
			conditionValue = edgeValue.split(">=")[1];
		}else if (edgeValue.contains("<=")){
			conditionValue = edgeValue.split("<=")[1];
		} else if (edgeValue.contains(">")){
			conditionValue = edgeValue.split(">")[1];
		}else if (edgeValue.contains("<")){
			conditionValue = edgeValue.split("<")[1];
		}

		return conditionValue;
	}

	public String getConditionType(String conditionValue){
		String conditionType = "string";
		if(StringUtils.isNumeric(conditionValue)){
			conditionType="float";
		}
		return conditionType;
	}

	public String getConditionValueOperator(String edgeValue){
		String conditionValue = "=";

		if(edgeValue.contains(">")){
			conditionValue = ">";
		}else if(edgeValue.contains("<")){
			conditionValue = ">";
		}

		if(edgeValue.contains("=")){
			conditionValue += "=";
		}

		return conditionValue;
	}

	public String searchEdges(int source, int destination, List<MxGraphModel.Root.MxCell> edges){
		for (MxGraphModel.Root.MxCell edge : edges){
			if(edge.getSource().intValue() == source && edge.getTarget().intValue() == destination){
				return edge.getValue();
			}
		}

		return null;
	}

	public String searchVertix(int source, List<MxGraphModel.Root.MxCell> edges){
		for (MxGraphModel.Root.MxCell edge : edges){
			if(edge.getId() == source){
				return edge.getValue();
			}
		}

		return null;
	}

	public Integer searchSourceVertix(List<MxGraphModel.Root.MxCell> vertices, List<MxGraphModel.Root.MxCell> edges){
		for(MxGraphModel.Root.MxCell vertix : vertices){
			if(vertix.getId() > 1){
				boolean isSourceVeritx = true;

				for(MxGraphModel.Root.MxCell edge : edges){
					if(edge.getTarget() == vertix.getId()){
						isSourceVeritx = false;
						break;
					}
				}

				if(isSourceVeritx){
					return vertix.getId().intValue();
				}
			}
		}

		return null;
	}

	public List<Integer> searchDestinationVertices(List<MxGraphModel.Root.MxCell> vertices, List<MxGraphModel.Root.MxCell> edges){
		List<Integer> destinationIds = new ArrayList<>();

		for(MxGraphModel.Root.MxCell vertix : vertices){
			if(vertix.getId() > 1){
				boolean isDestinationVeritx = true;

				for(MxGraphModel.Root.MxCell edge : edges){
					if(edge.getSource() == vertix.getId()){
						isDestinationVeritx = false;
						break;
					}
				}

				if(isDestinationVeritx){
					destinationIds.add(vertix.getId().intValue());
				}
			}
		}

		return destinationIds;
	}

	@RequestMapping(value = "viewDT/load", method = RequestMethod.GET)
	public @ResponseBody KnowledgeDecisionTree load(@RequestParam(value = "dtXmlId") Long dtXmlId) {

		KnowledgeDecisionTree knowledgeDecisionTree = knowledgeDecisionTreeRepository
				.findKnowledgeDecisionTreeById(dtXmlId);

		return knowledgeDecisionTree;
	}

}
