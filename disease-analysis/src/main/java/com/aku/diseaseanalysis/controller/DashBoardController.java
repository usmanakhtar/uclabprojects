package com.aku.diseaseanalysis.controller;


import com.aku.diseaseanalysis.model.KnowledgeDecisionTree;
import com.aku.diseaseanalysis.repository.knowledgeDecisionTreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class DashBoardController {

    @Autowired
    knowledgeDecisionTreeRepository knowledgeDecisionTreeRepository;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model) {

        List<KnowledgeDecisionTree> models = new ArrayList<>();
        Iterable<KnowledgeDecisionTree> iList = knowledgeDecisionTreeRepository.findAll();
        iList.forEach(models::add);

        model.addAttribute("DecisionTreeList", models);
        return "/dashboard";
    }

   
    @RequestMapping(value = "/dashboard/{id}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long dtId, Model model) {
        knowledgeDecisionTreeRepository.deleteById(dtId);

        return "redirect:/dashboard";
    }

}
