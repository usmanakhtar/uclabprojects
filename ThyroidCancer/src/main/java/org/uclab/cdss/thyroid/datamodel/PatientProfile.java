package org.uclab.cdss.thyroid.datamodel;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */
@Entity
@Table(name="tblPatientProfile")
public class PatientProfile {

	@Id
    @Column(name="PatientID")
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int PatientID;

	public int getPatientID() {
		return PatientID;
	}

	public void setPatientID(int patientID) {
		PatientID = patientID;
	}
	
	@Column(name="PatientMRNNo")
	private String PatientMRNNo;
	
	public String getPatientMRNNo() {
		return PatientMRNNo;
	}

	public void setPatientMRNNo(String patientMRNNo) {
		PatientMRNNo = patientMRNNo;
	}

	@Column(name="PatientName")
	private String PatientName;
	
	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String patientName) {
		PatientName = patientName;
	}

	@Column(name="PatientGender")
	private String PatientGender;
	public String getPatientGender() {
		return PatientGender;
	}

	public void setPatientGender(String patientGender) {
		PatientGender = patientGender;
	}
	
	
	@Column(name="PatientAge")
	private int PatientAge;
	
	public int getPatientAge() {
		return PatientAge;
	}

	public void setPatientAge(int patientAge) {
		PatientAge = patientAge;
	}

	@Column(name="FNAResult")
	private String FNAResult;
	
	public String getFNAResult() {
		return FNAResult;
	}

	public void setFNAResult(String fNAResult) {
		FNAResult = fNAResult;
	}

	@Column(name="Pregnancy")
	private String Pregnancy;
	
	public String getPregnancy() {
		return Pregnancy;
	}

	public void setPregnancy(String pregnancy) {
		Pregnancy = pregnancy;
	}

	@Column(name="DeliveryDone")
	private String DeliveryDone;
	
	public String getDeliveryDone() {
		return DeliveryDone;
	}

	public void setDeliveryDone(String deliveryDone) {
		DeliveryDone = deliveryDone;
	}

	@Column(name="ETE")
	private String ETE;
	
	public String getETE() {
		return ETE;
	}

	public void setETE(String eTE) {
		ETE = eTE;
	}

	@Column(name="LNM")
	private String LNM;
	
	public String getLNM() {
		return LNM;
	}

	public void setLNM(String lNM) {
		LNM = lNM;
	}

	@Column(name="DM")
	private String DM;
	
	public String getDM() {
		return DM;
	}

	public void setDM(String dM) {
		DM = dM;
	}

	@Column(name="Multifocality")
	private String Multifocality;
	
	public String getMultifocality() {
		return Multifocality;
	}
	
	@Column(name="ThyroidNoduleExpected")
	private String ThyroidNoduleExpected;

	public String getThyroidNoduleExpected() {
		return ThyroidNoduleExpected;
	}

	public void setThyroidNoduleExpected(String thyroidNoduleExpected) {
		ThyroidNoduleExpected = thyroidNoduleExpected;
	}

	public void setMultifocality(String multifocality) {
		Multifocality = multifocality;
	}

	@Column(name="Radiation")
	private String Radiation;
	
	public String getRadiation() {
		return Radiation;
	}

	public void setRadiation(String radiation) {
		Radiation = radiation;
	}

	@Column(name="FHC")
	private String FHC;
	
	public String getFHC() {
		return FHC;
	}

	public void setFHC(String fHC) {
		FHC = fHC;
	}

	@Column(name="TSH")
	private String TSH;
	
	public String getTSH() {
		return TSH;
	}

	public void setTSH(String tSH) {
		TSH = tSH;
	}

	@Column(name="SuspicionPatterns")
	private String SuspicionPatterns;
	
	public String getSuspicionPatterns() {
		return SuspicionPatterns;
	}

	public void setSuspicionPatterns(String suspicionPatterns) {
		SuspicionPatterns = suspicionPatterns;
	}

	@Column(name="Size")
	private float Size;
	
	public float getSize() {
		return Size;
	}

	public void setSize(float size) {
		Size = size;
	}

	@Column(name="MetastasisLevel")
	private String MetastasisLevel;
	
	public String getMetastasisLevel() {
		return MetastasisLevel;
	}

	public void setMetastasisLevel(String metastasisLevel) {
		MetastasisLevel = metastasisLevel;
	}

	@Column(name="DiseaseGrowth")
	private String DiseaseGrowth;
	
	public String getDiseaseGrowth() {
		return DiseaseGrowth;
	}

	public void setDiseaseGrowth(String diseaseGrowth) {
		DiseaseGrowth = diseaseGrowth;
	}

	@Column(name="CLN")
	private String CLN;
	
	public String getCLN() {
		return CLN;
	}

	public void setCLN(String cLN) {
		CLN = cLN;
	}

	@Column(name="GestationPeriod")
	private String GestationPeriod;
	
	public String getGestationPeriod() {
		return GestationPeriod;
	}

	public void setGestationPeriod(String gestationPeriod) {
		GestationPeriod = gestationPeriod;
	}

	@Column(name="PP")
	private String PP;
	
	public String getPP() {
		return PP;
	}

	public void setPP(String pP) {
		PP = pP;
	}

	@Column(name="CS")
	private String CS;
	
	public String getCS() {
		return CS;
	}

	public void setCS(String cS) {
		CS = cS;
	}

	@Column(name="VC")
	private String VC;
	
	public String getVC() {
		return VC;
	}

	public void setVC(String vC) {
		VC = vC;
	}

	@Column(name="BCC")
	private String BCC;
	
	public String getBCC() {
		return BCC;
	}

	public void setBCC(String bCC) {
		BCC = bCC;
	}

	@Column(name="CurrentCNBResult")
	private String CurrentCNBResult;
	
	public String getCurrentCNBResult() {
		return CurrentCNBResult;
	}

	public void setCurrentCNBResult(String currentCNBResult) {
		CurrentCNBResult = currentCNBResult;
	}

	@Column(name="ImmediatePriorCNB")
	private String ImmediatePriorCNB;
	
	public String getImmediatePriorCNB() {
		return ImmediatePriorCNB;
	}

	public void setImmediatePriorCNB(String immediatePriorCNB) {
		ImmediatePriorCNB = immediatePriorCNB;
	}

	@Column(name="AUSFLUSPriorCNB")
	private String AUSFLUSPriorCNB;
	
	public String getAUSFLUSPriorCNB() {
		return AUSFLUSPriorCNB;
	}

	public void setAUSFLUSPriorCNB(String aUSFLUSPriorCNB) {
		AUSFLUSPriorCNB = aUSFLUSPriorCNB;
	}

	@Column(name="TreatmentRecommendation")
	private String TreatmentRecommendation;

	public String getTreatmentRecommendation() {
		return TreatmentRecommendation;
	}
	
	@Column(name="resultDate")
	private Date resultDate;
		
	
	public Date getResultDate() {
		return resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}

	public void setTreatmentRecommendation(String treatmentRecommendation) {
		TreatmentRecommendation = treatmentRecommendation;
	}
	
	@Column(name="ApprovedTreatmentRecommendation")
	private String ApprovedTreatmentRecommendation;

	public String getApprovedTreatmentRecommendation() {
		return ApprovedTreatmentRecommendation;
	}

	public void setApprovedTreatmentRecommendation(String approvedTreatmentRecommendation) {
		ApprovedTreatmentRecommendation = approvedTreatmentRecommendation;
	}

	@Column(name="TreatmentRecChangeRemarks")
	private String TreatmentRecChangeRemarks;
	
	public String getTreatmentRecChangeRemarks() {
		return TreatmentRecChangeRemarks;
	}

	public void setTreatmentRecChangeRemarks(String treatmentRecChangeRemarks) {
		TreatmentRecChangeRemarks = treatmentRecChangeRemarks;
	}
	
	@Column(name="FollowupRecommendation")
	private String FollowupRecommendation;
	
	public String getFollowupRecommendation() {
		return FollowupRecommendation;
	}

	public void setFollowupRecommendation(String followupRecommendation) {
		FollowupRecommendation = followupRecommendation;
	}
	
	@Column(name="ApprovedFollowupRecommendation")
	private String ApprovedFollowupRecommendation;

	public String getApprovedFollowupRecommendation() {
		return ApprovedFollowupRecommendation;
	}

	public void setApprovedFollowupRecommendation(String approvedFollowupRecommendation) {
		ApprovedFollowupRecommendation = approvedFollowupRecommendation;
	}
	
	@Column(name="FollowupRecChangeRemarks")
	private String FollowupRecChangeRemarks;


	public String getFollowupRecChangeRemarks() {
		return FollowupRecChangeRemarks;
	}

	public void setFollowupRecChangeRemarks(String followupRecChangeRemarks) {
		FollowupRecChangeRemarks = followupRecChangeRemarks;
	}
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="PatientID")
	@JsonManagedReference
	private List<PatientCNBResult> patientCNBResultList = new ArrayList<PatientCNBResult>();

	public List<PatientCNBResult> getPatientCNBResultList() {
		return patientCNBResultList;
	}

	public void setPatientCNBResultList(List<PatientCNBResult> patientCNBResultList) {
		this.patientCNBResultList = patientCNBResultList;
	}
	
	@Column(name="ContralateralNodule")
	private String ContralateralNodule;

	public String getContralateralNodule() {
		return ContralateralNodule;
	}

	public void setContralateralNodule(String contralateralNodule) {
		ContralateralNodule = contralateralNodule;
	}
	
	@Column(name="ContralateralSiteFNACNB")
	private String ContralateralSiteFNACNB;

	public String getContralateralSiteFNACNB() {
		return ContralateralSiteFNACNB;
	}

	public void setContralateralSiteFNACNB(String contralateralSiteFNACNB) {
		ContralateralSiteFNACNB = contralateralSiteFNACNB;
	}
	
	@Column(name="Goiter")
	private String Goiter;

	public String getGoiter() {
		return Goiter;
	}

	public void setGoiter(String goiter) {
		Goiter = goiter;
	}

}
