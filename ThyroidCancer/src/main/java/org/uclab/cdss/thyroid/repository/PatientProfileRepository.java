package org.uclab.cdss.thyroid.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uclab.cdss.thyroid.datamodel.PatientProfile;

public interface PatientProfileRepository extends JpaRepository<PatientProfile, Integer> {

	@Query("Select pp from PatientProfile pp where pp.PatientMRNNo like CONCAT('%',:patientMRNNo,'%')")
    List<PatientProfile> findByPatientMRNNoContaining(@Param("patientMRNNo") String patientMRNNo);
		
}
