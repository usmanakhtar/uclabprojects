package org.uclab.cdss.thyroid.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.JoinColumn;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Taqdir Ali
 *
 */

@Entity
@Table(name="tblPatientCNBResult")
public class PatientCNBResult {

	@Id
    @Column(name="PatientCNBResultID")
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int PatientCNBResultID;
	
	public int getPatientCNBResultID() {
		return PatientCNBResultID;
	}

	public void setPatientCNBResultID(int patientCNBResultID) {
		PatientCNBResultID = patientCNBResultID;
	}
	
	@Column(name="CNBResult")
	private String CNBResult;
	
	public String getCNBResult() {
		return CNBResult;
	}

	public void setCNBResult(String cNBResult) {
		CNBResult = cNBResult;
	}
	
	@Column(name="ResultDate")
	private Date ResultDate;
	
	public Date getResultDate() {
		return ResultDate;
	}

	public void setResultDate(Date resultDate) {
		ResultDate = resultDate;
	}
	
	@Column(name="CNBRecommendation")
	private String CNBRecommendation;
	
	public String getCNBRecommendation() {
		return CNBRecommendation;
	}

	public void setCNBRecommendation(String cNBRecommendation) {
		CNBRecommendation = cNBRecommendation;
	}
	
	@Column(name="ApprovedCNBRecommendation")
	private String ApprovedCNBRecommendation;
	
	public String getApprovedCNBRecommendation() {
		return ApprovedCNBRecommendation;
	}

	public void setApprovedCNBRecommendation(String approvedCNBRecommendation) {
		ApprovedCNBRecommendation = approvedCNBRecommendation;
	}
	
	@Column(name="CNBRecChangeRemarks")
	private String CNBRecChangeRemarks;

	public String getCNBRecChangeRemarks() {
		return CNBRecChangeRemarks;
	}

	public void setCNBRecChangeRemarks(String cNBRecChangeRemarks) {
		CNBRecChangeRemarks = cNBRecChangeRemarks;
	}

	@ManyToOne
	@JoinColumn(name="PatientID")
	@JsonBackReference
	private PatientProfile patientProfile;
	
	public PatientProfile getPatientProfile() {
		return patientProfile;
	}

	public void setPatientProfile(PatientProfile patientProfile) {
		this.patientProfile = patientProfile;
	}
	
}
