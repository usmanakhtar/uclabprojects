package org.uclab.cdss.thyroid.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.uclab.cdss.thyroid.datamodel.PatientProfile;
import org.uclab.cdss.thyroid.datamodel.PatientCNBResult;
import org.uclab.cdss.thyroid.service.PatientService;

import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin
@RestController
@EnableAutoConfiguration
@Component
public class PatientProfileController {

	@Autowired
	private PatientService objPatientService;

	
//    public PatientProfileController(final PatientService objPatientService) {
//        this.objPatientService = objPatientService;
//    }

	@CrossOrigin
    @RequestMapping(value = "/AddPatientProfile", method = RequestMethod.POST)
    public PatientProfile createPatientProfile(@RequestBody @Valid final PatientProfile objPatientProfile) {
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			System.out.println(mapper.writeValueAsString(objPatientProfile));
		}
		catch(Exception ex)
		{
			
		}
    	return objPatientService.addPatientProfile(objPatientProfile);
    }
    
	@CrossOrigin
    @RequestMapping(value = "/UpdatePatientProfile", method = RequestMethod.POST)
    public PatientProfile updatePatientProfile(@RequestBody @Valid final PatientProfile objPatientProfile) {
        return objPatientService.updatePatientProfile(objPatientProfile);
    }
    
	@CrossOrigin
    @RequestMapping(value = "/getPatientProfile/{patientID}", method = RequestMethod.GET)
    public @ResponseBody PatientProfile getPatientProfile(@PathVariable("patientID") int patientID) {
    	return objPatientService.getPatientProfileById(patientID);
    }
    
	@CrossOrigin
    @RequestMapping(value = "/getPatientProfileList", method = RequestMethod.GET)
    public @ResponseBody List<PatientProfile> getPatientProfileList() {
    	return objPatientService.listPatientProfile();
    }
    
	@CrossOrigin
    @RequestMapping(value = "/getPatientProfileList/{patientMRNNo}", method = RequestMethod.GET)
    public @ResponseBody List<PatientProfile> getPatientProfileList(@PathVariable("patientMRNNo") String patientMRNNo) {
		System.out.println(patientMRNNo);
    	return objPatientService.listPatientProfile(patientMRNNo);
    }
    
	@CrossOrigin
    @RequestMapping(value = "/deletePatientProfile/{patientID}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String deletePatientProfile(@PathVariable("patientID") int patientID) {
    	return objPatientService.removePatientProfile(patientID);
    }
    
}
