package org.uclab.cdss.thyroid.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.uclab.cdss.thyroid.datamodel.PatientProfile;



/**
 * Patient Profile Service Interface
 * Patient Profile Service
 * @author Taqdir Ali
 *
 */

public interface PatientService {

	public PatientProfile addPatientProfile(PatientProfile objPatientProfile);
    public PatientProfile updatePatientProfile(PatientProfile objPatientProfile);
    public List<PatientProfile> listPatientProfile(String patientMRNo);
    public List<PatientProfile> listPatientProfile();
    public PatientProfile getPatientProfileById(int id);
    public String removePatientProfile(int id);
    
}
