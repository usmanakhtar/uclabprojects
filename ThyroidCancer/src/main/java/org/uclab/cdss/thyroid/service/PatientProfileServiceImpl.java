package org.uclab.cdss.thyroid.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uclab.cdss.thyroid.datamodel.PatientProfile;
import org.uclab.cdss.thyroid.datamodel.PatientCNBResult;
import org.uclab.cdss.thyroid.repository.PatientProfileRepository;

@Service
public class PatientProfileServiceImpl implements PatientService {

	private final PatientProfileRepository repository;
	@Inject
    public PatientProfileServiceImpl(final PatientProfileRepository repository) {
        this.repository = repository;
    }
	
	@Override
    @Transactional
    public PatientProfile addPatientProfile(final PatientProfile objPatientProfile) {
		try
		{
			Calendar calendar = Calendar.getInstance();
			Date todayDate = new Date(calendar.getTime().getTime());
			objPatientProfile.setResultDate(todayDate);
			for(PatientCNBResult objPatientCNBResult : objPatientProfile.getPatientCNBResultList())
			{
				if(objPatientCNBResult.getResultDate() == null)
				{
					objPatientCNBResult.setResultDate(todayDate);
				}
			}
	        return repository.save(objPatientProfile);
		}
		catch(Exception ex)
		{
			String a = ex.getMessage();
			return null;
		}
    }
	
	@Override
	@Transactional
	public PatientProfile updatePatientProfile(final PatientProfile objPatientProfile) {
		try
		{
			Calendar calendar = Calendar.getInstance();
			Date todayDate = new Date(calendar.getTime().getTime());
			if(objPatientProfile.getResultDate() == null)
			{ objPatientProfile.setResultDate(todayDate); }
		
			for(PatientCNBResult objPatientCNBResult : objPatientProfile.getPatientCNBResultList())
			{
				if(objPatientCNBResult.getResultDate() == null)
				{
					objPatientCNBResult.setResultDate(todayDate);
				}
			}
			
			repository.save(objPatientProfile);
			return objPatientProfile;
		}
		catch(Exception ex)
		{
			String a = ex.getMessage();
			return null;
		}
	}

	@Override
	public List<PatientProfile> listPatientProfile() {
		// TODO Auto-generated method stub
		try
		{
			List<PatientProfile> objListPatientProfile = new ArrayList<PatientProfile>();
			objListPatientProfile = repository.findAll(new Sort(Sort.Direction.DESC, "resultDate"));
			return objListPatientProfile;
		}
		catch(Exception ex)
		{
			return null;
		}
	}

	@Override
	public PatientProfile getPatientProfileById(int id) {
		// TODO Auto-generated method stub
		try
		{
			PatientProfile objPatientProfile = new PatientProfile();
			objPatientProfile = repository.findOne(id);
			return objPatientProfile;
		}
		catch(Exception ex)
		{
			return null;
		}
	}

	@Override
	public String removePatientProfile(int id) {
		try
		{
		    repository.delete(id);
		    return "{ \"message\" : \"Patient profile deleted successfuly\" }";
		}
		catch(Exception ex)
		{
			return "{ \"message\" : \"Error occured in Patient profile deleting\" }";
		}
	}

	@Override
	public List<PatientProfile> listPatientProfile(String patientMRNo) {
		try
		{
			List<PatientProfile> objListPatientProfile = new ArrayList<PatientProfile>();
			objListPatientProfile = repository.findByPatientMRNNoContaining(patientMRNo);
			return objListPatientProfile;
		}
		catch(Exception ex)
		{
			return null;
		}
	}

}
